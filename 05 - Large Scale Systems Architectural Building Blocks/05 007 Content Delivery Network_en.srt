1
00:00:00,310 --> 00:00:03,400
Hey, welcome back to another lecture.

2
00:00:03,400 --> 00:00:07,320
In this lecture, we're going to talk about CDNs.

3
00:00:07,320 --> 00:00:09,660
Although CDNs are not so much

4
00:00:09,660 --> 00:00:12,870
an architectural building block but more of a service,

5
00:00:12,870 --> 00:00:15,760
it is one of the most important technologies

6
00:00:15,760 --> 00:00:18,840
that power the internet as we know today.

7
00:00:18,840 --> 00:00:22,113
So what problem are we solving using a CDN?

8
00:00:23,230 --> 00:00:25,430
Even with distributed web hosting

9
00:00:25,430 --> 00:00:27,510
across multiple data centers,

10
00:00:27,510 --> 00:00:29,490
which are powered by technologies

11
00:00:29,490 --> 00:00:31,980
like Global Server Load Balancing,

12
00:00:31,980 --> 00:00:34,440
there is still a significant latency

13
00:00:34,440 --> 00:00:37,680
caused by the physical distance between an end user

14
00:00:37,680 --> 00:00:40,223
and the location of the hosting servers.

15
00:00:41,120 --> 00:00:44,520
Additionally, each request between an end user

16
00:00:44,520 --> 00:00:46,270
and the destination server

17
00:00:46,270 --> 00:00:48,400
has to go through multiple hops

18
00:00:48,400 --> 00:00:50,670
between different network routers.

19
00:00:50,670 --> 00:00:53,983
This adds even more to the overall latency.

20
00:00:55,270 --> 00:00:58,890
To get a better idea of where a CDN comes in handy,

21
00:00:58,890 --> 00:01:02,410
let's take an example of a user located in Brazil

22
00:01:02,410 --> 00:01:05,810
who wants to load the homepage of our online store

23
00:01:05,810 --> 00:01:07,610
hosted in a data center

24
00:01:07,610 --> 00:01:10,573
somewhere on the east coast of the United States.

25
00:01:11,730 --> 00:01:14,710
Let's assume that this HTML webpage

26
00:01:14,710 --> 00:01:17,510
contains links to 10 different assets,

27
00:01:17,510 --> 00:01:20,883
like Images, JavaScript, and CSS files.

28
00:01:22,060 --> 00:01:25,870
Also, for simplicity, let's assume that the network latency

29
00:01:25,870 --> 00:01:28,270
between the user's computer in Brazil

30
00:01:28,270 --> 00:01:30,700
and the data center in the United States

31
00:01:30,700 --> 00:01:32,893
is about 200 milliseconds.

32
00:01:33,980 --> 00:01:37,880
Now, since HTTP uses TCP under the hood

