# Software Architecture & Design of Modern Large Scale Systems 2022 - Udemy

### 01 - Introduction/:

- 01 001 Introduction.mp4

### 02 - System Requirements & Architectural Drivers/:

- 02 001 Introduction to System Design & Architectural Direct Drivers.mp4
- 02 002 Feature Requirements - Step by Step Process.mp4
- 02 003 System Quality Attributes Requirements.mp4
- 02 004 System Constraints.mp4

### 03 - Most Important Quality Attributes in Large Scale Systems/:

- 03 001 Performance.mp4
- 03 002 Scalability.mp4
- 03 003 Availability - Introduction & Measurement.mp4
- 03 004 Fault Tolerance & High Availability.mp4
- 03 005 SLA, SLO, SLI.mp4
- 03 006 Industry SLA Examples.html

### 04 - API Design/:

- 04 001 Introduction to API Design.mp4
- 04 002 RPC.mp4
- 04 003 Popular RPC Frameworks.html
- 04 004 REST API.mp4
04 external-assets-links.txt

### 05 - Large Scale Systems Architectural Building Blocks/:

- 05 001 DNS, Load Balancing & GSLB.mp4
- 05 002 Load Balancing Solutions.html
- 05 003 Message Brokers.mp4
- 05 004 Message Brokers Solutions.html
- 05 005 API Gateway.mp4
- 05 006 API Gateway Solutions.html
- 05 007 Content Delivery Network_en.vtt
- 05 007 Content Delivery Network.mp4
- 05 008 CDN Solutions.html

### 06 - Data Storage at Global Scale/:

- 06 001 Relational Databases & ACID Transactions.mp4
- 06 002 Non-Relational Databases.mp4
- 06 003 Non-Relational Databases - Solutions.html
- 06 004 Techniques to Improve Performance, Availability & Scalability Of Databases.mp4
- 06 005 Brewer

### 07 - Software Architecture Patterns/:

- 07 001 Introduction to Software Architectural Patterns.mp4
- 07 002 Multi-Tier Architecture.mp4
- 07 003 Microservices Architecture.mp4
- 07 004 Event Driven Architecture.mp4

### 08 - Big Data Architecture Patterns/:

- 08 001 Introduction to Big Data.mp4
- 08 002 Big Data Processing Strategies.mp4
- 08 003 Lambda Architecture.mp4

### 09 - Bonus Section/:

- 09 001 Bonus Lecture - Keep learning.html
