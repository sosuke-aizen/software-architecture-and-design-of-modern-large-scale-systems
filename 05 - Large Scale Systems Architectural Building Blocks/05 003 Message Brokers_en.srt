1
00:00:00,310 --> 00:00:02,600
Hey, welcome back.

2
00:00:02,600 --> 00:00:04,600
Now, it's time for us to learn

3
00:00:04,600 --> 00:00:07,820
about the most fundamental architecture building block

4
00:00:07,820 --> 00:00:11,620
for asynchronous architectures, the message broker.

5
00:00:11,620 --> 00:00:13,560
We will start with getting the motivation

6
00:00:13,560 --> 00:00:16,810
for message brokers by exploring some use cases

7
00:00:16,810 --> 00:00:20,080
where an asynchronous architecture can provide us

8
00:00:20,080 --> 00:00:23,260
with more benefits and better capabilities.

9
00:00:23,260 --> 00:00:25,570
And finally, we'll learn what kind

10
00:00:25,570 --> 00:00:30,290
of quality attributes message brokers can add to our system.

11
00:00:30,290 --> 00:00:33,403
So what is a message broker and why do we need it?

12
00:00:34,470 --> 00:00:37,270
If we recall from the previous lecture where we learned

13
00:00:37,270 --> 00:00:40,180
about load balancers, you probably noticed

14
00:00:40,180 --> 00:00:41,380
that in each case,

15
00:00:41,380 --> 00:00:44,470
when we mentioned two applications talking to each other,

16
00:00:44,470 --> 00:00:47,720
either directly or through a load balancer

17
00:00:47,720 --> 00:00:50,260
there was always an implicit assumption

18
00:00:50,260 --> 00:00:52,080
that both the sender application

19
00:00:52,080 --> 00:00:54,640
and the receiver application maintained

20
00:00:54,640 --> 00:00:56,460
an active connection.

21
00:00:56,460 --> 00:00:58,980
This implies that they were both healthy

22
00:00:58,980 --> 00:01:01,390
and running in the same time.

23
00:01:01,390 --> 00:01:02,970
This type of communication

24
00:01:02,970 --> 00:01:05,163
is called synchronous communication.

25
00:01:06,200 --> 00:01:07,930
While synchronous communication

26
00:01:07,930 --> 00:01:09,790
is the most straightforward type

27
00:01:09,790 --> 00:01:14,130
of communication between services, it has a few drawbacks.

28
00:01:14,130 --> 00:01:17,450
The first drawback that we already mentioned is the fact

29
00:01:17,450 --> 00:01:20,370
that both application instances that establish

30
00:01:20,370 --> 00:01:23,600
the communication with each other have to remain healthy

31
00:01:23,600 --> 00:01:26,893
and maintain this connection to complete the transaction.

32
00:01:27,830 --> 00:01:29,570
While it's easy to achieve

33
00:01:29,570 --> 00:01:33,470
when we have two services that exchange very short messages

34
00:01:33,470 --> 00:01:36,870
that take a very short time to process and respond to

35
00:01:36,870 --> 00:01:39,280
things get a lot more complex

36
00:01:39,280 --> 00:01:42,080
when we have a service that takes a long time

37
00:01:42,080 --> 00:01:45,063
to complete its operation and provide a response.

38
00:01:46,010 --> 00:01:49,770
As an example, let's consider a system that sells tickets

39
00:01:49,770 --> 00:01:50,790
to different shows

40
00:01:50,790 --> 00:01:54,710
or performances offered by different theaters.

41
00:01:54,710 --> 00:01:57,350
Let's assume that we have two services.

42
00:01:57,350 --> 00:01:59,970
The first service simply provides the front end

43
00:01:59,970 --> 00:02:03,860
to the user and get requests for purchasing tickets.

44
00:02:03,860 --> 00:02:06,490
The second service fulfills the order

45
00:02:06,490 --> 00:02:09,850
by reserving the ticket through an external API.

46
00:02:09,850 --> 00:02:11,820
Then it builds the user

47
00:02:11,820 --> 00:02:14,470
by communicating with a credit card company.

48
00:02:14,470 --> 00:02:17,720
And in the end, it may send a confirmation email

49
00:02:17,720 --> 00:02:19,920
or talk to an external service

50
00:02:19,920 --> 00:02:22,653
that would send a physical ticket to the user.

51
00:02:23,630 --> 00:02:26,490
Now, while the ticket's reservation service

52
00:02:26,490 --> 00:02:29,860
is doing its job, the front end service will have

53
00:02:29,860 --> 00:02:33,880
to maintain an open connection and wait for our response.

54
00:02:33,880 --> 00:02:34,990
But while it's waiting

55
00:02:34,990 --> 00:02:38,100
for the response from the tickets reservation service

56
00:02:38,100 --> 00:02:40,780
it's also holding the user in suspense

57
00:02:40,780 --> 00:02:43,130
because until the ticket is reserved

58
00:02:43,130 --> 00:02:45,130
and the user is successfully built

59
00:02:45,130 --> 00:02:48,370
we won't know if the operation is successful.

60
00:02:48,370 --> 00:02:51,170
But besides the fact that the operation itself

61
00:02:51,170 --> 00:02:54,070
even when successful may take a long time

62
00:02:54,070 --> 00:02:56,020
things can get a lot worse

63
00:02:56,020 --> 00:02:58,550
if the application server that performs all

64
00:02:58,550 --> 00:03:01,010
those operations suddenly crashes

65
00:03:01,010 --> 00:03:02,663
and we need to start over.

66
00:03:04,100 --> 00:03:06,950
Another drawback of synchronous communication is

67
00:03:06,950 --> 00:03:08,130
that there is no padding

68
00:03:08,130 --> 00:03:10,900
in the system to absorb a sudden increase

69
00:03:10,900 --> 00:03:12,563
in traffic or load.

70
00:03:13,530 --> 00:03:16,640
For example, let's say we have an online store

71
00:03:16,640 --> 00:03:19,420
that has two services just like before.

72
00:03:19,420 --> 00:03:22,290
One for receiving direct traffic from users

73
00:03:22,290 --> 00:03:25,650
and another to fulfill the actual purchase.

74
00:03:25,650 --> 00:03:27,090
And let's also assume

75
00:03:27,090 --> 00:03:29,510
that we're running a limited time promotion

76
00:03:29,510 --> 00:03:31,780
for one of our products.

77
00:03:31,780 --> 00:03:35,050
That promotion results in manual requests from users

78
00:03:35,050 --> 00:03:38,360
to our front end service, which we can easily handle.

79
00:03:38,360 --> 00:03:40,030
But it also results

80
00:03:40,030 --> 00:03:43,270
in a very large number of purchase fulfillment requests

81
00:03:43,270 --> 00:03:45,380
to our order fulfillment service

82
00:03:45,380 --> 00:03:49,550
which we cannot easily handle even if we scale that service

83
00:03:49,550 --> 00:03:51,860
to many server instances.

84
00:03:51,860 --> 00:03:55,560
The reason for that is simply because fulfilling each order

85
00:03:55,560 --> 00:03:58,993
involves many operations that take a very long time.

86
00:04:00,300 --> 00:04:03,090
All those scenarios can be easily solved

87
00:04:03,090 --> 00:04:05,110
with an architectural building block

88
00:04:05,110 --> 00:04:06,563
called message broker.

89
00:04:07,680 --> 00:04:11,200
A message broker is a software architectural building block

90
00:04:11,200 --> 00:04:14,640
that uses the queue data structure to store messages

91
00:04:14,640 --> 00:04:17,329
between senders and receivers.

92
00:04:17,329 --> 00:04:21,640
To be clear unlike a load balancer that can be easily used

93
00:04:21,640 --> 00:04:24,933
to take external traffic from a client application

94
00:04:24,933 --> 00:04:28,070
a message broker is an architectural building block

95
00:04:28,070 --> 00:04:30,100
that is used inside our system

96
00:04:30,100 --> 00:04:33,203
and is generally not exposed externally.

97
00:04:34,300 --> 00:04:36,500
In addition to simply storing

98
00:04:36,500 --> 00:04:39,110
or temporarily buffering the messages,

99
00:04:39,110 --> 00:04:42,580
message brokers can provide additional functionality

100
00:04:42,580 --> 00:04:46,170
such as message routing, transformation, validation

101
00:04:46,170 --> 00:04:47,973
and even load balancing.

102
00:04:48,890 --> 00:04:52,502
However, unlike load balancers message brokers

103
00:04:52,502 --> 00:04:55,940
entirely decouple senders from the receivers

104
00:04:55,940 --> 00:05:00,490
by providing their own communication protocols and APIs.

105
00:05:00,490 --> 00:05:03,710
Message brokers are the fundamental building block

106
00:05:03,710 --> 00:05:07,273
for any type of asynchronous software architecture.

107
00:05:08,250 --> 00:05:11,280
When we have two services communicating with each other

108
00:05:11,280 --> 00:05:14,900
through a message broker, the sender doesn't have to wait

109
00:05:14,900 --> 00:05:17,350
for any confirmation from the receiver

110
00:05:17,350 --> 00:05:19,973
after it sends the message to the message broker.

111
00:05:20,920 --> 00:05:24,460
In fact, the receiver may not even be available

112
00:05:24,460 --> 00:05:28,153
to take any messages while sender, send that message.

113
00:05:29,160 --> 00:05:32,300
So in the case of the ticket reservation system

114
00:05:32,300 --> 00:05:35,340
the end user may get an acknowledgement immediately

115
00:05:35,340 --> 00:05:37,520
after placing the order.

116
00:05:37,520 --> 00:05:40,830
And later it will get an email asynchronously

117
00:05:40,830 --> 00:05:43,810
with an official confirmation for purchasing the ticket

118
00:05:43,810 --> 00:05:46,130
after the ticket reservation service

119
00:05:46,130 --> 00:05:48,193
actually completes the transaction.

120
00:05:49,150 --> 00:05:50,980
By using the message broker

121
00:05:50,980 --> 00:05:53,570
we can break the ticket reservation service

122
00:05:53,570 --> 00:05:55,160
into multiple services.

123
00:05:55,160 --> 00:05:57,730
Each for one stage in the transaction

124
00:05:57,730 --> 00:06:00,690
and each pair of services is also decoupled

125
00:06:00,690 --> 00:06:02,773
from each other by a message broker.

126
00:06:04,010 --> 00:06:07,510
Another important benefit that a message broker provides us

127
00:06:07,510 --> 00:06:11,273
with is buffering of messages to absorb traffic spikes.

128
00:06:12,330 --> 00:06:14,660
In our online store scenario,

129
00:06:14,660 --> 00:06:17,710
when we have a lot of orders in a short period of time

130
00:06:17,710 --> 00:06:21,120
the front end service may simply decrement a counter

131
00:06:21,120 --> 00:06:24,490
in a database for the number of items left in stock

132
00:06:24,490 --> 00:06:26,720
while the actual orders are stored

133
00:06:26,720 --> 00:06:28,683
inside the message brokers queue.

134
00:06:29,726 --> 00:06:31,950
And those orders can be fulfilled one by one

135
00:06:31,950 --> 00:06:33,840
after the sale is already over

136
00:06:33,840 --> 00:06:36,253
and the traffic to our store goes down.

137
00:06:37,230 --> 00:06:39,560
Most message broker implementations

138
00:06:39,560 --> 00:06:42,220
also offer the published subscribe pattern

139
00:06:42,220 --> 00:06:44,960
where multiple services can publish messages

140
00:06:44,960 --> 00:06:47,720
to a particular channel and multiple services

141
00:06:47,720 --> 00:06:50,790
can subscribe to that channel and get notified

142
00:06:50,790 --> 00:06:52,833
when a new event is published.

143
00:06:53,760 --> 00:06:56,700
With this pattern we can take the same online store,

144
00:06:56,700 --> 00:07:00,280
for example and without any modification into the system

145
00:07:00,280 --> 00:07:03,660
we can easily add another service that would subscribe

146
00:07:03,660 --> 00:07:05,040
to the orders channel

147
00:07:05,040 --> 00:07:08,170
and feed that data to the analytic service.

148
00:07:08,170 --> 00:07:11,490
This service would analyze the purchasing pattern

149
00:07:11,490 --> 00:07:14,360
from different users and suggest certain products

150
00:07:14,360 --> 00:07:16,570
to users in the future.

151
00:07:16,570 --> 00:07:20,720
Similarly, we can add another service that for every order

152
00:07:20,720 --> 00:07:23,760
from the user, it would send a push notification

153
00:07:23,760 --> 00:07:25,700
to the user's mobile phone.

154
00:07:25,700 --> 00:07:28,350
This way a user can be alerted

155
00:07:28,350 --> 00:07:31,393
if a purchase order was placed from their account.

156
00:07:32,420 --> 00:07:36,480
And later, just as easily we can add another service

157
00:07:36,480 --> 00:07:40,170
that for every purchase from a user would schedule a survey

158
00:07:40,170 --> 00:07:42,730
or request a review a certain time

159
00:07:42,730 --> 00:07:45,230
after the purchase has been made.

160
00:07:45,230 --> 00:07:47,500
And as you can see with this pattern

161
00:07:47,500 --> 00:07:49,460
all those services were added

162
00:07:49,460 --> 00:07:52,053
without any modifications to the system.

163
00:07:52,990 --> 00:07:56,630
So now that we understand all the benefits and capabilities

164
00:07:56,630 --> 00:07:59,620
of a message broker, let's speak more concretely

165
00:07:59,620 --> 00:08:02,110
about what quality attributes we get

166
00:08:02,110 --> 00:08:04,893
from using a message broker in our system.

167
00:08:05,950 --> 00:08:09,840
A message broker adds a lot of full tolerance to our system

168
00:08:09,840 --> 00:08:12,330
since it allows different services to communicate

169
00:08:12,330 --> 00:08:13,790
with each other while some

170
00:08:13,790 --> 00:08:17,330
of them may be unavailable temporarily.

171
00:08:17,330 --> 00:08:21,490
Also, message brokers, prevent messages from being lost

172
00:08:21,490 --> 00:08:25,163
which is another characteristic of a full tolerance system.

173
00:08:26,110 --> 00:08:29,430
All this additional full tolerance helps us provide

174
00:08:29,430 --> 00:08:32,640
with higher availability to our users.

175
00:08:32,640 --> 00:08:35,549
Additionally, since a message broker can queue

176
00:08:35,549 --> 00:08:38,409
up messages when there is a traffic spike

177
00:08:38,409 --> 00:08:41,789
it allows our system to scale to high traffic

178
00:08:41,789 --> 00:08:45,343
without needing to make any modifications to the system.

179
00:08:46,260 --> 00:08:48,000
Now it's worth noting that

180
00:08:48,000 --> 00:08:50,000
while a message broker provides us

181
00:08:50,000 --> 00:08:52,950
with superior availability and scalability

182
00:08:52,950 --> 00:08:55,690
we do pay a little bit in performance

183
00:08:55,690 --> 00:08:57,580
when it comes to latency.

184
00:08:57,580 --> 00:09:01,520
The reason for that additional latency is a message broker

185
00:09:01,520 --> 00:09:03,710
at a significant indirection

186
00:09:03,710 --> 00:09:06,120
between two services, in comparison

187
00:09:06,120 --> 00:09:09,050
to the straightforward synchronous communication

188
00:09:09,050 --> 00:09:11,800
even through a load balancer.

189
00:09:11,800 --> 00:09:14,480
But generally, this performance penalty

190
00:09:14,480 --> 00:09:17,623
is not too significant for most systems.

191
00:09:18,800 --> 00:09:20,360
In this lecture, we've learned learn

192
00:09:20,360 --> 00:09:23,600
about a very important architectural building block

193
00:09:23,600 --> 00:09:24,860
which is fundamental

194
00:09:24,860 --> 00:09:27,590
to any asynchronous software architecture,

195
00:09:27,590 --> 00:09:28,723
the message broker.

196
00:09:29,770 --> 00:09:32,680
We got the motivation for using a message broker

197
00:09:32,680 --> 00:09:35,230
by comparing it to the synchronous communication

198
00:09:35,230 --> 00:09:38,300
we typically use between services directly

199
00:09:38,300 --> 00:09:40,870
or through a load balancer.

200
00:09:40,870 --> 00:09:43,460
Later, we talked about the different benefits

201
00:09:43,460 --> 00:09:46,640
and capabilities that a message broker provides us with

202
00:09:46,640 --> 00:09:51,110
such as asynchronous architecture, buffering, and others.

203
00:09:51,110 --> 00:09:52,600
And finally, we talked

204
00:09:52,600 --> 00:09:55,610
about the two main quality attributes that we get

205
00:09:55,610 --> 00:09:57,750
from using a message broker.

206
00:09:57,750 --> 00:10:00,770
Those quality attributes are high availability

207
00:10:00,770 --> 00:10:03,920
that comes thanks to the superior full tolerance

208
00:10:03,920 --> 00:10:08,020
to service outages and message loss and high scalability

209
00:10:08,020 --> 00:10:11,840
which comes mainly due to the ability to buffer messages

210
00:10:11,840 --> 00:10:15,163
when there are sudden spikes in the load on our system.

211
00:10:16,060 --> 00:10:17,870
I will see you in the next lecture

212
00:10:17,870 --> 00:10:20,823
with another exciting architectural building block.

