1
00:00:00,000 --> 00:00:01,010
In this lecture,

2
00:00:01,010 --> 00:00:02,820
we're going to learn about the first

3
00:00:02,820 --> 00:00:04,590
and one of the most important

4
00:00:04,590 --> 00:00:06,670
software architecture building blocks,

5
00:00:06,670 --> 00:00:09,190
which is used in pretty much 100%

6
00:00:09,190 --> 00:00:12,400
of all real-life, large-scale systems.

7
00:00:12,400 --> 00:00:15,153
This building block is called a load balancer.

8
00:00:16,710 --> 00:00:19,860
After getting some motivation for using a load balancer,

9
00:00:19,860 --> 00:00:22,090
we will learn what quality attributes

10
00:00:22,090 --> 00:00:25,020
this building block can provide to our system.

11
00:00:25,020 --> 00:00:26,760
And finally, we will learn about

12
00:00:26,760 --> 00:00:29,340
the different types of load balancing solutions

13
00:00:29,340 --> 00:00:31,220
and how to use those solutions

14
00:00:31,220 --> 00:00:34,493
in architecting a real-life, large-scale system.

15
00:00:35,720 --> 00:00:38,903
So let's start with an introduction to load balancers.

16
00:00:40,609 --> 00:00:43,940
As the name suggests, the basic role of a load balancer

17
00:00:43,940 --> 00:00:45,760
is to balance the traffic load

18
00:00:45,760 --> 00:00:48,293
among a group of servers in our system.

19
00:00:49,390 --> 00:00:51,790
If we remember from the previous lectures,

20
00:00:51,790 --> 00:00:54,380
the best way to achieve high availability

21
00:00:54,380 --> 00:00:56,130
and horizontal scalability

22
00:00:56,130 --> 00:00:59,750
is running multiple identical instances of our application

23
00:00:59,750 --> 00:01:01,820
on multiple computers.

24
00:01:01,820 --> 00:01:04,030
However, without a load balancer,

25
00:01:04,030 --> 00:01:05,390
the client application

26
00:01:05,390 --> 00:01:08,110
that may run on our customer's computers

27
00:01:08,110 --> 00:01:11,040
will have to know the addresses of those computers,

28
00:01:11,040 --> 00:01:14,320
as well as the number of the application instances.

29
00:01:14,320 --> 00:01:17,410
This tightly couples the client application

30
00:01:17,410 --> 00:01:19,930
to our system's internal implementation

31
00:01:19,930 --> 00:01:23,083
and makes it very hard for us to make any changes.

32
00:01:24,170 --> 00:01:27,330
So while the main purpose of a load balancer

33
00:01:27,330 --> 00:01:30,300
is to balance the load among a group of servers

34
00:01:30,300 --> 00:01:32,640
to make sure that no individual server

35
00:01:32,640 --> 00:01:35,270
is overloaded as an added feature,

36
00:01:35,270 --> 00:01:38,990
most load balancing solutions also provide an abstraction

37
00:01:38,990 --> 00:01:42,520
between the client application and our group of servers.

38
00:01:42,520 --> 00:01:45,440
This abstraction makes our entire system

39
00:01:45,440 --> 00:01:47,180
look like a single server,

40
00:01:47,180 --> 00:01:50,960
capable of immense computing power and a lot of memory.

41
00:01:50,960 --> 00:01:53,420
Now, different load balancing solutions

42
00:01:53,420 --> 00:01:55,720
offer different levels of abstraction,

43
00:01:55,720 --> 00:01:57,210
which we will see very soon

44
00:01:57,210 --> 00:01:59,963
when we talk about different types of load balancers.

45
00:02:01,030 --> 00:02:04,570
Now, let's talk about what specific quality attributes

46
00:02:04,570 --> 00:02:07,263
a load balancer can provide to our system.

47
00:02:08,570 --> 00:02:11,710
The first quality attribute we get from a load balancer

48
00:02:11,710 --> 00:02:13,053
is high scalability.

49
00:02:14,120 --> 00:02:17,580
As we already mentioned, by hiding a group of servers

50
00:02:17,580 --> 00:02:21,240
behind a load balancer, we can scale our system horizontally

51
00:02:21,240 --> 00:02:24,860
both up and down by adding additional servers

52
00:02:24,860 --> 00:02:27,230
when the load on our system increases

53
00:02:27,230 --> 00:02:29,300
and remove unnecessary servers

54
00:02:29,300 --> 00:02:32,563
when the load on our system decreases to save money.

55
00:02:33,790 --> 00:02:35,240
In a cloud environment

56
00:02:35,240 --> 00:02:38,370
where we can easily rent more hardware on demand,

57
00:02:38,370 --> 00:02:40,520
we can use auto-scaling policies

58
00:02:40,520 --> 00:02:43,460
to intelligently add or remove servers

59
00:02:43,460 --> 00:02:45,430
based on different criteria,

60
00:02:45,430 --> 00:02:47,570
like the number of requests per second,

61
00:02:47,570 --> 00:02:49,533
network bandwidth, and so on.

62
00:02:50,640 --> 00:02:52,170
The next quality attribute

63
00:02:52,170 --> 00:02:55,663
the load balancer provides us with is high availability.

64
00:02:56,660 --> 00:02:59,980
Most load balancers can easily be configured

65
00:02:59,980 --> 00:03:04,020
to stop sending traffic to servers that cannot be reached.

66
00:03:04,020 --> 00:03:06,070
By having this monitoring feature,

67
00:03:06,070 --> 00:03:09,320
load balancers can intelligently balance the load

68
00:03:09,320 --> 00:03:11,340
only among healthy servers

69
00:03:11,340 --> 00:03:14,100
while ignoring the ones that are considered to be dead

70
00:03:14,100 --> 00:03:15,453
or excessively slow.

71
00:03:16,340 --> 00:03:18,830
Now, let's see how load balancers

72
00:03:18,830 --> 00:03:20,713
affect the system's performance.

73
00:03:21,770 --> 00:03:23,260
When it comes to performance,

74
00:03:23,260 --> 00:03:26,620
load balancers may add a little bit of latency

75
00:03:26,620 --> 00:03:29,150
and increase their response time to the user,

76
00:03:29,150 --> 00:03:32,680
but it's generally an acceptable price to pay

77
00:03:32,680 --> 00:03:35,563
for an increased performance in terms of throughput.

78
00:03:36,690 --> 00:03:38,720
Since the load balancer allows us

79
00:03:38,720 --> 00:03:42,640
to theoretically have as many backend servers as we like,

80
00:03:42,640 --> 00:03:45,010
of course, with some reasonable limitations,

81
00:03:45,010 --> 00:03:47,280
the number of requests or tasks

82
00:03:47,280 --> 00:03:49,750
that we can perform per unit of time

83
00:03:49,750 --> 00:03:52,850
is much larger than the throughput we could get

84
00:03:52,850 --> 00:03:54,263
from a single server.

85
00:03:55,630 --> 00:03:57,930
Another important quality attribute

86
00:03:57,930 --> 00:04:01,483
that the load balancer helps us achieve is maintainability.

87
00:04:02,480 --> 00:04:06,360
Since we can easily add or remove servers to the rotation,

88
00:04:06,360 --> 00:04:09,770
we can take down individual servers one-by-one

89
00:04:09,770 --> 00:04:11,020
to prefer maintenance

90
00:04:11,020 --> 00:04:13,470
or upgrade the application version

91
00:04:13,470 --> 00:04:16,550
without any disruption to the client.

92
00:04:16,550 --> 00:04:19,760
And when the maintenance on that server is complete,

93
00:04:19,760 --> 00:04:21,970
we can add it back to the load balancer

94
00:04:21,970 --> 00:04:24,490
and take down the next server.

95
00:04:24,490 --> 00:04:26,970
This way we can have a rolling release,

96
00:04:26,970 --> 00:04:30,603
while still keeping our SLA in terms of availability.

97
00:04:31,680 --> 00:04:33,540
Now, finally, let's talk about

98
00:04:33,540 --> 00:04:35,730
a few different types of load balancers

99
00:04:35,730 --> 00:04:36,930
that we can choose from.

100
00:04:38,110 --> 00:04:40,740
One of the most basic load balancers

101
00:04:40,740 --> 00:04:42,913
can be achieved through DNS.

102
00:04:44,190 --> 00:04:45,970
The Domain Name System

103
00:04:45,970 --> 00:04:48,380
is part of the internet infrastructure

104
00:04:48,380 --> 00:04:52,450
that maps human-friendly URLs, like amazon.com,

105
00:04:52,450 --> 00:04:55,210
netflix.com, or apple.com

106
00:04:55,210 --> 00:04:58,970
to IP addresses that can be used by network routers

107
00:04:58,970 --> 00:05:03,000
to route requests to individual computers on the web.

108
00:05:03,000 --> 00:05:06,003
It's essentially the phone book of the internet.

109
00:05:07,020 --> 00:05:09,670
So when a user or a client application

110
00:05:09,670 --> 00:05:11,830
wants to communicate with our system,

111
00:05:11,830 --> 00:05:15,420
the user sends a DNS query to the DNS server

112
00:05:15,420 --> 00:05:18,280
and the DNS responds with an IP address

113
00:05:18,280 --> 00:05:21,140
that corresponds to our domain name.

114
00:05:21,140 --> 00:05:24,610
Then, the client application can use that IP address

115
00:05:24,610 --> 00:05:27,123
to send a request directly to the server.

116
00:05:28,330 --> 00:05:32,780
However, a single DNS record doesn't have to be mapped

117
00:05:32,780 --> 00:05:34,460
to a single IP address

118
00:05:34,460 --> 00:05:36,080
and can be easily configured

119
00:05:36,080 --> 00:05:38,340
to return a list of IP addresses

120
00:05:38,340 --> 00:05:40,930
corresponding to different servers.

121
00:05:40,930 --> 00:05:44,470
Most DNS servers are implemented in such a way

122
00:05:44,470 --> 00:05:47,550
that they return the list of addresses for each domain

123
00:05:47,550 --> 00:05:50,540
in a different order on each client request

124
00:05:51,430 --> 00:05:54,640
and by convention, most client applications

125
00:05:54,640 --> 00:05:57,110
simply pick the first address in the list

126
00:05:57,110 --> 00:06:01,020
that uses the resolved IP address for a particular domain.

127
00:06:01,020 --> 00:06:03,510
This way, the domain naming system

128
00:06:03,510 --> 00:06:06,510
essentially balances the load on our servers

129
00:06:06,510 --> 00:06:10,083
by simply rotating this list in a round-robin fashion.

130
00:06:11,010 --> 00:06:13,320
Now, although this way of providing

131
00:06:13,320 --> 00:06:17,240
load balancing capability is super simple and cheap,

132
00:06:17,240 --> 00:06:19,010
as it essentially comes for free

133
00:06:19,010 --> 00:06:20,940
by purchasing a domain name,

134
00:06:20,940 --> 00:06:22,473
it has a few drawbacks.

135
00:06:23,530 --> 00:06:26,030
The main drawback is that DNS

136
00:06:26,030 --> 00:06:28,973
doesn't monitor the health of our servers.

137
00:06:30,240 --> 00:06:34,060
In other words, if one of our servers stops responding,

138
00:06:34,060 --> 00:06:37,000
the Domain Name System will not know about it

139
00:06:37,000 --> 00:06:38,930
and will continue referring clients

140
00:06:38,930 --> 00:06:40,523
to that particular server.

141
00:06:41,690 --> 00:06:45,330
This list of IP addresses changes only so often

142
00:06:45,330 --> 00:06:47,410
and is based on the time to live

143
00:06:47,410 --> 00:06:51,230
that was configured for that particular DNS record.

144
00:06:51,230 --> 00:06:53,940
Additionally, this list of addresses

145
00:06:53,940 --> 00:06:56,650
that a particular domain name is mapped to

146
00:06:56,650 --> 00:06:59,070
can be cached in different locations,

147
00:06:59,070 --> 00:07:01,270
such as the client's computer.

148
00:07:01,270 --> 00:07:02,850
That makes the time

149
00:07:02,850 --> 00:07:05,230
between a particular server going down

150
00:07:05,230 --> 00:07:06,790
and the point that the requests

151
00:07:07,815 --> 00:07:10,115
are no longer sent to that server even longer.

152
00:07:11,050 --> 00:07:14,240
Another drawback of DNS-based load balancing

153
00:07:14,240 --> 00:07:16,460
is that the load balancing strategy

154
00:07:16,460 --> 00:07:19,880
is always just as simple as round-robin,

155
00:07:19,880 --> 00:07:21,810
which doesn't take into account

156
00:07:21,810 --> 00:07:24,360
the fact that some of our application instances

157
00:07:24,360 --> 00:07:28,320
may be running on more powerful servers than others,

158
00:07:28,320 --> 00:07:30,900
nor can it detect that one of our servers

159
00:07:30,900 --> 00:07:33,483
may be more overloaded than the others.

160
00:07:34,640 --> 00:07:38,090
The third drawback of the DNS-based load balancing

161
00:07:38,090 --> 00:07:39,800
is the declined application

162
00:07:39,800 --> 00:07:43,423
gets the direct IP addresses of all our servers.

163
00:07:44,410 --> 00:07:48,340
This exposes some implementation details of our system

164
00:07:48,340 --> 00:07:52,200
and more importantly, makes our system less secure.

165
00:07:52,200 --> 00:07:55,448
The reason for that is that there is nothing

166
00:07:55,448 --> 00:07:57,210
that prevents a malicious client application

167
00:07:57,210 --> 00:07:59,420
from just picking one IP address

168
00:07:59,420 --> 00:08:02,700
and send requests only to that particular server

169
00:08:02,700 --> 00:08:05,823
which, of course, would overload it more than others.

170
00:08:06,930 --> 00:08:08,950
To address all those drawbacks,

171
00:08:08,950 --> 00:08:11,050
there are two load balancing solutions

172
00:08:11,050 --> 00:08:14,510
that are a lot more powerful and intelligent.

173
00:08:14,510 --> 00:08:18,080
Those two types of solutions are hardware load balancers

174
00:08:18,080 --> 00:08:19,923
and software load balancers.

175
00:08:20,970 --> 00:08:22,260
The only difference

176
00:08:22,260 --> 00:08:24,610
between those two types of load balancers

177
00:08:24,610 --> 00:08:28,570
is that hardware load balancers run on dedicated devices

178
00:08:28,570 --> 00:08:32,690
designed and optimized specifically for load balancing,

179
00:08:32,690 --> 00:08:35,770
while software load balancers are just programs

180
00:08:35,770 --> 00:08:38,610
that can run on any general-purpose computer

181
00:08:38,610 --> 00:08:41,283
and perform the load balancing function.

182
00:08:42,250 --> 00:08:45,620
In the case of software and hardware load balancers,

183
00:08:45,620 --> 00:08:48,140
in contrast to DNS load balancing,

184
00:08:48,140 --> 00:08:50,250
all the communication between the client

185
00:08:50,250 --> 00:08:54,450
and our group of servers is done through the load balancer.

186
00:08:54,450 --> 00:08:57,570
In other words, the individual IP addresses,

187
00:08:57,570 --> 00:08:59,150
as well as the number of servers

188
00:08:59,150 --> 00:09:00,970
we have behind the load balancer

189
00:09:00,970 --> 00:09:03,010
are not exposed to the users,

190
00:09:03,010 --> 00:09:05,623
which makes our system a lot more secure.

191
00:09:06,580 --> 00:09:10,270
Another feature of hardware and software load balancers

192
00:09:10,270 --> 00:09:13,770
is that they can actively monitor the health of our servers

193
00:09:13,770 --> 00:09:15,940
and send them periodic health checks

194
00:09:15,940 --> 00:09:18,450
to actively detect if one of our servers

195
00:09:18,450 --> 00:09:19,823
became unresponsive.

196
00:09:20,960 --> 00:09:24,480
Finally, both hardware and software load balancers

197
00:09:24,480 --> 00:09:26,970
can balance the load among our servers

198
00:09:26,970 --> 00:09:28,740
a lot more intelligently,

199
00:09:28,740 --> 00:09:31,630
taking to account the different types of hardware

200
00:09:31,630 --> 00:09:34,350
our application instances are running on,

201
00:09:34,350 --> 00:09:36,530
the current load on each server,

202
00:09:36,530 --> 00:09:39,023
the number of open connections, and so on.

203
00:09:40,000 --> 00:09:43,790
The nice thing about software and hardware load balancers

204
00:09:43,790 --> 00:09:47,670
is in addition to balancing requests from external users,

205
00:09:47,670 --> 00:09:50,120
they can also be used inside our system

206
00:09:50,120 --> 00:09:53,083
to create an abstraction between different services.

207
00:09:54,130 --> 00:09:57,000
For example, if we have an online store system,

208
00:09:57,000 --> 00:09:58,720
we can separate at the service

209
00:09:58,720 --> 00:10:01,290
that responds directly to client requests

210
00:10:01,290 --> 00:10:03,950
and serves the front end to the client browsers

211
00:10:03,950 --> 00:10:07,640
from the fulfillment service and the billing service.

212
00:10:07,640 --> 00:10:10,890
Each such service can be deployed independently

213
00:10:10,890 --> 00:10:13,040
as multiple application instances

214
00:10:13,040 --> 00:10:15,023
running on a group of servers.

215
00:10:16,030 --> 00:10:18,880
And those services communicate with each other

216
00:10:18,880 --> 00:10:20,720
through a load balancer.

217
00:10:20,720 --> 00:10:23,490
This way, we can scale each such service

218
00:10:23,490 --> 00:10:25,200
completely independently

219
00:10:25,200 --> 00:10:27,523
and transparently to the other service.

220
00:10:28,550 --> 00:10:31,530
While software and hardware load balancers

221
00:10:31,530 --> 00:10:35,930
are superior to DNS in terms of load balancing, monitoring,

222
00:10:35,930 --> 00:10:38,200
failure recovery and security,

223
00:10:38,200 --> 00:10:41,520
they are usually collocated with the group of servers

224
00:10:41,520 --> 00:10:43,530
they balance the load on.

225
00:10:43,530 --> 00:10:46,750
The reason for that is if we put the load balancer

226
00:10:46,750 --> 00:10:49,060
too far from the actual servers,

227
00:10:49,060 --> 00:10:51,810
we're adding a lot of extra latency

228
00:10:51,810 --> 00:10:53,540
since all the communication,

229
00:10:53,540 --> 00:10:56,350
both to the servers and back to the client,

230
00:10:56,350 --> 00:10:58,513
has to go through the load balancer.

231
00:10:59,760 --> 00:11:04,170
So if we run system in multiple geographical locations,

232
00:11:04,170 --> 00:11:06,940
which are commonly referred to as data centers,

233
00:11:06,940 --> 00:11:09,520
then having only one load balancer

234
00:11:09,520 --> 00:11:13,070
for both groups of servers will sacrifice the performance

235
00:11:13,070 --> 00:11:15,663
for at least one of those locations.

236
00:11:16,800 --> 00:11:19,750
Additionally, load balancers, on their own,

237
00:11:19,750 --> 00:11:22,760
do not solve the DNS resolution problem,

238
00:11:22,760 --> 00:11:25,680
so we would still need some kind of DNS solution

239
00:11:25,680 --> 00:11:28,963
to map human readable domain names to an IP address.

240
00:11:29,980 --> 00:11:33,200
For that, there is a fourth load balancing solution,

241
00:11:33,200 --> 00:11:35,900
which is called Global Server Load Balancer,

242
00:11:35,900 --> 00:11:37,543
or GSLB in short.

243
00:11:38,600 --> 00:11:43,070
A GSLB is somewhat of a hybrid between a DNS service

244
00:11:43,070 --> 00:11:46,053
and the hardware or software load balancer.

245
00:11:47,437 --> 00:11:51,170
A GSLB solution typically can provide a DNS service

246
00:11:51,170 --> 00:11:54,930
just like any other DNS server that we're familiar with.

247
00:11:54,930 --> 00:11:56,460
However, in addition,

248
00:11:56,460 --> 00:12:00,400
it also can make more intelligent routing decisions.

249
00:12:00,400 --> 00:12:04,840
On one hand, the GSLB can figure out the user's location

250
00:12:04,840 --> 00:12:08,513
based on the origin IP inside the incoming request.

251
00:12:09,530 --> 00:12:10,700
On the other hand,

252
00:12:10,700 --> 00:12:14,540
a GSLB service has similar monitoring capabilities

253
00:12:14,540 --> 00:12:17,820
to a typical software or hardware load balancer.

254
00:12:17,820 --> 00:12:20,850
So at any given moment, it knows the location

255
00:12:20,850 --> 00:12:25,163
and state of each server that we register with our GSLB.

256
00:12:26,160 --> 00:12:29,090
In a typical large-scale system deployment,

257
00:12:29,090 --> 00:12:31,580
those servers are load balancers

258
00:12:31,580 --> 00:12:34,000
located in different data centers

259
00:12:34,000 --> 00:12:36,113
in different geographical locations.

260
00:12:37,000 --> 00:12:41,080
So when a user sends a DNS query to the GSLB,

261
00:12:41,080 --> 00:12:44,270
the GSLB may return just the address

262
00:12:44,270 --> 00:12:46,683
of the most nearby load balancer.

263
00:12:47,640 --> 00:12:51,650
From that point on, the user will use that IP address

264
00:12:51,650 --> 00:12:53,360
to communicate directly

265
00:12:53,360 --> 00:12:55,670
with our system in that data center

266
00:12:55,670 --> 00:12:59,263
through a collocated software or hardware load balancer.

267
00:13:00,380 --> 00:13:02,520
The cool part about GSLBs

268
00:13:02,520 --> 00:13:06,440
is that most GSLBs can be configured to route traffic

269
00:13:06,440 --> 00:13:08,430
based on a variety of strategies

270
00:13:08,430 --> 00:13:10,533
and not just by physical location.

271
00:13:11,410 --> 00:13:13,640
Since they're in constant communication

272
00:13:13,640 --> 00:13:15,220
with our data centers,

273
00:13:15,220 --> 00:13:17,790
they can be configured to route users

274
00:13:17,790 --> 00:13:19,510
based on their current traffic

275
00:13:19,510 --> 00:13:22,100
or CPU load on each data center

276
00:13:22,100 --> 00:13:25,180
or based on the best estimated response time

277
00:13:25,180 --> 00:13:27,300
or bandwidth between the user

278
00:13:27,300 --> 00:13:29,630
and that particular data center.

279
00:13:29,630 --> 00:13:31,300
Thanks to this great feature,

280
00:13:31,300 --> 00:13:35,270
we can provide the best performance possible for each user,

281
00:13:35,270 --> 00:13:37,853
regardless of their geographical location.

282
00:13:38,810 --> 00:13:42,790
Additionally, GSLBs play a very important role

283
00:13:42,790 --> 00:13:45,450
in disaster recovery situations.

284
00:13:45,450 --> 00:13:47,170
If there's a natural disaster

285
00:13:47,170 --> 00:13:50,350
or a power outage in one of our data centers,

286
00:13:50,350 --> 00:13:54,050
the users can be easily routed to different locations,

287
00:13:54,050 --> 00:13:56,513
which provides us with higher availability.

288
00:13:57,520 --> 00:14:00,010
Finally, to prevent a load balancer

289
00:14:00,010 --> 00:14:03,210
from being a single point of failure in each region,

290
00:14:03,210 --> 00:14:05,590
we can place multiple load balancers

291
00:14:05,590 --> 00:14:07,710
and register all their addresses

292
00:14:07,710 --> 00:14:12,600
with the GSLB's DNS service or any other DNS service.

293
00:14:12,600 --> 00:14:14,200
So the client applications

294
00:14:14,200 --> 00:14:17,230
can get a list of all our load balancers

295
00:14:17,230 --> 00:14:20,240
and either send a request to the first one in the list

296
00:14:20,240 --> 00:14:22,493
or pick one themselves randomly.

297
00:14:23,490 --> 00:14:25,420
We learned a lot in this lecture,

298
00:14:25,420 --> 00:14:27,630
so let's quickly summarize it.

299
00:14:27,630 --> 00:14:28,750
In this lecture,

300
00:14:28,750 --> 00:14:30,650
we learned about a very important

301
00:14:30,650 --> 00:14:32,650
software architecture building block,

302
00:14:32,650 --> 00:14:34,400
the load balancer.

303
00:14:34,400 --> 00:14:37,240
We learned about four load balancing solutions,

304
00:14:37,240 --> 00:14:39,210
which are DNS load balancing,

305
00:14:39,210 --> 00:14:40,850
hardware load balancing,

306
00:14:40,850 --> 00:14:42,450
software load balancing,

307
00:14:42,450 --> 00:14:44,583
and Global Server Load Balancing.

308
00:14:45,580 --> 00:14:48,700
Later, we talked about the different quality attributes

309
00:14:48,700 --> 00:14:51,300
that the load balancer provides to our system.

310
00:14:51,300 --> 00:14:53,870
And finally, we saw how we can combine

311
00:14:53,870 --> 00:14:55,640
all those different solutions

312
00:14:55,640 --> 00:14:58,260
to architect a large-scale system

313
00:14:58,260 --> 00:15:01,630
that can provide high performance and high availability,

314
00:15:01,630 --> 00:15:03,930
and scale to millions of users

315
00:15:03,930 --> 00:15:06,723
located in different geographical locations.

316
00:15:07,830 --> 00:15:09,923
I will see you soon in the next lecture.

