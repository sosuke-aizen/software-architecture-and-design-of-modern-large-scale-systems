1
00:00:00,380 --> 00:00:01,880
Hey, welcome back.

2
00:00:02,900 --> 00:00:03,980
In this lecture,

3
00:00:03,980 --> 00:00:07,170
we're going to learn about the API Gateway component,

4
00:00:07,170 --> 00:00:09,130
which is an architectural building block

5
00:00:09,130 --> 00:00:10,370
and design pattern

6
00:00:10,370 --> 00:00:14,173
used pretty much by all large scale systems in the industry.

7
00:00:15,040 --> 00:00:19,270
But before we talk about what an API Gateway can do for us,

8
00:00:19,270 --> 00:00:22,203
let's first understand the problem we're trying to solve.

9
00:00:23,160 --> 00:00:25,700
Let's imagine we're building a video sharing

10
00:00:25,700 --> 00:00:29,550
and streaming system where users can upload their videos,

11
00:00:29,550 --> 00:00:33,073
and also watch and comment on other people's videos.

12
00:00:34,080 --> 00:00:35,090
In the beginning,

13
00:00:35,090 --> 00:00:37,800
all we have is one service in our system

14
00:00:37,800 --> 00:00:39,860
that serves the frontend to the user

15
00:00:39,860 --> 00:00:42,640
in a form of HTML and JavaScript,

16
00:00:42,640 --> 00:00:45,710
and also takes care of the user profile,

17
00:00:45,710 --> 00:00:48,410
channel subscriptions, notifications,

18
00:00:48,410 --> 00:00:50,150
video storing and streaming,

19
00:00:50,150 --> 00:00:52,303
and comments left for each video.

20
00:00:53,160 --> 00:00:56,900
Additionally, this service needs to implement security

21
00:00:56,900 --> 00:01:00,270
because if a particular user wants to upload a new video,

22
00:01:00,270 --> 00:01:02,000
delete an existing video,

23
00:01:02,000 --> 00:01:03,860
or update their profile,

24
00:01:03,860 --> 00:01:06,900
we first need to make sure that that user

25
00:01:06,900 --> 00:01:09,680
is properly authenticated and authorized

26
00:01:09,680 --> 00:01:11,640
to make those changes.

27
00:01:11,640 --> 00:01:15,970
Over time, we realized that this one service code base

28
00:01:15,970 --> 00:01:18,270
becomes too big and complex for us

29
00:01:18,270 --> 00:01:19,960
to develop and maintain,

30
00:01:19,960 --> 00:01:23,310
so we apply the organizational scalability principle

31
00:01:23,310 --> 00:01:26,660
and split this one service into multiple services.

32
00:01:26,660 --> 00:01:28,963
Each one for only one purpose.

33
00:01:30,350 --> 00:01:32,480
The consequences of this change

34
00:01:32,480 --> 00:01:35,590
are that the single API we originally exposed

35
00:01:35,590 --> 00:01:38,580
is now split into multiple APIs

36
00:01:38,580 --> 00:01:41,090
implemented by each service.

37
00:01:41,090 --> 00:01:43,920
So now, we need to update the frontend code

38
00:01:43,920 --> 00:01:46,050
that runs on the client's web browser

39
00:01:46,050 --> 00:01:48,480
to be aware of the internal organization

40
00:01:48,480 --> 00:01:49,460
of our system,

41
00:01:49,460 --> 00:01:51,820
consisting of different services.

42
00:01:51,820 --> 00:01:54,090
And make calls to different services

43
00:01:54,090 --> 00:01:55,513
depending on the task.

44
00:01:56,440 --> 00:02:00,370
For example, when a user wants to simply go to the homepage

45
00:02:00,370 --> 00:02:02,840
and see the latest activity on the website,

46
00:02:02,840 --> 00:02:06,230
the client code would have to call the Frontend Service

47
00:02:06,230 --> 00:02:08,020
and the Users service.

48
00:02:08,020 --> 00:02:11,190
And when a user wants to watch a particular video,

49
00:02:11,190 --> 00:02:14,460
they would need to also make a call to the Frontend Service

50
00:02:14,460 --> 00:02:16,340
to load the different pages.

51
00:02:16,340 --> 00:02:19,840
Then call the Video Service to load the actual video,

52
00:02:19,840 --> 00:02:21,940
and then call the Comments Service

53
00:02:21,940 --> 00:02:24,930
to get all the comments that other users left

54
00:02:24,930 --> 00:02:26,503
on that particular video.

55
00:02:27,410 --> 00:02:29,390
Now, because we split our system

56
00:02:29,390 --> 00:02:31,030
into multiple services

57
00:02:31,030 --> 00:02:32,910
and the client application code

58
00:02:32,910 --> 00:02:35,130
makes separate calls to each service,

59
00:02:35,130 --> 00:02:38,680
each service needs to reimplement its own security,

60
00:02:38,680 --> 00:02:40,900
authentication, and authorization,

61
00:02:40,900 --> 00:02:44,383
which adds a lot of performance overhead and duplication.

62
00:02:45,330 --> 00:02:48,000
So to eliminate all those redundancies,

63
00:02:48,000 --> 00:02:49,910
decouple the client application

64
00:02:49,910 --> 00:02:52,570
from the internal organization of our system,

65
00:02:52,570 --> 00:02:55,270
and simplify our external API,

66
00:02:55,270 --> 00:02:57,220
we add an additional abstraction

67
00:02:57,220 --> 00:03:00,443
in a form of a service called API Gateway.

68
00:03:01,470 --> 00:03:05,090
An API Gateway is an API management service

69
00:03:05,090 --> 00:03:07,190
that sits in between the client

70
00:03:07,190 --> 00:03:09,583
and the collection of backend services.

71
00:03:10,580 --> 00:03:13,020
The API Gateway follows a software

72
00:03:13,020 --> 00:03:16,720
architectural pattern called API composition.

73
00:03:16,720 --> 00:03:17,900
In this pattern,

74
00:03:17,900 --> 00:03:21,430
we compose all the different APIs of our services

75
00:03:21,430 --> 00:03:25,530
that we want to expose externally into one single API

76
00:03:25,530 --> 00:03:28,720
the client applications can call by sending a requests

77
00:03:28,720 --> 00:03:30,693
to only one single service.

78
00:03:31,710 --> 00:03:34,610
The API Gateway creates an abstraction

79
00:03:34,610 --> 00:03:37,170
between the client and the rest of our system

80
00:03:37,170 --> 00:03:40,490
that provides us with a lot of benefits.

81
00:03:40,490 --> 00:03:43,260
The first benefit of the API Gateway

82
00:03:43,260 --> 00:03:46,090
is that it allows us to make internal changes

83
00:03:46,090 --> 00:03:49,860
to our system completely seamlessly and transparently

84
00:03:49,860 --> 00:03:51,493
to our API consumers.

85
00:03:52,700 --> 00:03:56,740
For example, if we have both desktop and mobile users,

86
00:03:56,740 --> 00:03:57,760
at some point,

87
00:03:57,760 --> 00:04:00,420
we may want to split our Frontend Service

88
00:04:00,420 --> 00:04:02,270
into two different services

89
00:04:02,270 --> 00:04:05,270
that serve different data depending on the device

90
00:04:05,270 --> 00:04:07,103
the request originated from.

91
00:04:08,040 --> 00:04:11,550
And similarly, we can split our video streaming service

92
00:04:11,550 --> 00:04:13,500
into two separate services.

93
00:04:13,500 --> 00:04:15,630
One for high resolution video,

94
00:04:15,630 --> 00:04:17,209
optimized for desktops,

95
00:04:17,209 --> 00:04:19,670
and another for lower resolutions,

96
00:04:19,670 --> 00:04:21,653
optimized for mobile devices.

97
00:04:22,540 --> 00:04:25,530
The second benefit of having an API Gateway

98
00:04:25,530 --> 00:04:27,490
in the front door to our system,

99
00:04:27,490 --> 00:04:29,740
is we can consolidate all the security,

100
00:04:29,740 --> 00:04:31,860
authorization, and authentication

101
00:04:31,860 --> 00:04:33,393
in one single place.

102
00:04:34,310 --> 00:04:36,560
So if we have a malicious user

103
00:04:36,560 --> 00:04:39,000
trying to impersonate another user,

104
00:04:39,000 --> 00:04:42,330
we can stop their request at the API Gateway.

105
00:04:42,330 --> 00:04:45,880
While a real user can be successfully authenticated

106
00:04:45,880 --> 00:04:47,380
at the API Gateway

107
00:04:47,380 --> 00:04:50,020
where we can perform an SSL termination

108
00:04:50,020 --> 00:04:52,110
and forward the decrypted request

109
00:04:52,110 --> 00:04:53,683
to the rest of the services.

110
00:04:54,780 --> 00:04:56,829
Additionally, we can allow a user

111
00:04:56,829 --> 00:04:58,960
to perform different operations

112
00:04:58,960 --> 00:05:01,960
depending on his permissions and role.

113
00:05:01,960 --> 00:05:04,220
A few examples of such operations

114
00:05:04,220 --> 00:05:06,090
that we can allow to some users

115
00:05:06,090 --> 00:05:08,450
and disallow to other users

116
00:05:08,450 --> 00:05:10,640
include viewing private videos,

117
00:05:10,640 --> 00:05:14,490
deleting, or uploading new videos, and so on.

118
00:05:14,490 --> 00:05:16,500
As part of the security feature,

119
00:05:16,500 --> 00:05:20,410
we can also implement rate limiting at the API Gateway

120
00:05:20,410 --> 00:05:23,853
to block denial of service attacks from malicious users.

121
00:05:24,850 --> 00:05:28,010
Now, another benefit of the API Gateway

122
00:05:28,010 --> 00:05:31,073
is it can improve the performance of our system.

123
00:05:31,920 --> 00:05:35,030
Besides the fact that we save a lot of overhead

124
00:05:35,030 --> 00:05:37,560
of authenticating every request from the user

125
00:05:37,560 --> 00:05:38,740
at each service

126
00:05:38,740 --> 00:05:41,670
by performing all the security and authentication

127
00:05:41,670 --> 00:05:42,880
in a single place,

128
00:05:42,880 --> 00:05:46,380
we can also save the user from making multiple requests

129
00:05:46,380 --> 00:05:48,310
to different services.

130
00:05:48,310 --> 00:05:51,003
This feature is called request routing.

131
00:05:51,950 --> 00:05:55,500
For example, when a user wants to watch a particular movie

132
00:05:55,500 --> 00:05:57,460
without the API Gateway,

133
00:05:57,460 --> 00:06:01,190
the user would have to make three calls to our system.

134
00:06:01,190 --> 00:06:03,170
One, to load the frontend page

135
00:06:03,170 --> 00:06:04,780
from the Frontend Service,

136
00:06:04,780 --> 00:06:07,680
then, load the video from the Video Service,

137
00:06:07,680 --> 00:06:10,680
and then make another call to the Comments Service

138
00:06:10,680 --> 00:06:13,880
to load all the comments that other users left

139
00:06:13,880 --> 00:06:15,363
for that particular video.

140
00:06:16,290 --> 00:06:19,290
By having the API Gateway in between the user

141
00:06:19,290 --> 00:06:20,570
and our services,

142
00:06:20,570 --> 00:06:24,660
the client code can make a single call to the API Gateway,

143
00:06:24,660 --> 00:06:26,010
which would route the request

144
00:06:26,010 --> 00:06:27,840
to all the appropriate services,

145
00:06:27,840 --> 00:06:29,790
and aggregate all the responses

146
00:06:29,790 --> 00:06:31,653
into one single response.

147
00:06:32,690 --> 00:06:36,470
Another way the API Gateway can improve our performance

148
00:06:36,470 --> 00:06:38,620
is by caching study content,

149
00:06:38,620 --> 00:06:41,303
as well as responses to certain requests.

150
00:06:42,290 --> 00:06:45,780
This, of course, reduces the response time to the user

151
00:06:45,780 --> 00:06:48,690
because if we already have the cached responses

152
00:06:48,690 --> 00:06:50,150
for particular requests,

153
00:06:50,150 --> 00:06:53,270
we can return it immediately from the API Gateway

154
00:06:53,270 --> 00:06:57,023
without the need to make requests to the different services.

155
00:06:58,410 --> 00:07:02,040
Another added benefit we get from routing all the traffic

156
00:07:02,040 --> 00:07:05,473
through one single service is monitoring and alerting.

157
00:07:06,460 --> 00:07:08,350
By adding monitoring logic

158
00:07:08,350 --> 00:07:10,300
into our API Gateway,

159
00:07:10,300 --> 00:07:12,460
we can gain real-time visibility

160
00:07:12,460 --> 00:07:14,030
into the traffic pattern

161
00:07:14,030 --> 00:07:16,420
and the load on our system.

162
00:07:16,420 --> 00:07:19,160
This also allows us to create alerts

163
00:07:19,160 --> 00:07:21,290
in case the traffic suddenly drops

164
00:07:21,290 --> 00:07:24,010
or increases unexpectedly.

165
00:07:24,010 --> 00:07:25,620
This feature helps us

166
00:07:25,620 --> 00:07:29,443
in improving our system's observability and availability.

167
00:07:30,530 --> 00:07:32,890
Finally, the API Gateway

168
00:07:32,890 --> 00:07:35,580
allows us to perform protocol translation

169
00:07:35,580 --> 00:07:37,123
in one single place.

170
00:07:38,110 --> 00:07:42,050
For example, externally, we can expose a rest API

171
00:07:42,050 --> 00:07:45,480
that uses JSON to represent different objects.

172
00:07:45,480 --> 00:07:48,290
But internally, some of our services

173
00:07:48,290 --> 00:07:52,490
may use different RPC technologies or different formats

174
00:07:52,490 --> 00:07:54,750
to represent their objects.

175
00:07:54,750 --> 00:07:57,020
At the same time, we may even have

176
00:07:57,020 --> 00:07:58,660
some legacy services

177
00:07:58,660 --> 00:08:01,807
that support all their protocols like HTTP 1

178
00:08:01,807 --> 00:08:04,713
and represent their objects using XML.

179
00:08:05,700 --> 00:08:07,070
Now, on the other hand,

180
00:08:07,070 --> 00:08:11,110
externally, we may also integrate with other systems

181
00:08:11,110 --> 00:08:13,880
that may bring us additional revenue.

182
00:08:13,880 --> 00:08:16,010
For example, we may integrate

183
00:08:16,010 --> 00:08:18,280
with a digital advertising company

184
00:08:18,280 --> 00:08:21,280
that may want to run ads on our videos.

185
00:08:21,280 --> 00:08:23,120
Or maybe another system

186
00:08:23,120 --> 00:08:25,760
wants to host their videos on our platform,

187
00:08:25,760 --> 00:08:27,750
and simply request them on demand

188
00:08:27,750 --> 00:08:30,570
to embed them on their webpages.

189
00:08:30,570 --> 00:08:33,570
All those companies may already have systems

190
00:08:33,570 --> 00:08:36,140
that support only particular protocols,

191
00:08:36,140 --> 00:08:37,929
and they may be reluctant

192
00:08:37,929 --> 00:08:40,130
to make big changes to their system

193
00:08:40,130 --> 00:08:44,150
to support our API's protocols and formats.

194
00:08:44,150 --> 00:08:45,240
In this case,

195
00:08:45,240 --> 00:08:47,580
we can simply extend our API

196
00:08:47,580 --> 00:08:50,120
to support their protocol and formats

197
00:08:50,120 --> 00:08:51,710
at the API Gateway,

198
00:08:51,710 --> 00:08:53,980
and perform the appropriate translation

199
00:08:53,980 --> 00:08:57,683
to call our services using the existing implementation.

200
00:08:58,560 --> 00:09:01,480
So now that we listed all the great benefits

201
00:09:01,480 --> 00:09:03,020
of an API Gateway,

202
00:09:03,020 --> 00:09:05,250
and mention all the quality attributes

203
00:09:05,250 --> 00:09:07,620
that the API Gateway provides us with,

204
00:09:07,620 --> 00:09:11,100
such as security, performance, and high availability

205
00:09:11,100 --> 00:09:12,860
that we get through monitoring,

206
00:09:12,860 --> 00:09:16,370
let's talk about a few best practices and anti-patterns

207
00:09:16,370 --> 00:09:18,873
of using this architectural building block.

208
00:09:19,800 --> 00:09:21,930
The first important consideration

209
00:09:21,930 --> 00:09:23,710
in using this design pattern

210
00:09:23,710 --> 00:09:26,550
is making sure that our API Gateway

211
00:09:26,550 --> 00:09:28,883
doesn't contain any business logic.

212
00:09:29,800 --> 00:09:32,680
While security caching and monitoring

213
00:09:32,680 --> 00:09:34,420
are nice added features,

214
00:09:34,420 --> 00:09:37,260
the main purpose of an API Gateway

215
00:09:37,260 --> 00:09:40,480
is API composition and routing of requests

216
00:09:40,480 --> 00:09:42,250
to different services.

217
00:09:42,250 --> 00:09:44,400
Those services are the ones

218
00:09:44,400 --> 00:09:46,230
that make the business decisions,

219
00:09:46,230 --> 00:09:48,713
and are performing the actual tasks.

220
00:09:49,640 --> 00:09:52,860
If we make the mistake and follow the anti-pattern

221
00:09:52,860 --> 00:09:55,670
of adding business logic to our API Gateway,

222
00:09:55,670 --> 00:09:57,290
and making it too smart,

223
00:09:57,290 --> 00:10:00,380
we may end up again with a single service

224
00:10:00,380 --> 00:10:01,820
that does all the work

225
00:10:01,820 --> 00:10:04,720
and contains an unmanageable amount of code.

226
00:10:04,720 --> 00:10:08,210
Which is actually the problem we wanted to solve initially

227
00:10:08,210 --> 00:10:11,183
by splitting our system into multiple services.

228
00:10:12,330 --> 00:10:13,920
The next thing to consider

229
00:10:13,920 --> 00:10:16,380
is since all the traffic to our system

230
00:10:16,380 --> 00:10:18,720
now goes through the API Gateway,

231
00:10:18,720 --> 00:10:22,673
our API Gateway may become a Single Point of Failure.

232
00:10:23,680 --> 00:10:26,210
We can easily solve the scalability,

233
00:10:26,210 --> 00:10:28,800
availability, and performance aspect

234
00:10:28,800 --> 00:10:31,140
by simply deploying multiple instances

235
00:10:31,140 --> 00:10:33,410
of our API Gateway service,

236
00:10:33,410 --> 00:10:36,313
and placing them all behind a load balancer.

237
00:10:37,380 --> 00:10:39,710
But another thing to consider

238
00:10:39,710 --> 00:10:41,570
is if we push a bad release

239
00:10:41,570 --> 00:10:43,380
or we introduce a bug

240
00:10:43,380 --> 00:10:46,240
that may crush our API Gateway service,

241
00:10:46,240 --> 00:10:50,293
our entire system becomes unavailable to the clients.

242
00:10:51,430 --> 00:10:53,620
So we need to be extra careful

243
00:10:53,620 --> 00:10:56,480
to eliminate any possibility of human error,

244
00:10:56,480 --> 00:11:00,000
and deploy new releases to the API Gateway service

245
00:11:00,000 --> 00:11:01,453
with extreme caution.

246
00:11:02,560 --> 00:11:05,890
Now, finally, we do need to acknowledge

247
00:11:05,890 --> 00:11:07,920
that adding an additional service

248
00:11:07,920 --> 00:11:10,000
that our client needs to go through

249
00:11:10,000 --> 00:11:12,730
any time a request is sent to our system

250
00:11:12,730 --> 00:11:16,200
does add a little bit of performance overhead.

251
00:11:16,200 --> 00:11:17,990
Now, while overall,

252
00:11:17,990 --> 00:11:20,050
by having an API Gateway,

253
00:11:20,050 --> 00:11:22,140
we typically benefit more

254
00:11:22,140 --> 00:11:24,800
than we sacrifice in terms of performance.

255
00:11:24,800 --> 00:11:26,300
But in certain situations,

256
00:11:26,300 --> 00:11:29,050
we may be tempted to over-optimize

257
00:11:29,050 --> 00:11:32,050
and bypass the API Gateway.

258
00:11:32,050 --> 00:11:35,513
But this is an anti-pattern that we should try to avoid.

259
00:11:37,100 --> 00:11:38,750
For example, in the case

260
00:11:38,750 --> 00:11:40,410
that the user service team

261
00:11:40,410 --> 00:11:42,960
wants to make a change in their API.

262
00:11:42,960 --> 00:11:45,330
If they know that the only service

263
00:11:45,330 --> 00:11:48,670
that calls them externally is the API Gateway,

264
00:11:48,670 --> 00:11:52,540
they can simply make the changes in the API Gateway service,

265
00:11:52,540 --> 00:11:55,373
and then safely release their new changes.

266
00:11:56,380 --> 00:11:59,230
However, if other external clients

267
00:11:59,230 --> 00:12:01,220
can call the service directly,

268
00:12:01,220 --> 00:12:03,030
then the Users Service team

269
00:12:03,030 --> 00:12:06,040
will have to be a lot more cautious and slower

270
00:12:06,040 --> 00:12:07,800
in releasing those changes.

271
00:12:07,800 --> 00:12:09,950
This is because they will have to go

272
00:12:09,950 --> 00:12:12,020
and update each client's code

273
00:12:12,020 --> 00:12:14,423
before they can release their change.

274
00:12:15,330 --> 00:12:18,360
This, again, tightly couples the service

275
00:12:18,360 --> 00:12:19,860
to external client's code,

276
00:12:19,860 --> 00:12:21,360
which is the problem we solve

277
00:12:21,360 --> 00:12:23,983
by using the API Gateway to begin with.

278
00:12:25,120 --> 00:12:28,080
So now that we know all the do's and don'ts

279
00:12:28,080 --> 00:12:30,630
of using an API Gateway in our system,

280
00:12:30,630 --> 00:12:33,740
let's summarize what we learned in this lecture.

281
00:12:33,740 --> 00:12:34,573
In this lecture,

282
00:12:34,573 --> 00:12:36,820
we learned about a very important

283
00:12:36,820 --> 00:12:40,020
architectural building block and design pattern

284
00:12:40,020 --> 00:12:42,060
of large scale systems.

285
00:12:42,060 --> 00:12:45,370
We'll learn about some of the benefits of an API Gateway,

286
00:12:45,370 --> 00:12:48,170
such as API composition, security,

287
00:12:48,170 --> 00:12:50,733
caching, monitoring, and so on.

288
00:12:51,730 --> 00:12:54,830
And finally, we talked about a few considerations

289
00:12:54,830 --> 00:12:58,180
for correctly using an API Gateway.

290
00:12:58,180 --> 00:13:01,270
Those considerations were keeping the business logic

291
00:13:01,270 --> 00:13:03,100
out of the API Gateway,

292
00:13:03,100 --> 00:13:04,460
being extra careful,

293
00:13:04,460 --> 00:13:07,460
and making modifications to the API Gateway.

294
00:13:07,460 --> 00:13:09,340
And finally, not breaking

295
00:13:09,340 --> 00:13:12,093
the API Gateway abstraction externally.

296
00:13:13,150 --> 00:13:15,520
I hope you already learned a lot so far,

297
00:13:15,520 --> 00:13:17,703
so I will see you soon in the next lecture.

