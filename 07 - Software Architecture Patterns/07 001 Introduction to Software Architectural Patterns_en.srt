1
00:00:00,320 --> 00:00:02,290
Welcome back.

2
00:00:02,290 --> 00:00:04,390
In this section, we're going to learn

3
00:00:04,390 --> 00:00:08,310
about the most popular and useful Architectural Patterns

4
00:00:08,310 --> 00:00:10,233
for modern software systems.

5
00:00:11,270 --> 00:00:13,960
But before we talk about any of those patterns,

6
00:00:13,960 --> 00:00:15,810
let's have a brief introduction

7
00:00:15,810 --> 00:00:19,193
to what Architectural Patterns are and when to use them.

8
00:00:20,200 --> 00:00:22,030
Software Architectural Patterns

9
00:00:22,030 --> 00:00:24,500
are general repeatable solutions

10
00:00:24,500 --> 00:00:27,920
to commonly occurring system design problems.

11
00:00:27,920 --> 00:00:32,100
Unlike design patterns, such as a singleton, factory,

12
00:00:32,100 --> 00:00:34,700
or strategy that you may have heard of,

13
00:00:34,700 --> 00:00:38,420
which are used to organize code within a single application,

14
00:00:38,420 --> 00:00:41,880
Software Architectural Patterns are common solutions

15
00:00:41,880 --> 00:00:44,120
to software architectural problems

16
00:00:44,120 --> 00:00:46,430
that involve multiple components

17
00:00:46,430 --> 00:00:48,730
that run a separate runtime units,

18
00:00:48,730 --> 00:00:51,163
such as applications or services.

19
00:00:52,470 --> 00:00:56,710
Over the years, many software architects have been observing

20
00:00:56,710 --> 00:00:59,770
how other companies in similar industries

21
00:00:59,770 --> 00:01:03,530
went about solving similar design problems to their own

22
00:01:03,530 --> 00:01:07,240
and tried to learn what worked for them and why.

23
00:01:07,240 --> 00:01:08,700
Even more importantly,

24
00:01:08,700 --> 00:01:11,430
they observed what mistakes were made

25
00:01:11,430 --> 00:01:15,360
so that other companies wouldn't have to waste resources

26
00:01:15,360 --> 00:01:18,420
repeating those same anti-patterns.

27
00:01:18,420 --> 00:01:21,560
Those general software architecture practices

28
00:01:21,560 --> 00:01:23,170
that seemed to be successful

29
00:01:23,170 --> 00:01:27,083
became what we know as Software Architectural Patterns.

30
00:01:28,130 --> 00:01:30,880
Now, let's talk about why we should be using

31
00:01:30,880 --> 00:01:33,083
those Software Architectural Patterns.

32
00:01:34,140 --> 00:01:37,130
As software architects and system designers,

33
00:01:37,130 --> 00:01:41,100
we're not strictly required to follow any of those patterns.

34
00:01:41,100 --> 00:01:45,800
However, we do have a few incentives to follow them.

35
00:01:45,800 --> 00:01:50,140
The first incentive is to save valuable time and resources

36
00:01:50,140 --> 00:01:52,793
for ourselves and our organization.

37
00:01:53,740 --> 00:01:57,750
Essentially, if somebody tells us that other companies

38
00:01:57,750 --> 00:02:00,600
that had a very similar use case to the problem

39
00:02:00,600 --> 00:02:02,960
were trying to solve and operate

40
00:02:02,960 --> 00:02:04,880
on a similar scale to ours

41
00:02:04,880 --> 00:02:07,100
already found an architecture

42
00:02:07,100 --> 00:02:10,370
and development practice that works for them,

43
00:02:10,370 --> 00:02:13,880
then it's better for us to take that knowledge and use it

44
00:02:13,880 --> 00:02:15,670
instead of reinventing the wheel

45
00:02:15,670 --> 00:02:18,403
and trying to come up with something completely new.

46
00:02:19,310 --> 00:02:20,700
The second motivation

47
00:02:20,700 --> 00:02:23,620
to use an existing Software Architectural Pattern

48
00:02:23,620 --> 00:02:27,280
is to mitigate the risk of getting into a situation

49
00:02:27,280 --> 00:02:28,880
where our architecture

50
00:02:28,880 --> 00:02:32,473
resembles what's called a big ball of mud.

51
00:02:33,370 --> 00:02:37,280
The big ball of mud is an anti-pattern of a system

52
00:02:37,280 --> 00:02:39,880
that seemingly lacks any structure,

53
00:02:39,880 --> 00:02:43,190
where every service talks to every other service,

54
00:02:43,190 --> 00:02:45,760
everything is tightly coupled together,

55
00:02:45,760 --> 00:02:48,930
all the information is global or duplicated,

56
00:02:48,930 --> 00:02:51,740
and there's no clear scope of responsibility

57
00:02:51,740 --> 00:02:53,820
for any of the components.

58
00:02:53,820 --> 00:02:55,130
It's needless to say

59
00:02:55,130 --> 00:02:57,743
that it's not a situation we want to be in.

60
00:02:58,760 --> 00:03:02,890
Surprisingly, many companies got into this situation

61
00:03:02,890 --> 00:03:05,250
or some variation of that situation

62
00:03:05,250 --> 00:03:08,270
due to many reasons such as rapid growth

63
00:03:08,270 --> 00:03:12,610
or lack of overarching, well-defined software architecture

64
00:03:12,610 --> 00:03:15,700
that the entire organization adhered to.

65
00:03:15,700 --> 00:03:18,450
A system that gets into this situation

66
00:03:18,450 --> 00:03:21,920
is very hard to develop, maintain, and scale,

67
00:03:21,920 --> 00:03:25,380
which can have detrimental consequences for our business.

68
00:03:25,380 --> 00:03:27,643
So we definitely want to avoid it.

69
00:03:28,620 --> 00:03:30,930
And finally, the third incentive

70
00:03:30,930 --> 00:03:33,990
to follow a well-known Software Architectural Pattern

71
00:03:33,990 --> 00:03:36,820
is that other software engineers and architects

72
00:03:36,820 --> 00:03:39,210
can continue working on our system

73
00:03:39,210 --> 00:03:43,500
and can easily carry on and stick to the same architecture

74
00:03:44,610 --> 00:03:48,160
because everyone can read about the pattern we're following

75
00:03:48,160 --> 00:03:51,723
and understand exactly what to do and what not to do.

76
00:03:52,610 --> 00:03:55,430
However, with all of that in mind

77
00:03:55,430 --> 00:03:57,600
all the Software Architectural Patterns

78
00:03:57,600 --> 00:04:00,330
we're going to learn are just guidelines.

79
00:04:00,330 --> 00:04:01,730
And at the end of the day,

80
00:04:01,730 --> 00:04:05,100
our job is to define the best software architecture

81
00:04:05,100 --> 00:04:06,410
for our use case

82
00:04:06,410 --> 00:04:10,400
and apply what is relevant to our unique situation.

83
00:04:10,400 --> 00:04:11,550
Before we proceed

84
00:04:11,550 --> 00:04:14,240
to learn about the first Architectural Pattern,

85
00:04:14,240 --> 00:04:17,200
there's one more thing I want to point out.

86
00:04:17,200 --> 00:04:20,800
As systems evolve, certain Architectural Patterns

87
00:04:20,800 --> 00:04:23,240
that used to fit our system perfectly

88
00:04:23,240 --> 00:04:24,920
may not fit us anymore,

89
00:04:24,920 --> 00:04:26,910
and that is expected.

90
00:04:26,910 --> 00:04:30,430
At that point, we would need to do some restructuring

91
00:04:30,430 --> 00:04:31,610
and potentially migrate

92
00:04:31,610 --> 00:04:34,620
to a different Software Architectural Pattern

93
00:04:34,620 --> 00:04:37,160
that now fits us better.

94
00:04:37,160 --> 00:04:39,060
However, the best part

95
00:04:39,060 --> 00:04:42,020
about following those common Architectural Patterns

96
00:04:42,020 --> 00:04:43,560
is that many companies

97
00:04:43,560 --> 00:04:46,990
already went through such migrations in the past

98
00:04:46,990 --> 00:04:49,810
so we can follow their best practices

99
00:04:49,810 --> 00:04:53,600
to make those migrations quickly and safely.

100
00:04:53,600 --> 00:04:55,850
So now that we got some introduction

101
00:04:55,850 --> 00:04:58,080
to what we're going to learn in this section,

102
00:04:58,080 --> 00:05:00,330
let's proceed to the next lecture

103
00:05:00,330 --> 00:05:03,363
and learn about our first Architectural Pattern.

