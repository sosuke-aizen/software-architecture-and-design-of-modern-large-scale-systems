1
00:00:00,400 --> 00:00:02,400
Welcome back.

2
00:00:02,400 --> 00:00:04,380
In this lecture, we're going to learn

3
00:00:04,380 --> 00:00:07,750
about another very popular architectural pattern

4
00:00:07,750 --> 00:00:11,090
which is called Microservices Architecture.

5
00:00:11,090 --> 00:00:12,950
We will start with the motivation

6
00:00:12,950 --> 00:00:16,140
for using this architectural pattern by comparing it

7
00:00:16,140 --> 00:00:19,410
to the Monolithic three-tier Architecture pattern,

8
00:00:19,410 --> 00:00:21,960
which we learned in a previous lecture.

9
00:00:21,960 --> 00:00:26,010
And later we will talk about the advantages, best practices,

10
00:00:26,010 --> 00:00:30,460
and challenges that come with this architectural pattern.

11
00:00:30,460 --> 00:00:33,340
So what is microservices architecture

12
00:00:33,340 --> 00:00:34,863
and when should we use it?

13
00:00:35,950 --> 00:00:39,670
In the lecture about the multi-tier architectural pattern,

14
00:00:39,670 --> 00:00:42,360
we learned about the three-tier architecture

15
00:00:42,360 --> 00:00:45,780
which we also refer to as a monolithic architecture,

16
00:00:45,780 --> 00:00:48,880
because all the business logic was concentrated

17
00:00:48,880 --> 00:00:51,913
in one single service in the application tier.

18
00:00:52,850 --> 00:00:54,480
As we mentioned earlier,

19
00:00:54,480 --> 00:00:58,490
the monolithic three-tier architecture is the perfect choice

20
00:00:58,490 --> 00:01:01,900
for small teams that manage a fairly small

21
00:01:01,900 --> 00:01:03,933
and not too complex codebase.

22
00:01:05,310 --> 00:01:10,270
However, as the size and complexity of our codebase grow,

23
00:01:10,270 --> 00:01:13,420
it becomes extremely difficult to troubleshoot

24
00:01:13,420 --> 00:01:17,420
and add new features to it, as well as build, test,

25
00:01:17,420 --> 00:01:20,213
and even load the entire codebase in our IDE.

26
00:01:21,840 --> 00:01:24,570
On the organizational scalability dimension,

27
00:01:24,570 --> 00:01:26,960
we also start having problems,

28
00:01:26,960 --> 00:01:29,960
because the more engineers we add to the team,

29
00:01:29,960 --> 00:01:32,730
the more code merge conflicts we get

30
00:01:32,730 --> 00:01:37,420
and our meetings become larger, longer, and less productive.

31
00:01:37,420 --> 00:01:39,790
Once we start seeing those problems,

32
00:01:39,790 --> 00:01:41,710
we need to start switching gears

33
00:01:41,710 --> 00:01:44,580
and consider migrating our architecture

34
00:01:44,580 --> 00:01:45,680
towards microservices.

35
00:01:47,210 --> 00:01:50,250
So what is microservices architecture

36
00:01:50,250 --> 00:01:52,650
and how does it solve all the problems

37
00:01:52,650 --> 00:01:54,053
that we just mentioned?

38
00:01:55,210 --> 00:01:58,930
In contrast to the monolithic three-tier architecture,

39
00:01:58,930 --> 00:02:03,190
microservices architecture organizes our business logic

40
00:02:03,190 --> 00:02:05,750
as a collection of loosely coupled

41
00:02:05,750 --> 00:02:08,949
and independently deployed services.

42
00:02:08,949 --> 00:02:12,190
Each service is owned by a small team

43
00:02:12,190 --> 00:02:15,023
and has a narrow scope of responsibility.

44
00:02:16,070 --> 00:02:20,580
This architectural style offers us a lot of advantages.

45
00:02:20,580 --> 00:02:24,260
The narrow scope of responsibility in each microservice

46
00:02:24,260 --> 00:02:26,810
makes the codebase a lot smaller

47
00:02:26,810 --> 00:02:29,743
than what we had in the monolithic architecture.

48
00:02:30,750 --> 00:02:34,890
This provides us with a wide range of benefits.

49
00:02:34,890 --> 00:02:36,450
With a small codebase,

50
00:02:36,450 --> 00:02:41,010
development just becomes a lot easier and faster.

51
00:02:41,010 --> 00:02:45,250
For example, now the code base loads instantaneously

52
00:02:45,250 --> 00:02:46,083
in our IDE.

53
00:02:47,090 --> 00:02:51,360
Also building and testing the code becomes much easier

54
00:02:51,360 --> 00:02:55,170
and faster simply because there are far fewer things

55
00:02:55,170 --> 00:02:57,090
to build and test.

56
00:02:57,090 --> 00:03:01,120
Additionally, troubleshooting or adding new features

57
00:03:01,120 --> 00:03:03,140
also becomes much easier

58
00:03:03,140 --> 00:03:07,360
because the code itself is much easier to reason about,

59
00:03:07,360 --> 00:03:09,720
and new developers who join the team

60
00:03:09,720 --> 00:03:12,733
can become fully productive a lot faster.

61
00:03:13,640 --> 00:03:16,160
In terms of performance and scalability,

62
00:03:16,160 --> 00:03:18,250
we also get a lot of benefits

63
00:03:18,250 --> 00:03:21,413
from breaking the monolithic application into microservices.

64
00:03:22,940 --> 00:03:25,180
Each instance of our microservice

65
00:03:25,180 --> 00:03:27,750
becomes a lot less CPU intensive

66
00:03:27,750 --> 00:03:30,290
and consumes far less memory

67
00:03:30,290 --> 00:03:34,230
so it can run much smoother on commodity hardware.

68
00:03:34,230 --> 00:03:37,470
And we can scale each such service horizontally

69
00:03:37,470 --> 00:03:41,883
by adding more instances of fairly low-end computers.

70
00:03:42,870 --> 00:03:45,060
On the organizational scalability,

71
00:03:45,060 --> 00:03:47,213
we also get a lot of advantages.

72
00:03:48,270 --> 00:03:51,660
Since each service can be independently developed,

73
00:03:51,660 --> 00:03:55,060
maintained, and deployed by a separate small team,

74
00:03:55,060 --> 00:03:57,260
we can get very high throughput

75
00:03:57,260 --> 00:03:59,993
from the entire organization as a whole.

76
00:04:01,060 --> 00:04:05,000
On top of that, each team is completely autonomous

77
00:04:05,000 --> 00:04:08,240
to decide what programming languages, frameworks,

78
00:04:08,240 --> 00:04:10,300
and technologies they want to use

79
00:04:10,300 --> 00:04:13,460
and what kind of release schedule or process

80
00:04:13,460 --> 00:04:14,623
they want to follow.

81
00:04:15,480 --> 00:04:19,100
Finally, if all those benefits aren't enough,

82
00:04:19,100 --> 00:04:23,573
we also get better security in a form of Fault Isolation,

83
00:04:24,570 --> 00:04:28,820
which means that if we have an issue in one of our services

84
00:04:28,820 --> 00:04:31,970
or one of our services starts crashing,

85
00:04:31,970 --> 00:04:34,790
it's a lot easier for us to isolate

86
00:04:34,790 --> 00:04:37,810
and mitigate the problem than if we had an issue

87
00:04:37,810 --> 00:04:40,483
in our single monolithic application.

88
00:04:41,550 --> 00:04:44,030
Now it's hard not to get excited

89
00:04:44,030 --> 00:04:46,010
about this style of architecture.

90
00:04:46,010 --> 00:04:50,440
And unfortunately, a lot of organizations jump too quickly

91
00:04:50,440 --> 00:04:54,483
to microservices without considering two major factors.

92
00:04:55,580 --> 00:04:58,620
The first factor is while theoretically

93
00:04:58,620 --> 00:05:00,490
we can achieve all those benefits

94
00:05:00,490 --> 00:05:02,800
from migrating to microservices,

95
00:05:02,800 --> 00:05:06,240
we don't get all of them for free out of the box

96
00:05:06,240 --> 00:05:09,030
just by splitting our monolithic codebase

97
00:05:09,030 --> 00:05:11,823
into an arbitrary collection of services.

98
00:05:12,750 --> 00:05:15,960
So in order for us to get the full benefit

99
00:05:15,960 --> 00:05:19,170
of this architecture, there are a fewer rules of thumb

100
00:05:19,170 --> 00:05:22,050
and best practices that we need to follow.

101
00:05:22,050 --> 00:05:23,910
And if we don't follow them,

102
00:05:23,910 --> 00:05:28,583
we can easily fall into our nemesis, the Big Ball of Mud.

103
00:05:29,700 --> 00:05:33,370
The second factor to consider is that microservices

104
00:05:33,370 --> 00:05:37,320
do come with a fair amount of overhead and challenges

105
00:05:37,320 --> 00:05:39,870
which have to be taken into consideration

106
00:05:39,870 --> 00:05:42,853
before migrating to this architectural style.

107
00:05:43,890 --> 00:05:46,400
So first of all, in order for us

108
00:05:46,400 --> 00:05:49,320
to achieve full organizational decoupling

109
00:05:49,320 --> 00:05:52,050
so that each team can operate independently,

110
00:05:52,050 --> 00:05:54,550
we need to make sure that the services

111
00:05:54,550 --> 00:05:56,910
are logically separated in a way

112
00:05:56,910 --> 00:06:00,080
that every change in the system can happen

113
00:06:00,080 --> 00:06:01,840
only in one service

114
00:06:01,840 --> 00:06:05,140
so it wouldn't involve multiple teams.

115
00:06:05,140 --> 00:06:08,440
Otherwise, if every single change requires

116
00:06:08,440 --> 00:06:11,910
meeting with another team, coordinating the development

117
00:06:11,910 --> 00:06:14,000
and the release of the new feature,

118
00:06:14,000 --> 00:06:18,630
then we don't gain much from the migration to microservices.

119
00:06:19,610 --> 00:06:22,680
To achieve this, there are a few best practices

120
00:06:22,680 --> 00:06:23,973
that we need to follow.

121
00:06:24,890 --> 00:06:26,520
The first best practice

122
00:06:26,520 --> 00:06:29,173
is the Single Responsibility Principle,

123
00:06:30,040 --> 00:06:33,880
which means that each service needs to be responsible

124
00:06:33,880 --> 00:06:37,120
for only one business capability, domain,

125
00:06:37,120 --> 00:06:39,053
resource, or action.

126
00:06:39,950 --> 00:06:43,370
For example, if we have an online dating service,

127
00:06:43,370 --> 00:06:46,480
we can have the following microservices separated

128
00:06:46,480 --> 00:06:50,210
by their business capability and sub domains.

129
00:06:50,210 --> 00:06:53,010
The user profile service is responsible

130
00:06:53,010 --> 00:06:56,650
for the business object that relates to the user's profile

131
00:06:56,650 --> 00:07:00,460
and every way that the user can interact with it.

132
00:07:00,460 --> 00:07:03,740
The image service takes care of the storage,

133
00:07:03,740 --> 00:07:08,740
resizing, and presentation of the profile related images.

134
00:07:08,900 --> 00:07:11,320
The matching service is responsible

135
00:07:11,320 --> 00:07:13,180
for matching different users

136
00:07:13,180 --> 00:07:15,570
based on the rules that we define

137
00:07:15,570 --> 00:07:17,430
as well as the preferences

138
00:07:17,430 --> 00:07:20,690
that the users define in their profiles.

139
00:07:20,690 --> 00:07:23,730
And finally, the billing service is responsible

140
00:07:23,730 --> 00:07:27,190
for everything related to charging the user's money

141
00:07:27,190 --> 00:07:29,333
for the services they're consuming.

142
00:07:30,350 --> 00:07:34,070
So now when the user wants to update their profile

143
00:07:34,070 --> 00:07:36,680
or see someone else's profile,

144
00:07:36,680 --> 00:07:41,000
they would simply be directed to the user profile service.

145
00:07:41,000 --> 00:07:43,180
If they want to upload images,

146
00:07:43,180 --> 00:07:45,780
they can be directed to the image service.

147
00:07:45,780 --> 00:07:47,960
And when they want to get suggestions

148
00:07:47,960 --> 00:07:49,760
for potential partners,

149
00:07:49,760 --> 00:07:52,100
they can contact the matching service,

150
00:07:52,100 --> 00:07:55,410
which would in turn talk to the user profile service

151
00:07:55,410 --> 00:07:58,500
to figure out which profiles would be a good fit

152
00:07:58,500 --> 00:08:00,400
for the current user.

153
00:08:00,400 --> 00:08:03,560
And of course whenever the user wants to purchase

154
00:08:03,560 --> 00:08:06,140
additional services, they can be directed

155
00:08:06,140 --> 00:08:07,500
to the billing service,

156
00:08:07,500 --> 00:08:11,050
which in turn may contact the user profile service

157
00:08:11,050 --> 00:08:14,713
to unlock certain features for that particular user.

158
00:08:15,750 --> 00:08:19,340
Let's take another example from the e-commerce space

159
00:08:19,340 --> 00:08:22,740
and this time we will separate the microservices

160
00:08:22,740 --> 00:08:26,190
by the actions related to the way users interact

161
00:08:26,190 --> 00:08:28,690
with our system as well as the entities

162
00:08:28,690 --> 00:08:30,790
we have in our system.

163
00:08:30,790 --> 00:08:34,340
On the action side, we have the product search service

164
00:08:34,340 --> 00:08:36,770
which given a search query from the user

165
00:08:36,770 --> 00:08:39,840
performs a search and returns a personalized view

166
00:08:39,840 --> 00:08:43,809
of the relevant products for that particular user.

167
00:08:43,809 --> 00:08:45,700
But to find those products

168
00:08:45,700 --> 00:08:48,350
it talks to the product inventory service

169
00:08:48,350 --> 00:08:51,550
which encapsulates the products entity.

170
00:08:51,550 --> 00:08:54,640
Another action oriented microservice we have

171
00:08:54,640 --> 00:08:56,880
is the checkout service.

172
00:08:56,880 --> 00:08:59,620
So when the user goes to checkout,

173
00:08:59,620 --> 00:09:01,920
they will see what they have in the cart

174
00:09:01,920 --> 00:09:05,080
and also see the amount of tax they need to pay

175
00:09:05,080 --> 00:09:06,370
which we accomplish

176
00:09:06,370 --> 00:09:09,550
by talking to another action oriented microservice,

177
00:09:09,550 --> 00:09:11,500
the tax calculator.

178
00:09:11,500 --> 00:09:14,150
This service will calculate the tax

179
00:09:14,150 --> 00:09:16,950
based on the product's price, category,

180
00:09:16,950 --> 00:09:18,523
and the user's location.

181
00:09:19,440 --> 00:09:22,780
And finally, when the user confirms the purchase,

182
00:09:22,780 --> 00:09:26,980
the checkout service will orchestrate the entire transaction

183
00:09:26,980 --> 00:09:30,080
by talking to the billing service, shipping service,

184
00:09:30,080 --> 00:09:32,230
and the product inventory service,

185
00:09:32,230 --> 00:09:35,163
which are all entity oriented services.

186
00:09:36,160 --> 00:09:40,280
Additionally, we can also break the API gateway

187
00:09:40,280 --> 00:09:42,410
that used to be a monolithic service

188
00:09:42,410 --> 00:09:44,920
into multiple microservices.

189
00:09:44,920 --> 00:09:48,270
So depending on the type of devices or clients,

190
00:09:48,270 --> 00:09:51,210
we can have separate API Gateway services

191
00:09:51,210 --> 00:09:55,053
that are more specialized and therefore more lightweight.

192
00:09:56,000 --> 00:09:58,920
Now the second best practice to make sure

193
00:09:58,920 --> 00:10:01,950
that there is no coupling between different services

194
00:10:01,950 --> 00:10:05,213
is to have a separate database for each service.

195
00:10:06,470 --> 00:10:10,480
Otherwise, if two services share a single database,

196
00:10:10,480 --> 00:10:15,180
then every single schema or document structure change

197
00:10:15,180 --> 00:10:19,323
will require careful coordination between multiple teams.

198
00:10:20,280 --> 00:10:24,640
On the other hand, if each service has its own database,

199
00:10:24,640 --> 00:10:26,900
then the database essentially

200
00:10:26,900 --> 00:10:30,210
becomes an implementation detail of each service,

201
00:10:30,210 --> 00:10:33,100
and it can be easily updated or replaced

202
00:10:33,100 --> 00:10:36,920
completely transparently to the rest of the system.

203
00:10:36,920 --> 00:10:40,120
It's important to note that when we split the original

204
00:10:40,120 --> 00:10:43,060
monolithic database and provide each service

205
00:10:43,060 --> 00:10:46,610
with its own database, the data has to be split

206
00:10:46,610 --> 00:10:49,430
in such a way that each microservice

207
00:10:49,430 --> 00:10:52,750
can be completely independent and fully capable

208
00:10:52,750 --> 00:10:54,080
of doing its work

209
00:10:54,080 --> 00:10:58,100
while minimizing the need to call other services.

210
00:10:58,100 --> 00:11:01,600
Of course, some data duplication is expected

211
00:11:01,600 --> 00:11:04,150
and is the overhead that we need to accept

212
00:11:04,150 --> 00:11:06,483
when we move to this type of architecture.

213
00:11:07,470 --> 00:11:11,620
So in conclusion, following all those best practices

214
00:11:11,620 --> 00:11:15,090
will definitely set us in the right direction for success

215
00:11:15,090 --> 00:11:17,510
using this architectural style.

216
00:11:17,510 --> 00:11:19,940
However, I want to reiterate

217
00:11:19,940 --> 00:11:24,340
that microservices architecture provides all those benefits

218
00:11:24,340 --> 00:11:26,940
despite the complexity and overhead

219
00:11:26,940 --> 00:11:29,520
only when we reach a certain complexity

220
00:11:29,520 --> 00:11:31,950
and organizational scale.

221
00:11:31,950 --> 00:11:33,570
And it's always best

222
00:11:33,570 --> 00:11:37,150
to start with the simple monolithic approach first

223
00:11:37,150 --> 00:11:40,890
and only when that architecture stops working for us,

224
00:11:40,890 --> 00:11:42,783
we should consider microservices.

225
00:11:44,170 --> 00:11:47,310
In this lecture, we'll learn about a very popular

226
00:11:47,310 --> 00:11:50,000
and useful architectural pattern and style

227
00:11:50,000 --> 00:11:53,310
which is called Microservices Architecture.

228
00:11:53,310 --> 00:11:55,300
We talked about all the benefits

229
00:11:55,300 --> 00:11:58,160
that this architectural style provides us with,

230
00:11:58,160 --> 00:12:02,230
especially when we reach a point of complexity and scale

231
00:12:02,230 --> 00:12:05,010
that the monolithic three-tier architecture

232
00:12:05,010 --> 00:12:07,570
stops being a good fit for us.

233
00:12:07,570 --> 00:12:11,210
All those benefits result in higher organizational

234
00:12:11,210 --> 00:12:14,570
and operational scalability, better performance,

235
00:12:14,570 --> 00:12:18,233
faster development, better security, and so on.

236
00:12:19,230 --> 00:12:22,290
And finally, we talked about a few considerations

237
00:12:22,290 --> 00:12:24,980
and best practices that we need to follow

238
00:12:24,980 --> 00:12:28,303
to achieve all those benefits when we use Microservices.

239
00:12:29,910 --> 00:12:31,913
I will see you all in the next lecture.

