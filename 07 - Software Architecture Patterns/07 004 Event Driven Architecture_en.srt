1
00:00:00,250 --> 00:00:02,150
Welcome back.

2
00:00:02,150 --> 00:00:04,040
In this lecture, we're going to learn

3
00:00:04,040 --> 00:00:07,410
about another architectural style and pattern,

4
00:00:07,410 --> 00:00:10,640
which is called Event-Driven Architecture.

5
00:00:10,640 --> 00:00:12,990
First, we will get an introduction

6
00:00:12,990 --> 00:00:15,580
to what Event-Driven Architecture is.

7
00:00:15,580 --> 00:00:18,470
And later, we will learn about all its benefits,

8
00:00:18,470 --> 00:00:20,860
as well as use cases where this type

9
00:00:20,860 --> 00:00:24,283
of architecture can be very powerful and useful.

10
00:00:25,290 --> 00:00:28,060
So what is an Event-Driven Architecture

11
00:00:28,060 --> 00:00:30,163
and what are its main components?

12
00:00:31,250 --> 00:00:34,480
If we recall from the lecture about microservices,

13
00:00:34,480 --> 00:00:38,670
if microservice A wants to communicate with microservice B,

14
00:00:38,670 --> 00:00:42,520
then not only microservice A needs to be aware

15
00:00:42,520 --> 00:00:44,900
of the existence of microservice B

16
00:00:44,900 --> 00:00:49,080
but also it needs to know what API microservice B provides

17
00:00:49,080 --> 00:00:50,293
and how to call it.

18
00:00:51,420 --> 00:00:53,130
Also, at runtime,

19
00:00:53,130 --> 00:00:57,330
microservice A has to call microservice B synchronously

20
00:00:57,330 --> 00:00:58,973
and wait for its response.

21
00:01:00,150 --> 00:01:02,550
So from what we just described,

22
00:01:02,550 --> 00:01:04,580
we can see that, essentially,

23
00:01:04,580 --> 00:01:08,863
microservice A has a dependency on microservice B.

24
00:01:09,920 --> 00:01:11,920
In an Event-Driven Architecture,

25
00:01:11,920 --> 00:01:15,340
instead of direct messages that issue commands

26
00:01:15,340 --> 00:01:17,860
or requests that ask for data,

27
00:01:17,860 --> 00:01:19,623
we have only events.

28
00:01:20,690 --> 00:01:25,353
An event is an immutable statement of a fact or a change.

29
00:01:26,390 --> 00:01:29,730
For example, a user clicking on a digital ad

30
00:01:29,730 --> 00:01:33,430
or an item being added to a shopping cart can be thought of

31
00:01:33,430 --> 00:01:35,100
as fact events

32
00:01:35,100 --> 00:01:39,040
while a player of a video game or an IoT device,

33
00:01:39,040 --> 00:01:40,370
like a vacuum cleaner,

34
00:01:40,370 --> 00:01:43,640
moving from one position to another can be thought of

35
00:01:43,640 --> 00:01:44,943
as change events.

36
00:01:46,330 --> 00:01:48,210
In an Event-Driven Architecture,

37
00:01:48,210 --> 00:01:50,110
we have three components.

38
00:01:50,110 --> 00:01:51,390
On the sending side,

39
00:01:51,390 --> 00:01:53,020
we have the event emitters,

40
00:01:53,020 --> 00:01:55,580
which are also referred to as producers.

41
00:01:55,580 --> 00:01:56,940
On the receiving end,

42
00:01:56,940 --> 00:01:58,930
we have the event consumers.

43
00:01:58,930 --> 00:02:00,000
And in between,

44
00:02:00,000 --> 00:02:01,560
we have the event channel,

45
00:02:01,560 --> 00:02:04,700
which is essentially the message broker we learned about

46
00:02:04,700 --> 00:02:05,883
in a past lecture.

47
00:02:06,960 --> 00:02:09,650
When we use the Event-Driven Architecture style

48
00:02:09,650 --> 00:02:12,210
with microservices, we can get a lot

49
00:02:12,210 --> 00:02:14,883
of very useful properties and benefits.

50
00:02:15,840 --> 00:02:19,600
For example, now, if microservice A communicates

51
00:02:19,600 --> 00:02:22,500
with microservice B by producing events,

52
00:02:22,500 --> 00:02:24,960
the dependency of microservice A

53
00:02:24,960 --> 00:02:27,293
on microservice B is removed.

54
00:02:28,440 --> 00:02:32,360
In fact, microservice A doesn't need to know anything

55
00:02:32,360 --> 00:02:35,023
about the existence of microservice B.

56
00:02:36,090 --> 00:02:39,170
And once microservice A produces the event,

57
00:02:39,170 --> 00:02:43,313
it doesn't need to wait for any response from any consumer.

58
00:02:44,320 --> 00:02:46,710
Because services don't need to know

59
00:02:46,710 --> 00:02:49,730
about each other's existence or API

60
00:02:49,730 --> 00:02:51,800
and all the messages are exchanged

61
00:02:51,800 --> 00:02:55,560
completely asynchronously, we can decouple microservices

62
00:02:55,560 --> 00:02:59,110
more effectively, which in turn provides our system

63
00:02:59,110 --> 00:03:00,740
with higher scalability.

64
00:03:00,740 --> 00:03:03,240
And we can add more services to the system

65
00:03:03,240 --> 00:03:05,393
without making any changes.

66
00:03:06,520 --> 00:03:09,310
For example, if we have a banking system,

67
00:03:09,310 --> 00:03:12,130
we can start with just two services.

68
00:03:12,130 --> 00:03:14,910
The first service is the front-end service,

69
00:03:14,910 --> 00:03:18,210
which simply provides the user with the user interface

70
00:03:18,210 --> 00:03:20,290
and takes in the user's input,

71
00:03:20,290 --> 00:03:21,810
such as money deposits,

72
00:03:21,810 --> 00:03:24,830
transfers between accounts, and so on.

73
00:03:24,830 --> 00:03:27,560
The second service is the account service,

74
00:03:27,560 --> 00:03:31,163
which maintains and updates the balance for each user.

75
00:03:32,210 --> 00:03:35,370
Now, if we use an Event-Driven Architecture,

76
00:03:35,370 --> 00:03:38,060
then every action that the user performs

77
00:03:38,060 --> 00:03:41,330
on their account can be events that are produced

78
00:03:41,330 --> 00:03:43,380
by the front-end service.

79
00:03:43,380 --> 00:03:46,980
And the account service simply subscribes to those events

80
00:03:46,980 --> 00:03:50,700
so it can consume them and update the user's balance.

81
00:03:50,700 --> 00:03:53,980
Now, because we decouple those services using

82
00:03:53,980 --> 00:03:55,130
a message broker,

83
00:03:55,130 --> 00:03:58,560
we can easily add a mobile notification service,

84
00:03:58,560 --> 00:04:00,810
which subscribes to the same channel.

85
00:04:00,810 --> 00:04:04,310
And every time a user makes changes to their account,

86
00:04:04,310 --> 00:04:07,390
the service can send push notifications

87
00:04:07,390 --> 00:04:10,240
to the user's mobile device about the activity

88
00:04:10,240 --> 00:04:11,323
in their account.

89
00:04:12,460 --> 00:04:15,880
Notice that adding the service was super easy

90
00:04:15,880 --> 00:04:18,570
and did not require us to make any changes

91
00:04:18,570 --> 00:04:20,769
to the front-end service.

92
00:04:20,769 --> 00:04:25,130
Later, we can just as easily add a Fraud Detection Service,

93
00:04:25,130 --> 00:04:29,540
again, without making any changes to the existing services.

94
00:04:29,540 --> 00:04:33,080
And similarly, we can add another producer service

95
00:04:33,080 --> 00:04:36,800
that can integrate with other third-party services.

96
00:04:36,800 --> 00:04:40,400
For example, it can integrate with utility companies

97
00:04:40,400 --> 00:04:43,560
that charge our clients automatically for gas,

98
00:04:43,560 --> 00:04:45,630
electricity, or water.

99
00:04:45,630 --> 00:04:48,660
Or we can integrate with payroll services

100
00:04:48,660 --> 00:04:51,850
that perform direct deposits to the client's account

101
00:04:51,850 --> 00:04:54,670
on behalf of the client's employer.

102
00:04:54,670 --> 00:04:58,180
And adding this producer will also not require us

103
00:04:58,180 --> 00:05:00,603
to make any changes to our system.

104
00:05:01,730 --> 00:05:03,720
In addition to the horizontal

105
00:05:03,720 --> 00:05:05,780
and organizational scalability,

106
00:05:05,780 --> 00:05:08,060
Event-Driven Architecture allows us

107
00:05:08,060 --> 00:05:10,100
to analyze streams of data,

108
00:05:10,100 --> 00:05:14,240
detect patterns, and act upon them in real time.

109
00:05:14,240 --> 00:05:17,550
For example, the Fraud Detection Service can detect

110
00:05:17,550 --> 00:05:20,180
suspicious activity in the user's account

111
00:05:20,180 --> 00:05:22,190
by looking at the stream of events

112
00:05:22,190 --> 00:05:24,520
that happen in the account in real time

113
00:05:24,520 --> 00:05:27,950
without waiting for this data to be post-processed

114
00:05:27,950 --> 00:05:29,993
and stored in some database.

115
00:05:31,160 --> 00:05:34,540
For example, it can look at the recent transactions,

116
00:05:34,540 --> 00:05:36,900
and notice that in the last hour,

117
00:05:36,900 --> 00:05:40,460
two transactions happened in stores and restaurants

118
00:05:40,460 --> 00:05:42,270
in Los Angeles, California

119
00:05:42,270 --> 00:05:45,780
while one of the transactions happened in a remote location

120
00:05:45,780 --> 00:05:47,683
somewhere in a different state.

121
00:05:48,750 --> 00:05:51,690
The Fraud Detection Service can easily detect

122
00:05:51,690 --> 00:05:53,090
the suspicious activity,

123
00:05:53,090 --> 00:05:54,870
which is possibly a result

124
00:05:54,870 --> 00:05:57,630
of someone stealing our user's credit card

125
00:05:57,630 --> 00:06:01,073
or account information and trying to make a purchase.

126
00:06:01,970 --> 00:06:04,260
Similarly, by analyzing a stream

127
00:06:04,260 --> 00:06:06,210
of transactions in real time,

128
00:06:06,210 --> 00:06:09,910
it can detect that five different transactions were made

129
00:06:09,910 --> 00:06:11,940
within a short amount of time,

130
00:06:11,940 --> 00:06:15,463
which simply cannot correlate with real-human activity.

131
00:06:16,630 --> 00:06:20,580
This activity would be immediately flagged as fraudulent

132
00:06:20,580 --> 00:06:23,020
and would trigger the Fraud Detection Service

133
00:06:23,020 --> 00:06:25,440
to communicate with the account service,

134
00:06:25,440 --> 00:06:27,690
which would freeze the user's account.

135
00:06:27,690 --> 00:06:31,280
And it would also communicate with the notification service,

136
00:06:31,280 --> 00:06:34,323
which would alert the user about the situation.

137
00:06:35,410 --> 00:06:38,640
When we store all the events that happen in our system

138
00:06:38,640 --> 00:06:40,310
inside a message broker

139
00:06:40,310 --> 00:06:43,030
besides performing real-time analysis,

140
00:06:43,030 --> 00:06:45,460
we can also use this information

141
00:06:45,460 --> 00:06:49,000
to implement very powerful architectural patterns.

142
00:06:49,000 --> 00:06:50,920
One of those architectural patterns

143
00:06:50,920 --> 00:06:52,543
is called event sourcing.

144
00:06:53,500 --> 00:06:57,350
If we continue with the same example of the banking system

145
00:06:57,350 --> 00:07:00,340
and imagine what a hypothetical log of events

146
00:07:00,340 --> 00:07:02,760
for a particular user would look like,

147
00:07:02,760 --> 00:07:05,100
we can notice that, essentially,

148
00:07:05,100 --> 00:07:08,390
this event log represents all the transactions

149
00:07:08,390 --> 00:07:11,150
that happened in the user's account.

150
00:07:11,150 --> 00:07:13,860
And if we replay all those transactions

151
00:07:13,860 --> 00:07:15,430
from the beginning of time,

152
00:07:15,430 --> 00:07:17,890
we can arrive at the account balance

153
00:07:17,890 --> 00:07:21,253
that is currently stored in the account service database.

154
00:07:22,490 --> 00:07:25,320
So by using the event sourcing pattern

155
00:07:25,320 --> 00:07:28,860
instead of storing the current state in a database,

156
00:07:28,860 --> 00:07:31,290
we can simply store only the events,

157
00:07:31,290 --> 00:07:34,150
which can be replayed whenever we need to know

158
00:07:34,150 --> 00:07:35,420
the current state,

159
00:07:35,420 --> 00:07:38,560
and we eliminate the need for the database.

160
00:07:38,560 --> 00:07:40,860
Now, because events are immutable,

161
00:07:40,860 --> 00:07:42,550
we never modify them.

162
00:07:42,550 --> 00:07:46,253
And we simply append new events to the log as they come.

163
00:07:47,480 --> 00:07:49,000
By using this pattern,

164
00:07:49,000 --> 00:07:52,870
we can add another service that can generate a statement

165
00:07:52,870 --> 00:07:55,470
or look up any number of transactions

166
00:07:55,470 --> 00:07:57,640
that happened in our user's account

167
00:07:57,640 --> 00:08:01,233
simply by looking at the last N events in the log.

168
00:08:02,240 --> 00:08:05,490
And if our Fraud Detection Service decided

169
00:08:05,490 --> 00:08:08,360
that one of those transactions was not approved

170
00:08:08,360 --> 00:08:09,470
by the customer,

171
00:08:09,470 --> 00:08:12,640
it can easily be fixed by adding another event

172
00:08:12,640 --> 00:08:15,220
into the log that compensates for it,

173
00:08:15,220 --> 00:08:17,650
and all of that without the need to freeze

174
00:08:17,650 --> 00:08:21,970
the user's account or modifying records in a database.

175
00:08:21,970 --> 00:08:25,710
For example, if the user was charged $100

176
00:08:25,710 --> 00:08:29,110
by somebody who stole the user's credit card information,

177
00:08:29,110 --> 00:08:31,900
the Fraud Detection Service can simply credit

178
00:08:31,900 --> 00:08:35,010
the user's account with $100.

179
00:08:35,010 --> 00:08:37,830
And all of that information will be reflected

180
00:08:37,830 --> 00:08:40,053
in the user's statement immediately.

181
00:08:41,159 --> 00:08:43,200
Using the event sourcing pattern,

182
00:08:43,200 --> 00:08:45,170
we can choose to store those events

183
00:08:45,170 --> 00:08:47,150
for as long as we want

184
00:08:47,150 --> 00:08:50,850
and allow the user to look back at his transactions

185
00:08:50,850 --> 00:08:53,800
from 5 or even 10 years ago.

186
00:08:53,800 --> 00:08:56,890
But we can also make our querying faster

187
00:08:56,890 --> 00:08:58,750
by adding snapshot events,

188
00:08:58,750 --> 00:09:01,300
let's say, every month that summarizes

189
00:09:01,300 --> 00:09:04,433
everything that happened until that point of time.

190
00:09:05,380 --> 00:09:08,170
Another very powerful architectural pattern

191
00:09:08,170 --> 00:09:11,480
that we can implement using Event-Driven Architecture

192
00:09:11,480 --> 00:09:13,493
is called CQRS.

193
00:09:14,570 --> 00:09:19,523
CQRS stands for command query responsibility segregation.

194
00:09:20,450 --> 00:09:23,610
This pattern can solve two problems for us.

195
00:09:23,610 --> 00:09:26,710
The first problem is optimizing a database

196
00:09:26,710 --> 00:09:31,013
that has a high load of both Read and Update operations.

197
00:09:32,000 --> 00:09:34,900
When we have a database that has a high load

198
00:09:34,900 --> 00:09:37,550
of both Read and Update operations,

199
00:09:37,550 --> 00:09:40,080
concurrent operations to the same records

200
00:09:40,080 --> 00:09:42,570
or tables contend with each other,

201
00:09:42,570 --> 00:09:45,770
making all the operations very slow.

202
00:09:45,770 --> 00:09:48,880
Additionally, if we use a distributed database,

203
00:09:48,880 --> 00:09:53,420
generally, we can optimize it only for one type of operation

204
00:09:53,420 --> 00:09:55,880
at the expense of the other.

205
00:09:55,880 --> 00:09:59,260
For example, if we have a read-intensive workload,

206
00:09:59,260 --> 00:10:01,800
we can compromise on slower writes,

207
00:10:01,800 --> 00:10:04,510
or if we have our write-intensive workload,

208
00:10:04,510 --> 00:10:08,460
we can compromise on the performance of Read operations.

209
00:10:08,460 --> 00:10:10,200
However, in the case

210
00:10:10,200 --> 00:10:13,160
when both operations are equally frequent,

211
00:10:13,160 --> 00:10:14,383
we have a problem.

212
00:10:15,400 --> 00:10:18,450
The CQRS architectural pattern allows us

213
00:10:18,450 --> 00:10:21,400
to separate Update and Read operations

214
00:10:21,400 --> 00:10:23,100
into separate databases,

215
00:10:23,100 --> 00:10:26,510
sitting behind two separate services.

216
00:10:26,510 --> 00:10:27,490
In this case,

217
00:10:27,490 --> 00:10:30,760
service A would take all the Update operations

218
00:10:30,760 --> 00:10:33,770
and perform those updates in its own database,

219
00:10:33,770 --> 00:10:37,660
where it optimally stores the data for such updates.

220
00:10:37,660 --> 00:10:39,700
But additionally, every time

221
00:10:39,700 --> 00:10:41,860
an Update operation is performed,

222
00:10:41,860 --> 00:10:45,233
it also publishes an event into a message broker.

223
00:10:46,230 --> 00:10:50,450
Meanwhile, service B subscribes to those update events

224
00:10:50,450 --> 00:10:52,310
and applies all those changes

225
00:10:52,310 --> 00:10:55,083
in its own read optimized database.

226
00:10:56,010 --> 00:11:00,650
And now all the Read operations will only go to service B.

227
00:11:00,650 --> 00:11:04,010
Now both Update and Read operations can go

228
00:11:04,010 --> 00:11:05,770
to two separate services

229
00:11:05,770 --> 00:11:07,970
without interfering with each other.

230
00:11:07,970 --> 00:11:11,890
And the data in each service is stored in an optimized way

231
00:11:11,890 --> 00:11:13,853
for each type of operation.

232
00:11:14,810 --> 00:11:16,070
The second problem

233
00:11:16,070 --> 00:11:19,610
that CQRS architectural pattern helps us solve

234
00:11:19,610 --> 00:11:22,500
is joining multiple tables that are located

235
00:11:22,500 --> 00:11:24,810
in separate databases that belong

236
00:11:24,810 --> 00:11:26,423
to different microservices.

237
00:11:27,350 --> 00:11:30,150
Before we split our monolithic application

238
00:11:30,150 --> 00:11:33,680
into microservices, we had all the data tables

239
00:11:33,680 --> 00:11:36,570
inside one single database.

240
00:11:36,570 --> 00:11:38,720
If that was a relational database,

241
00:11:38,720 --> 00:11:42,380
we could easily and relatively quickly combine

242
00:11:42,380 --> 00:11:45,250
and analyze records from multiple tables

243
00:11:45,250 --> 00:11:48,283
by simply using the SQL Join operation.

244
00:11:49,340 --> 00:11:53,450
However, once we migrate to microservices architecture

245
00:11:53,450 --> 00:11:55,080
and follow the best practice

246
00:11:55,080 --> 00:11:58,350
of having a separate database for each microservice,

247
00:11:58,350 --> 00:12:01,560
those Join operations are a lot harder.

248
00:12:01,560 --> 00:12:05,050
Now we need to send a request to each service separately,

249
00:12:05,050 --> 00:12:06,550
which is a lot slower.

250
00:12:06,550 --> 00:12:10,540
And then we also need to combine this data programmatically

251
00:12:10,540 --> 00:12:12,920
because now we potentially have

252
00:12:12,920 --> 00:12:15,050
different types of databases,

253
00:12:15,050 --> 00:12:18,713
some of which may not even be relational databases.

254
00:12:19,940 --> 00:12:24,400
So CQRS solves exactly that problem.

255
00:12:24,400 --> 00:12:26,580
Now, every time there is a change

256
00:12:26,580 --> 00:12:31,160
in the data stored in service A or service B databases,

257
00:12:31,160 --> 00:12:35,470
those services would also publish those changes as events

258
00:12:35,470 --> 00:12:37,703
to which service C subscribes.

259
00:12:38,610 --> 00:12:41,170
Meanwhile, service C stores

260
00:12:41,170 --> 00:12:43,330
what's called a materialized view

261
00:12:43,330 --> 00:12:47,567
of the joined ready-to-query data from both service A

262
00:12:47,567 --> 00:12:51,483
and service B in its own read-only database.

263
00:12:52,490 --> 00:12:55,890
And now, whenever we need to get a join view,

264
00:12:55,890 --> 00:12:59,570
we don't need to send a request to two different services.

265
00:12:59,570 --> 00:13:03,170
Instead, we need to send a request to service C only,

266
00:13:03,170 --> 00:13:06,123
which already has the data ready for us.

267
00:13:07,380 --> 00:13:11,010
Now let's demonstrate both of those use cases

268
00:13:11,010 --> 00:13:14,890
for CQRS pattern in a real-life example.

269
00:13:14,890 --> 00:13:17,570
Let's assume that we have an online store

270
00:13:17,570 --> 00:13:20,450
where we have hundreds of thousands of products

271
00:13:20,450 --> 00:13:23,520
and we have millions of users who search for

272
00:13:23,520 --> 00:13:27,300
and leave reviews for those products daily.

273
00:13:27,300 --> 00:13:30,510
So if we use the microservices architecture,

274
00:13:30,510 --> 00:13:34,400
we would have the product service that has its own database.

275
00:13:34,400 --> 00:13:38,060
That database contains things like the product's name,

276
00:13:38,060 --> 00:13:39,550
their inventory count,

277
00:13:39,550 --> 00:13:42,730
description, price, and so on.

278
00:13:42,730 --> 00:13:45,460
We would also have the review service,

279
00:13:45,460 --> 00:13:47,970
which manages and stores all the reviews

280
00:13:47,970 --> 00:13:49,453
for all those products.

281
00:13:50,360 --> 00:13:52,160
So as we can imagine,

282
00:13:52,160 --> 00:13:55,340
reviews are coming to our system constantly

283
00:13:55,340 --> 00:13:59,540
so updates to the reviews database are very frequent.

284
00:13:59,540 --> 00:14:03,330
Similarly, our product's inventory keeps changing,

285
00:14:03,330 --> 00:14:05,210
as people purchase products

286
00:14:05,210 --> 00:14:08,513
or additional products are added and updated.

287
00:14:09,410 --> 00:14:11,610
But we also need to read

288
00:14:11,610 --> 00:14:14,120
and combine the product's information

289
00:14:14,120 --> 00:14:18,110
and their reviews information very quickly and frequently

290
00:14:18,110 --> 00:14:21,460
because users constantly search for products

291
00:14:21,460 --> 00:14:24,830
and want to see both their description and prices,

292
00:14:24,830 --> 00:14:27,080
which would come from the product service,

293
00:14:27,080 --> 00:14:30,590
and also see each product's reviews and rating,

294
00:14:30,590 --> 00:14:33,403
which we would need to get from the review service.

295
00:14:34,470 --> 00:14:36,120
So to solve this problem,

296
00:14:36,120 --> 00:14:40,080
we can use the CQRS architectural pattern.

297
00:14:40,080 --> 00:14:41,610
By using this pattern,

298
00:14:41,610 --> 00:14:44,020
we can add the product search service,

299
00:14:44,020 --> 00:14:46,120
which would have its own database.

300
00:14:46,120 --> 00:14:50,210
This database would store all the necessary combined data

301
00:14:50,210 --> 00:14:53,210
for each product and its reviews.

302
00:14:53,210 --> 00:14:57,670
And the service would also subscribe to updates to this data

303
00:14:57,670 --> 00:15:00,723
from the review service and the product service.

304
00:15:01,690 --> 00:15:06,150
So now a user who wants to search for a vacuum cleaner

305
00:15:06,150 --> 00:15:09,000
or maybe a pair of shoes can get a page

306
00:15:09,000 --> 00:15:12,140
with all the potential results very quickly

307
00:15:12,140 --> 00:15:16,560
by sending a request only to the product search service.

308
00:15:16,560 --> 00:15:19,270
And if the user wants to sort by

309
00:15:19,270 --> 00:15:22,590
or filter by the number of reviews or rating,

310
00:15:22,590 --> 00:15:25,743
it can also be done very easily and quickly.

311
00:15:26,760 --> 00:15:27,800
In this lecture,

312
00:15:27,800 --> 00:15:31,320
we learned about the Event-Driven Architecture.

313
00:15:31,320 --> 00:15:33,200
We learned about a few benefits

314
00:15:33,200 --> 00:15:36,130
that this type of architecture provides to us,

315
00:15:36,130 --> 00:15:37,890
especially when combined

316
00:15:37,890 --> 00:15:40,340
with the microservices architecture.

317
00:15:40,340 --> 00:15:44,100
A few of those benefits include decoupling microservices,

318
00:15:44,100 --> 00:15:46,480
which provides us with higher horizontal

319
00:15:46,480 --> 00:15:49,120
and organizational scalability.

320
00:15:49,120 --> 00:15:53,360
Additionally, Event-Driven Architecture allows us to analyze

321
00:15:53,360 --> 00:15:57,350
and respond to large streams of data in real time.

322
00:15:57,350 --> 00:16:00,150
We also learned about two very useful

323
00:16:00,150 --> 00:16:02,730
and powerful architectural patterns

324
00:16:02,730 --> 00:16:05,550
within Event-Driven Architecture.

325
00:16:05,550 --> 00:16:08,150
The first one was event sourcing,

326
00:16:08,150 --> 00:16:11,860
which allowed us to store and audit the current state

327
00:16:11,860 --> 00:16:16,050
of a business entity by only appending immutable events

328
00:16:16,050 --> 00:16:19,110
and replaying them when we need to.

329
00:16:19,110 --> 00:16:22,620
And the second architectural pattern was CQRS,

330
00:16:22,620 --> 00:16:25,480
which allowed us to optimize our database

331
00:16:25,480 --> 00:16:28,180
for both Update and Read operations

332
00:16:28,180 --> 00:16:30,610
by completely splitting the operations

333
00:16:30,610 --> 00:16:32,470
to different services.

334
00:16:32,470 --> 00:16:35,360
This pattern also allowed us to quickly

335
00:16:35,360 --> 00:16:37,690
and efficiently join data

336
00:16:37,690 --> 00:16:41,073
from completely different services and databases.

337
00:16:42,040 --> 00:16:44,090
I hope you learned a lot in this lecture,

338
00:16:44,090 --> 00:16:46,043
and I will see you all very soon.

