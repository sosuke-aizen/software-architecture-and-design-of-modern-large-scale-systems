1
00:00:00,000 --> 00:00:02,490
[Michael Pogrebinsky] Hey, welcome back.

2
00:00:02,490 --> 00:00:04,530
In this lecture, we're going to learn

3
00:00:04,530 --> 00:00:06,700
our first architectural pattern:

4
00:00:06,700 --> 00:00:09,200
The Multi-Tier Architecture.

5
00:00:09,200 --> 00:00:12,430
We will first get a general introduction to this pattern,

6
00:00:12,430 --> 00:00:14,970
and later, we will proceed to learn

7
00:00:14,970 --> 00:00:18,870
about some of the most common variations of this pattern.

8
00:00:18,870 --> 00:00:21,860
Most importantly, by the end of the lecture,

9
00:00:21,860 --> 00:00:25,070
we'll know when to apply this pattern to our system

10
00:00:25,070 --> 00:00:27,683
and when to seek other solutions.

11
00:00:28,590 --> 00:00:31,380
So let's start with a general introduction

12
00:00:31,380 --> 00:00:33,643
to the Multi-Tier architectural pattern.

13
00:00:34,590 --> 00:00:38,250
The Multi-Tier Architecture organizes our system

14
00:00:38,250 --> 00:00:41,263
into multiple physical and logical tiers.

15
00:00:42,470 --> 00:00:46,550
The logical separation limits the scope of responsibility

16
00:00:46,550 --> 00:00:47,770
on each tier,

17
00:00:47,770 --> 00:00:51,980
and the physical separation allows each tier to be deployed,

18
00:00:51,980 --> 00:00:56,133
upgraded, and scaled separately by different teams.

19
00:00:57,170 --> 00:01:00,760
It's important to point out that, although occasionally,

20
00:01:00,760 --> 00:01:03,000
you may hear people using the terms

21
00:01:03,000 --> 00:01:07,310
Multi-Tier and Multilayer architecture interchangeably,

22
00:01:07,310 --> 00:01:09,973
they are, in fact, two different concepts.

23
00:01:10,940 --> 00:01:14,300
Multi layered architecture usually refers

24
00:01:14,300 --> 00:01:18,230
to the internal separation inside a single application

25
00:01:18,230 --> 00:01:21,800
into multiple logical layers or modules.

26
00:01:21,800 --> 00:01:24,100
However, even if the application

27
00:01:24,100 --> 00:01:27,220
is logically separated into multiple layers,

28
00:01:27,220 --> 00:01:30,620
at runtime, it will run as a single unit

29
00:01:30,620 --> 00:01:33,193
and will be considered as a single tier.

30
00:01:34,110 --> 00:01:36,800
When we talk about Multi-Tier Architecture,

31
00:01:36,800 --> 00:01:39,740
we mean that the applications in each tier

32
00:01:39,740 --> 00:01:42,563
physically run on different infrastructure.

33
00:01:43,520 --> 00:01:47,410
Besides the benefits of logical and physical separation

34
00:01:47,410 --> 00:01:49,670
that allows us to develop, update

35
00:01:49,670 --> 00:01:52,360
and scale each tier independently,

36
00:01:52,360 --> 00:01:55,480
there are a few restrictions in this architectural pattern

37
00:01:55,480 --> 00:01:57,233
that simplify the design.

38
00:01:58,310 --> 00:02:02,280
The first restriction is that each pair of applications

39
00:02:02,280 --> 00:02:05,850
that belong to adjacent tiers communicate with each other

40
00:02:05,850 --> 00:02:08,120
using the Client-Server Model,

41
00:02:08,120 --> 00:02:11,960
which is a perfect model for RESTful APIs, for example.

42
00:02:11,960 --> 00:02:15,400
The second restriction discourages communication

43
00:02:15,400 --> 00:02:17,610
that skips through tiers.

44
00:02:17,610 --> 00:02:19,810
This restriction keeps the tiers

45
00:02:19,810 --> 00:02:21,670
loosely coupled with each other,

46
00:02:21,670 --> 00:02:25,460
which again, allows us to easily make changes to each tier

47
00:02:25,460 --> 00:02:27,893
without affecting the entire system.

48
00:02:28,840 --> 00:02:30,930
So now that we got an introduction

49
00:02:30,930 --> 00:02:33,380
to the Multi-Tier architectural pattern,

50
00:02:33,380 --> 00:02:35,650
let's talk about one of the most common

51
00:02:35,650 --> 00:02:38,160
Multi-Tier architectural variations,

52
00:02:38,160 --> 00:02:40,473
which is the Three-Tier Architecture.

53
00:02:41,340 --> 00:02:44,220
The Three-Tier Architecture, to this day,

54
00:02:44,220 --> 00:02:48,410
is one of the most common and popular architectural patterns

55
00:02:48,410 --> 00:02:51,583
for client-server, web-based services.

56
00:02:52,550 --> 00:02:54,530
In the Three-Tier Architecture,

57
00:02:54,530 --> 00:02:57,990
the top level tier contains the user interface,

58
00:02:57,990 --> 00:03:01,103
and is often referred to as the Presentation Tier.

59
00:03:02,170 --> 00:03:04,770
The responsibility of the Presentation Tier

60
00:03:04,770 --> 00:03:07,200
is to display information to the user

61
00:03:07,200 --> 00:03:09,570
and also take the user's input

62
00:03:09,570 --> 00:03:12,003
through a graphical user interface.

63
00:03:12,900 --> 00:03:15,810
Examples of the Presentation Tier include

64
00:03:15,810 --> 00:03:18,830
a webpage that runs on the client's browser,

65
00:03:18,830 --> 00:03:21,380
a mobile app that interacts with the user

66
00:03:21,380 --> 00:03:22,890
on a mobile device,

67
00:03:22,890 --> 00:03:25,543
or a desktop GUI application.

68
00:03:26,490 --> 00:03:30,070
The Presentation Tier normally does not contain

69
00:03:30,070 --> 00:03:33,120
any business logic for many reasons.

70
00:03:33,120 --> 00:03:36,900
One of those reasons, that applies specifically to code

71
00:03:36,900 --> 00:03:38,960
that runs in the client's browser,

72
00:03:38,960 --> 00:03:41,900
is that this code is directly accessible

73
00:03:41,900 --> 00:03:44,480
and visible to the user.

74
00:03:44,480 --> 00:03:47,592
Since, generally, we don't want the user to see

75
00:03:47,592 --> 00:03:50,860
and potentially change our business rules,

76
00:03:50,860 --> 00:03:55,240
it is an anti-pattern to include any business-specific logic

77
00:03:55,240 --> 00:03:57,393
in the Presentation Tier in general.

78
00:03:58,460 --> 00:04:02,050
The second, middle tier, is called the Application Tier

79
00:04:02,050 --> 00:04:05,460
or sometimes also referred to as the Business Tier

80
00:04:05,460 --> 00:04:06,803
or the Logic Tier.

81
00:04:07,750 --> 00:04:10,680
The Application Tier is the tier that provides

82
00:04:10,680 --> 00:04:12,900
all the functionality and features

83
00:04:12,900 --> 00:04:16,529
that we gathered from our functional requirements.

84
00:04:16,529 --> 00:04:19,800
This tier is responsible for processing the data

85
00:04:19,800 --> 00:04:21,930
that comes from the Presentation Tier

86
00:04:21,930 --> 00:04:25,053
and applying the relevant business logic to it.

87
00:04:26,130 --> 00:04:28,253
The last tier is the Data Tier.

88
00:04:29,180 --> 00:04:33,110
This tier is responsible for storage and persistence

89
00:04:33,110 --> 00:04:35,703
of user and business-specific data.

90
00:04:36,680 --> 00:04:38,720
This tier may include files

91
00:04:38,720 --> 00:04:41,480
on our file system, and most commonly,

92
00:04:41,480 --> 00:04:43,143
it includes a database.

93
00:04:44,200 --> 00:04:47,170
So, what makes the Three-Tier Architecture

94
00:04:47,170 --> 00:04:48,633
such a popular choice?

95
00:04:49,810 --> 00:04:53,060
One of the reasons that the Three-Tier Architecture

96
00:04:53,060 --> 00:04:55,760
is so popular is because it fits

97
00:04:55,760 --> 00:04:58,670
a large variety of use cases.

98
00:04:58,670 --> 00:05:02,210
Almost any web-based service fits this model,

99
00:05:02,210 --> 00:05:05,300
be it an online store, a news website,

100
00:05:05,300 --> 00:05:09,110
or even a video or audio streaming service.

101
00:05:09,110 --> 00:05:12,610
It's also pretty easy to scale horizontally

102
00:05:12,610 --> 00:05:15,870
to take large traffic and handle a lot of data.

103
00:05:15,870 --> 00:05:17,023
And let's see how.

104
00:05:18,150 --> 00:05:20,100
The Presentation Tier, of course,

105
00:05:20,100 --> 00:05:22,560
does not need any special scaling

106
00:05:22,560 --> 00:05:25,580
because it simply runs on the user's devices,

107
00:05:25,580 --> 00:05:28,143
so it essentially scales by itself.

108
00:05:29,190 --> 00:05:32,060
If we keep the Application Tier stateless,

109
00:05:32,060 --> 00:05:35,530
like we should when we use something like a REST API,

110
00:05:35,530 --> 00:05:39,370
we can then easily place the Application Tier instances

111
00:05:39,370 --> 00:05:43,823
behind a load balancer and run as many instances as we need.

112
00:05:44,850 --> 00:05:49,460
And finally, the database can also be scaled very easily

113
00:05:49,460 --> 00:05:52,810
if we use a well-established distributed database,

114
00:05:52,810 --> 00:05:55,200
which we can scale by using the techniques

115
00:05:55,200 --> 00:05:56,550
that we already learned,

116
00:05:56,550 --> 00:05:59,053
such as replication and partitioning.

117
00:06:00,070 --> 00:06:02,580
Finally, this architectural style

118
00:06:02,580 --> 00:06:05,670
is very easy to maintain and develop

119
00:06:05,670 --> 00:06:09,380
because all the logic is concentrated in one place,

120
00:06:09,380 --> 00:06:10,850
in the Application Tier,

121
00:06:10,850 --> 00:06:14,310
where all the back end development actually happens.

122
00:06:14,310 --> 00:06:17,260
And we don't need to worry about the integration

123
00:06:17,260 --> 00:06:21,023
of multiple code bases, services or projects.

124
00:06:22,090 --> 00:06:24,890
So, when does the Three-Tier Architecture

125
00:06:24,890 --> 00:06:27,250
stop working for us?

126
00:06:27,250 --> 00:06:31,510
Well, the Three-Tier Architecture has one major drawback,

127
00:06:31,510 --> 00:06:33,460
which is exactly the reason

128
00:06:33,460 --> 00:06:36,760
why it's so popular and easy to apply.

129
00:06:36,760 --> 00:06:41,550
This drawback is the monolithic structure of our logic tier.

130
00:06:41,550 --> 00:06:43,800
Since, as we mentioned earlier,

131
00:06:43,800 --> 00:06:46,380
we do not want to have any business logic

132
00:06:46,380 --> 00:06:48,910
in the Presentation Tier, and of course,

133
00:06:48,910 --> 00:06:52,138
we can't have any logic at all in the data tier,

134
00:06:52,138 --> 00:06:55,730
we are in a situation where all our business logic

135
00:06:55,730 --> 00:06:58,740
is concentrated in a single code base

136
00:06:58,740 --> 00:07:01,563
that runs as a single runtime unit.

137
00:07:02,700 --> 00:07:04,900
That has two implications.

138
00:07:04,900 --> 00:07:07,180
The first implication of this drawback

139
00:07:07,180 --> 00:07:10,100
is that each of our application instances

140
00:07:10,100 --> 00:07:12,570
simply becomes too CPU intensive

141
00:07:12,570 --> 00:07:15,103
and starts consuming too much memory.

142
00:07:16,140 --> 00:07:19,950
This makes our applications slower and less responsive,

143
00:07:19,950 --> 00:07:24,770
especially with memory-managed languages like Java or C#,

144
00:07:24,770 --> 00:07:27,520
which, as a result, will have much longer

145
00:07:27,520 --> 00:07:30,460
and more frequent garbage collections.

146
00:07:30,460 --> 00:07:33,060
All those issues may require us

147
00:07:33,060 --> 00:07:37,160
to start upgrading each computer we run our application on

148
00:07:37,160 --> 00:07:40,050
and we already know that vertical scaling

149
00:07:40,050 --> 00:07:42,453
is both expensive and limited.

150
00:07:43,700 --> 00:07:46,910
The second implication of the monolithic nature

151
00:07:46,910 --> 00:07:50,873
of the Application Tier is Low Development Velocity.

152
00:07:51,940 --> 00:07:55,400
As our code base becomes larger and more complex,

153
00:07:55,400 --> 00:08:00,400
it gets much harder to develop, maintain and reason about.

154
00:08:00,400 --> 00:08:04,500
And hiring more developers will not add too much value

155
00:08:04,500 --> 00:08:06,660
because more concurrent development

156
00:08:06,660 --> 00:08:10,763
will simply cause more merge conflicts and higher overhead.

157
00:08:11,940 --> 00:08:15,060
We could, somewhat, mitigate this problem

158
00:08:15,060 --> 00:08:18,170
by logically splitting the application's codebase

159
00:08:18,170 --> 00:08:20,070
into separate modules.

160
00:08:20,070 --> 00:08:22,850
However, those modules will still be

161
00:08:22,850 --> 00:08:25,000
somewhat tightly coupled

162
00:08:25,000 --> 00:08:28,010
because we can release new versions of those modules

163
00:08:28,010 --> 00:08:31,760
only when the entire application is upgraded.

164
00:08:31,760 --> 00:08:35,520
So, in other words, the organizational scalability

165
00:08:35,520 --> 00:08:38,763
of the Three-Tier Architecture is somewhat limited.

166
00:08:39,860 --> 00:08:43,460
So in conclusion, the Three-Tier Architecture

167
00:08:43,460 --> 00:08:45,940
is the perfect choice for companies

168
00:08:45,940 --> 00:08:50,160
whose code base is relatively small and not too complex,

169
00:08:50,160 --> 00:08:52,850
and also, it's maintained by a relatively

170
00:08:52,850 --> 00:08:55,410
small team of developers.

171
00:08:55,410 --> 00:08:58,760
This includes early-stage startup companies,

172
00:08:58,760 --> 00:09:01,330
as well as well-established companies

173
00:09:01,330 --> 00:09:04,240
that fit those criteria in terms of the size

174
00:09:04,240 --> 00:09:06,783
of the codebase and the organization.

175
00:09:07,820 --> 00:09:10,560
In addition to the Three-Tier Architecture,

176
00:09:10,560 --> 00:09:12,700
there are a few other variations

177
00:09:12,700 --> 00:09:15,210
to the Multi-Tier architectural pattern.

178
00:09:15,210 --> 00:09:17,403
So, let's take a look at a few of them.

179
00:09:18,510 --> 00:09:21,680
Besides the obvious One-Tier Architecture,

180
00:09:21,680 --> 00:09:24,370
which is simply a standalone application,

181
00:09:24,370 --> 00:09:26,920
we have the Two-Tier architectural pattern,

182
00:09:26,920 --> 00:09:30,210
which is maybe less common than the Three-Tier Architecture,

183
00:09:30,210 --> 00:09:32,033
but is still pretty popular.

184
00:09:33,240 --> 00:09:35,230
In the Two-Tier Architecture,

185
00:09:35,230 --> 00:09:37,320
the top-level tier includes

186
00:09:37,320 --> 00:09:40,470
both the presentation and the business logic

187
00:09:40,470 --> 00:09:43,210
and usually runs as a feature-rich mobile

188
00:09:43,210 --> 00:09:45,910
or desktop application.

189
00:09:45,910 --> 00:09:48,740
The second tier is the data tier, which,

190
00:09:48,740 --> 00:09:52,850
just like before, takes care of the storage and persistence

191
00:09:52,850 --> 00:09:55,133
of the users and business data.

192
00:09:56,160 --> 00:09:59,640
The Two-Tier Architecture eliminates the overhead

193
00:09:59,640 --> 00:10:03,790
of the middle Logic Tier and usually provides a faster

194
00:10:03,790 --> 00:10:07,060
and more native experience to the users.

195
00:10:07,060 --> 00:10:11,080
A few examples of the Two-Tier Architecture use cases

196
00:10:11,080 --> 00:10:15,110
include desktop or mobile versions of a document,

197
00:10:15,110 --> 00:10:19,280
image, or music editor that provides all the functionality

198
00:10:19,280 --> 00:10:22,580
and the graphical interface on the user's device,

199
00:10:22,580 --> 00:10:25,870
while all the storage and backup takes place

200
00:10:25,870 --> 00:10:28,570
on a remote server that belongs to the company

201
00:10:28,570 --> 00:10:30,383
that provides this application.

202
00:10:31,370 --> 00:10:33,420
On the other end of the spectrum,

203
00:10:33,420 --> 00:10:35,220
we can have a fourth tier

204
00:10:35,220 --> 00:10:38,410
in between the Presentation Tier and the Business Tier,

205
00:10:38,410 --> 00:10:40,910
which separates some of the functionality

206
00:10:40,910 --> 00:10:44,300
that doesn't belong in either of those tiers.

207
00:10:44,300 --> 00:10:48,140
For example, if we support multiple client applications

208
00:10:48,140 --> 00:10:52,190
in the Presentation Tier that have different APIs to us

209
00:10:52,190 --> 00:10:55,590
or have different data and performance requirements,

210
00:10:55,590 --> 00:10:59,410
we can introduce something like an API Gateway Tier

211
00:10:59,410 --> 00:11:04,110
that can take care of security, API, and format translation,

212
00:11:04,110 --> 00:11:05,333
as well as caching.

213
00:11:06,400 --> 00:11:09,960
Having more than four tiers is extremely rare

214
00:11:09,960 --> 00:11:14,520
because more tiers, normally, do not provide much value

215
00:11:14,520 --> 00:11:18,600
and simply adds additional performance overhead.

216
00:11:18,600 --> 00:11:21,300
This overhead comes from the restriction

217
00:11:21,300 --> 00:11:23,220
against bypassing tiers,

218
00:11:23,220 --> 00:11:27,120
which otherwise, would lead to tight coupling.

219
00:11:27,120 --> 00:11:29,820
So now, every request from the client

220
00:11:29,820 --> 00:11:32,630
has to pass through multiple services,

221
00:11:32,630 --> 00:11:36,030
which can increase the response time for the user.

222
00:11:36,030 --> 00:11:39,860
However, we rarely need to pass every request

223
00:11:39,860 --> 00:11:41,810
through so many services.

224
00:11:41,810 --> 00:11:43,740
So in the following lectures,

225
00:11:43,740 --> 00:11:46,670
we will look at other and better options

226
00:11:46,670 --> 00:11:48,500
of organizing our system

227
00:11:48,500 --> 00:11:52,083
if we need to split our code base into multiple components.

228
00:11:53,020 --> 00:11:53,853
In this lecture,

229
00:11:53,853 --> 00:11:56,660
we learned about the first architectural pattern,

230
00:11:56,660 --> 00:11:59,290
the Multi-Tier Architecture.

231
00:11:59,290 --> 00:12:01,810
We learned about its general structure,

232
00:12:01,810 --> 00:12:04,030
which splits our system architecture

233
00:12:04,030 --> 00:12:06,950
into multiple logical and physical tiers,

234
00:12:06,950 --> 00:12:09,660
which allows us to develop, upgrade,

235
00:12:09,660 --> 00:12:13,630
and scale each tier completely independently.

236
00:12:13,630 --> 00:12:16,850
After that, we talked about the most common variation

237
00:12:16,850 --> 00:12:18,710
of the Multi-Tier Architecture,

238
00:12:18,710 --> 00:12:21,400
which is the Three-Tier Architecture.

239
00:12:21,400 --> 00:12:24,710
More importantly, we talked about the use cases,

240
00:12:24,710 --> 00:12:27,760
the scale, and the complexity of the code base

241
00:12:27,760 --> 00:12:30,800
that best fits this architectural model,

242
00:12:30,800 --> 00:12:33,903
and when we should consider other alternatives.

243
00:12:35,010 --> 00:12:38,710
And finally, we concluded with a few other variations

244
00:12:38,710 --> 00:12:40,740
of the Multi-Tier Architecture,

245
00:12:40,740 --> 00:12:44,510
the Two-Tier Architecture and the Four-Tier architectures,

246
00:12:44,510 --> 00:12:46,583
which are also very common.

247
00:12:47,690 --> 00:12:49,623
I'll see you soon in the next lecture.

