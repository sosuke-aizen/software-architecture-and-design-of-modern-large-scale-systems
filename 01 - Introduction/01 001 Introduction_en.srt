1
00:00:00,270 --> 00:00:02,850
Welcome to the first lecture.

2
00:00:02,850 --> 00:00:05,670
In this lecture, we're going to get an introduction

3
00:00:05,670 --> 00:00:07,280
to software architecture

4
00:00:07,280 --> 00:00:11,550
and also learn why software architecture is so important.

5
00:00:11,550 --> 00:00:14,280
After that we will define more formally

6
00:00:14,280 --> 00:00:17,330
and explain what software architecture means for us

7
00:00:17,330 --> 00:00:19,670
in the context of this course.

8
00:00:19,670 --> 00:00:23,570
And finally, we'll discuss where software architecture fits

9
00:00:23,570 --> 00:00:27,293
in the overall picture of the software development cycle.

10
00:00:28,250 --> 00:00:30,530
So let's start with some analogies

11
00:00:30,530 --> 00:00:32,590
from outside the software world

12
00:00:32,590 --> 00:00:34,690
which will give us a good intuition

13
00:00:34,690 --> 00:00:37,593
of what software architecture means to us.

14
00:00:38,530 --> 00:00:40,920
Everything we build has a structure,

15
00:00:40,920 --> 00:00:43,330
whether we know about it or not,

16
00:00:43,330 --> 00:00:45,860
and whether we thought about it ahead of time

17
00:00:45,860 --> 00:00:48,900
or arrived at it spontaneously.

18
00:00:48,900 --> 00:00:51,420
The more we invest in building a product,

19
00:00:51,420 --> 00:00:54,220
the harder it becomes to change its structure

20
00:00:54,220 --> 00:00:55,530
after the fact.

21
00:00:55,530 --> 00:00:58,130
But what is the importance of a structure,

22
00:00:58,130 --> 00:01:02,070
and why would we want to change it at all in the future?

23
00:01:02,070 --> 00:01:05,519
Well, the thing is the structure of our system

24
00:01:05,519 --> 00:01:09,763
describes both the intent of our product and its qualities.

25
00:01:10,840 --> 00:01:14,460
For example, if we look at the architecture of a theater

26
00:01:14,460 --> 00:01:18,980
we see that its intent is to have shows and performances,

27
00:01:18,980 --> 00:01:22,040
but if we make people live or work in a theater

28
00:01:22,040 --> 00:01:24,990
instead of in a suitable house or an office

29
00:01:24,990 --> 00:01:27,580
they would find themselves very uncomfortable,

30
00:01:27,580 --> 00:01:29,743
and would have a terrible experience.

31
00:01:30,600 --> 00:01:33,330
On the other hand, if we look at the architecture

32
00:01:33,330 --> 00:01:36,900
of a residential home, we see that it would be perfect

33
00:01:36,900 --> 00:01:38,450
for people to live there,

34
00:01:38,450 --> 00:01:41,260
but hosting big shows or performances

35
00:01:41,260 --> 00:01:42,963
would not work very well.

36
00:01:43,960 --> 00:01:48,640
Now when it comes to software, the same principles apply.

37
00:01:48,640 --> 00:01:51,440
There's almost an infinite number of ways

38
00:01:51,440 --> 00:01:53,350
for us to organize our code

39
00:01:53,350 --> 00:01:56,020
to achieve the functionality of the system,

40
00:01:56,020 --> 00:01:57,860
but different organizations

41
00:01:57,860 --> 00:02:00,340
will give us different properties.

42
00:02:00,340 --> 00:02:03,360
In other words, the software architecture

43
00:02:03,360 --> 00:02:05,830
impacts how our product will perform

44
00:02:05,830 --> 00:02:10,150
and scale how easy it will be for us to add new features

45
00:02:10,150 --> 00:02:12,160
and grow our engineering team,

46
00:02:12,160 --> 00:02:15,750
and also how well it's going to respond to failures

47
00:02:15,750 --> 00:02:17,600
or security attacks.

48
00:02:17,600 --> 00:02:20,280
And similarly to physical structures,

49
00:02:20,280 --> 00:02:23,930
if we organize our software in a suboptimal way

50
00:02:23,930 --> 00:02:26,690
the cost of a redesign will be significant,

51
00:02:26,690 --> 00:02:29,310
both in terms of time and money,

52
00:02:29,310 --> 00:02:33,283
especially so when we're working on a large scale system.

53
00:02:34,300 --> 00:02:36,960
So now that we have some basic intuition

54
00:02:36,960 --> 00:02:39,450
and motivation for software architecture

55
00:02:39,450 --> 00:02:41,690
let's define it a bit more formally

56
00:02:41,690 --> 00:02:44,080
so we know exactly what we're talking about

57
00:02:44,080 --> 00:02:45,473
when we use this term.

58
00:02:46,720 --> 00:02:50,520
Now, there are many ways define software architecture,

59
00:02:50,520 --> 00:02:53,300
and for many years people have been arguing

60
00:02:53,300 --> 00:02:55,700
about the best way to define it.

61
00:02:55,700 --> 00:02:59,140
So in our course, the definition we're going to use

62
00:02:59,140 --> 00:03:00,273
is as follows,

63
00:03:01,370 --> 00:03:03,940
the software architecture of a system

64
00:03:03,940 --> 00:03:07,480
is a high level description of the system structure,

65
00:03:07,480 --> 00:03:09,110
it's different components,

66
00:03:09,110 --> 00:03:12,210
and how those components communicate with each other

67
00:03:12,210 --> 00:03:15,563
to fulfill the system's requirements and constraints.

68
00:03:16,630 --> 00:03:18,970
It's a heavily loaded definition,

69
00:03:18,970 --> 00:03:21,573
so let's unpack it piece by piece.

70
00:03:22,690 --> 00:03:24,580
The first part of the definition

71
00:03:24,580 --> 00:03:27,150
states that the software architecture

72
00:03:27,150 --> 00:03:30,260
is a high level description of the system.

73
00:03:30,260 --> 00:03:32,750
It means that it's an abstraction

74
00:03:32,750 --> 00:03:34,950
that shows us the important components

75
00:03:34,950 --> 00:03:37,800
that help us reason about the system

76
00:03:37,800 --> 00:03:41,750
while hiding the implementation details out of the view.

77
00:03:41,750 --> 00:03:45,040
This implies that things like technologies

78
00:03:45,040 --> 00:03:48,810
or programming languages we use to implement the system

79
00:03:48,810 --> 00:03:51,580
are not part of the software architecture,

80
00:03:51,580 --> 00:03:54,970
and are part of the implementation instead.

81
00:03:54,970 --> 00:03:58,340
This is an important point because many engineers

82
00:03:58,340 --> 00:04:01,420
falsely assume that software architecture

83
00:04:01,420 --> 00:04:05,550
is just about picking the right technologies or frameworks.

84
00:04:05,550 --> 00:04:08,280
This could not be further from the truth,

85
00:04:08,280 --> 00:04:11,640
and in fact, we want to delay making these choices

86
00:04:11,640 --> 00:04:14,233
until the very end of our design.

87
00:04:15,250 --> 00:04:17,100
The second part of the definition

88
00:04:17,100 --> 00:04:18,980
talks about the different components

89
00:04:18,980 --> 00:04:21,720
and how they communicate with each other.

90
00:04:21,720 --> 00:04:23,960
The components that we're talking about

91
00:04:23,960 --> 00:04:26,100
when we talk about software architecture

92
00:04:26,100 --> 00:04:28,160
are black box elements

93
00:04:28,160 --> 00:04:31,840
that are defined by their behavior and APIs.

94
00:04:31,840 --> 00:04:33,100
As a matter of fact,

95
00:04:33,100 --> 00:04:36,870
those components may themselves be complex systems

96
00:04:36,870 --> 00:04:38,120
that are described

97
00:04:38,120 --> 00:04:41,260
through their own software architectural diagrams.

98
00:04:41,260 --> 00:04:45,623
So in a sense, this definition may be recursive when needed.

99
00:04:46,640 --> 00:04:49,600
Finally, the last part of the definition

100
00:04:49,600 --> 00:04:52,370
talks about fulfilling the system's requirements

101
00:04:52,370 --> 00:04:53,540
and constraints,

102
00:04:53,540 --> 00:04:56,220
which means that the software architecture

103
00:04:56,220 --> 00:04:59,950
should describe how all those components are coming together

104
00:04:59,950 --> 00:05:01,780
to do what the system must do,

105
00:05:01,780 --> 00:05:03,950
which is basically our requirements,

106
00:05:03,950 --> 00:05:07,420
and how the system does not do what it shouldn't do,

107
00:05:07,420 --> 00:05:10,133
which is described in the system constraints.

108
00:05:11,130 --> 00:05:13,410
We're going to talk about all those components

109
00:05:13,410 --> 00:05:16,000
in great detail throughout the course,

110
00:05:16,000 --> 00:05:18,610
but I think having this definition upfront

111
00:05:18,610 --> 00:05:20,300
is going to set the stage

112
00:05:20,300 --> 00:05:23,253
for what we're going to learn in the following lectures.

113
00:05:24,400 --> 00:05:27,040
Now when it comes software development,

114
00:05:27,040 --> 00:05:29,340
we can talk about software architecture

115
00:05:29,340 --> 00:05:31,930
on many different levels of abstraction,

116
00:05:31,930 --> 00:05:34,920
starting from the lowest level abstractions

117
00:05:34,920 --> 00:05:37,526
like different classes or structs,

118
00:05:37,526 --> 00:05:40,580
depending on the programming language, and the organization,

119
00:05:40,580 --> 00:05:44,820
and communication between objects inside a program.

120
00:05:44,820 --> 00:05:47,390
We can also go one level up

121
00:05:47,390 --> 00:05:51,140
and talk about modules, packages, or libraries,

122
00:05:51,140 --> 00:05:53,433
and how they interact with each other.

123
00:05:54,270 --> 00:05:56,040
But since in this course

124
00:05:56,040 --> 00:05:59,250
we're going to be focusing on large scale systems,

125
00:05:59,250 --> 00:06:02,320
we're going to talk about a higher level abstraction

126
00:06:02,320 --> 00:06:05,830
where the individual components are separate services

127
00:06:05,830 --> 00:06:10,080
that run as individual processes or groups of processes

128
00:06:10,080 --> 00:06:12,433
potentially on different computers.

129
00:06:13,880 --> 00:06:17,190
It turns out that taking this more distributed

130
00:06:17,190 --> 00:06:21,380
multi-service approach allows us to architect systems

131
00:06:21,380 --> 00:06:24,850
that can handle large amounts of requests, process,

132
00:06:24,850 --> 00:06:27,730
and store very large amounts of data,

133
00:06:27,730 --> 00:06:31,150
and serve thousands, hundreds of thousands,

134
00:06:31,150 --> 00:06:34,023
or even millions of users every day.

135
00:06:35,160 --> 00:06:37,630
A few examples of such systems

136
00:06:37,630 --> 00:06:41,560
include online software services such as ride-sharing,

137
00:06:41,560 --> 00:06:45,800
video-on-demand, social media, online video games,

138
00:06:45,800 --> 00:06:49,943
investing services and banks, and many, many others.

139
00:06:51,310 --> 00:06:53,860
When we set our goal to build a product

140
00:06:53,860 --> 00:06:55,860
that operates on such a scale,

141
00:06:55,860 --> 00:06:58,120
getting the architecture just right

142
00:06:58,120 --> 00:07:00,490
can mean going from a small start-up

143
00:07:00,490 --> 00:07:02,800
to a multi-billion dollar company

144
00:07:02,800 --> 00:07:05,910
and making a positive impact on millions of people

145
00:07:05,910 --> 00:07:07,143
all around the world.

146
00:07:08,290 --> 00:07:09,490
On the flip side,

147
00:07:09,490 --> 00:07:12,750
if we don't do a good job at the design phase

148
00:07:12,750 --> 00:07:16,370
we can potentially waste months of engineering time

149
00:07:16,370 --> 00:07:19,960
building a system that doesn't meet our requirements

150
00:07:19,960 --> 00:07:22,610
and that nobody wants to use.

151
00:07:22,610 --> 00:07:24,370
And restructuring a system

152
00:07:24,370 --> 00:07:26,570
that was not architected correctly

153
00:07:26,570 --> 00:07:29,250
is very hard and expensive.

154
00:07:29,250 --> 00:07:32,440
So as we can see, the stakes here are high,

155
00:07:32,440 --> 00:07:35,090
which makes what we're going to learn in this course

156
00:07:35,090 --> 00:07:36,403
super important.

157
00:07:37,520 --> 00:07:39,770
Now before we conclude this lecture,

158
00:07:39,770 --> 00:07:41,870
I want to talk about one last thing

159
00:07:41,870 --> 00:07:44,870
which is the place where software architecture

160
00:07:44,870 --> 00:07:47,630
fits in the overall picture.

161
00:07:47,630 --> 00:07:50,950
Software development can roughly be described

162
00:07:50,950 --> 00:07:52,430
in four phases,

163
00:07:52,430 --> 00:07:56,333
design, implementation, testing, and deployment.

164
00:07:57,200 --> 00:07:59,520
Since generally software products

165
00:07:59,520 --> 00:08:02,340
keep evolving over a long period of time,

166
00:08:02,340 --> 00:08:05,890
those four phases can be repeated many times,

167
00:08:05,890 --> 00:08:10,040
where arguably the first iteration is the most critical,

168
00:08:10,040 --> 00:08:13,520
and subsequent iterations make incremental changes

169
00:08:13,520 --> 00:08:15,420
to the existing system.

170
00:08:15,420 --> 00:08:18,130
Now something that I already alluded to

171
00:08:18,130 --> 00:08:21,830
but didn't state formally is that software architecture

172
00:08:21,830 --> 00:08:24,227
is the output of the design phase phase,

173
00:08:24,227 --> 00:08:27,340
and the input to the implementation phase.

174
00:08:27,340 --> 00:08:29,630
In this course, we're going to focus

175
00:08:29,630 --> 00:08:32,070
on arguably the most important step,

176
00:08:32,070 --> 00:08:34,070
which is the design phase.

177
00:08:34,070 --> 00:08:37,110
The design phase is essentially a process

178
00:08:37,110 --> 00:08:40,150
of defining the software architecture of the system

179
00:08:40,150 --> 00:08:44,059
that an entire team or even multiple teams of engineers

180
00:08:44,059 --> 00:08:46,040
later proceed to implement,

181
00:08:46,040 --> 00:08:50,003
sometimes over a course of multiple weeks or months.

182
00:08:50,980 --> 00:08:53,090
Now there are many challenges

183
00:08:53,090 --> 00:08:56,620
of defining a good software architecture for our system

184
00:08:56,620 --> 00:08:59,290
but the biggest challenge that software engineers

185
00:08:59,290 --> 00:09:00,970
struggle with the most

186
00:09:00,970 --> 00:09:04,550
is the fact that unlike an algorithm or a formula

187
00:09:04,550 --> 00:09:08,210
that can be proven to be both correct and optimal,

188
00:09:08,210 --> 00:09:11,680
we can't do the same for software architecture.

189
00:09:11,680 --> 00:09:13,800
So to guarantee our success,

190
00:09:13,800 --> 00:09:18,030
what we can do is follow a methodical design process

191
00:09:18,030 --> 00:09:22,250
as well as apply industry proven architectural patterns

192
00:09:22,250 --> 00:09:23,660
and best practices,

193
00:09:23,660 --> 00:09:26,510
which is what we're going to learn throughout the course.

194
00:09:27,410 --> 00:09:30,040
But before we proceed to the first topic,

195
00:09:30,040 --> 00:09:32,883
let's quickly summarize what we learned in this lecture.

196
00:09:33,840 --> 00:09:37,640
In this lecture, we got the intuition and motivation

197
00:09:37,640 --> 00:09:39,700
for software architecture.

198
00:09:39,700 --> 00:09:43,610
We learned that every software system has an architecture

199
00:09:43,610 --> 00:09:45,500
which is basically its structure,

200
00:09:45,500 --> 00:09:50,290
and its structure is absolutely critical for its success.

201
00:09:50,290 --> 00:09:53,840
We later define software architecture more formally

202
00:09:53,840 --> 00:09:57,300
as a high level description of the system structure,

203
00:09:57,300 --> 00:09:58,900
it's different components,

204
00:09:58,900 --> 00:10:02,100
and how those components communicate with each other

205
00:10:02,100 --> 00:10:06,030
to fulfill the system's requirements and constraints.

206
00:10:06,030 --> 00:10:09,460
And we concluded with placing software architecture

207
00:10:09,460 --> 00:10:12,190
in the overall software development cycle

208
00:10:12,190 --> 00:10:14,547
as the output of the design phase

209
00:10:14,547 --> 00:10:17,930
and the input to our systems implementation.

210
00:10:17,930 --> 00:10:20,620
So now that we got a solid introduction

211
00:10:20,620 --> 00:10:22,040
to what we're going to learn,

212
00:10:22,040 --> 00:10:24,410
let's go ahead and start learning

213
00:10:24,410 --> 00:10:26,433
the first topic of the course.

