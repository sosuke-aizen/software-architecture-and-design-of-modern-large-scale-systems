1
00:00:00,340 --> 00:00:02,850
Welcome to another lecture.

2
00:00:02,850 --> 00:00:05,080
In this lecture we're going to learn about

3
00:00:05,080 --> 00:00:06,980
the first type of API

4
00:00:06,980 --> 00:00:10,410
which is commonly referred to as Remote Procedure Calls

5
00:00:10,410 --> 00:00:12,480
or RPC in short.

6
00:00:12,480 --> 00:00:15,600
We're first going to learn what it is and how it works,

7
00:00:15,600 --> 00:00:19,130
and later we'll talk about its benefits, its drawbacks

8
00:00:19,130 --> 00:00:22,483
and when to prefer this style of API over the others.

9
00:00:23,460 --> 00:00:25,343
So what is an RPC?

10
00:00:26,490 --> 00:00:28,820
A Remote Procedure Call is the ability

11
00:00:28,820 --> 00:00:32,210
of a client application to execute a sub routine

12
00:00:32,210 --> 00:00:33,503
on a remote server.

13
00:00:34,700 --> 00:00:37,660
However, what's unique about this type of API

14
00:00:37,660 --> 00:00:40,060
is that this remote method invocation

15
00:00:40,060 --> 00:00:43,610
looks and feels like calling a normal local method,

16
00:00:43,610 --> 00:00:47,180
in terms of the code that the developer needs to write.

17
00:00:47,180 --> 00:00:51,270
This unique feature of an RPC is also commonly referred to

18
00:00:51,270 --> 00:00:52,950
as local transparency.

19
00:00:52,950 --> 00:00:54,750
As in the eyes of the developer

20
00:00:54,750 --> 00:00:58,230
of the client application, a method executed locally

21
00:00:58,230 --> 00:01:01,113
or remotely look almost exactly the same.

22
00:01:02,030 --> 00:01:04,950
Another feature that usually comes with most

23
00:01:04,950 --> 00:01:08,053
but not all RPC framework implementations is support

24
00:01:08,053 --> 00:01:10,650
for multiple programming languages,

25
00:01:10,650 --> 00:01:12,020
so that applications

26
00:01:12,020 --> 00:01:13,990
written in different programming languages

27
00:01:13,990 --> 00:01:16,793
can seemingly talk to each other using RPC.

28
00:01:17,810 --> 00:01:20,890
The way an RPC works is as follows,

29
00:01:20,890 --> 00:01:23,450
the API as well as the data types

30
00:01:23,450 --> 00:01:25,430
that are used in the API methods

31
00:01:25,430 --> 00:01:29,090
are declared using a special interface description language,

32
00:01:29,090 --> 00:01:30,600
which varies between different

33
00:01:30,600 --> 00:01:33,010
RPC framework implementations.

34
00:01:33,010 --> 00:01:35,730
This is essentially a schema definition

35
00:01:35,730 --> 00:01:39,240
of the communication between a remote client and the server

36
00:01:39,240 --> 00:01:41,360
which is part of our system.

37
00:01:41,360 --> 00:01:45,020
The value of defining the interface and the data types

38
00:01:45,020 --> 00:01:46,730
using the specialized language

39
00:01:46,730 --> 00:01:49,280
is once we have this interface definition,

40
00:01:49,280 --> 00:01:52,970
we can use a special compiler or a code generation tool

41
00:01:52,970 --> 00:01:55,040
which is part of the RPC framework

42
00:01:55,040 --> 00:01:59,180
to generate two separate implementations of the API method.

43
00:01:59,180 --> 00:02:01,030
One for the server application,

44
00:02:01,030 --> 00:02:02,863
and one for the client application.

45
00:02:04,200 --> 00:02:07,600
The server side implementation of the RPC method

46
00:02:07,600 --> 00:02:09,320
is called the server stub,

47
00:02:09,320 --> 00:02:11,380
and the auto generated implementation

48
00:02:11,380 --> 00:02:14,840
on the client application side is called the client stub.

49
00:02:14,840 --> 00:02:18,490
Those stubs take care of all the implementation details

50
00:02:18,490 --> 00:02:20,633
of the remote procedure invocation.

51
00:02:21,590 --> 00:02:24,370
Additionally, all the custom object types

52
00:02:24,370 --> 00:02:27,610
that we declare using the interface description language

53
00:02:27,610 --> 00:02:30,340
are compiled into classes or structs,

54
00:02:30,340 --> 00:02:32,660
depending on the programming language.

55
00:02:32,660 --> 00:02:35,170
Those auto generated object types,

56
00:02:35,170 --> 00:02:36,840
are also commonly referred to

57
00:02:36,840 --> 00:02:40,493
as Data Transfer Objects or DTOs in short.

58
00:02:41,570 --> 00:02:45,160
Now at run time, whenever the client application calls

59
00:02:45,160 --> 00:02:48,750
that particular RPC method with some set of parameters

60
00:02:48,750 --> 00:02:52,250
the client stub takes care of then coding of the data

61
00:02:52,250 --> 00:02:54,040
which is also commonly referred to

62
00:02:54,040 --> 00:02:56,830
as serialization or marshalling.

63
00:02:56,830 --> 00:02:59,700
After then coding it initiates the connection

64
00:02:59,700 --> 00:03:01,640
to the remote server application

65
00:03:01,640 --> 00:03:05,410
and sends the data over to the remote server stub.

66
00:03:05,410 --> 00:03:07,580
On the other end, the server stub

67
00:03:07,580 --> 00:03:10,480
for that particular RPC method is listening

68
00:03:10,480 --> 00:03:12,840
to the client applications messages,

69
00:03:12,840 --> 00:03:16,390
and when a message is received, the data is deserialized

70
00:03:16,390 --> 00:03:19,250
and then the real implementation of the method

71
00:03:19,250 --> 00:03:21,653
is invoked on the server application.

72
00:03:22,560 --> 00:03:24,970
Once the server completes the operation

73
00:03:24,970 --> 00:03:28,040
the result is then passed back to the server stub

74
00:03:28,040 --> 00:03:30,600
which in turn serializes the response

75
00:03:30,600 --> 00:03:34,010
and sends it back over to the client application.

76
00:03:34,010 --> 00:03:37,520
Finally, the client stub for that particular method

77
00:03:37,520 --> 00:03:41,050
receives the encoded response, deserializes it

78
00:03:41,050 --> 00:03:44,050
and gives it back to the caller as the return value

79
00:03:44,050 --> 00:03:46,933
for what looks like a local method invocation.

80
00:03:48,090 --> 00:03:51,910
This concept of implementing an API using an RPC

81
00:03:51,910 --> 00:03:53,710
has been around for decades

82
00:03:53,710 --> 00:03:57,500
and the only thing that changes over time are the frameworks

83
00:03:57,500 --> 00:04:00,773
the details of their implementation and their efficiency.

84
00:04:01,820 --> 00:04:04,350
So our job as API developers

85
00:04:04,350 --> 00:04:06,360
is to pick an appropriate framework

86
00:04:06,360 --> 00:04:09,790
define the API as well as the relevant data types

87
00:04:09,790 --> 00:04:12,717
using the frameworks interface description language

88
00:04:12,717 --> 00:04:14,313
and publish that description.

89
00:04:15,370 --> 00:04:18,700
At this point the client which uses our system

90
00:04:18,700 --> 00:04:21,290
and the server which is part of our system

91
00:04:21,290 --> 00:04:23,050
are completely decoupled.

92
00:04:23,050 --> 00:04:26,450
When we finish designing and implementing our system

93
00:04:26,450 --> 00:04:29,290
we can generate that stub for the server,

94
00:04:29,290 --> 00:04:32,060
and when a new client wants to integrate with us

95
00:04:32,060 --> 00:04:35,550
and make API calls to us, all they have to do

96
00:04:35,550 --> 00:04:38,570
is use the publicly available frameworks tools

97
00:04:38,570 --> 00:04:40,420
to generate their client's stub

98
00:04:40,420 --> 00:04:43,583
based on the API definition that we published earlier.

99
00:04:44,690 --> 00:04:47,490
Furthermore, if we pick an RPC framework

100
00:04:47,490 --> 00:04:49,870
that supports multiple programming languages

101
00:04:49,870 --> 00:04:52,090
then we don't limit ourselves

102
00:04:52,090 --> 00:04:55,500
to the choice of programming language for the server side.

103
00:04:55,500 --> 00:04:56,980
And we allow the client

104
00:04:56,980 --> 00:04:59,290
to pick their favorite programming language,

105
00:04:59,290 --> 00:05:01,143
to communicate with our system.

106
00:05:02,290 --> 00:05:04,520
Now, let's talk about the benefits

107
00:05:04,520 --> 00:05:08,113
and some of the drawbacks of picking this style of API.

108
00:05:09,040 --> 00:05:11,850
The first benefit is of course the convenience,

109
00:05:11,850 --> 00:05:15,630
we provide to the developers of the client applications.

110
00:05:15,630 --> 00:05:17,380
Once they generate their stub,

111
00:05:17,380 --> 00:05:19,900
they can communicate with our system easily

112
00:05:19,900 --> 00:05:22,190
by simply calling methods on objects

113
00:05:22,190 --> 00:05:24,680
which look and feel exactly

114
00:05:24,680 --> 00:05:27,550
like calling normal local methods.

115
00:05:27,550 --> 00:05:31,330
And all the details of how the communication is established

116
00:05:31,330 --> 00:05:34,860
or how the data is passed between the client to the server

117
00:05:34,860 --> 00:05:38,660
are entirely abstracted away from the developers.

118
00:05:38,660 --> 00:05:42,300
Also any failures in the communication with the server

119
00:05:42,300 --> 00:05:45,460
simply result in either an error or exception,

120
00:05:45,460 --> 00:05:47,340
depending on the programming language

121
00:05:47,340 --> 00:05:50,113
just like in the case of any normal method.

122
00:05:51,100 --> 00:05:54,053
Now let's talk about the drawbacks of RPC.

123
00:05:55,230 --> 00:05:58,310
The main drawback of using an RPC style

124
00:05:58,310 --> 00:06:02,000
is unlike local methods executed on the client side,

125
00:06:02,000 --> 00:06:04,970
those remote methods are a lot slower

126
00:06:04,970 --> 00:06:06,823
and a lot less reliable.

127
00:06:07,740 --> 00:06:11,030
The slowness may introduce surprising performance

128
00:06:11,030 --> 00:06:13,870
bottlenecks because the client never knows

129
00:06:13,870 --> 00:06:16,380
how long those remote method in vocations,

130
00:06:16,380 --> 00:06:18,320
can actually we take.

131
00:06:18,320 --> 00:06:20,930
However, on the surface code wise

132
00:06:20,930 --> 00:06:23,530
they look very much like local methods

133
00:06:23,530 --> 00:06:25,320
which are generally fast.

134
00:06:25,320 --> 00:06:27,380
So as API designers,

135
00:06:27,380 --> 00:06:30,160
we need to help the client application developers

136
00:06:30,160 --> 00:06:32,400
avoid blocking their code execution

137
00:06:32,400 --> 00:06:35,990
when we introduce methods that we know are going to be slow

138
00:06:35,990 --> 00:06:37,770
on our end.

139
00:06:37,770 --> 00:06:38,940
The way to do it

140
00:06:38,940 --> 00:06:41,570
is by introducing asynchronous versions

141
00:06:41,570 --> 00:06:44,930
for those slow methods, which is not surprisingly

142
00:06:44,930 --> 00:06:48,060
is one of the best practices for those situations

143
00:06:48,060 --> 00:06:50,810
that we learned in the previous lecture.

144
00:06:50,810 --> 00:06:54,860
Now, the unreliability stems from the fact that the client

145
00:06:54,860 --> 00:06:57,610
is located remotely running on a computer

146
00:06:57,610 --> 00:07:00,180
that potentially belongs to a different company

147
00:07:00,180 --> 00:07:02,490
and is using the network to communicate

148
00:07:02,490 --> 00:07:04,523
with a server in our system.

149
00:07:05,390 --> 00:07:08,600
So if we're not careful in designing the API

150
00:07:08,600 --> 00:07:11,810
we can introduce some very confusing situations

151
00:07:11,810 --> 00:07:13,923
for the client application developers.

152
00:07:15,160 --> 00:07:18,120
For example, if we're a credit card company

153
00:07:18,120 --> 00:07:20,830
and the client is running an online store

154
00:07:20,830 --> 00:07:23,370
and is trying to call the debit account method

155
00:07:23,370 --> 00:07:25,650
which is part of our API,

156
00:07:25,650 --> 00:07:28,650
a failure or an exception in calling such a method

157
00:07:28,650 --> 00:07:30,340
can leave them with a dilemma

158
00:07:30,340 --> 00:07:33,010
of whether they should retry calling the method

159
00:07:33,010 --> 00:07:35,980
and running the risk of charging the user twice

160
00:07:35,980 --> 00:07:39,120
or not retrying it and running the risk

161
00:07:39,120 --> 00:07:41,233
of not charging the user at all.

162
00:07:42,300 --> 00:07:44,440
That of course comes from the fact

163
00:07:44,440 --> 00:07:47,070
that there is no real way for the client

164
00:07:47,070 --> 00:07:49,530
to know whether the server on our system

165
00:07:49,530 --> 00:07:52,230
received the message and the acknowledgement message

166
00:07:52,230 --> 00:07:54,130
simply got lost in the network

167
00:07:54,130 --> 00:07:56,780
or the server in our system simply crashed

168
00:07:56,780 --> 00:07:59,360
and never received the message.

169
00:07:59,360 --> 00:08:01,890
Now, although we can't really solve

170
00:08:01,890 --> 00:08:05,320
the unreliability problem we can mitigate the issue

171
00:08:05,320 --> 00:08:07,950
by sticking to one of the other best practices

172
00:08:07,950 --> 00:08:09,710
we learned in the previous lecture,

173
00:08:09,710 --> 00:08:13,663
which is making our operations idempotent when possible.

174
00:08:14,670 --> 00:08:18,320
Now let's talk about when we should prefer the RPC style

175
00:08:18,320 --> 00:08:19,980
for defining our API

176
00:08:19,980 --> 00:08:22,383
and when we should take a different approach.

177
00:08:23,300 --> 00:08:26,710
Remote Procedure Calls are very commonly used

178
00:08:26,710 --> 00:08:29,790
in communication between two backend systems.

179
00:08:29,790 --> 00:08:33,950
And although there are frameworks that support RPC calls

180
00:08:33,950 --> 00:08:36,809
from frontend clients like the web browser

181
00:08:36,809 --> 00:08:38,623
they are usually less common.

182
00:08:39,590 --> 00:08:44,270
So in summary, the RPC style is a perfect choice for an API,

183
00:08:44,270 --> 00:08:46,050
we provide to a different company

184
00:08:46,050 --> 00:08:49,190
instead of an end user app or webpage.

185
00:08:49,190 --> 00:08:50,870
And it's also great choice

186
00:08:50,870 --> 00:08:54,330
for communication between different components internally

187
00:08:54,330 --> 00:08:56,730
within a large scale system.

188
00:08:56,730 --> 00:08:59,990
The RPC API style is also a good choice

189
00:08:59,990 --> 00:09:02,730
for situations when we want to completely

190
00:09:02,730 --> 00:09:05,270
abstract the way the network communication

191
00:09:05,270 --> 00:09:09,210
and focus only on the actions the client wants to perform

192
00:09:09,210 --> 00:09:10,623
on the server system.

193
00:09:11,550 --> 00:09:15,550
This isn't always the case for all types of APIs.

194
00:09:15,550 --> 00:09:17,650
So in cases where we don't want

195
00:09:17,650 --> 00:09:19,960
to abstract the network communication away,

196
00:09:19,960 --> 00:09:22,750
and we do want to take direct advantage

197
00:09:22,750 --> 00:09:25,900
of things like HTTP cookies or headers

198
00:09:25,900 --> 00:09:29,260
using the RPC approach would not be a good fit

199
00:09:29,260 --> 00:09:32,180
and there are other styles that would be a better match

200
00:09:32,180 --> 00:09:33,493
for this use case.

201
00:09:34,400 --> 00:09:38,550
Finally, the RPC revolves more around actions

202
00:09:38,550 --> 00:09:41,800
and less around data or resources.

203
00:09:41,800 --> 00:09:44,550
The way it manifests itself in the style

204
00:09:44,550 --> 00:09:47,640
is that every action is simply a new method

205
00:09:47,640 --> 00:09:50,440
with a different name and a different signature.

206
00:09:50,440 --> 00:09:53,410
And we can easily define as many methods

207
00:09:53,410 --> 00:09:56,633
or actions as we want almost without limitation.

208
00:09:57,580 --> 00:10:00,250
However, if we are designing an API

209
00:10:00,250 --> 00:10:02,050
that is more data centric

210
00:10:02,050 --> 00:10:05,850
and all the operations we need are simple crowd operations

211
00:10:05,850 --> 00:10:08,670
then there are other styles of APIs

212
00:10:08,670 --> 00:10:12,510
that can be a much better fit for this type of scenario.

213
00:10:12,510 --> 00:10:14,630
We're going to talk about one of those styles

214
00:10:14,630 --> 00:10:15,880
in the following lecture.

215
00:10:16,840 --> 00:10:20,270
In this lecture, we talked about the first type of API,

216
00:10:20,270 --> 00:10:22,250
the Remote Procedure Call.

217
00:10:22,250 --> 00:10:25,250
We mentioned the three components of the RPC style,

218
00:10:25,250 --> 00:10:27,150
which is the API definition

219
00:10:27,150 --> 00:10:30,330
typically done using the interface description language,

220
00:10:30,330 --> 00:10:32,703
the client stub, and the server stub.

221
00:10:33,650 --> 00:10:36,940
We also talked about some of the benefits of RPCs

222
00:10:36,940 --> 00:10:39,100
which include local transparency

223
00:10:39,100 --> 00:10:42,600
and full abstraction of network communication.

224
00:10:42,600 --> 00:10:46,360
And finally, we mentioned some of the drawbacks of RPCs

225
00:10:46,360 --> 00:10:49,430
which include slowness and unreliability.

226
00:10:49,430 --> 00:10:51,760
Those drawbacks can be mitigated

227
00:10:51,760 --> 00:10:54,080
by applying the best practices we learned

228
00:10:54,080 --> 00:10:55,313
in a previous lecture.

229
00:10:56,320 --> 00:10:58,320
I will see you guys in the next lecture.

