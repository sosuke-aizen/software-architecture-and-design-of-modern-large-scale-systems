1
00:00:00,500 --> 00:00:02,000
Hey, welcome back.

2
00:00:02,860 --> 00:00:03,920
In this lecture,

3
00:00:03,920 --> 00:00:06,670
we're going to talk about designing the application

4
00:00:06,670 --> 00:00:09,900
programming interface for our system.

5
00:00:09,900 --> 00:00:12,800
As always, we will start with some introduction

6
00:00:12,800 --> 00:00:15,920
and motivation for why we need to design an API

7
00:00:15,920 --> 00:00:17,110
for our system.

8
00:00:17,110 --> 00:00:20,910
And later we will talk about a few categories of APIs

9
00:00:20,910 --> 00:00:23,550
as well as the best practices and patterns

10
00:00:23,550 --> 00:00:26,423
for designing a good API for our clients.

11
00:00:27,360 --> 00:00:30,420
So let's start with defining what an API is

12
00:00:30,420 --> 00:00:31,513
and why we need it.

13
00:00:32,460 --> 00:00:35,400
After we capture all the functional requirements,

14
00:00:35,400 --> 00:00:38,730
we can start thinking about our system as a black box

15
00:00:38,730 --> 00:00:42,613
that has behavior as well as a well defined interface.

16
00:00:43,470 --> 00:00:46,440
That interface is essentially a contract

17
00:00:46,440 --> 00:00:49,300
between us the engineers who implement the system,

18
00:00:49,300 --> 00:00:52,143
and the client applications who use our system.

19
00:00:53,040 --> 00:00:55,480
Since this interface is going to be called

20
00:00:55,480 --> 00:00:57,290
by other applications,

21
00:00:57,290 --> 00:00:58,810
we'll refer to this interface

22
00:00:58,810 --> 00:01:03,490
as an application programming interface or API in short.

23
00:01:03,490 --> 00:01:07,170
To clarify, since we are building a large scale system,

24
00:01:07,170 --> 00:01:09,200
and not a class or a library,

25
00:01:09,200 --> 00:01:12,380
this API is going to be called by other applications

26
00:01:12,380 --> 00:01:14,303
remotely through the network.

27
00:01:15,350 --> 00:01:18,420
The applications that are going to be calling our API

28
00:01:18,420 --> 00:01:21,440
may be front-end clients like mobile applications

29
00:01:21,440 --> 00:01:23,000
or web browsers.

30
00:01:23,000 --> 00:01:25,050
They can be other backend systems

31
00:01:25,050 --> 00:01:26,830
that belong into other companies,

32
00:01:26,830 --> 00:01:31,120
or they can be internal systems within our organization.

33
00:01:31,120 --> 00:01:32,110
Additionally,

34
00:01:32,110 --> 00:01:35,330
once we finish the internal design of our system

35
00:01:35,330 --> 00:01:37,490
and decide on its different components,

36
00:01:37,490 --> 00:01:40,600
each such component will have its own API

37
00:01:40,600 --> 00:01:42,840
which will be called by other applications

38
00:01:42,840 --> 00:01:44,053
within our system.

39
00:01:44,970 --> 00:01:48,210
Now let's talk about the different in categories of APIs

40
00:01:48,210 --> 00:01:50,310
depending on the system we're designing

41
00:01:50,310 --> 00:01:52,673
and the type of clients that are using it.

42
00:01:53,610 --> 00:01:57,590
Generally we can classify APIs into three groups.

43
00:01:57,590 --> 00:02:02,590
public APIs, private or internal APIs and partner APIs.

44
00:02:03,690 --> 00:02:06,900
Public APIs are exposed to the general public

45
00:02:06,900 --> 00:02:09,880
and any developer can use them and call them

46
00:02:09,880 --> 00:02:11,770
from their application.

47
00:02:11,770 --> 00:02:14,480
A good general practice for public APIs

48
00:02:14,480 --> 00:02:17,680
is requiring the users to register with us

49
00:02:17,680 --> 00:02:21,533
before we allow them to send us requests and use our system.

50
00:02:22,470 --> 00:02:25,010
This allows us to have better control

51
00:02:25,010 --> 00:02:27,320
over who uses our system externally

52
00:02:27,320 --> 00:02:29,130
and how they're using it.

53
00:02:29,130 --> 00:02:32,010
This in turn provides us with better security

54
00:02:32,010 --> 00:02:34,760
and also allows us to blacklist users

55
00:02:34,760 --> 00:02:36,543
that don't play by our rules.

56
00:02:37,560 --> 00:02:42,560
Private APIs are exposed only internally within our company.

57
00:02:42,970 --> 00:02:47,460
Private APIs allow other teams or part of our organization

58
00:02:47,460 --> 00:02:51,210
to take advantage of our system and for provide bigger value

59
00:02:51,210 --> 00:02:54,930
for our company without exposing our system directly

60
00:02:54,930 --> 00:02:56,573
outside the organization.

61
00:02:57,580 --> 00:03:01,430
Partner APIs are very similar to public APIs,

62
00:03:01,430 --> 00:03:02,940
but instead of being exposed

63
00:03:02,940 --> 00:03:05,840
and available to anyone who wants to use them,

64
00:03:05,840 --> 00:03:08,640
they're exposed only to companies or users

65
00:03:08,640 --> 00:03:10,980
that have a business relationship with us

66
00:03:10,980 --> 00:03:12,870
in a form of a customer agreement

67
00:03:12,870 --> 00:03:16,693
after buying our product or subscribing to our service.

68
00:03:17,660 --> 00:03:19,430
Having an explicit contract

69
00:03:19,430 --> 00:03:23,163
in the form of an API with our clients has a few benefits.

70
00:03:24,120 --> 00:03:27,260
The main benefit of a well defined API

71
00:03:27,260 --> 00:03:30,340
is that the client who uses it can immediately

72
00:03:30,340 --> 00:03:32,860
and easily enhance their business

73
00:03:32,860 --> 00:03:36,140
by using our system without knowing anything

74
00:03:36,140 --> 00:03:40,210
about our system's internal design or implementation.

75
00:03:40,210 --> 00:03:42,750
However, there are a few other benefits

76
00:03:42,750 --> 00:03:44,530
that should be considered.

77
00:03:44,530 --> 00:03:47,820
As we're going through the design process of our system,

78
00:03:47,820 --> 00:03:51,130
once we define our API and expose it to anyone

79
00:03:51,130 --> 00:03:52,970
who wants to use it in the future,

80
00:03:52,970 --> 00:03:55,270
those clients don't have to wait

81
00:03:55,270 --> 00:03:57,770
until we finish implementing our system.

82
00:03:57,770 --> 00:03:59,720
And once they know our API,

83
00:03:59,720 --> 00:04:01,880
they can already start making progress

84
00:04:01,880 --> 00:04:03,763
towards their integration with us.

85
00:04:04,620 --> 00:04:07,800
Another benefit is once we define our API,

86
00:04:07,800 --> 00:04:10,350
it's a lot easier for us to design

87
00:04:10,350 --> 00:04:13,400
and architect the internal structure of our system

88
00:04:13,400 --> 00:04:17,339
because this interface essentially defines the endpoints

89
00:04:17,339 --> 00:04:18,630
to the different routes

90
00:04:18,630 --> 00:04:21,093
that the user can take to use our system.

91
00:04:22,100 --> 00:04:24,990
Now, let's talk about the important considerations,

92
00:04:24,990 --> 00:04:27,030
patterns and best practices

93
00:04:27,030 --> 00:04:30,103
for designing a good and easy to use API.

94
00:04:31,040 --> 00:04:35,000
The first rule that would make a good API for any system

95
00:04:35,000 --> 00:04:37,060
not only a large scale system

96
00:04:37,060 --> 00:04:40,080
is complete encapsulation of the internal design

97
00:04:40,080 --> 00:04:42,600
and implementation and abstracting it away

98
00:04:42,600 --> 00:04:45,303
from the developer that wants to use our system.

99
00:04:46,180 --> 00:04:49,650
In other words, if a client who wants to use our API

100
00:04:49,650 --> 00:04:52,160
finds himself requiring information

101
00:04:52,160 --> 00:04:55,210
about how this API is implemented internally,

102
00:04:55,210 --> 00:04:58,440
or needs to know too much about our business logic

103
00:04:58,440 --> 00:04:59,630
for them to use it,

104
00:04:59,630 --> 00:05:02,703
the whole purpose of the API is defeated.

105
00:05:03,690 --> 00:05:07,610
In addition, we want our API to be completely decoupled

106
00:05:07,610 --> 00:05:10,360
from our internal design and implementation

107
00:05:10,360 --> 00:05:14,040
so that we can change that design anytime in the future

108
00:05:14,040 --> 00:05:17,253
without breaking our contract with existing clients.

109
00:05:18,250 --> 00:05:21,260
Another thing to consider when designing an API

110
00:05:21,260 --> 00:05:24,430
is that it needs to be easy to use, easy to understand

111
00:05:24,430 --> 00:05:28,363
and impossible to misuse accidentally or on purpose.

112
00:05:29,290 --> 00:05:32,540
A few things that can help make an API simple

113
00:05:32,540 --> 00:05:35,410
are having only one way to get certain data

114
00:05:35,410 --> 00:05:37,920
or perform a task and not many.

115
00:05:37,920 --> 00:05:40,300
Having descriptive names for our actions

116
00:05:40,300 --> 00:05:43,750
and resources and exposing only the information

117
00:05:43,750 --> 00:05:47,800
and the actions that the user need, and not more than that.

118
00:05:47,800 --> 00:05:51,730
Also keeping things consistent all across our API

119
00:05:51,730 --> 00:05:53,833
will make using it a lot easier.

120
00:05:54,770 --> 00:05:57,360
The next best practice for a good API

121
00:05:57,360 --> 00:06:01,203
is keeping the operations idempotent as much as possible.

122
00:06:02,120 --> 00:06:03,990
An idempotent operation

123
00:06:03,990 --> 00:06:07,260
is an operation that doesn't have any additional effect

124
00:06:07,260 --> 00:06:10,353
on the result if it's performed more than once.

125
00:06:11,230 --> 00:06:14,820
For example, updating the user's address to a new address

126
00:06:14,820 --> 00:06:18,750
is an idempotent operation because result is the same

127
00:06:18,750 --> 00:06:22,130
regardless whether we perform this operation one time,

128
00:06:22,130 --> 00:06:24,253
five times or 10 times.

129
00:06:25,180 --> 00:06:26,220
On the other hand,

130
00:06:26,220 --> 00:06:29,360
incrementing a user's balance by $100

131
00:06:29,360 --> 00:06:31,540
is not an idempotent operation

132
00:06:31,540 --> 00:06:33,780
because the result will be different

133
00:06:33,780 --> 00:06:37,033
depending on the number of times we perform this operation.

134
00:06:38,150 --> 00:06:42,240
The reason we prefer idempotent operations for our API

135
00:06:42,240 --> 00:06:46,200
is that our API is going to be used through the network.

136
00:06:46,200 --> 00:06:49,110
So if the client application sends us a message,

137
00:06:49,110 --> 00:06:51,440
that message can be either lost.

138
00:06:51,440 --> 00:06:53,930
The response to that message may be lost

139
00:06:53,930 --> 00:06:57,760
or a critical component inside our system may go down

140
00:06:57,760 --> 00:06:59,963
and the message wasn't even received.

141
00:07:00,950 --> 00:07:02,860
Because of this network decoupling

142
00:07:02,860 --> 00:07:05,470
the client application has no idea

143
00:07:05,470 --> 00:07:08,360
which one of those scenarios actually happened.

144
00:07:08,360 --> 00:07:10,960
So if that operation is idempotent,

145
00:07:10,960 --> 00:07:13,800
then the client application can simply resend

146
00:07:13,800 --> 00:07:17,923
the same message again without risking adverse consequences.

147
00:07:18,830 --> 00:07:22,880
The next best practice is called API pagination.

148
00:07:22,880 --> 00:07:26,080
Pagination is an important API feature

149
00:07:26,080 --> 00:07:28,480
for scenario when the response from our system

150
00:07:28,480 --> 00:07:29,760
to the client request

151
00:07:29,760 --> 00:07:33,480
contains a very large payload or dataset.

152
00:07:33,480 --> 00:07:36,740
Without pagination, most client applications

153
00:07:36,740 --> 00:07:40,130
would either not be able to handle such big responses

154
00:07:40,130 --> 00:07:42,853
or result in a poor user experience.

155
00:07:43,730 --> 00:07:45,440
Imagine what would happen

156
00:07:45,440 --> 00:07:48,410
if you opened your email account on your web browser,

157
00:07:48,410 --> 00:07:52,220
and instead of seeing only the last 10 or 20 emails,

158
00:07:52,220 --> 00:07:55,440
you would be presented with all the thousands of emails

159
00:07:55,440 --> 00:07:59,120
that you ever received since you opened your email account

160
00:07:59,120 --> 00:08:01,010
all on one page.

161
00:08:01,010 --> 00:08:03,750
Similarly, you can imagine what would happen

162
00:08:03,750 --> 00:08:07,320
if you opened your favorite online store or search engine

163
00:08:07,320 --> 00:08:09,910
on your mobile phone or web browser.

164
00:08:09,910 --> 00:08:13,090
And anytime you searched for a particular item,

165
00:08:13,090 --> 00:08:17,030
you would get all the thousands or maybe millions of items

166
00:08:17,030 --> 00:08:19,473
that somehow matched your search query.

167
00:08:20,470 --> 00:08:23,720
Obviously, if your application or web browser

168
00:08:23,720 --> 00:08:27,560
was even able to handle so much data without crushing,

169
00:08:27,560 --> 00:08:30,290
which in most cases is highly unlikely,

170
00:08:30,290 --> 00:08:32,240
it would take an unreasonable time

171
00:08:32,240 --> 00:08:33,863
to show all those results.

172
00:08:35,039 --> 00:08:37,500
So pagination allows the client

173
00:08:37,500 --> 00:08:40,740
to request only a small segment of the response

174
00:08:40,740 --> 00:08:43,700
by specifying the maximum size of each response

175
00:08:43,700 --> 00:08:44,860
from our our system,

176
00:08:44,860 --> 00:08:47,623
and an offset within the overall dataset.

177
00:08:48,580 --> 00:08:50,820
And if they want to receive the next segment,

178
00:08:50,820 --> 00:08:52,883
they simply increment the offset.

179
00:08:53,770 --> 00:08:57,750
The next best practice and pattern refers to the operations

180
00:08:57,750 --> 00:09:01,090
that take a long time for our system to complete.

181
00:09:01,090 --> 00:09:02,920
Unlike the previous examples

182
00:09:02,920 --> 00:09:05,140
where partial results were meaningful,

183
00:09:05,140 --> 00:09:07,460
so we could solve them using pagination,

184
00:09:07,460 --> 00:09:11,100
some operations just need one big result at the end

185
00:09:11,100 --> 00:09:13,940
and there is nothing meaningful that we can provide

186
00:09:13,940 --> 00:09:16,433
before the entire operation finishes.

187
00:09:17,460 --> 00:09:19,610
A few examples of such operations

188
00:09:19,610 --> 00:09:21,550
can be running a big report

189
00:09:21,550 --> 00:09:24,910
that requires our system to talk to a lot of databases,

190
00:09:24,910 --> 00:09:29,270
big data analysis that scans a lot of records or log files

191
00:09:29,270 --> 00:09:31,300
for aggregation purposes

192
00:09:31,300 --> 00:09:34,003
or compression of large video files.

193
00:09:35,040 --> 00:09:38,950
In all those use cases, if the operation takes a long time,

194
00:09:38,950 --> 00:09:42,520
the client application is forced to wait for the result

195
00:09:42,520 --> 00:09:46,000
without the ability to make any progress.

196
00:09:46,000 --> 00:09:49,190
The pattern that is used for this type of situation

197
00:09:49,190 --> 00:09:51,940
is called an asynchronous API.

198
00:09:51,940 --> 00:09:53,860
With an asynchronous API,

199
00:09:53,860 --> 00:09:57,250
a client application receives a response immediately

200
00:09:57,250 --> 00:10:00,350
without having to wait for the final result.

201
00:10:00,350 --> 00:10:04,420
That response usually includes some kind of identifier

202
00:10:04,420 --> 00:10:07,800
that allows the client application to track the progress

203
00:10:07,800 --> 00:10:09,540
and status of the operation,

204
00:10:09,540 --> 00:10:12,930
and eventually receive the final result.

205
00:10:12,930 --> 00:10:15,140
The final important best practice

206
00:10:15,140 --> 00:10:19,070
for designing a good API is a explicitly versioning it

207
00:10:19,070 --> 00:10:20,540
and allowing the client

208
00:10:20,540 --> 00:10:24,870
to know which version of the API they're currently using.

209
00:10:24,870 --> 00:10:28,970
The motivation behind versioning our API is as follows.

210
00:10:28,970 --> 00:10:32,690
As we mentioned earlier, the best API we can design

211
00:10:32,690 --> 00:10:34,890
is the one that allows us to make changes

212
00:10:34,890 --> 00:10:37,500
to the internal design and implementation

213
00:10:37,500 --> 00:10:41,270
without needing to make any changes to the API.

214
00:10:41,270 --> 00:10:45,670
However, in practice, we can't always predict the future

215
00:10:45,670 --> 00:10:47,020
and we may be forced

216
00:10:47,020 --> 00:10:51,160
to potentially make non-backward compatible API changes

217
00:10:51,160 --> 00:10:53,260
at one point or another.

218
00:10:53,260 --> 00:10:56,130
So if we explicitly version the APIs,

219
00:10:56,130 --> 00:10:59,330
we can essentially maintain two versions of the API

220
00:10:59,330 --> 00:11:03,320
at the same time and deprecate the older one gradually

221
00:11:03,320 --> 00:11:05,750
with proper communication with the clients

222
00:11:05,750 --> 00:11:07,093
who are still using it.

223
00:11:08,100 --> 00:11:11,820
Now, theoretically we're free to define our API

224
00:11:11,820 --> 00:11:13,400
in any way we want.

225
00:11:13,400 --> 00:11:15,870
As long as we adhere to those best practices

226
00:11:15,870 --> 00:11:17,220
we just mentioned.

227
00:11:17,220 --> 00:11:20,890
But over time, a few APIs became more standard

228
00:11:20,890 --> 00:11:22,460
in the industry.

229
00:11:22,460 --> 00:11:24,792
So we're going to cover those types of APIs

230
00:11:24,792 --> 00:11:26,333
in the next lectures.

231
00:11:27,290 --> 00:11:28,600
But before we go there,

232
00:11:28,600 --> 00:11:32,200
let's quickly summarize what we learn in this lecture.

233
00:11:32,200 --> 00:11:34,690
In this lecture, we defined an API

234
00:11:34,690 --> 00:11:38,110
as a contract that we define for other applications

235
00:11:38,110 --> 00:11:41,480
so they can use our system without knowing anything

236
00:11:41,480 --> 00:11:44,840
about its internal design and implementation.

237
00:11:44,840 --> 00:11:48,400
Later, we talked about the three categories of APIs

238
00:11:48,400 --> 00:11:52,823
which are the public API, private API and partner APIs.

239
00:11:53,810 --> 00:11:54,740
And finally,

240
00:11:54,740 --> 00:11:57,660
we talked about a few best practices and patterns

241
00:11:57,660 --> 00:12:01,170
for designing a good API, which are encapsulation,

242
00:12:01,170 --> 00:12:04,730
ease of use, making our operations idempotent,

243
00:12:04,730 --> 00:12:08,893
pagination, asynchronous operations and API versioning.

244
00:12:10,010 --> 00:12:12,043
I will see you soon in the next lecture.

