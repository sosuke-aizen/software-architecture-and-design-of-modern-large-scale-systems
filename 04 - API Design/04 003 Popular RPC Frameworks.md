

#### [ **gRPC**](https://grpc.io/)

[ _gRPC_](https://grpc.io/) is a modern open source high performance Remote
Procedure Call (RPC) framework. It was originally developed by Google in 2015
as the next generation of its own internal RPC infrastructure.

It uses [HTTP/2](https://en.wikipedia.org/wiki/HTTP/2) as its transport
protocol and [Protocol Buffers](https://developers.google.com/protocol-
buffers) as its **_Interface Description Language.  
_** _gRPC_ currently supports the following languages:

  * [C#](https://grpc.io/docs/languages/csharp/)

  * [C++](https://grpc.io/docs/languages/cpp/)

  * [Dart](https://grpc.io/docs/languages/dart/)

  * [Go](https://grpc.io/docs/languages/go/)

  * [Java](https://grpc.io/docs/languages/java/)

  * [Kotlin](https://grpc.io/docs/languages/kotlin/)

  * [Node](https://grpc.io/docs/languages/node/)

  * [Objective-C](https://grpc.io/docs/languages/objective-c/)

  * [PHP](https://grpc.io/docs/languages/php/)

  * [Python](https://grpc.io/docs/languages/python/)

  * [Ruby](https://grpc.io/docs/languages/ruby/)

#### [ **Apache Thrift**](https://thrift.apache.org/)

[Thrift](https://thrift.apache.org/) is a lightweight, language-independent
software stack for point-to-point RPC.

Thrift makes it easy for programs written in different programming languages
to share data and call remote procedures since it [supports more than 28
languages](https://github.com/apache/thrift/blob/master/LANGUAGES.md),
including C++, Java, Python, Go, Scala, Swift, PHP, Ruby, Perl, C#,
JavaScript, Node.js, and other languages.

Thrift was created at Facebook for "scalable cross-language services
development". It uses its own _Interface Description Language called_[Thrift
interface description language](https://thrift.apache.org/docs/idl).

A full tutorial for all the supported languages can be found
[here](https://thrift.apache.org/tutorial/).

#### [ **Java Remote Method Invocation
(RMI)**](https://docs.oracle.com/javase/tutorial/rmi/)

RPC framework that allows one Java virtual machine to invoke methods on an
object running in another Java virtual machine.

RMI uses Java as the _Interface Description Language._

You can find the full tutorial
[here](https://docs.oracle.com/javase/tutorial/rmi/).

