1
00:00:00,310 --> 00:00:02,620
Hey, welcome back.

2
00:00:02,620 --> 00:00:05,550
Now let's talk about another style of APIs

3
00:00:05,550 --> 00:00:06,963
which is called REST API.

4
00:00:07,810 --> 00:00:09,850
We're first going to get familiar

5
00:00:09,850 --> 00:00:13,440
with the general concepts and benefits of REST API.

6
00:00:13,440 --> 00:00:15,920
And later, we'll learn how to define

7
00:00:15,920 --> 00:00:18,033
a REST API step by step.

8
00:00:19,000 --> 00:00:21,053
So what is a REST API?

9
00:00:21,980 --> 00:00:24,470
REST is a relatively new style

10
00:00:24,470 --> 00:00:26,760
that originated from a dissertation

11
00:00:26,760 --> 00:00:30,340
published by Roy Fielding in the year 2000.

12
00:00:30,340 --> 00:00:34,010
REST, which stands for Representational State Transfer,

13
00:00:34,010 --> 00:00:37,470
is a set of architectural constraints and best practices

14
00:00:37,470 --> 00:00:39,893
for defining APIs for the web.

15
00:00:40,810 --> 00:00:42,150
It's important to note

16
00:00:42,150 --> 00:00:44,960
that it is not a standard or a protocol,

17
00:00:44,960 --> 00:00:48,530
but just an architectural style for designing APIs

18
00:00:48,530 --> 00:00:52,310
that are easy for our clients to use and understand.

19
00:00:52,310 --> 00:00:55,280
And it makes it easy for us to build a system

20
00:00:55,280 --> 00:00:57,870
with quality attributes such as scalability,

21
00:00:57,870 --> 00:01:00,103
high availability, and performance.

22
00:01:01,200 --> 00:01:04,830
An API that obeys the REST architectural constraints

23
00:01:04,830 --> 00:01:07,763
is commonly referred to as a RESTful API.

24
00:01:08,710 --> 00:01:11,070
It's easy to understand the REST API

25
00:01:11,070 --> 00:01:14,260
when we compare it to the RPC API style

26
00:01:14,260 --> 00:01:16,759
that we learned in the previous lecture.

27
00:01:16,759 --> 00:01:20,180
The RPC API style revolves around methods

28
00:01:20,180 --> 00:01:21,840
that are exposed to the client

29
00:01:21,840 --> 00:01:26,810
and are organized in an interface or a set of interfaces.

30
00:01:26,810 --> 00:01:27,890
In other words,

31
00:01:27,890 --> 00:01:31,110
our system is obstructed away from the client

32
00:01:31,110 --> 00:01:34,760
through a set of methods that the client can call.

33
00:01:34,760 --> 00:01:37,460
And if we want to expand our API

34
00:01:37,460 --> 00:01:40,880
and allow the user to perform more operations on our system,

35
00:01:40,880 --> 00:01:44,323
we would do that by adding more methods to the API.

36
00:01:45,280 --> 00:01:48,120
In contrast, the REST API style

37
00:01:48,120 --> 00:01:50,690
takes a more resource-oriented approach,

38
00:01:50,690 --> 00:01:54,610
so the main abstraction to the user is a named resource

39
00:01:54,610 --> 00:01:55,950
and not a method.

40
00:01:55,950 --> 00:01:59,260
Those resources encapsulate different entities

41
00:01:59,260 --> 00:02:00,960
in our system.

42
00:02:00,960 --> 00:02:04,133
Also in contrast to the RPC API style,

43
00:02:04,133 --> 00:02:08,880
REST API allows the user to manipulate those resources

44
00:02:08,880 --> 00:02:11,583
only through a small number of methods.

45
00:02:12,520 --> 00:02:13,783
In a REST API,

46
00:02:13,783 --> 00:02:17,310
a client requests one of those named resources

47
00:02:17,310 --> 00:02:20,700
and our system responds with a representation

48
00:02:20,700 --> 00:02:23,930
of the current state of that resource to the client

49
00:02:23,930 --> 00:02:26,333
where that representation can be used.

50
00:02:27,320 --> 00:02:29,240
The protocol that is commonly used

51
00:02:29,240 --> 00:02:32,053
to request those resources is HTTP.

52
00:02:33,020 --> 00:02:35,610
It's important to note that our system

53
00:02:35,610 --> 00:02:39,470
sends back only the representation of the resource state

54
00:02:39,470 --> 00:02:42,080
and the resource itself can be implemented

55
00:02:42,080 --> 00:02:44,010
in a completely different way

56
00:02:44,010 --> 00:02:45,843
that is transparent to the client.

57
00:02:46,880 --> 00:02:49,940
Let's clarify the statement with an example.

58
00:02:49,940 --> 00:02:52,960
Let's say we're running an online news magazine

59
00:02:52,960 --> 00:02:55,400
and the resource that we provide to the clients

60
00:02:55,400 --> 00:02:57,230
is the homepage.

61
00:02:57,230 --> 00:02:59,220
So when a client requests

62
00:02:59,220 --> 00:03:01,710
the representation of that resource state,

63
00:03:01,710 --> 00:03:04,020
they will get a webpage with a title

64
00:03:04,020 --> 00:03:06,540
and a bunch of articles and pictures.

65
00:03:06,540 --> 00:03:09,330
However, this is just the representation

66
00:03:09,330 --> 00:03:12,370
of the current state of the homepage resource.

67
00:03:12,370 --> 00:03:16,060
And in reality, our system can implement that resource

68
00:03:16,060 --> 00:03:18,630
using many entities spread out

69
00:03:18,630 --> 00:03:21,240
through multiple database tables, files,

70
00:03:21,240 --> 00:03:23,540
or even external services.

71
00:03:23,540 --> 00:03:27,240
This is what we mean by the resource being an abstraction

72
00:03:27,240 --> 00:03:30,410
whose representation can be requested on demand

73
00:03:30,410 --> 00:03:32,283
by calling our REST API.

74
00:03:33,530 --> 00:03:36,680
Another big difference from the general RPC world

75
00:03:36,680 --> 00:03:40,070
is the dynamic nature of the REST API.

76
00:03:40,070 --> 00:03:44,100
In the RPC world, the only actions that the client can take

77
00:03:44,100 --> 00:03:46,980
regardless of its state are statically defined

78
00:03:46,980 --> 00:03:50,750
ahead of time in the interface description language.

79
00:03:50,750 --> 00:03:53,120
However, in a RESTful API,

80
00:03:53,120 --> 00:03:56,800
this interface is a lot more dynamic through a concept

81
00:03:56,800 --> 00:04:01,230
called Hypermedia as the Engine of the Application State.

82
00:04:01,230 --> 00:04:04,030
The way this is achieved is by accompanying

83
00:04:04,030 --> 00:04:06,920
a state representation response to the client

84
00:04:06,920 --> 00:04:08,810
with hypermedia links,

85
00:04:08,810 --> 00:04:11,070
so the client can follow those links

86
00:04:11,070 --> 00:04:13,283
and progress its internal state.

87
00:04:14,260 --> 00:04:17,649
For example, if we have a chat or email system,

88
00:04:17,649 --> 00:04:21,529
a client may send us a request for their current messages.

89
00:04:21,529 --> 00:04:24,490
In response, we can send the client application

90
00:04:24,490 --> 00:04:27,610
not only the object that represents all the information

91
00:04:27,610 --> 00:04:29,000
about their messages,

92
00:04:29,000 --> 00:04:33,210
but also an additional object that describes the resources

93
00:04:33,210 --> 00:04:35,330
that the client application can use

94
00:04:35,330 --> 00:04:37,480
to either get additional information

95
00:04:37,480 --> 00:04:39,273
or take specific actions.

96
00:04:41,060 --> 00:04:44,110
Now let's talk about how RESTful APIs

97
00:04:44,110 --> 00:04:47,050
help us achieve high performance, scalability,

98
00:04:47,050 --> 00:04:49,703
and high availability quality attributes.

99
00:04:50,720 --> 00:04:53,290
One important requirement of a system

100
00:04:53,290 --> 00:04:57,400
that provides RESTful API is that the server is stateless

101
00:04:57,400 --> 00:05:00,080
and does not maintain any session information

102
00:05:00,080 --> 00:05:01,690
about a client.

103
00:05:01,690 --> 00:05:04,280
Instead, each message should be served

104
00:05:04,280 --> 00:05:06,170
by the server in isolation

105
00:05:06,170 --> 00:05:09,910
without any information about previous requests.

106
00:05:09,910 --> 00:05:11,960
This requirement helps us achieve

107
00:05:11,960 --> 00:05:15,163
high scalability and availability and let's see why.

108
00:05:16,820 --> 00:05:20,520
If the server does not maintain any session information,

109
00:05:20,520 --> 00:05:23,240
we can easily run a group of servers

110
00:05:23,240 --> 00:05:26,060
and spread the load of the requests from the client

111
00:05:26,060 --> 00:05:28,140
among a large number of machines,

112
00:05:28,140 --> 00:05:31,110
completely transparently to the client.

113
00:05:31,110 --> 00:05:34,540
So the client may send one request to one machine

114
00:05:34,540 --> 00:05:36,750
and another request to a different machine

115
00:05:36,750 --> 00:05:38,423
and he won't even notice.

116
00:05:39,500 --> 00:05:42,873
Now, the second important requirement is cacheability.

117
00:05:43,820 --> 00:05:48,420
That means the server has to either explicitly or implicitly

118
00:05:48,420 --> 00:05:52,743
define each response as either cacheable or non-cacheable.

119
00:05:53,910 --> 00:05:57,800
This allows the client to eliminate the potential roundtrip

120
00:05:57,800 --> 00:05:59,270
to the server and back

121
00:05:59,270 --> 00:06:01,460
if the response to a particular request

122
00:06:01,460 --> 00:06:04,910
is already cached somewhere closer to the client.

123
00:06:04,910 --> 00:06:06,650
As a side benefit to us,

124
00:06:06,650 --> 00:06:10,003
it will also reduce the load on our system.

125
00:06:11,010 --> 00:06:13,130
Now let's talk about the resources

126
00:06:13,130 --> 00:06:15,680
and discuss what those resources can be,

127
00:06:15,680 --> 00:06:18,193
how they should be named and organized.

128
00:06:19,170 --> 00:06:20,630
In a RESTful API,

129
00:06:20,630 --> 00:06:24,363
each resource is named and addressed using a URI.

130
00:06:25,490 --> 00:06:28,530
The resources are organized in a hierarchy

131
00:06:28,530 --> 00:06:31,590
where each resource is either a simple resource

132
00:06:31,590 --> 00:06:33,293
or a collection resource.

133
00:06:34,330 --> 00:06:38,500
The hierarchy is represented using forward slashes.

134
00:06:38,500 --> 00:06:41,220
Now, a simple resource has a state,

135
00:06:41,220 --> 00:06:45,930
and optionally, it can contain one or more sub-resources,

136
00:06:45,930 --> 00:06:49,350
and a collection resource is a special resource

137
00:06:49,350 --> 00:06:52,823
that contains a list of resources of the same type.

138
00:06:53,710 --> 00:06:56,720
For example, if we have a movie streaming service,

139
00:06:56,720 --> 00:07:00,060
we can have a collection resource called movies

140
00:07:00,060 --> 00:07:02,560
and each sub-resource in that collection

141
00:07:02,560 --> 00:07:04,283
is of the type of movie.

142
00:07:05,240 --> 00:07:07,590
However, additionally, each movie,

143
00:07:07,590 --> 00:07:09,230
which is a simple resource,

144
00:07:09,230 --> 00:07:12,637
can have a collection sub-resource of its directors

145
00:07:12,637 --> 00:07:14,880
and another collection sub-resource

146
00:07:14,880 --> 00:07:16,673
that represents its actors.

147
00:07:17,800 --> 00:07:21,680
Additionally, each actor, which is also a simple resource,

148
00:07:21,680 --> 00:07:24,220
can have a profile picture sub-resource

149
00:07:24,220 --> 00:07:26,593
and a contact information sub-resource.

150
00:07:27,780 --> 00:07:30,540
Now, the representation of each resource state

151
00:07:30,540 --> 00:07:33,590
can be expressed in a variety of ways.

152
00:07:33,590 --> 00:07:37,480
It can be an image, a link to a movie stream, an object,

153
00:07:37,480 --> 00:07:42,360
an HTML page, some binary blob, or even an executable code

154
00:07:42,360 --> 00:07:45,733
like JavaScript that can be executed in the browser.

155
00:07:46,810 --> 00:07:49,650
Now let's talk about some of the best practices

156
00:07:49,650 --> 00:07:51,523
for naming our resources.

157
00:07:52,560 --> 00:07:55,750
The first best practice is naming our resources

158
00:07:55,750 --> 00:07:57,333
using nouns only.

159
00:07:58,300 --> 00:08:02,350
Naming our resources using nouns makes a clear distinction

160
00:08:02,350 --> 00:08:04,490
between the actions that we're going to take

161
00:08:04,490 --> 00:08:07,823
on those resources for which we're going to use verbs.

162
00:08:08,940 --> 00:08:11,930
The second best practice is making a distinction

163
00:08:11,930 --> 00:08:15,393
between collection resources and simple resources.

164
00:08:16,360 --> 00:08:18,260
The way we make that distinction

165
00:08:18,260 --> 00:08:20,840
is by using plural names for collections

166
00:08:20,840 --> 00:08:23,583
and singular names for simple resources.

167
00:08:24,540 --> 00:08:26,600
If we look at the previous example,

168
00:08:26,600 --> 00:08:30,300
we can see that the movies resource, directors resource,

169
00:08:30,300 --> 00:08:33,169
and actors resource are all collections,

170
00:08:33,169 --> 00:08:36,113
while the other resources are simple resources.

171
00:08:37,659 --> 00:08:40,669
The next best practice is giving our resources

172
00:08:40,669 --> 00:08:42,732
clear and meaningful names.

173
00:08:43,809 --> 00:08:46,630
If we give our resources meaningful names,

174
00:08:46,630 --> 00:08:50,750
our users will find it very easy to use our API

175
00:08:50,750 --> 00:08:54,033
which will help prevent incorrect usages and mistakes.

176
00:08:55,470 --> 00:08:59,170
Some overly generic collection names like elements,

177
00:08:59,170 --> 00:09:04,000
entities, items, instances, values, or objects

178
00:09:04,000 --> 00:09:07,600
should be avoided because they can really mean anything

179
00:09:07,600 --> 00:09:10,003
and heavily depend on the context.

180
00:09:11,660 --> 00:09:15,470
And the final best practice is the resource identifiers

181
00:09:15,470 --> 00:09:17,830
should be unique and URL friendly

182
00:09:17,830 --> 00:09:21,253
so they can be easily and safely used on the web.

183
00:09:22,370 --> 00:09:25,400
Now let's switch gears and talk about the methods

184
00:09:25,400 --> 00:09:29,823
and operations that we can perform on REST API resources.

185
00:09:30,800 --> 00:09:33,890
As we mentioned earlier, unlike in RPCs,

186
00:09:33,890 --> 00:09:36,840
the REST API limits the number of methods

187
00:09:36,840 --> 00:09:38,870
we can perform on each resource

188
00:09:38,870 --> 00:09:42,140
to just a few predefined operations.

189
00:09:42,140 --> 00:09:45,480
Those operations are creating a new resource,

190
00:09:45,480 --> 00:09:47,700
updating an existing resource,

191
00:09:47,700 --> 00:09:49,720
deleting an existing resource,

192
00:09:49,720 --> 00:09:53,030
and getting the current state of a resource.

193
00:09:53,030 --> 00:09:56,580
As a side note, when the resource is a collection resource,

194
00:09:56,580 --> 00:09:58,860
getting its state usually means

195
00:09:58,860 --> 00:10:01,433
getting the list of its sub-resources.

196
00:10:02,400 --> 00:10:07,370
Now because RESTful APIs are commonly implemented using HTTP

197
00:10:07,370 --> 00:10:10,200
which already has several standard methods,

198
00:10:10,200 --> 00:10:14,870
the REST operations are mapped to HTTP methods as follows.

199
00:10:14,870 --> 00:10:17,060
When we want to create a new resource,

200
00:10:17,060 --> 00:10:19,490
we use the POST HTTP method.

201
00:10:19,490 --> 00:10:22,160
When we want update an existing resource,

202
00:10:22,160 --> 00:10:23,930
we use the PUT method.

203
00:10:23,930 --> 00:10:26,420
When we want to delete an existing resource,

204
00:10:26,420 --> 00:10:28,180
we use the DELETE method.

205
00:10:28,180 --> 00:10:31,760
And finally, when we want to get the state of a resource

206
00:10:31,760 --> 00:10:34,870
or list of the sub-resources of a collection,

207
00:10:34,870 --> 00:10:36,580
we use the GET method.

208
00:10:36,580 --> 00:10:38,310
Now, in some situations,

209
00:10:38,310 --> 00:10:41,330
we may want to define additional custom methods,

210
00:10:41,330 --> 00:10:43,483
but those situations are uncommon.

211
00:10:44,440 --> 00:10:47,710
The HTTP semantics give us a few guarantees

212
00:10:47,710 --> 00:10:49,110
about those methods.

213
00:10:49,110 --> 00:10:52,840
For example, the GET method is considered to be safe,

214
00:10:52,840 --> 00:10:55,230
meaning that applying it to a resource

215
00:10:55,230 --> 00:10:58,250
would not change its state in any way.

216
00:10:58,250 --> 00:11:01,530
Additionally, the GET, PUT, and DELETE methods

217
00:11:01,530 --> 00:11:03,050
are idempotent, which,

218
00:11:03,050 --> 00:11:05,250
as we remember from the previous lecture,

219
00:11:05,250 --> 00:11:08,500
means that applying those operations multiple times

220
00:11:08,500 --> 00:11:12,750
would result in the same state change as applying them once.

221
00:11:12,750 --> 00:11:15,490
Also, GET requests are usually considered

222
00:11:15,490 --> 00:11:17,120
cacheable by default,

223
00:11:17,120 --> 00:11:21,000
while responses to POST requests can be made cacheable

224
00:11:21,000 --> 00:11:23,520
by setting the appropriate a HTTP headers

225
00:11:23,520 --> 00:11:26,590
sent as part of the response to the client.

226
00:11:26,590 --> 00:11:30,530
This feature of the HTTP protocol allows us to conform

227
00:11:30,530 --> 00:11:33,573
to the cacheability requirements of a REST API.

228
00:11:34,650 --> 00:11:37,050
Finally, when the client needs to send

229
00:11:37,050 --> 00:11:39,210
additional information to our system

230
00:11:39,210 --> 00:11:42,270
as part of a POST or PUT command, for example,

231
00:11:42,270 --> 00:11:44,350
we would use the JSON format,

232
00:11:44,350 --> 00:11:47,933
although other formats like XML are also acceptable.

233
00:11:49,060 --> 00:11:53,490
Now let's learn how to create a REST API step by step

234
00:11:53,490 --> 00:11:55,623
by going through a real life example.

235
00:11:56,570 --> 00:11:58,480
The system we're going to design

236
00:11:58,480 --> 00:12:00,760
is going to be a movie streaming service

237
00:12:00,760 --> 00:12:03,073
which we already saw a few examples of.

238
00:12:04,030 --> 00:12:06,930
The first step of creating a REST API

239
00:12:06,930 --> 00:12:10,000
is identifying the different entities in our system,

240
00:12:10,000 --> 00:12:13,283
which will serve as the resources of our API.

241
00:12:14,210 --> 00:12:15,460
To keep things simple,

242
00:12:15,460 --> 00:12:18,130
let's assume that the only entities we have

243
00:12:18,130 --> 00:12:21,030
in our movie streaming service are users,

244
00:12:21,030 --> 00:12:23,833
movies, reviews, and actors.

245
00:12:24,940 --> 00:12:26,810
Now, the second step would be

246
00:12:26,810 --> 00:12:29,053
mapping those entities to URIs.

247
00:12:30,690 --> 00:12:33,250
In this step, we define the resources

248
00:12:33,250 --> 00:12:36,730
based on the entities we identified in the previous step

249
00:12:36,730 --> 00:12:40,020
and also organize their resources in a hierarchy

250
00:12:40,020 --> 00:12:41,933
based on their relationships.

251
00:12:42,970 --> 00:12:47,600
For example, users and movies are two collection resources

252
00:12:47,600 --> 00:12:50,230
that are entirely independent of each other.

253
00:12:50,230 --> 00:12:53,200
The actors collection is also independent

254
00:12:53,200 --> 00:12:56,890
because the same actors can appear in different movies.

255
00:12:56,890 --> 00:13:00,190
However, the reviews are going to be a sub-resource

256
00:13:00,190 --> 00:13:04,040
of the movies collection because each review is associated

257
00:13:04,040 --> 00:13:05,763
with only a single movie.

258
00:13:06,690 --> 00:13:10,200
At this point, we have all the resources defined,

259
00:13:10,200 --> 00:13:13,200
so the next step is choosing the representation

260
00:13:13,200 --> 00:13:15,460
of each type of resource.

261
00:13:15,460 --> 00:13:18,310
Theoretically, we can represent each resource

262
00:13:18,310 --> 00:13:20,050
in any way we like,

263
00:13:20,050 --> 00:13:22,930
but the most common way to represent a resource

264
00:13:22,930 --> 00:13:24,593
is using the JSON format.

265
00:13:25,770 --> 00:13:28,680
For example, the movies collection resource

266
00:13:28,680 --> 00:13:30,690
would be represented as an object

267
00:13:30,690 --> 00:13:35,030
containing an array of movie names mapped to movie IDs.

268
00:13:35,030 --> 00:13:37,410
This representation makes it easy

269
00:13:37,410 --> 00:13:39,800
to search for a particular movie by name

270
00:13:39,800 --> 00:13:42,610
and also get its identifier.

271
00:13:42,610 --> 00:13:44,300
Using this identifier,

272
00:13:44,300 --> 00:13:47,750
a client application can append it to the movies URI

273
00:13:47,750 --> 00:13:50,003
and get a particular movie resource.

274
00:13:51,120 --> 00:13:53,330
A single movie resource object

275
00:13:53,330 --> 00:13:55,210
would contain all the information

276
00:13:55,210 --> 00:13:56,820
about that particular movie,

277
00:13:56,820 --> 00:13:59,710
including links that can help us play the movie

278
00:13:59,710 --> 00:14:01,200
on the user's device,

279
00:14:01,200 --> 00:14:03,790
as well as performing additional operations

280
00:14:03,790 --> 00:14:07,690
such as getting the movie reviews and the movie's actors.

281
00:14:07,690 --> 00:14:11,070
Those links allow our API to be driven

282
00:14:11,070 --> 00:14:14,883
by the hypermedia as the engine of the application state.

283
00:14:15,830 --> 00:14:18,610
The last step in creating a REST API

284
00:14:18,610 --> 00:14:21,520
is assigning HTTP methods to actions

285
00:14:21,520 --> 00:14:24,063
that we can perform on our resources.

286
00:14:24,980 --> 00:14:28,150
For example, we can define the POST operation

287
00:14:28,150 --> 00:14:30,170
on the user's collection resource

288
00:14:30,170 --> 00:14:33,950
to register a new user in our movie streaming service.

289
00:14:33,950 --> 00:14:35,800
The response to that request

290
00:14:35,800 --> 00:14:39,010
would contain a newly created user ID.

291
00:14:39,010 --> 00:14:42,870
Then we can define a GET method on a particular user

292
00:14:42,870 --> 00:14:46,800
to get all the information stored for that particular user

293
00:14:46,800 --> 00:14:48,990
including the list of favorite movies

294
00:14:48,990 --> 00:14:52,650
or the ones that they want to watch in the future.

295
00:14:52,650 --> 00:14:55,740
Similarly, we can define a PUT operation

296
00:14:55,740 --> 00:14:57,690
on a particular user resource

297
00:14:57,690 --> 00:15:00,080
to update the current user's profile,

298
00:15:00,080 --> 00:15:02,090
or if we want to make any changes

299
00:15:02,090 --> 00:15:05,260
to the user's information or preferences.

300
00:15:05,260 --> 00:15:08,290
And finally, we can define a DELETE operation

301
00:15:08,290 --> 00:15:10,830
on a particular user to remove that user

302
00:15:10,830 --> 00:15:13,300
entirely from our system.

303
00:15:13,300 --> 00:15:15,720
Now to finish designing our API,

304
00:15:15,720 --> 00:15:17,330
we need to repeat this process

305
00:15:17,330 --> 00:15:20,053
on all the resources we have in our system.

306
00:15:21,140 --> 00:15:24,560
In this lecture, we'll learn about a new style of API,

307
00:15:24,560 --> 00:15:27,340
which is called Representational State Transfer

308
00:15:27,340 --> 00:15:29,480
or REST in short.

309
00:15:29,480 --> 00:15:33,140
We compared the REST API to the general RPC approach

310
00:15:33,140 --> 00:15:37,740
by emphasizing that the REST API is more resource-oriented

311
00:15:37,740 --> 00:15:40,160
and limits the number of operations

312
00:15:40,160 --> 00:15:42,670
we can perform on those named resources

313
00:15:42,670 --> 00:15:44,283
to just a few methods.

314
00:15:45,230 --> 00:15:48,780
We later talked about how the REST API requirements

315
00:15:48,780 --> 00:15:50,800
allow us to provide high performance,

316
00:15:50,800 --> 00:15:53,800
high availability, and scalability.

317
00:15:53,800 --> 00:15:56,060
After that, we talked in detail

318
00:15:56,060 --> 00:15:59,580
about what those resources are, how they're organized,

319
00:15:59,580 --> 00:16:03,630
and what operations we can perform on those resources.

320
00:16:03,630 --> 00:16:05,930
And finally, we concluded by providing

321
00:16:05,930 --> 00:16:09,580
a step-by-step process on defining the REST API

322
00:16:09,580 --> 00:16:12,623
by following a practical real life example.

323
00:16:13,570 --> 00:16:14,830
I hope you're having fun

324
00:16:14,830 --> 00:16:17,203
and I will see you soon in the next lecture.

