1
00:00:02,920 --> 00:00:04,810
Now that we thoroughly explored

2
00:00:04,810 --> 00:00:06,320
functional requirements,

3
00:00:06,320 --> 00:00:08,880
as well as how to capture and visualize them,

4
00:00:08,880 --> 00:00:11,720
let's expand on the next type of requirements,

5
00:00:11,720 --> 00:00:13,060
the quality attributes,

6
00:00:13,060 --> 00:00:16,120
otherwise known as nonfunctional requirements.

7
00:00:16,120 --> 00:00:19,290
This type of requirements has a direct effect

8
00:00:19,290 --> 00:00:20,963
on the software architecture.

9
00:00:21,890 --> 00:00:24,150
So let's start with some motivation

10
00:00:24,150 --> 00:00:27,063
and a formal definition of a quality attribute.

11
00:00:28,510 --> 00:00:30,740
Studies have shown that systems

12
00:00:30,740 --> 00:00:33,690
are frequently redesigned and restructured,

13
00:00:33,690 --> 00:00:36,860
mostly not because they're functionally deficient

14
00:00:36,860 --> 00:00:39,270
or because of new functionality requirements,

15
00:00:39,270 --> 00:00:42,650
but because the existing system, as it stands,

16
00:00:42,650 --> 00:00:45,710
is either not fast enough, can't scale well enough

17
00:00:45,710 --> 00:00:48,780
to the growing number of users or volume of data,

18
00:00:48,780 --> 00:00:50,830
or the structure of the system

19
00:00:50,830 --> 00:00:55,560
makes development, maintenance, or security very hard.

20
00:00:55,560 --> 00:00:58,760
And in most cases, after such a major redesign,

21
00:00:58,760 --> 00:01:01,350
the system provides either equivalent

22
00:01:01,350 --> 00:01:04,629
or exactly the same functionality as it did before.

23
00:01:04,629 --> 00:01:08,120
So architecting our system in a way that provides us

24
00:01:08,120 --> 00:01:11,200
with the right quality attributes right from the start

25
00:01:11,200 --> 00:01:14,963
is extremely important to avoid such major redesigns.

26
00:01:16,670 --> 00:01:18,990
Now let's define more formally

27
00:01:18,990 --> 00:01:21,950
what quality attributes actually are.

28
00:01:21,950 --> 00:01:25,480
Quality attributes are nonfunctional requirements

29
00:01:25,480 --> 00:01:27,550
that describe the qualities

30
00:01:27,550 --> 00:01:29,770
of the system's functional requirements,

31
00:01:29,770 --> 00:01:32,743
or the overall properties of the system.

32
00:01:34,010 --> 00:01:36,680
In other words, the quality attributes

33
00:01:36,680 --> 00:01:39,200
do not say what the system does,

34
00:01:39,200 --> 00:01:42,170
but instead, they provide a quality measure

35
00:01:42,170 --> 00:01:46,650
on how well our system performs on a particular dimension.

36
00:01:46,650 --> 00:01:48,170
And as we mentioned before,

37
00:01:48,170 --> 00:01:51,510
the quality attributes have direct correlation

38
00:01:51,510 --> 00:01:53,940
with the architecture of the system.

39
00:01:53,940 --> 00:01:55,690
To clarify the definition,

40
00:01:55,690 --> 00:01:59,850
let's see how a quality attribute provides a quality measure

41
00:01:59,850 --> 00:02:01,603
to a functional requirement.

42
00:02:02,830 --> 00:02:05,610
Let's take an example of an online store

43
00:02:05,610 --> 00:02:08,120
where users can look for products

44
00:02:08,120 --> 00:02:10,360
based on some search keywords.

45
00:02:10,360 --> 00:02:12,350
So one general requirement

46
00:02:12,350 --> 00:02:15,180
may be that when a user clicks on a Search button

47
00:02:15,180 --> 00:02:18,120
after they typed in a particular search keywords,

48
00:02:18,120 --> 00:02:21,370
the user will be provided with a list of products

49
00:02:21,370 --> 00:02:24,760
within at most 100 milliseconds.

50
00:02:24,760 --> 00:02:26,610
So the functional requirement

51
00:02:26,610 --> 00:02:29,060
is the action that our system takes

52
00:02:29,060 --> 00:02:31,720
when a user clicks on the Search button,

53
00:02:31,720 --> 00:02:33,230
and the quality attribute,

54
00:02:33,230 --> 00:02:35,860
in this case, in the performance dimension,

55
00:02:35,860 --> 00:02:39,653
describes how fast this action is to be performed.

56
00:02:40,790 --> 00:02:43,080
Another example that is not tied

57
00:02:43,080 --> 00:02:45,310
to a particular functional requirement,

58
00:02:45,310 --> 00:02:48,120
but instead pertains to our entire system,

59
00:02:48,120 --> 00:02:51,900
is that our online store has to be available to the users

60
00:02:51,900 --> 00:02:56,050
at least 99.9% of the time.

61
00:02:56,050 --> 00:02:58,780
So in this case, this quality attribute

62
00:02:58,780 --> 00:03:00,883
is in the availability dimension.

63
00:03:02,010 --> 00:03:05,370
Quality attributes don't only need to satisfy

64
00:03:05,370 --> 00:03:06,730
the needs of the clients.

65
00:03:06,730 --> 00:03:10,420
Instead, they need to satisfy the quality requirements

66
00:03:10,420 --> 00:03:12,700
of all the stakeholders.

67
00:03:12,700 --> 00:03:16,380
To illustrate this, let's take another quality requirement,

68
00:03:16,380 --> 00:03:18,380
which is, our development team

69
00:03:18,380 --> 00:03:21,640
can deploy a new version of our online store

70
00:03:21,640 --> 00:03:23,580
at least twice a week.

71
00:03:23,580 --> 00:03:26,790
This property also pertains to our entire system

72
00:03:26,790 --> 00:03:29,590
and describes how our system performs

73
00:03:29,590 --> 00:03:31,513
in the deployability dimension.

74
00:03:32,570 --> 00:03:34,840
So now that we got some intuition

75
00:03:34,840 --> 00:03:36,890
for system quality attributes,

76
00:03:36,890 --> 00:03:39,810
let's discuss some very important considerations

77
00:03:39,810 --> 00:03:41,900
about quality attributes in general

78
00:03:41,900 --> 00:03:44,543
when it comes to designing a software system.

79
00:03:45,520 --> 00:03:48,180
One of the most important considerations

80
00:03:48,180 --> 00:03:49,730
about quality attributes

81
00:03:49,730 --> 00:03:53,380
is that they have to be measurable and testable.

82
00:03:53,380 --> 00:03:57,070
If we can't objectively measure and consistently test

83
00:03:57,070 --> 00:03:59,200
to prove that our system satisfies

84
00:03:59,200 --> 00:04:03,320
the required quality attributes, then we can't actually know

85
00:04:03,320 --> 00:04:06,600
if our system performs well or performs poorly

86
00:04:06,600 --> 00:04:08,633
in regards to that requirement.

87
00:04:09,620 --> 00:04:12,380
For example, if the requirement we get

88
00:04:12,380 --> 00:04:14,880
is when a user clicks on the Buy button,

89
00:04:14,880 --> 00:04:16,329
the purchase confirmation

90
00:04:16,329 --> 00:04:19,209
must be displayed quickly to the user.

91
00:04:19,209 --> 00:04:21,130
Now, if we design a system

92
00:04:21,130 --> 00:04:23,530
such that it displays the confirmation

93
00:04:23,530 --> 00:04:26,030
within 200 milliseconds.

94
00:04:26,030 --> 00:04:27,820
Now, since we couldn't measure

95
00:04:27,820 --> 00:04:30,130
what the word quickly really meant,

96
00:04:30,130 --> 00:04:34,313
we can't know if 200 milliseconds is good or bad.

97
00:04:35,410 --> 00:04:37,690
The second important consideration

98
00:04:37,690 --> 00:04:40,360
is that there is no single architecture

99
00:04:40,360 --> 00:04:43,720
that can provide all the quality attributes.

100
00:04:43,720 --> 00:04:46,760
Essentially, if there was such an architecture,

101
00:04:46,760 --> 00:04:49,800
then everyone would use it for every system

102
00:04:49,800 --> 00:04:52,853
and the job of a software architect wouldn't exist.

103
00:04:53,770 --> 00:04:55,510
The reason it's impossible

104
00:04:55,510 --> 00:04:57,650
is that certain quality attributes

105
00:04:57,650 --> 00:04:59,150
contradict with each other,

106
00:04:59,150 --> 00:05:01,960
so some combinations of quality attributes

107
00:05:01,960 --> 00:05:06,450
are either very hard to achieve or are even impossible.

108
00:05:06,450 --> 00:05:09,450
That is why our job as software architects

109
00:05:09,450 --> 00:05:11,380
is to make the right trade-off,

110
00:05:11,380 --> 00:05:15,250
prioritizing some quality attributes over the others

111
00:05:15,250 --> 00:05:18,500
and designing the system in a way that will give us

112
00:05:18,500 --> 00:05:20,430
the highest chance for success

113
00:05:20,430 --> 00:05:22,943
based on the requirements of the business.

114
00:05:23,920 --> 00:05:27,350
For example, we may have have two conflicting requirements

115
00:05:27,350 --> 00:05:30,020
for our system's login page.

116
00:05:30,020 --> 00:05:31,630
One requirement states

117
00:05:31,630 --> 00:05:35,530
that a user should log in to our system very quickly,

118
00:05:35,530 --> 00:05:37,913
preferably under one second.

119
00:05:38,810 --> 00:05:42,230
The other requirement tells us to provide security

120
00:05:42,230 --> 00:05:44,600
to protect the user's account.

121
00:05:44,600 --> 00:05:47,520
That entails that for the user to log in,

122
00:05:47,520 --> 00:05:50,420
they will need to type in some sort of password,

123
00:05:50,420 --> 00:05:52,700
and also, to protect that password,

124
00:05:52,700 --> 00:05:54,840
we need to use something like SSL,

125
00:05:54,840 --> 00:05:56,000
which is a technology

126
00:05:56,000 --> 00:05:58,320
that allows us to protect that password

127
00:05:58,320 --> 00:06:00,913
from being intercepted by a third party.

128
00:06:01,840 --> 00:06:05,660
But making a user type in their username and password,

129
00:06:05,660 --> 00:06:09,250
and also making the network request using SSL,

130
00:06:09,250 --> 00:06:11,650
makes the login process slower,

131
00:06:11,650 --> 00:06:14,880
which goes directly against the first requirement

132
00:06:14,880 --> 00:06:17,903
to make the login process as fast as possible.

133
00:06:18,930 --> 00:06:20,930
The third important consideration

134
00:06:20,930 --> 00:06:23,030
in regards to quality attributes,

135
00:06:23,030 --> 00:06:26,400
it's the feasibility of their requirements.

136
00:06:26,400 --> 00:06:29,400
It is up to us, the designers of the system,

137
00:06:29,400 --> 00:06:32,750
to make sure that the system that we're attempting to design

138
00:06:32,750 --> 00:06:35,270
and later build is even capable

139
00:06:35,270 --> 00:06:38,580
of delivering what the client is asking for.

140
00:06:38,580 --> 00:06:42,220
In some cases, because the client is not technical,

141
00:06:42,220 --> 00:06:44,060
they may ask for something

142
00:06:44,060 --> 00:06:46,200
that is either technically impossible,

143
00:06:46,200 --> 00:06:49,150
or prohibitively expensive to implement,

144
00:06:49,150 --> 00:06:53,373
and it is our job to call that out early on in the process.

145
00:06:54,370 --> 00:06:57,220
One example of an unfeasible requirement

146
00:06:57,220 --> 00:07:01,010
is unrealistic low latency expectations.

147
00:07:01,010 --> 00:07:03,630
For example, if the network latency

148
00:07:03,630 --> 00:07:06,110
between a client in South America

149
00:07:06,110 --> 00:07:07,810
and the closest data center

150
00:07:07,810 --> 00:07:10,380
where we run our system in U.S. East,

151
00:07:10,380 --> 00:07:14,430
is anywhere between 100 or 150 milliseconds,

152
00:07:14,430 --> 00:07:17,050
then we cannot guarantee page loads

153
00:07:17,050 --> 00:07:19,960
to be less than 100 milliseconds.

154
00:07:19,960 --> 00:07:23,440
Actually, we can't even guarantee a latency close to that,

155
00:07:23,440 --> 00:07:25,980
because loading a page using HTTP

156
00:07:25,980 --> 00:07:29,510
requires multiple round trips as part of its protocol,

157
00:07:29,510 --> 00:07:32,563
as well as potentially loading multiple assets.

158
00:07:33,610 --> 00:07:36,170
Another example can be a requirement

159
00:07:36,170 --> 00:07:39,540
for 100% availability of the system.

160
00:07:39,540 --> 00:07:43,280
This basically means that our system can never fail

161
00:07:43,280 --> 00:07:45,310
and we will never have a chance

162
00:07:45,310 --> 00:07:48,263
to take it down for maintenance or upgrades.

163
00:07:49,110 --> 00:07:52,050
Other examples of unfeasible requirements

164
00:07:52,050 --> 00:07:54,700
can be full protection against hackers,

165
00:07:54,700 --> 00:07:56,770
streaming high resolution movies

166
00:07:56,770 --> 00:07:59,520
in areas where the bandwidth doesn't allow it,

167
00:07:59,520 --> 00:08:01,660
or a requirement for our system

168
00:08:01,660 --> 00:08:06,050
to increase its storage capacity at an unreasonable rate.

169
00:08:06,050 --> 00:08:08,340
So, before approving a requirement,

170
00:08:08,340 --> 00:08:10,830
we may even need to consult with people

171
00:08:10,830 --> 00:08:12,470
with domain expertise

172
00:08:12,470 --> 00:08:15,060
to make sure that we can actually deliver

173
00:08:15,060 --> 00:08:16,333
on what we promise.

174
00:08:17,500 --> 00:08:19,810
In this lecture, we got the motivation

175
00:08:19,810 --> 00:08:22,380
for system quality attributes.

176
00:08:22,380 --> 00:08:26,030
We mentioned that the majority of the system redesigns

177
00:08:26,030 --> 00:08:28,860
happen because the system, as it stands,

178
00:08:28,860 --> 00:08:32,820
does not provide the required quality attributes.

179
00:08:32,820 --> 00:08:36,330
We also officially defined quality attributes

180
00:08:36,330 --> 00:08:40,022
as a measure on how well our system performs

181
00:08:40,022 --> 00:08:41,900
on a particular dimension.

182
00:08:41,900 --> 00:08:43,510
And finally, we talked about

183
00:08:43,510 --> 00:08:45,790
the three important considerations

184
00:08:45,790 --> 00:08:49,460
when defining the quality attributes for a system.

185
00:08:49,460 --> 00:08:52,370
The first consideration was the testability

186
00:08:52,370 --> 00:08:55,490
and measurability of the quality attribute.

187
00:08:55,490 --> 00:08:58,950
The second was the necessity to make trade-offs

188
00:08:58,950 --> 00:09:03,050
and choose the right quality attributes over the others.

189
00:09:03,050 --> 00:09:04,650
And the third consideration

190
00:09:04,650 --> 00:09:07,620
was the feasibility of the quality attribute

191
00:09:07,620 --> 00:09:10,853
to make sure we can deliver on what we promise.

192
00:09:12,230 --> 00:09:13,913
See you guys in the next lecture.

