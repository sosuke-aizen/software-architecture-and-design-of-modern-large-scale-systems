1
00:00:00,000 --> 00:00:01,373
Welcome back.

2
00:00:02,800 --> 00:00:05,860
In this lecture, we're going to continue our discussion

3
00:00:05,860 --> 00:00:07,630
about systems requirements

4
00:00:07,630 --> 00:00:11,250
and learn about a formal step by step method together

5
00:00:11,250 --> 00:00:14,763
and visualize the functional requirements of our system.

6
00:00:16,010 --> 00:00:20,050
So let's go ahead and learn about a step by step way

7
00:00:20,050 --> 00:00:23,273
to capture all the functional requirements of our system.

8
00:00:24,740 --> 00:00:27,440
In the previous lecture, we talked about the importance

9
00:00:27,440 --> 00:00:30,250
of gathering all the requirements for our system

10
00:00:30,250 --> 00:00:33,620
before starting designing our system architecture.

11
00:00:33,620 --> 00:00:35,890
And we also talked about the challenges

12
00:00:35,890 --> 00:00:37,780
in capturing those requirements

13
00:00:37,780 --> 00:00:40,913
which involve ambiguity as well as the large scope.

14
00:00:41,840 --> 00:00:44,240
So one native way we can go about it,

15
00:00:44,240 --> 00:00:46,420
is to just ask the client to describe

16
00:00:46,420 --> 00:00:48,610
everything they need our system to do

17
00:00:48,610 --> 00:00:51,870
hoping that they do not forget any detail.

18
00:00:51,870 --> 00:00:55,410
However, for complex systems with many features

19
00:00:55,410 --> 00:00:57,360
and multiple actors involved,

20
00:00:57,360 --> 00:00:58,933
this is not a good approach.

21
00:01:00,280 --> 00:01:02,540
A more powerful and methodical way

22
00:01:02,540 --> 00:01:04,900
of gathering requirements and capturing

23
00:01:04,900 --> 00:01:07,550
the important features of the desired system,

24
00:01:07,550 --> 00:01:10,670
is through use cases and user flows.

25
00:01:10,670 --> 00:01:14,500
A use case is a particular situation or scenario

26
00:01:14,500 --> 00:01:18,750
in which our system is used to achieve a user's goal.

27
00:01:18,750 --> 00:01:22,130
A user flow is a more detailed step by step

28
00:01:22,130 --> 00:01:25,763
or graphical representation of each such use case.

29
00:01:27,200 --> 00:01:30,500
So the steps in capturing all the functional requirements

30
00:01:30,500 --> 00:01:33,220
in a formal way are as follows:

31
00:01:33,220 --> 00:01:36,240
first, we need to identify all the actors

32
00:01:36,240 --> 00:01:38,530
or users in our system

33
00:01:38,530 --> 00:01:41,100
otherwise we will likely not capture

34
00:01:41,100 --> 00:01:43,370
all their relevant use cases.

35
00:01:43,370 --> 00:01:45,490
The second step is to describe

36
00:01:45,490 --> 00:01:48,530
all the possible use cases or scenarios

37
00:01:48,530 --> 00:01:52,710
in which an actor or user can use our system.

38
00:01:52,710 --> 00:01:56,260
And finally, the third step is to take each use case

39
00:01:56,260 --> 00:01:59,010
and expand it through a flow of events

40
00:01:59,010 --> 00:02:02,680
or interactions between the actor and our system

41
00:02:02,680 --> 00:02:05,250
and in each event in this interaction

42
00:02:05,250 --> 00:02:09,070
we capture the action and also take note of the data

43
00:02:09,070 --> 00:02:12,493
that flows with it to, and from our system.

44
00:02:13,870 --> 00:02:17,130
If we continue with the hitchhiking service example

45
00:02:17,130 --> 00:02:18,930
we mentioned in the previous lecture,

46
00:02:18,930 --> 00:02:21,240
that allows people to join drivers

47
00:02:21,240 --> 00:02:24,160
that are already driving on a particular route

48
00:02:24,160 --> 00:02:28,150
and are willing to take passengers on their way for a fee

49
00:02:28,150 --> 00:02:31,210
in this case, we only have two actors,

50
00:02:31,210 --> 00:02:34,533
the driver willing to take passengers and the rider.

51
00:02:35,940 --> 00:02:38,130
Now, let's take the second step

52
00:02:38,130 --> 00:02:40,950
and find all the use cases where a driver

53
00:02:40,950 --> 00:02:44,830
or a rider interacts with our hitchhiking service.

54
00:02:44,830 --> 00:02:48,010
For example, one use case is the registration

55
00:02:48,010 --> 00:02:51,290
of a new rider to our system the first time.

56
00:02:51,290 --> 00:02:54,380
Similarly, we have a registration of a new driver

57
00:02:54,380 --> 00:02:57,280
to our service, it's another use case.

58
00:02:57,280 --> 00:03:00,760
Another use case is logging in of an existing user

59
00:03:00,760 --> 00:03:03,330
to our system to initiate a ride.

60
00:03:03,330 --> 00:03:07,250
Similarly, we have the use case where a driver logs in

61
00:03:07,250 --> 00:03:09,950
and is willing to pick up a rider.

62
00:03:09,950 --> 00:03:11,870
Then we have a successful match

63
00:03:11,870 --> 00:03:13,630
between a rider and a driver,

64
00:03:13,630 --> 00:03:15,940
which starts and completes a ride

65
00:03:15,940 --> 00:03:18,010
and finally, we have the use case

66
00:03:18,010 --> 00:03:20,120
where we an unsuccessful match

67
00:03:20,120 --> 00:03:21,480
where in other words,

68
00:03:21,480 --> 00:03:24,080
a rider could not find a driver

69
00:03:24,080 --> 00:03:26,340
that is willing to pick him up.

70
00:03:26,340 --> 00:03:28,970
Of course, these are not all the use cases

71
00:03:28,970 --> 00:03:30,093
but we get the idea.

72
00:03:31,490 --> 00:03:33,710
Now let's walk through an example

73
00:03:33,710 --> 00:03:36,130
of expanding the most interesting use case

74
00:03:36,130 --> 00:03:39,533
of a successful match between a rider and a driver.

75
00:03:41,020 --> 00:03:43,500
There are many ways we can represent the flow

76
00:03:43,500 --> 00:03:45,570
of actions in a user flow,

77
00:03:45,570 --> 00:03:47,420
the way we're going to represent it

78
00:03:47,420 --> 00:03:49,823
is through what's called a sequence diagram.

79
00:03:51,210 --> 00:03:54,800
Generally, a sequence diagram is a type of diagram

80
00:03:54,800 --> 00:03:59,070
that represents the interactions between actors and objects.

81
00:03:59,070 --> 00:04:02,970
This type of diagram is part of a Unified Modeling Language

82
00:04:02,970 --> 00:04:06,043
which is a standard for visualizing system design.

83
00:04:07,520 --> 00:04:10,460
It's important to point out that in practice,

84
00:04:10,460 --> 00:04:14,550
UML diagrams are used mostly for software design

85
00:04:14,550 --> 00:04:16,550
and there is no real standard

86
00:04:16,550 --> 00:04:20,370
for representing software architecture in a high level.

87
00:04:20,370 --> 00:04:23,100
Furthermore, UML as a language

88
00:04:23,100 --> 00:04:25,990
is not strictly followed in the industry

89
00:04:25,990 --> 00:04:29,380
but sequence diagrams are still frequently used

90
00:04:29,380 --> 00:04:32,200
to represent interactions between different entities

91
00:04:32,200 --> 00:04:34,593
in the system and not just objects.

92
00:04:36,180 --> 00:04:37,670
In a sequence diagram,

93
00:04:37,670 --> 00:04:40,270
the time goes from top to bottom

94
00:04:40,270 --> 00:04:44,280
and each entity is represented as a vertical line.

95
00:04:44,280 --> 00:04:46,480
The communication between the entities

96
00:04:46,480 --> 00:04:48,380
is represented by arrows,

97
00:04:48,380 --> 00:04:51,710
going from one entity to another entity

98
00:04:51,710 --> 00:04:54,840
while responses are represented as broken lines

99
00:04:54,840 --> 00:04:58,303
going from the destination entity to the caller entity.

100
00:04:59,910 --> 00:05:02,160
So now let's walk through an example

101
00:05:02,160 --> 00:05:04,420
of expanding the most interesting use case

102
00:05:04,420 --> 00:05:05,980
from the hitchhiking service

103
00:05:05,980 --> 00:05:09,560
the one that are represents a successful match and the ride

104
00:05:09,560 --> 00:05:11,480
starting with the ride initiation

105
00:05:11,480 --> 00:05:13,690
and define its entire user flow

106
00:05:13,690 --> 00:05:15,113
in the sequence diagram.

107
00:05:16,370 --> 00:05:18,510
We start with particular driver

108
00:05:18,510 --> 00:05:20,970
that already logged into our service

109
00:05:20,970 --> 00:05:23,000
and makes a call to our system

110
00:05:23,000 --> 00:05:24,920
that they are on a particular route

111
00:05:24,920 --> 00:05:27,580
and they're ready to pick up a rider.

112
00:05:27,580 --> 00:05:31,550
Then a rider who's also already logged into our system

113
00:05:31,550 --> 00:05:36,150
sends us a message telling us about his origin, destination

114
00:05:36,150 --> 00:05:40,450
and also that they're looking for a driver to pick them up.

115
00:05:40,450 --> 00:05:42,710
Please note that in this diagram,

116
00:05:42,710 --> 00:05:45,000
we're not going to indicate the data

117
00:05:45,000 --> 00:05:46,750
that is sent in each message

118
00:05:46,750 --> 00:05:50,483
but we will take note of that data for the future.

119
00:05:51,920 --> 00:05:54,600
Now at this point, our system tries

120
00:05:54,600 --> 00:05:57,530
to match that rider to an existing driver

121
00:05:57,530 --> 00:05:58,980
and if a match found,

122
00:05:58,980 --> 00:06:01,823
both the rider and the driver are notified.

123
00:06:03,120 --> 00:06:06,110
And finally when the driver makes his way

124
00:06:06,110 --> 00:06:08,710
to the rider's location and picks him up,

125
00:06:08,710 --> 00:06:10,410
they will start the ride

126
00:06:10,410 --> 00:06:13,830
also at that point, the rider will be notified

127
00:06:13,830 --> 00:06:16,823
that the ride has began as a form of confirmation.

128
00:06:18,300 --> 00:06:20,740
Now let's fast forward to the point

129
00:06:20,740 --> 00:06:23,510
where the driver arrives at the destination

130
00:06:23,510 --> 00:06:25,030
and finishes the ride

131
00:06:25,030 --> 00:06:28,810
and when that happens a message is sent to our system.

132
00:06:28,810 --> 00:06:32,430
Once we get this message, our system charges the rider

133
00:06:32,430 --> 00:06:36,150
from their bank account and sends the receipt to the rider.

134
00:06:36,150 --> 00:06:39,650
After that our system takes a portion of that charge

135
00:06:39,650 --> 00:06:43,010
as a service fee and credits the driver's account

136
00:06:43,010 --> 00:06:45,330
with the rest of the money from the rider.

137
00:06:45,330 --> 00:06:47,900
Finally, the driver is notified

138
00:06:47,900 --> 00:06:50,023
about the new credit in their account.

139
00:06:51,560 --> 00:06:55,960
Now a great side benefit of capturing all those interactions

140
00:06:55,960 --> 00:06:59,760
between the actors in our system as part of our user flow

141
00:06:59,760 --> 00:07:02,690
is that we can easily take this user flow

142
00:07:02,690 --> 00:07:06,170
and identify the future API of our system

143
00:07:06,170 --> 00:07:08,660
because essentially each interaction

144
00:07:08,660 --> 00:07:12,170
is an API call between an actor and our system

145
00:07:12,170 --> 00:07:15,810
and the data flowing between those actors and our system

146
00:07:15,810 --> 00:07:19,420
is essentially the arguments in those API calls.

147
00:07:19,420 --> 00:07:22,853
But we'll talk about designing APIs in a different lecture.

148
00:07:24,300 --> 00:07:27,100
In this lecture, we learned about a formal way

149
00:07:27,100 --> 00:07:30,010
to capture and document all the features

150
00:07:30,010 --> 00:07:32,870
and functional requirements of our system.

151
00:07:32,870 --> 00:07:35,880
This formal three step process involves

152
00:07:35,880 --> 00:07:38,720
identifying all the users and actors,

153
00:07:38,720 --> 00:07:40,490
gathering all the use cases

154
00:07:40,490 --> 00:07:42,930
where the actors interact with our system,

155
00:07:42,930 --> 00:07:46,330
and finally expanding upon each use case

156
00:07:46,330 --> 00:07:49,130
by describing the entire flow of interactions

157
00:07:49,130 --> 00:07:51,890
between the actors in our system.

158
00:07:51,890 --> 00:07:55,430
Also, we learned a way to visualize those interactions

159
00:07:55,430 --> 00:07:58,310
using a very common and useful diagram

160
00:07:58,310 --> 00:07:59,823
called sequence diagram.

161
00:08:01,520 --> 00:08:03,370
See you guys all in the next lecture.

