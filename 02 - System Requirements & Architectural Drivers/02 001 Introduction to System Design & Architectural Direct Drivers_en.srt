1
00:00:00,630 --> 00:00:02,023
Welcome back.

2
00:00:03,210 --> 00:00:06,080
In this lecture, we're going to talk about gathering,

3
00:00:06,080 --> 00:00:08,890
classifying and analyzing requirements

4
00:00:08,890 --> 00:00:12,920
as the first step in designing a large scale system.

5
00:00:12,920 --> 00:00:15,330
But before we get into any details,

6
00:00:15,330 --> 00:00:17,133
let's first get some motivation.

7
00:00:18,580 --> 00:00:22,710
System requirements is just a formal word for figuring out

8
00:00:22,710 --> 00:00:25,590
and narrowing down what exactly we need to build

9
00:00:25,590 --> 00:00:27,200
for our client.

10
00:00:27,200 --> 00:00:28,530
As software engineers,

11
00:00:28,530 --> 00:00:31,490
we shouldn't be new to receiving informal requirements

12
00:00:31,490 --> 00:00:33,510
for the task we need to accomplish.

13
00:00:33,510 --> 00:00:36,700
But when it comes to the design of a large scale system,

14
00:00:36,700 --> 00:00:38,220
there are a few differences

15
00:00:38,220 --> 00:00:40,970
from the usual requirements we are used to getting

16
00:00:40,970 --> 00:00:44,223
for implementing a method, an algorithm or a class.

17
00:00:46,150 --> 00:00:48,150
The first difference is the scope

18
00:00:48,150 --> 00:00:50,720
and level of obstruction of the requirements

19
00:00:50,720 --> 00:00:54,100
and especially the solution we need to design.

20
00:00:54,100 --> 00:00:57,270
For example, when we're tasked to implement a method

21
00:00:57,270 --> 00:00:59,670
or an algorithm in an existing code base,

22
00:00:59,670 --> 00:01:03,420
we typically know what the input and output look like.

23
00:01:03,420 --> 00:01:05,450
We are also somewhat limited

24
00:01:05,450 --> 00:01:08,790
to the programming languages we're going to use.

25
00:01:08,790 --> 00:01:11,640
As we go up to a more high level obstruction

26
00:01:11,640 --> 00:01:13,810
like designing a class, a module,

27
00:01:13,810 --> 00:01:15,860
a library or an application,

28
00:01:15,860 --> 00:01:17,410
the range of possible ways

29
00:01:17,410 --> 00:01:20,230
to solve the problem becomes bigger and bigger

30
00:01:20,230 --> 00:01:22,930
as we have more degrees of freedom.

31
00:01:22,930 --> 00:01:26,200
Also the scope of the problem becomes so high

32
00:01:26,200 --> 00:01:29,443
that it's hard for us to even visualize the implementation.

33
00:01:30,530 --> 00:01:33,300
So when we are asked to design an entire system

34
00:01:33,300 --> 00:01:35,350
we may feel so overwhelmed

35
00:01:35,350 --> 00:01:37,513
that we may not even know where to begin.

36
00:01:38,570 --> 00:01:39,530
For example,

37
00:01:39,530 --> 00:01:42,770
imagine you are asked to design a file storage system,

38
00:01:42,770 --> 00:01:45,900
a video streaming solution, or a ride sharing service,

39
00:01:45,900 --> 00:01:49,390
it needs to scale to serving millions of users per day.

40
00:01:49,390 --> 00:01:52,123
It's hard not to get overwhelmed by such a task.

41
00:01:53,810 --> 00:01:55,730
The second challenge and difference

42
00:01:55,730 --> 00:01:59,560
from what we're normally used to is the level of ambiguity.

43
00:01:59,560 --> 00:02:02,600
The reason for this ambiguity is twofold.

44
00:02:02,600 --> 00:02:04,660
First of all, in many cases,

45
00:02:04,660 --> 00:02:07,800
the requirements are not even coming from an engineer,

46
00:02:07,800 --> 00:02:10,000
and sometimes they're not even coming

47
00:02:10,000 --> 00:02:12,090
from a very technical person.

48
00:02:12,090 --> 00:02:14,490
So the client or product manager

49
00:02:14,490 --> 00:02:16,930
may ask for something very high level.

50
00:02:16,930 --> 00:02:20,510
And it's our responsibility to transform those requests

51
00:02:20,510 --> 00:02:23,370
into precise and technical requirements.

52
00:02:23,370 --> 00:02:25,740
Those technical requirements will serve

53
00:02:25,740 --> 00:02:29,300
as the foundation for building our software architecture.

54
00:02:29,300 --> 00:02:32,270
The second reason for the high level of ambiguity

55
00:02:32,270 --> 00:02:34,400
is getting the specific requirements

56
00:02:34,400 --> 00:02:36,690
is already part of the solution.

57
00:02:36,690 --> 00:02:39,450
This may seem a little odd for us at first,

58
00:02:39,450 --> 00:02:40,860
but we have to remember

59
00:02:40,860 --> 00:02:44,750
that the client doesn't always know exactly what they need.

60
00:02:44,750 --> 00:02:47,350
The only thing they know for sure is the problem

61
00:02:47,350 --> 00:02:49,070
they need to solve.

62
00:02:49,070 --> 00:02:51,993
To demonstrate this, let's take a specific example.

63
00:02:53,980 --> 00:02:57,120
Let's say we're asked to design a hitch hiking service

64
00:02:57,120 --> 00:03:00,670
that allows people to join drivers that are already driving

65
00:03:00,670 --> 00:03:02,200
on a particular route,

66
00:03:02,200 --> 00:03:05,620
and are willing to take passengers on their way for a fee.

67
00:03:05,620 --> 00:03:09,060
That may be all the requirements we get from the client,

68
00:03:09,060 --> 00:03:11,700
and it is up to us to ask things like,

69
00:03:11,700 --> 00:03:13,870
is it going to be a real time service

70
00:03:13,870 --> 00:03:17,350
or the riders will have to contact the drivers in advance,

71
00:03:17,350 --> 00:03:18,890
is it going to be a mobile

72
00:03:18,890 --> 00:03:21,460
or desktop experience or maybe both,

73
00:03:21,460 --> 00:03:24,370
are we going to enable payment through our system

74
00:03:24,370 --> 00:03:27,840
or the riders will have to pay the drivers directly?

75
00:03:27,840 --> 00:03:31,390
In some cases, the client may not even know the answer

76
00:03:31,390 --> 00:03:33,070
to those questions right away

77
00:03:33,070 --> 00:03:35,640
until we actually ask those questions.

78
00:03:35,640 --> 00:03:39,010
That is why in most system design interviews,

79
00:03:39,010 --> 00:03:42,810
one of the things being tested is our ability to clarify

80
00:03:42,810 --> 00:03:45,410
and ask those questions ahead of time.

81
00:03:45,410 --> 00:03:46,860
Because as we can see,

82
00:03:46,860 --> 00:03:49,760
asking those questions and gathering those requirements

83
00:03:49,760 --> 00:03:53,630
is already part of the solution and it greatly narrows down

84
00:03:53,630 --> 00:03:55,483
what we need to design and build.

85
00:03:56,910 --> 00:03:58,190
Now, what happens

86
00:03:58,190 --> 00:04:01,110
if we don't get all the requirements right?

87
00:04:01,110 --> 00:04:03,200
Can't we just do it incrementally?

88
00:04:03,200 --> 00:04:05,250
Why can't we simply build something

89
00:04:05,250 --> 00:04:07,310
and see if it satisfies the client?

90
00:04:07,310 --> 00:04:09,040
And if it doesn't, no big deal.

91
00:04:09,040 --> 00:04:11,060
We can just fix it, can't we?

92
00:04:11,060 --> 00:04:14,430
After all, we're not building a building or a bridge here.

93
00:04:14,430 --> 00:04:18,000
There's similarly no big cost of materials in software,

94
00:04:18,000 --> 00:04:20,220
like in mechanical engineering, for example,

95
00:04:20,220 --> 00:04:21,803
or construction of a building.

96
00:04:23,050 --> 00:04:25,720
Well, the mental leap we need to make here

97
00:04:25,720 --> 00:04:29,440
from small projects like building a method or a few classes

98
00:04:29,440 --> 00:04:32,280
where we can easily rewrite the code many times

99
00:04:32,280 --> 00:04:33,600
until we get it right,

100
00:04:33,600 --> 00:04:36,580
large scale systems are big projects

101
00:04:36,580 --> 00:04:39,920
that cannot be changed easily overnight.

102
00:04:39,920 --> 00:04:42,500
Those projects take many engineers,

103
00:04:42,500 --> 00:04:45,230
sometimes even multiple teams of engineers.

104
00:04:45,230 --> 00:04:46,860
They can take months to build,

105
00:04:46,860 --> 00:04:50,780
which makes the cost of engineering time very significant.

106
00:04:50,780 --> 00:04:53,730
They also often require purchasing hardware

107
00:04:53,730 --> 00:04:56,380
and sometimes software licenses upfront.

108
00:04:56,380 --> 00:04:59,430
And those projects typically involve contracts

109
00:04:59,430 --> 00:05:02,960
with time commitments and financial obligations.

110
00:05:02,960 --> 00:05:06,190
Also not delivering the product to our clients

111
00:05:06,190 --> 00:05:10,000
or users on time may cause irreversible damage

112
00:05:10,000 --> 00:05:12,950
to our company's reputation and brand image,

113
00:05:12,950 --> 00:05:15,690
so getting the requirements right upfront

114
00:05:15,690 --> 00:05:17,263
is absolutely critical.

115
00:05:18,530 --> 00:05:21,960
So now that we got the motivation for system requirements,

116
00:05:21,960 --> 00:05:23,660
let's take a step further

117
00:05:23,660 --> 00:05:25,760
and learn to classify the requirements

118
00:05:25,760 --> 00:05:27,200
into a few categories,

119
00:05:27,200 --> 00:05:29,220
which have completely different effects

120
00:05:29,220 --> 00:05:31,173
on our architecture and design.

121
00:05:32,660 --> 00:05:34,679
The three main types of requirements

122
00:05:34,679 --> 00:05:36,760
are features of the system,

123
00:05:36,760 --> 00:05:40,550
also known as functional requirements, quality attributes,

124
00:05:40,550 --> 00:05:43,840
which are also known as non-functional requirements,

125
00:05:43,840 --> 00:05:47,230
and system constraints, which basically the limitations

126
00:05:47,230 --> 00:05:49,710
and boundaries of the system.

127
00:05:49,710 --> 00:05:52,600
Let's start talking about the first type of requirements,

128
00:05:52,600 --> 00:05:54,053
the features of the system.

129
00:05:55,720 --> 00:05:58,370
Those requirements essentially describe

130
00:05:58,370 --> 00:05:59,570
the system's behavior.

131
00:05:59,570 --> 00:06:00,620
In other words,

132
00:06:00,620 --> 00:06:04,260
what the system we're designing actually does.

133
00:06:04,260 --> 00:06:06,650
Those requirements are easily tied

134
00:06:06,650 --> 00:06:08,780
to the objective of the system.

135
00:06:08,780 --> 00:06:11,430
They are also called functional requirements

136
00:06:11,430 --> 00:06:13,920
because they essentially describe our system

137
00:06:13,920 --> 00:06:15,650
as a black box function.

138
00:06:15,650 --> 00:06:19,260
The user's input or external events are the inputs

139
00:06:19,260 --> 00:06:20,330
to that function,

140
00:06:20,330 --> 00:06:23,740
and the result or outcome of the operation taken

141
00:06:23,740 --> 00:06:26,593
by our system is the output of that function.

142
00:06:28,160 --> 00:06:30,920
It's important to point out that the features

143
00:06:30,920 --> 00:06:34,790
or functional requirements simply dictate the functionality

144
00:06:34,790 --> 00:06:38,530
of our system but do not determine its architecture.

145
00:06:38,530 --> 00:06:42,380
And generally any architecture can achieve any feature,

146
00:06:42,380 --> 00:06:43,860
which is what makes our job

147
00:06:43,860 --> 00:06:46,283
as software architects so difficult.

148
00:06:47,840 --> 00:06:50,210
Now, let's look at a few examples

149
00:06:50,210 --> 00:06:52,770
of features or functional requirements

150
00:06:52,770 --> 00:06:55,450
in our hitchhiking service example.

151
00:06:55,450 --> 00:06:58,210
One functional requirement can be as follows,

152
00:06:58,210 --> 00:07:00,880
when a rider logs into our mobile app,

153
00:07:00,880 --> 00:07:04,730
the system must display a map with nearby drivers

154
00:07:04,730 --> 00:07:06,871
within five miles radius.

155
00:07:06,871 --> 00:07:11,520
Here the input to our system is the user's login action,

156
00:07:11,520 --> 00:07:15,563
and the output is the view of a map with nearby drivers.

157
00:07:16,930 --> 00:07:19,170
Let's take another example.

158
00:07:19,170 --> 00:07:20,700
When the ride is complete,

159
00:07:20,700 --> 00:07:23,790
the system will charge the rider's credit card

160
00:07:23,790 --> 00:07:26,210
and transfer that money to the driver

161
00:07:26,210 --> 00:07:28,340
minus some service fees.

162
00:07:28,340 --> 00:07:31,830
Here the completion of the ride is the input event,

163
00:07:31,830 --> 00:07:33,470
and the transfer of the money

164
00:07:33,470 --> 00:07:35,463
is the outcome of the operation.

165
00:07:36,920 --> 00:07:39,790
Now, let's talk about the second type of requirements,

166
00:07:39,790 --> 00:07:43,393
the quality attributes or non-functional requirements.

167
00:07:44,970 --> 00:07:47,370
The quality attributes are properties

168
00:07:47,370 --> 00:07:48,990
that the system must have

169
00:07:48,990 --> 00:07:51,580
as opposed to what the system must do.

170
00:07:51,580 --> 00:07:54,750
Examples of such qualities include scalability,

171
00:07:54,750 --> 00:07:58,900
availability, reliability, security, performance,

172
00:07:58,900 --> 00:08:02,543
and this list can go on and change depending on the system.

173
00:08:03,750 --> 00:08:06,200
In contrast to the functional requirements,

174
00:08:06,200 --> 00:08:09,900
the quality attributes do dictate the software architecture

175
00:08:09,900 --> 00:08:11,320
of our system.

176
00:08:11,320 --> 00:08:14,370
Another way to look at this is, the software architecture

177
00:08:14,370 --> 00:08:17,010
defines the system quality attributes.

178
00:08:17,010 --> 00:08:19,390
And different architectures provide us

179
00:08:19,390 --> 00:08:21,363
with different quality attributes.

180
00:08:22,580 --> 00:08:25,913
The last type of requirements is system constraints.

181
00:08:27,540 --> 00:08:31,580
A few examples include strict deadlines, limited budget,

182
00:08:31,580 --> 00:08:35,320
or a small number of engineers that can work on our project.

183
00:08:35,320 --> 00:08:39,159
Such constraints may force us to make certain trade offs

184
00:08:39,159 --> 00:08:42,330
and sacrifices in our design and shift us

185
00:08:42,330 --> 00:08:45,100
towards certain software architectural decisions

186
00:08:45,100 --> 00:08:48,743
that we would not make if we didn't have those constraints.

187
00:08:50,120 --> 00:08:52,560
The three types of requirements that we learned

188
00:08:52,560 --> 00:08:55,180
in this lecture are also sometimes referred to

189
00:08:55,180 --> 00:08:56,900
as architectural drivers.

190
00:08:56,900 --> 00:09:00,580
Because they essentially drive our architectural decisions

191
00:09:00,580 --> 00:09:03,120
from an infinite universe of possibilities

192
00:09:03,120 --> 00:09:06,963
towards one solution that satisfies our clients' needs.

193
00:09:08,490 --> 00:09:09,590
In this lecture,

194
00:09:09,590 --> 00:09:12,140
we learned about the importance of requirements

195
00:09:12,140 --> 00:09:14,900
in the design of large scale systems.

196
00:09:14,900 --> 00:09:16,540
We discussed the few challenges

197
00:09:16,540 --> 00:09:18,440
of gathering those requirements,

198
00:09:18,440 --> 00:09:19,860
such as the large scope

199
00:09:19,860 --> 00:09:23,410
and ambiguity that we may not be familiar with

200
00:09:23,410 --> 00:09:26,780
coming from a smaller scale programming world.

201
00:09:26,780 --> 00:09:29,040
After that, we talked about the risks

202
00:09:29,040 --> 00:09:32,100
of not getting those requirements correctly upfront.

203
00:09:32,100 --> 00:09:32,960
And finally,

204
00:09:32,960 --> 00:09:35,970
we classified those requirements into three groups,

205
00:09:35,970 --> 00:09:38,420
which are also called the architectural drivers

206
00:09:38,420 --> 00:09:39,590
of the system.

207
00:09:39,590 --> 00:09:42,530
Those three groups are the features of the system,

208
00:09:42,530 --> 00:09:45,543
the quality attributes and the systems constraints.

209
00:09:46,910 --> 00:09:48,760
See you guys all in the next lecture.

