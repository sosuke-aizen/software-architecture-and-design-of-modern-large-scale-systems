1
00:00:00,260 --> 00:00:01,510
Welcome back.

2
00:00:02,580 --> 00:00:03,650
In this lecture,

3
00:00:03,650 --> 00:00:06,720
we're going to talk about the third architectural driver

4
00:00:06,720 --> 00:00:10,070
and type of requirement: the system constraints.

5
00:00:10,070 --> 00:00:12,750
We're going to talk about a few types of constraints

6
00:00:12,750 --> 00:00:14,580
classified by their source

7
00:00:14,580 --> 00:00:17,670
and also the effects that the system constraints have

8
00:00:17,670 --> 00:00:19,920
on our software architecture.

9
00:00:19,920 --> 00:00:23,000
Finally, we will talk about a few considerations

10
00:00:23,000 --> 00:00:24,700
that we need to take into account

11
00:00:24,700 --> 00:00:26,733
when we deal with system constraints.

12
00:00:27,890 --> 00:00:30,543
So what do we mean by system constraints?

13
00:00:31,610 --> 00:00:34,950
Typically, once we define the functional requirements

14
00:00:34,950 --> 00:00:36,680
and what our system must do,

15
00:00:36,680 --> 00:00:38,820
we have a high degree of freedom

16
00:00:38,820 --> 00:00:41,340
on how to structure our system.

17
00:00:41,340 --> 00:00:45,100
In other words, while defining the final architecture,

18
00:00:45,100 --> 00:00:47,580
we have to make a lot of decisions

19
00:00:47,580 --> 00:00:50,190
because we typically have many ways

20
00:00:50,190 --> 00:00:52,630
to achieve the desired quality attributes

21
00:00:52,630 --> 00:00:54,980
and the desired functionality.

22
00:00:54,980 --> 00:00:58,010
Specifically when it comes to quality attributes,

23
00:00:58,010 --> 00:01:00,770
we are allowed and are typically expected

24
00:01:00,770 --> 00:01:02,063
to make tradeoffs.

25
00:01:03,120 --> 00:01:06,650
So a system constraint is essentially a decision

26
00:01:06,650 --> 00:01:11,090
that was already either fully or partially made for us,

27
00:01:11,090 --> 00:01:13,900
restricting our degrees of freedom.

28
00:01:13,900 --> 00:01:16,163
But this is not always a bad thing.

29
00:01:17,290 --> 00:01:19,080
Essentially, what I'm alluding to

30
00:01:19,080 --> 00:01:21,190
is instead of looking at a constraint

31
00:01:21,190 --> 00:01:22,850
from a negative connotation

32
00:01:22,850 --> 00:01:25,660
as a choice that was taken away from us,

33
00:01:25,660 --> 00:01:29,880
we can also look at it as a decision that was made for us

34
00:01:29,880 --> 00:01:32,950
which sometimes makes our job easier.

35
00:01:32,950 --> 00:01:36,900
That's why sometimes people refer to system constraints

36
00:01:36,900 --> 00:01:40,350
as the pillars for our software architecture.

37
00:01:40,350 --> 00:01:43,080
The reason for that is that system constraints

38
00:01:43,080 --> 00:01:45,950
provide us with a solid starting point.

39
00:01:45,950 --> 00:01:49,400
And since usually constraints are non-negotiable,

40
00:01:49,400 --> 00:01:52,690
we sort of need to design the rest of the system

41
00:01:52,690 --> 00:01:54,253
around those constraints.

42
00:01:55,160 --> 00:01:58,523
So what type of constraints can we have in our system?

43
00:01:59,410 --> 00:02:02,520
Generally, there are three types of constraints:

44
00:02:02,520 --> 00:02:05,130
technical constraints, business constraints,

45
00:02:05,130 --> 00:02:06,823
and legal constraints.

46
00:02:07,790 --> 00:02:09,280
As software engineers,

47
00:02:09,280 --> 00:02:12,230
technical constraints are something that we deal with

48
00:02:12,230 --> 00:02:15,433
every day, so it's not something that is new to us.

49
00:02:16,440 --> 00:02:18,610
Examples of technical constraints

50
00:02:18,610 --> 00:02:21,990
include things like being locked to a particular hardware

51
00:02:21,990 --> 00:02:24,930
or cloud vendor because of an existing contract

52
00:02:24,930 --> 00:02:26,510
or security reasons,

53
00:02:26,510 --> 00:02:29,120
having to use a particular programming language

54
00:02:29,120 --> 00:02:31,840
because of existing frameworks we built in-house

55
00:02:31,840 --> 00:02:35,000
or maybe difficulty hiring engineers,

56
00:02:35,000 --> 00:02:38,280
having to use a particular database or technology

57
00:02:38,280 --> 00:02:41,650
or having to support certain platforms, browsers,

58
00:02:41,650 --> 00:02:45,870
or operating systems because of licenses, support cost,

59
00:02:45,870 --> 00:02:49,583
or maybe because our clients specifically ask for that.

60
00:02:50,630 --> 00:02:51,810
On the surface,

61
00:02:51,810 --> 00:02:55,190
technical constraints may all seem like they belong

62
00:02:55,190 --> 00:02:57,160
to the realm of implementation

63
00:02:57,160 --> 00:02:59,320
and not the software architecture.

64
00:02:59,320 --> 00:03:02,770
But unfortunately in practice, in many cases,

65
00:03:02,770 --> 00:03:06,600
they do affect the decisions we make in the design phase

66
00:03:06,600 --> 00:03:09,543
and put restrictions on our architecture.

67
00:03:10,520 --> 00:03:13,660
For example, if our company made the decision

68
00:03:13,660 --> 00:03:18,030
to run all our services on-premise or our own data centers,

69
00:03:18,030 --> 00:03:21,890
maybe for security reasons or financial reasons,

70
00:03:21,890 --> 00:03:25,140
then all the cloud architectures and paradigms

71
00:03:25,140 --> 00:03:27,980
like out of scaling or function as a service

72
00:03:27,980 --> 00:03:32,440
may be either unavailable to us or are very hard to achieve

73
00:03:32,440 --> 00:03:34,260
because we would have to implement

74
00:03:34,260 --> 00:03:36,893
a lot of non-trivial infrastructure.

75
00:03:37,970 --> 00:03:42,060
Another example is if we have to support some older browsers

76
00:03:42,060 --> 00:03:45,330
or low-end mobile devices that don't support

77
00:03:45,330 --> 00:03:47,350
certain communication protocols

78
00:03:47,350 --> 00:03:49,960
or can't handle as much data,

79
00:03:49,960 --> 00:03:53,580
we would have to adopt our architecture significantly

80
00:03:53,580 --> 00:03:56,650
to support those platforms and their APIs

81
00:03:56,650 --> 00:04:00,060
while providing a different, more high-end experience

82
00:04:00,060 --> 00:04:03,453
for newer browsers or higher-end devices.

83
00:04:04,420 --> 00:04:07,430
Now let's talk about the second type of constraint

84
00:04:07,430 --> 00:04:09,223
that come from our business.

85
00:04:10,230 --> 00:04:13,990
Typically as engineers, we want to make the right decisions

86
00:04:13,990 --> 00:04:17,430
and architectural choices from a technical perspective,

87
00:04:17,430 --> 00:04:20,490
but we're not always able to assess

88
00:04:20,490 --> 00:04:24,980
how those decisions align with the goals of our business.

89
00:04:24,980 --> 00:04:28,300
For that reason, we frequently get constraints

90
00:04:28,300 --> 00:04:30,160
that come from our business team

91
00:04:30,160 --> 00:04:32,960
which force us to make certain sacrifices

92
00:04:32,960 --> 00:04:36,123
both in terms of architecture and implementation.

93
00:04:37,400 --> 00:04:40,200
For example, if we get a business constraint

94
00:04:40,200 --> 00:04:43,740
in a form of a limited budget or a strict deadline,

95
00:04:43,740 --> 00:04:46,220
we will make very different choices

96
00:04:46,220 --> 00:04:50,760
than if we had an unlimited budget or unlimited time.

97
00:04:50,760 --> 00:04:54,120
In fact, there are certain software architectural patterns

98
00:04:54,120 --> 00:04:56,690
that are more suitable for small startups

99
00:04:56,690 --> 00:04:59,920
that have to get to the market fast with a small team

100
00:04:59,920 --> 00:05:02,440
and there are other architectural patterns

101
00:05:02,440 --> 00:05:05,710
that fit much better with bigger organizations

102
00:05:05,710 --> 00:05:09,730
that have many engineering teams and a much larger budget.

103
00:05:09,730 --> 00:05:12,040
We'll talk about those architectural patterns

104
00:05:12,040 --> 00:05:14,713
in much more detail farther in the course.

105
00:05:15,660 --> 00:05:18,170
Besides time and budget limitations,

106
00:05:18,170 --> 00:05:20,400
we can also get a business constraint

107
00:05:20,400 --> 00:05:23,130
that tells us to use third-party services

108
00:05:23,130 --> 00:05:25,570
as part of our architecture.

109
00:05:25,570 --> 00:05:28,560
For example, if we're building an online store,

110
00:05:28,560 --> 00:05:32,180
we may want to specialize in the experience of the clients

111
00:05:32,180 --> 00:05:33,870
who shop on our platform,

112
00:05:33,870 --> 00:05:36,420
but use third-party providers

113
00:05:36,420 --> 00:05:39,800
to take care of the shipping and the billing.

114
00:05:39,800 --> 00:05:43,530
Another example is if we're building an investment platform,

115
00:05:43,530 --> 00:05:45,740
we would typically need to integrate

116
00:05:45,740 --> 00:05:48,340
with different banks, broker services,

117
00:05:48,340 --> 00:05:51,820
as well as security and fraud detection services.

118
00:05:51,820 --> 00:05:54,700
Those are all examples of business decisions

119
00:05:54,700 --> 00:05:58,590
to specialize in only one area and not get distracted

120
00:05:58,590 --> 00:06:01,650
with things that we can get from other partners.

121
00:06:01,650 --> 00:06:04,010
But all those third-party providers

122
00:06:04,010 --> 00:06:07,330
have their own APIs and architectural paradigms

123
00:06:07,330 --> 00:06:09,040
that we would have to work with

124
00:06:09,040 --> 00:06:11,593
as part of designing our own system.

125
00:06:12,660 --> 00:06:15,430
Now, finally, the third type of constraint

126
00:06:15,430 --> 00:06:18,173
comes from certain rules and regulations.

127
00:06:19,170 --> 00:06:21,320
Those constraints may be global

128
00:06:21,320 --> 00:06:24,723
or specific to a particular geographical region.

129
00:06:25,740 --> 00:06:27,910
For example, in the United States,

130
00:06:27,910 --> 00:06:29,400
if you are developing a system

131
00:06:29,400 --> 00:06:33,090
that handles medical information or patient's records,

132
00:06:33,090 --> 00:06:35,610
you need to adhere to the HIPAA regulations

133
00:06:35,610 --> 00:06:37,270
that place certain constraints

134
00:06:37,270 --> 00:06:39,660
on who can access the patient's data,

135
00:06:39,660 --> 00:06:42,270
as well as the security we need to put in place

136
00:06:42,270 --> 00:06:44,810
to store the sensitive information.

137
00:06:44,810 --> 00:06:46,410
In the European Union,

138
00:06:46,410 --> 00:06:50,540
GDPR sets certain limitations on online services

139
00:06:50,540 --> 00:06:54,180
regarding what kind of data they can collect, store,

140
00:06:54,180 --> 00:06:57,710
and share with third parties about their users.

141
00:06:57,710 --> 00:07:00,810
This regulation also has certain limitations

142
00:07:00,810 --> 00:07:04,200
on how long some data can be retained.

143
00:07:04,200 --> 00:07:05,860
So depending on the country

144
00:07:05,860 --> 00:07:08,320
where we're planning to offer our services

145
00:07:08,320 --> 00:07:11,090
and the industry that our services belong to,

146
00:07:11,090 --> 00:07:13,260
we may have different regulations

147
00:07:13,260 --> 00:07:15,943
that affect the architecture of our system.

148
00:07:16,880 --> 00:07:19,830
Now, once we identify all the constraints,

149
00:07:19,830 --> 00:07:23,023
there are two considerations that we need to keep in mind.

150
00:07:24,150 --> 00:07:26,950
The first consideration is that we shouldn't take

151
00:07:26,950 --> 00:07:29,483
any constraint that's given to us lightly.

152
00:07:30,380 --> 00:07:33,440
What I mean by that is we want to make the distinction

153
00:07:33,440 --> 00:07:35,000
between real constraints

154
00:07:35,000 --> 00:07:37,120
that there is nothing we can do about

155
00:07:37,120 --> 00:07:40,410
and self-imposed constraints that we can negotiate

156
00:07:40,410 --> 00:07:41,963
and maybe even remove.

157
00:07:42,890 --> 00:07:44,680
For example, if we're working

158
00:07:44,680 --> 00:07:47,060
with external rules and regulations,

159
00:07:47,060 --> 00:07:50,320
maybe there is not much room for us to negotiate.

160
00:07:50,320 --> 00:07:53,230
But if we're getting budget and time constraints

161
00:07:53,230 --> 00:07:56,320
from our business, maybe there is room to postpone

162
00:07:56,320 --> 00:07:59,023
some of the deadlines or stretch the budget.

163
00:07:59,950 --> 00:08:03,020
If we are currently locked to a particular hardware,

164
00:08:03,020 --> 00:08:06,090
cloud vendor or a particular set of technologies,

165
00:08:06,090 --> 00:08:08,750
maybe this project we're working on

166
00:08:08,750 --> 00:08:12,160
is the perfect opportunity to explore other options

167
00:08:12,160 --> 00:08:15,360
which may benefit our business in the long run.

168
00:08:15,360 --> 00:08:18,400
Once we agree to take on a set of constraints,

169
00:08:18,400 --> 00:08:20,580
it's very hard to back away.

170
00:08:20,580 --> 00:08:22,540
And if we find out down the line

171
00:08:22,540 --> 00:08:25,270
that those constraints were not real constraints,

172
00:08:25,270 --> 00:08:27,150
our overall architecture

173
00:08:27,150 --> 00:08:29,533
may just not make any sense anymore.

174
00:08:31,120 --> 00:08:33,830
This brings us to the second consideration

175
00:08:33,830 --> 00:08:37,100
which is that when we do take on certain constraints,

176
00:08:37,100 --> 00:08:39,990
we need to leave enough room in our architecture

177
00:08:39,990 --> 00:08:43,053
to move away from those constraints in the future.

178
00:08:43,919 --> 00:08:45,930
For example, if we are limited

179
00:08:45,930 --> 00:08:48,480
to a particular database that we need to use

180
00:08:48,480 --> 00:08:52,070
or some third-party service that we have to integrate with,

181
00:08:52,070 --> 00:08:56,280
we need to make sure our system is not tightly coupled

182
00:08:56,280 --> 00:09:00,090
to that particular technology or those specific APIs.

183
00:09:00,090 --> 00:09:01,580
And if in the future,

184
00:09:01,580 --> 00:09:04,350
we are able to use different technologies

185
00:09:04,350 --> 00:09:05,780
or different services,

186
00:09:05,780 --> 00:09:08,010
we will need to make minimal changes

187
00:09:08,010 --> 00:09:12,290
instead of re-architecting the entire system from scratch.

188
00:09:12,290 --> 00:09:15,490
When we get to the topic of architectural building blocks,

189
00:09:15,490 --> 00:09:18,510
we will see many examples of specific tools

190
00:09:18,510 --> 00:09:21,100
and general techniques on how we can decouple

191
00:09:21,100 --> 00:09:22,650
different parts of the system

192
00:09:22,650 --> 00:09:25,360
in a way that they can be easily replaced

193
00:09:25,360 --> 00:09:27,360
or updated independently

194
00:09:27,360 --> 00:09:30,433
without the need to refactor the entire system.

195
00:09:31,350 --> 00:09:32,390
In this lecture,

196
00:09:32,390 --> 00:09:35,470
we learned about the third type of architectural driver,

197
00:09:35,470 --> 00:09:37,310
the system constraints.

198
00:09:37,310 --> 00:09:40,400
We first defined system constraints as a decision

199
00:09:40,400 --> 00:09:44,090
that was already either fully or partially made for us

200
00:09:44,090 --> 00:09:46,920
restricting our degrees of freedom.

201
00:09:46,920 --> 00:09:49,220
After that, we talked about the three types

202
00:09:49,220 --> 00:09:52,180
of system constraints, which are technical constraints,

203
00:09:52,180 --> 00:09:55,330
business constraints, and legal constraints.

204
00:09:55,330 --> 00:09:58,260
And finally, we discussed the two considerations

205
00:09:58,260 --> 00:09:59,820
we need to pay attention to

206
00:09:59,820 --> 00:10:01,740
when we deal with system constraints

207
00:10:01,740 --> 00:10:03,820
which are not taking them lightly

208
00:10:03,820 --> 00:10:06,970
and making sure we don't have any tight coupling

209
00:10:06,970 --> 00:10:08,253
to those constraints.

210
00:10:09,200 --> 00:10:11,393
I will see you soon in the next lecture.

