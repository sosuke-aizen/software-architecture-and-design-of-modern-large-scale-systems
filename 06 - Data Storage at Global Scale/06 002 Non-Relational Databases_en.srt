1
00:00:00,350 --> 00:00:01,850
Hey, welcome back.

2
00:00:02,750 --> 00:00:06,030
Now that we're familiar with the first type of databases,

3
00:00:06,030 --> 00:00:07,920
which is the relational databases,

4
00:00:07,920 --> 00:00:10,600
let's talk about the second type of databases,

5
00:00:10,600 --> 00:00:14,120
which is commonly referred to as non-relational databases

6
00:00:14,120 --> 00:00:16,770
or NoSQL databases.

7
00:00:16,770 --> 00:00:20,400
We're first going to get an introduction and motivation

8
00:00:20,400 --> 00:00:22,530
for using those type of databases.

9
00:00:22,530 --> 00:00:25,190
And after that, we'll learn a few categories

10
00:00:25,190 --> 00:00:27,210
of non-relational databases

11
00:00:27,210 --> 00:00:29,733
for different use cases and purposes.

12
00:00:30,900 --> 00:00:35,037
So let's start talking about non-relational databases.

13
00:00:35,037 --> 00:00:39,390
Non-relational databases are a relatively new concept

14
00:00:39,390 --> 00:00:43,010
which became very popular in the mid-2000s.

15
00:00:43,010 --> 00:00:45,670
They mainly came to solve the drawbacks

16
00:00:45,670 --> 00:00:49,473
of relational databases, which we saw in an earlier lecture.

17
00:00:50,490 --> 00:00:54,270
For example, in a relational database, records that belong

18
00:00:54,270 --> 00:00:56,900
to the same table have the same schema,

19
00:00:56,900 --> 00:01:00,640
which means they all have the same columns.

20
00:01:00,640 --> 00:01:04,930
So if we wanted to add additional data only to some records,

21
00:01:04,930 --> 00:01:08,650
we would have to change the schema of the entire table,

22
00:01:08,650 --> 00:01:11,540
even though that data makes sense only

23
00:01:11,540 --> 00:01:13,223
for a subset of records.

24
00:01:14,360 --> 00:01:18,940
So non-relational databases solve exactly that problem.

25
00:01:18,940 --> 00:01:22,970
They generally allow to logically group a set of records

26
00:01:22,970 --> 00:01:27,100
without forcing all of them to have the same structure.

27
00:01:27,100 --> 00:01:29,960
So we can easily add additional attributes

28
00:01:29,960 --> 00:01:34,090
to one or multiple records without affecting the rest

29
00:01:34,090 --> 00:01:35,673
of the existing records.

30
00:01:36,650 --> 00:01:39,980
Another problem we had with relational databases is

31
00:01:39,980 --> 00:01:42,500
that essentially, relational databases

32
00:01:42,500 --> 00:01:46,240
only support one data structure, which is a table.

33
00:01:46,240 --> 00:01:50,200
So while tables are very natural for human beings

34
00:01:50,200 --> 00:01:53,170
to analyze records, they are less intuitive

35
00:01:53,170 --> 00:01:56,300
for programmers and programming languages.

36
00:01:56,300 --> 00:02:00,050
In fact, most programming languages don't even have a table

37
00:02:00,050 --> 00:02:01,830
as a data structure.

38
00:02:01,830 --> 00:02:05,980
Instead, programming languages support other data structures

39
00:02:05,980 --> 00:02:08,280
that are more computer-science oriented,

40
00:02:08,280 --> 00:02:11,613
like lists, arrays, maps, and so on.

41
00:02:13,100 --> 00:02:17,030
So non-relational databases don't store data in tables,

42
00:02:17,030 --> 00:02:19,680
instead, depending on the type of database,

43
00:02:19,680 --> 00:02:22,230
they support more native data structures

44
00:02:22,230 --> 00:02:24,110
to programming languages.

45
00:02:24,110 --> 00:02:27,400
This typically eliminates the need for an ORM

46
00:02:27,400 --> 00:02:29,200
to translate the business logic

47
00:02:29,200 --> 00:02:31,500
as we store it inside our program

48
00:02:31,500 --> 00:02:34,683
to what it looks like inside the relational database.

49
00:02:35,580 --> 00:02:39,480
Finally, while relational databases were originally designed

50
00:02:39,480 --> 00:02:41,880
for efficient storage, back in the days

51
00:02:41,880 --> 00:02:44,070
when the storage used to be expensive,

52
00:02:44,070 --> 00:02:47,600
non-relational databases are typically more oriented

53
00:02:47,600 --> 00:02:49,630
towards faster queries.

54
00:02:49,630 --> 00:02:50,640
And different types

55
00:02:50,640 --> 00:02:53,590
of non-relational databases are optimized

56
00:02:53,590 --> 00:02:56,793
for different types of queries based on the use case.

57
00:02:57,810 --> 00:03:01,660
Of course, everything in software engineering is a trade-off

58
00:03:01,660 --> 00:03:04,390
and switching from a relational database

59
00:03:04,390 --> 00:03:07,630
to a non-relational database is no exception.

60
00:03:07,630 --> 00:03:09,860
When we allow flexible schemas

61
00:03:09,860 --> 00:03:13,120
and don't enforce any relationship between records,

62
00:03:13,120 --> 00:03:17,220
we lose the ability to easily analyze those records.

63
00:03:17,220 --> 00:03:20,320
Because essentially, now each record can have

64
00:03:20,320 --> 00:03:22,040
a completely different structure

65
00:03:22,040 --> 00:03:24,440
and completely different data.

66
00:03:24,440 --> 00:03:26,960
Similarly, analyzing multiple groups

67
00:03:26,960 --> 00:03:30,020
of records the same way we used to join tables

68
00:03:30,020 --> 00:03:33,980
in a relational database also becomes very hard.

69
00:03:33,980 --> 00:03:36,720
Most such operations are either limited

70
00:03:36,720 --> 00:03:38,510
or aren't supported at all

71
00:03:38,510 --> 00:03:41,880
since each database supports a completely different set

72
00:03:41,880 --> 00:03:45,830
of operations and a different set of data structures.

73
00:03:45,830 --> 00:03:48,790
Finally, ACID transaction guarantees are

74
00:03:48,790 --> 00:03:52,640
also rarely supported by non-relational databases,

75
00:03:52,640 --> 00:03:55,283
though there are a few exceptions to this rule.

76
00:03:56,380 --> 00:03:58,240
So now that we got familiar

77
00:03:58,240 --> 00:04:01,400
with what non-relational databases provide to us

78
00:04:01,400 --> 00:04:04,610
and what compromises we have to make to take full advantage

79
00:04:04,610 --> 00:04:07,490
of them, let's talk about the three main types

80
00:04:07,490 --> 00:04:10,610
of non-relational databases that are commonly used

81
00:04:10,610 --> 00:04:12,200
in the industry.

82
00:04:12,200 --> 00:04:14,370
As a side note, the boundaries

83
00:04:14,370 --> 00:04:17,519
between the different categories are somewhat blurry.

84
00:04:17,519 --> 00:04:21,079
So don't be surprised if you see the same database listed

85
00:04:21,079 --> 00:04:24,123
under a different category in different sources.

86
00:04:25,270 --> 00:04:27,220
The first and simplest type

87
00:04:27,220 --> 00:04:30,743
of non-relational databases is a key/value store.

88
00:04:31,986 --> 00:04:34,110
In a key/value store, we have a key

89
00:04:34,110 --> 00:04:37,550
that uniquely identifies a record and a value

90
00:04:37,550 --> 00:04:40,813
that represents the data associated with the record.

91
00:04:41,690 --> 00:04:45,070
This value is theoretically completely opaque

92
00:04:45,070 --> 00:04:47,600
to the database and can be as simple

93
00:04:47,600 --> 00:04:52,460
as an integer or a string, or as complex as an array, a set

94
00:04:52,460 --> 00:04:54,193
or even a binary blob.

95
00:04:55,090 --> 00:04:57,490
It's easy to think of a key/value store

96
00:04:57,490 --> 00:05:01,230
as essentially a large-scale hashtable or dictionary

97
00:05:01,230 --> 00:05:03,370
with very few constraints on the type

98
00:05:03,370 --> 00:05:06,690
of values we have for each key.

99
00:05:06,690 --> 00:05:09,070
Key/value stores are a perfect choice

100
00:05:09,070 --> 00:05:12,840
for use cases like counters that multiple services

101
00:05:12,840 --> 00:05:16,360
or application instances can read or increment,

102
00:05:16,360 --> 00:05:19,610
or for cashing pages or pieces of data

103
00:05:19,610 --> 00:05:23,150
that can be easily queried and fetched without the need

104
00:05:23,150 --> 00:05:26,100
to do any slow or complex SQL querying

105
00:05:26,100 --> 00:05:27,853
from a relational database.

106
00:05:29,020 --> 00:05:31,520
The next type of non-relational database

107
00:05:31,520 --> 00:05:32,873
is a document store.

108
00:05:34,020 --> 00:05:37,590
In a document store, we can store collections of documents

109
00:05:37,590 --> 00:05:42,090
with a little bit more structure inside each document.

110
00:05:42,090 --> 00:05:45,060
Each document can be thought of as an object

111
00:05:45,060 --> 00:05:46,640
with different attributes.

112
00:05:46,640 --> 00:05:49,310
And those attributes can be of different types.

113
00:05:49,310 --> 00:05:52,410
In a similar way, like a class, can have different fields

114
00:05:52,410 --> 00:05:54,200
of different types.

115
00:05:54,200 --> 00:05:57,440
So documents inside a document store are

116
00:05:57,440 --> 00:05:59,890
much more easily mapped to objects

117
00:05:59,890 --> 00:06:01,573
inside a programming language.

118
00:06:02,610 --> 00:06:03,600
A few examples

119
00:06:03,600 --> 00:06:07,760
of documents can be a JSON object representing a movie,

120
00:06:07,760 --> 00:06:11,330
a YAML configuration representing some business logic,

121
00:06:11,330 --> 00:06:15,323
or an XML representing a form submitted by a user.

122
00:06:16,400 --> 00:06:20,280
The final type of the database is a graph database,

123
00:06:20,280 --> 00:06:24,140
which is nothing more than an extension of a document store,

124
00:06:24,140 --> 00:06:27,900
but with additional capabilities to link, traverse,

125
00:06:27,900 --> 00:06:31,810
and analyze multiple records more efficiently.

126
00:06:31,810 --> 00:06:35,540
Those type of databases are particularly optimized

127
00:06:35,540 --> 00:06:38,450
for navigating and analyzing relationships

128
00:06:38,450 --> 00:06:41,073
between different records in our database.

129
00:06:42,180 --> 00:06:44,410
Some of the most popular use cases

130
00:06:44,410 --> 00:06:47,550
for graph databases include fraud detection,

131
00:06:47,550 --> 00:06:51,770
where multiple logical users may be easily identified

132
00:06:51,770 --> 00:06:53,510
as the same person trying

133
00:06:53,510 --> 00:06:56,980
to initiate multiple transactions using the same email

134
00:06:56,980 --> 00:06:58,960
or the same computer.

135
00:06:58,960 --> 00:07:02,760
Recommendation engines, such as the ones used in e-commerce,

136
00:07:02,760 --> 00:07:05,780
make extensive use of graph databases

137
00:07:05,780 --> 00:07:08,450
to recommend new products to users based

138
00:07:08,450 --> 00:07:10,810
on past purchase history of users

139
00:07:10,810 --> 00:07:12,760
with similar purchasing patterns

140
00:07:12,760 --> 00:07:15,033
or friends of a particular user.

141
00:07:16,010 --> 00:07:18,670
So the last question we need to answer is

142
00:07:18,670 --> 00:07:22,333
when should we use non-relational databases in our system.

143
00:07:23,220 --> 00:07:25,310
While following the same strategy,

144
00:07:25,310 --> 00:07:28,100
when deciding on using your relational database,

145
00:07:28,100 --> 00:07:30,410
we're going to take the same approach when it comes

146
00:07:30,410 --> 00:07:33,340
to choosing a non-relational database.

147
00:07:33,340 --> 00:07:36,220
We simply need to analyze our use case

148
00:07:36,220 --> 00:07:38,110
and figure out what properties

149
00:07:38,110 --> 00:07:40,840
of a database are the most important to us,

150
00:07:40,840 --> 00:07:43,083
and which one we can compromise on.

151
00:07:44,400 --> 00:07:45,910
Since generally speaking,

152
00:07:45,910 --> 00:07:48,690
non-relational databases are superior

153
00:07:48,690 --> 00:07:52,240
to relational databases when it comes to query speed,

154
00:07:52,240 --> 00:07:55,240
non-relational databases are a perfect choice

155
00:07:55,240 --> 00:07:56,443
for things like caching.

156
00:07:57,730 --> 00:08:01,400
So we can still have all our data stored efficiently

157
00:08:01,400 --> 00:08:04,290
without duplication in our relational database

158
00:08:04,290 --> 00:08:07,610
where we can perform complex queries when we need to,

159
00:08:07,610 --> 00:08:10,090
but additionally, we can store certain

160
00:08:10,090 --> 00:08:14,150
very common query results that correspond to user views

161
00:08:14,150 --> 00:08:18,110
in a non-relational database to improve the user experience.

162
00:08:18,110 --> 00:08:22,030
In-memory key/value stores are particularly optimized

163
00:08:22,030 --> 00:08:23,483
for such use cases.

164
00:08:24,660 --> 00:08:27,870
In other cases such as real-time big data

165
00:08:27,870 --> 00:08:30,680
where relational databases are just too slow

166
00:08:30,680 --> 00:08:32,270
or not scalable enough,

167
00:08:32,270 --> 00:08:35,130
we can also use non-relational databases

168
00:08:35,130 --> 00:08:37,190
such as document stores.

169
00:08:37,190 --> 00:08:40,919
Another perfect use case for a non-relational database is

170
00:08:40,919 --> 00:08:43,240
when our data is not structured

171
00:08:43,240 --> 00:08:46,860
and different records can contain different attributes.

172
00:08:46,860 --> 00:08:49,930
For those cases, non-relational databases

173
00:08:49,930 --> 00:08:53,090
like document stores are always a better choice

174
00:08:53,090 --> 00:08:54,653
than a relational database.

175
00:08:55,740 --> 00:09:00,070
A few examples include user profiles where different users

176
00:09:00,070 --> 00:09:02,730
may provide different types of information

177
00:09:02,730 --> 00:09:05,820
or content management, where we can have different types

178
00:09:05,820 --> 00:09:10,550
of user generated content such as images, comments, videos,

179
00:09:10,550 --> 00:09:12,200
and so on.

180
00:09:12,200 --> 00:09:15,390
Of course, if we're in neither of those use cases,

181
00:09:15,390 --> 00:09:19,610
then choosing a relational database is usually a safer bet

182
00:09:19,610 --> 00:09:22,633
due to its simplicity and longterm popularity.

183
00:09:23,690 --> 00:09:26,310
In this lecture, we learned about the second type

184
00:09:26,310 --> 00:09:29,100
of database, the non-relational database

185
00:09:29,100 --> 00:09:32,980
also commonly referred to as a NoSQL database.

186
00:09:32,980 --> 00:09:34,850
We'll learn some of the main advantages

187
00:09:34,850 --> 00:09:38,510
of non-relational databases such as flexible schemas,

188
00:09:38,510 --> 00:09:42,040
fast queries, and more natural data structures

189
00:09:42,040 --> 00:09:43,563
for programming languages.

190
00:09:44,500 --> 00:09:47,850
After that, we talked about the three main categories

191
00:09:47,850 --> 00:09:51,550
of non-relational databases, which are key/value stores,

192
00:09:51,550 --> 00:09:54,730
document stores, and graph databases.

193
00:09:54,730 --> 00:09:57,720
And finally, we talked about a few considerations

194
00:09:57,720 --> 00:10:00,130
for choosing a non-relational database

195
00:10:00,130 --> 00:10:02,620
and mentioned a few classic use cases

196
00:10:02,620 --> 00:10:06,273
that are particularly suitable for non-relational databases.

197
00:10:07,270 --> 00:10:08,993
See you guys in the next lecture.

