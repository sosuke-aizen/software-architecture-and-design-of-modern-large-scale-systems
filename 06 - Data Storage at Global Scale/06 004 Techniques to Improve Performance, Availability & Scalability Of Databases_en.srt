1
00:00:00,290 --> 00:00:01,540
Welcome back.

2
00:00:02,480 --> 00:00:04,630
Now that we have a good understanding

3
00:00:04,630 --> 00:00:07,820
of what types of databases are available to us

4
00:00:07,820 --> 00:00:10,890
and how to pick the right database for our use case,

5
00:00:10,890 --> 00:00:13,740
let's continue our discussion about databases

6
00:00:13,740 --> 00:00:16,910
in the context of a large scale system.

7
00:00:16,910 --> 00:00:18,120
So in this lecture,

8
00:00:18,120 --> 00:00:20,470
we're going to learn about three techniques

9
00:00:20,470 --> 00:00:24,040
to improve the performance, availability and scalability

10
00:00:24,040 --> 00:00:26,010
of our database.

11
00:00:26,010 --> 00:00:28,640
The first technique which improves the performance

12
00:00:28,640 --> 00:00:31,623
of our database queries is called indexing.

13
00:00:32,759 --> 00:00:36,940
The purpose of indexing is to speed up retrieval operations

14
00:00:36,940 --> 00:00:41,160
and locate the desired records in a sublinear time.

15
00:00:41,160 --> 00:00:42,610
Without indexing,

16
00:00:42,610 --> 00:00:46,270
those operations may require a full table scan

17
00:00:46,270 --> 00:00:50,530
which can take a very long time when we have large tables.

18
00:00:50,530 --> 00:00:53,673
Let's look at a few examples of such operations.

19
00:00:54,670 --> 00:00:59,000
Let's assume that we have a very large table with many rows

20
00:00:59,000 --> 00:01:02,610
that contain data about our company's users.

21
00:01:02,610 --> 00:01:04,550
If we want to perform a query

22
00:01:04,550 --> 00:01:07,990
to find all the users that live in a particular city

23
00:01:07,990 --> 00:01:11,760
so we can send them relevant notifications for example,

24
00:01:11,760 --> 00:01:14,253
our SQL query would look like this.

25
00:01:15,230 --> 00:01:18,800
Now, internally to find all the rows in the table

26
00:01:18,800 --> 00:01:20,380
that match this condition,

27
00:01:20,380 --> 00:01:23,240
our database would have to scan linearly

28
00:01:23,240 --> 00:01:24,670
through all the rows.

29
00:01:24,670 --> 00:01:27,883
Which for large tables can take a very long time.

30
00:01:28,840 --> 00:01:31,550
Other examples of costly operations

31
00:01:31,550 --> 00:01:33,760
that involve a full table scan

32
00:01:33,760 --> 00:01:37,530
include getting the list of users in our table sorted.

33
00:01:37,530 --> 00:01:41,610
For example, by their last name, age, or income.

34
00:01:41,610 --> 00:01:44,630
So in addition to the sorting operation,

35
00:01:44,630 --> 00:01:48,453
we would still need to go the entire table at least once.

36
00:01:49,540 --> 00:01:53,260
Either of those operations if performed very frequently

37
00:01:53,260 --> 00:01:57,250
or on very large tables can become a performance bottleneck

38
00:01:57,250 --> 00:02:00,070
and impact our users' performance.

39
00:02:00,070 --> 00:02:02,363
This is where indexing comes in.

40
00:02:03,440 --> 00:02:07,340
A database index is a helper table that we can create

41
00:02:07,340 --> 00:02:10,282
from a particular column or a group of columns.

42
00:02:11,770 --> 00:02:14,750
When the index is created from a single column,

43
00:02:14,750 --> 00:02:18,890
this index table contains a mapping from the column value

44
00:02:18,890 --> 00:02:21,543
to the record that contains that value.

45
00:02:22,920 --> 00:02:25,580
Once that index table is created,

46
00:02:25,580 --> 00:02:28,860
we can put that table inside a data structure.

47
00:02:28,860 --> 00:02:29,860
For example,

48
00:02:29,860 --> 00:02:33,630
we can use a hashmap which provides us very fast lookups

49
00:02:33,630 --> 00:02:36,580
or we can use a type of self-balancing tree

50
00:02:36,580 --> 00:02:38,330
like a B-Tree, for example,

51
00:02:38,330 --> 00:02:40,600
which keeps all the values sorted

52
00:02:40,600 --> 00:02:44,490
so it's easier to both find a particular record in it

53
00:02:44,490 --> 00:02:47,030
and return a sorted list of rows

54
00:02:47,030 --> 00:02:48,823
that match a certain condition.

55
00:02:49,740 --> 00:02:53,820
For example, if we create an index from the city column

56
00:02:53,820 --> 00:02:56,470
and place that index in a hash table,

57
00:02:56,470 --> 00:02:59,470
then a query that looks for all the users

58
00:02:59,470 --> 00:03:03,440
that live in the city of LA can return this list immediately

59
00:03:03,440 --> 00:03:06,943
without the need to scan through the entire table.

60
00:03:08,510 --> 00:03:12,820
Similarly, if we want to get a sorted view of all the users

61
00:03:12,820 --> 00:03:15,590
based on their age within a certain range,

62
00:03:15,590 --> 00:03:19,120
then an index that is organized in a balanced tree

63
00:03:19,120 --> 00:03:20,910
will provide us with this view

64
00:03:20,910 --> 00:03:22,940
in a logarithmic time complexity

65
00:03:22,940 --> 00:03:25,600
that avoids both scanning the entire table,

66
00:03:25,600 --> 00:03:27,943
and sorting it for every query.

67
00:03:29,010 --> 00:03:31,030
Now, as we already mentioned,

68
00:03:31,030 --> 00:03:34,620
indexes can be formed not only from one column,

69
00:03:34,620 --> 00:03:36,863
but also from a set of columns.

70
00:03:38,090 --> 00:03:41,720
An example for that would be if we want to get all the users

71
00:03:41,720 --> 00:03:43,520
that live in a particular city

72
00:03:43,520 --> 00:03:46,033
and also have a particular last name.

73
00:03:47,400 --> 00:03:51,130
If we were to create an index only for the city column,

74
00:03:51,130 --> 00:03:52,640
we would get all the users

75
00:03:52,640 --> 00:03:55,550
who live in that particular city immediately.

76
00:03:55,550 --> 00:03:58,830
However, we would still need to scan linearly

77
00:03:58,830 --> 00:04:00,360
through all those users

78
00:04:00,360 --> 00:04:03,933
and check if their last name is our query.

79
00:04:05,020 --> 00:04:08,160
On the other hand, if we create a composite index

80
00:04:08,160 --> 00:04:09,570
from both columns,

81
00:04:09,570 --> 00:04:13,000
we can have a direct mapping from a pair of values

82
00:04:13,000 --> 00:04:15,370
to the row that contains them.

83
00:04:15,370 --> 00:04:18,060
So for the same query that looks for people

84
00:04:18,060 --> 00:04:19,850
that live in a particular city

85
00:04:19,850 --> 00:04:22,240
and have a particular last name,

86
00:04:22,240 --> 00:04:26,213
we can get the result immediately without any linear scan.

87
00:04:27,150 --> 00:04:29,200
Of course, as always,

88
00:04:29,200 --> 00:04:32,180
if we prioritize one type of operation,

89
00:04:32,180 --> 00:04:35,290
we have to make a tradeoff somewhere.

90
00:04:35,290 --> 00:04:39,100
In the case of indexing, we make the read queries faster

91
00:04:39,100 --> 00:04:41,110
in the expense of additional space

92
00:04:41,110 --> 00:04:43,000
for storing the index tables,

93
00:04:43,000 --> 00:04:45,773
and also the speed of write operations.

94
00:04:46,870 --> 00:04:49,800
The reason the write operations become slower

95
00:04:49,800 --> 00:04:53,710
is that whenever we want to update or add a new row,

96
00:04:53,710 --> 00:04:56,983
now we also need to update the index table.

97
00:04:58,030 --> 00:04:59,420
It's important to note

98
00:04:59,420 --> 00:05:02,460
that although we use the relational database

99
00:05:02,460 --> 00:05:04,080
for all our examples,

100
00:05:04,080 --> 00:05:07,220
indexing is also extensively used

101
00:05:07,220 --> 00:05:10,690
in non-relational databases such as document stores

102
00:05:10,690 --> 00:05:12,073
to speed up queries.

103
00:05:13,190 --> 00:05:15,600
The next technique we're going to talk about

104
00:05:15,600 --> 00:05:17,233
is database replication.

105
00:05:18,220 --> 00:05:20,590
When we store a mission critical data

106
00:05:20,590 --> 00:05:22,620
about our business in a database,

107
00:05:22,620 --> 00:05:24,210
our database instance

108
00:05:24,210 --> 00:05:26,973
become a potential single point of failure.

109
00:05:27,950 --> 00:05:31,190
So just like we avoided a single point of failure

110
00:05:31,190 --> 00:05:32,940
for compute instances,

111
00:05:32,940 --> 00:05:36,570
we're going to apply the same technique for databases.

112
00:05:36,570 --> 00:05:38,690
If we replicate our data

113
00:05:38,690 --> 00:05:41,520
and run multiple instances of our database

114
00:05:41,520 --> 00:05:42,880
on different computers,

115
00:05:42,880 --> 00:05:45,780
we can increase the full tolerance of our system

116
00:05:45,780 --> 00:05:49,970
which in turn provides us with higher availability.

117
00:05:49,970 --> 00:05:52,440
With multiple replicas of our data,

118
00:05:52,440 --> 00:05:55,990
we can make sure that if one of the replicas goes down

119
00:05:55,990 --> 00:06:00,160
or becomes unavailable either temporarily or permanently,

120
00:06:00,160 --> 00:06:02,340
our business is not affected

121
00:06:02,340 --> 00:06:06,200
because queries can continue going to the available replicas

122
00:06:06,200 --> 00:06:09,140
while we work to either restore or replace

123
00:06:09,140 --> 00:06:10,563
the faulty instance.

124
00:06:11,570 --> 00:06:13,820
In addition to higher availability,

125
00:06:13,820 --> 00:06:16,200
we can also get better performance

126
00:06:16,200 --> 00:06:18,570
in a form of higher throughput.

127
00:06:18,570 --> 00:06:21,530
If we have thousands or millions of users

128
00:06:21,530 --> 00:06:25,090
making requests to our database through our services,

129
00:06:25,090 --> 00:06:28,290
we can handle a much larger volume of queries

130
00:06:28,290 --> 00:06:32,363
if we distribute them among a larger number of computers.

131
00:06:33,310 --> 00:06:34,950
The trade off that we make

132
00:06:34,950 --> 00:06:37,500
when we introduce replication in our database

133
00:06:37,500 --> 00:06:39,540
is much higher complexity

134
00:06:39,540 --> 00:06:42,350
especially when it comes to write, update,

135
00:06:42,350 --> 00:06:44,440
and delete operations.

136
00:06:44,440 --> 00:06:47,100
Making sure that concurrent modifications

137
00:06:47,100 --> 00:06:50,110
to the same records don't conflict with each other

138
00:06:50,110 --> 00:06:52,540
and provide some predictable guarantees

139
00:06:52,540 --> 00:06:55,000
in terms of consistency and correctness

140
00:06:55,000 --> 00:06:56,703
is not a trivial task.

141
00:06:57,670 --> 00:07:00,440
Distributed databases are notorious

142
00:07:00,440 --> 00:07:04,000
for being difficult to design, configure, and manage,

143
00:07:04,000 --> 00:07:05,890
especially on a high scale.

144
00:07:05,890 --> 00:07:07,940
And it requires some competency

145
00:07:07,940 --> 00:07:10,033
in the field of distributed systems.

146
00:07:11,130 --> 00:07:13,580
Database replication is supported

147
00:07:13,580 --> 00:07:16,530
by pretty much all modern databases.

148
00:07:16,530 --> 00:07:19,820
In particular, all the non-relational databases

149
00:07:19,820 --> 00:07:22,640
incorporate replication out-of-the-box.

150
00:07:22,640 --> 00:07:26,050
As those databases were designed in an age

151
00:07:26,050 --> 00:07:28,620
where high availability and large scale

152
00:07:28,620 --> 00:07:32,500
were already a big concern for most companies.

153
00:07:32,500 --> 00:07:35,790
The support for replication in relational databases

154
00:07:35,790 --> 00:07:39,653
varies among different relational database implementations.

155
00:07:40,550 --> 00:07:42,990
The third technique we're going to talk about

156
00:07:42,990 --> 00:07:44,610
is database partitioning,

157
00:07:44,610 --> 00:07:47,413
which is also referred to as database sharding.

158
00:07:48,400 --> 00:07:51,830
Unlike replication where we run multiple instances

159
00:07:51,830 --> 00:07:55,230
of our database with the same copy of the data

160
00:07:55,230 --> 00:07:56,810
on each one of them,

161
00:07:56,810 --> 00:07:58,960
when we do database partitioning,

162
00:07:58,960 --> 00:08:03,400
we split the data among different database instances.

163
00:08:03,400 --> 00:08:05,210
And for increased performance,

164
00:08:05,210 --> 00:08:08,913
we typically run each instance on a separate computer.

165
00:08:09,780 --> 00:08:13,440
When our data is split among a group of computers,

166
00:08:13,440 --> 00:08:17,380
we can store way more data than we could originally

167
00:08:17,380 --> 00:08:20,940
if we had only one machine for our disposal.

168
00:08:20,940 --> 00:08:22,810
Additionally with partitioning,

169
00:08:22,810 --> 00:08:26,300
different queries that touch different parts of our data

170
00:08:26,300 --> 00:08:29,500
can be performed completely in parallel.

171
00:08:29,500 --> 00:08:31,320
So with database partitioning,

172
00:08:31,320 --> 00:08:34,893
we get both better performance and higher scalability.

173
00:08:35,909 --> 00:08:39,650
Of course, just like in the case of database replication,

174
00:08:39,650 --> 00:08:43,220
database sharding essentially turns our database

175
00:08:43,220 --> 00:08:45,033
into a distributed a database.

176
00:08:46,070 --> 00:08:49,090
This increases the complexity of the database

177
00:08:49,090 --> 00:08:51,300
and also adds some overhead,

178
00:08:51,300 --> 00:08:54,790
as now we also need to be able to route queries

179
00:08:54,790 --> 00:08:56,050
to the right shards

180
00:08:56,050 --> 00:09:00,040
and make sure that neither of the shards becomes too large

181
00:09:00,040 --> 00:09:01,793
in comparison to the others.

182
00:09:02,820 --> 00:09:06,040
Database sharding is the first class feature

183
00:09:06,040 --> 00:09:10,040
in pretty much all non-relational databases.

184
00:09:10,040 --> 00:09:11,330
The reason for that

185
00:09:11,330 --> 00:09:14,540
is that non-relational databases by design

186
00:09:14,540 --> 00:09:17,290
decouple different records from each other.

187
00:09:17,290 --> 00:09:19,970
So storing the records on different computers

188
00:09:19,970 --> 00:09:23,193
is a lot more natural and easier to implement.

189
00:09:24,080 --> 00:09:26,300
When it comes to relational databases,

190
00:09:26,300 --> 00:09:28,350
just like in the case of replication,

191
00:09:28,350 --> 00:09:32,410
the support for partitioning depends on the implementation.

192
00:09:32,410 --> 00:09:36,050
The reason it's less supported in relational databases

193
00:09:36,050 --> 00:09:38,460
is because relational database queries

194
00:09:38,460 --> 00:09:42,080
that involve multiple records are a lot more common.

195
00:09:42,080 --> 00:09:45,700
And having those records spread across multiple machines

196
00:09:45,700 --> 00:09:48,330
is just a lot more challenging to implement

197
00:09:48,330 --> 00:09:49,870
in a performant way

198
00:09:49,870 --> 00:09:53,080
while still supporting things like asset transactions

199
00:09:53,080 --> 00:09:55,070
or table joints.

200
00:09:55,070 --> 00:09:58,450
So when we choose a relational database for a use case

201
00:09:58,450 --> 00:10:00,870
that involves a high volume of data,

202
00:10:00,870 --> 00:10:04,433
we need to make sure that partitioning is well supported.

203
00:10:05,410 --> 00:10:07,750
While we're on the topic of partitioning,

204
00:10:07,750 --> 00:10:09,220
it's worth mentioning

205
00:10:09,220 --> 00:10:12,960
that partitioning is not only used for databases

206
00:10:12,960 --> 00:10:17,193
but can also be used to logically split our infrastructure.

207
00:10:18,150 --> 00:10:21,510
For example, we can partition our compute instances

208
00:10:21,510 --> 00:10:23,530
using configuration files

209
00:10:23,530 --> 00:10:27,540
so that requests from paid customers go to some machines

210
00:10:27,540 --> 00:10:29,600
and traffic from free users

211
00:10:29,600 --> 00:10:32,743
go to other maybe less powerful machines.

212
00:10:33,700 --> 00:10:35,950
Alternatively, we can send traffic

213
00:10:35,950 --> 00:10:39,260
from mobile devices to one group of computers

214
00:10:39,260 --> 00:10:42,890
and send desktop traffic to another group of computers

215
00:10:42,890 --> 00:10:45,180
running the same application.

216
00:10:45,180 --> 00:10:48,620
This way, if we have an outage, we can easily know

217
00:10:48,620 --> 00:10:50,760
what type of users are affected

218
00:10:50,760 --> 00:10:53,023
and decide how to act upon it.

219
00:10:54,180 --> 00:10:55,450
As a final note,

220
00:10:55,450 --> 00:10:58,320
those three techniques, indexing, replication

221
00:10:58,320 --> 00:11:01,860
and partitioning are completely orthogonal to each other.

222
00:11:01,860 --> 00:11:05,490
So we don't need to choose one over the other.

223
00:11:05,490 --> 00:11:09,200
And in fact, all three of them are commonly used together

224
00:11:09,200 --> 00:11:12,513
in most real life, large scale systems.

225
00:11:13,430 --> 00:11:16,230
In this lecture, we learned about three techniques

226
00:11:16,230 --> 00:11:18,300
that we can apply to our database

227
00:11:18,300 --> 00:11:22,260
to make it much more robust in a large scale system.

228
00:11:22,260 --> 00:11:24,430
We first learned about indexing,

229
00:11:24,430 --> 00:11:27,090
which improves the performance of our database

230
00:11:27,090 --> 00:11:30,860
by speeding up search and retrieval operations.

231
00:11:30,860 --> 00:11:33,490
Then we learned about database replication

232
00:11:33,490 --> 00:11:36,140
which improves our systems availability

233
00:11:36,140 --> 00:11:39,430
and performance through increased throughput.

234
00:11:39,430 --> 00:11:42,110
And finally, we learned about the third technique

235
00:11:42,110 --> 00:11:44,350
which is database partitioning.

236
00:11:44,350 --> 00:11:48,130
Database partitioning improves our database scalability

237
00:11:48,130 --> 00:11:52,290
by splitting up our data across multiple database instances

238
00:11:52,290 --> 00:11:54,343
running on different computers.

239
00:11:55,750 --> 00:11:57,650
I'll see you soon in the next lecture.

