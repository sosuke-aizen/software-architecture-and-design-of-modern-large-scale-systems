

####  **Key/Value Stores Examples**

  * [ **Redis**](https://redis.io/)

  * [ **Aerospike**](https://aerospike.com/)

  * [ **Amazon DynamoDB**](https://aws.amazon.com/dynamodb/)

####  **Document Store Examples**

  * [ **Cassandra**](https://cassandra.apache.org/_/index.html)

  * [ **MongoDB**](https://www.mongodb.com/document-databases)

####  **Graph Databases Examples**

  * [ **Amazon Neptune**](https://aws.amazon.com/neptune/)

  * [ **NEO4J**](https://neo4j.com/)

