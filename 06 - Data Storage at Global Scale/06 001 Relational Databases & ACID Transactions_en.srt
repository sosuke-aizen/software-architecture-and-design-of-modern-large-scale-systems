1
00:00:00,360 --> 00:00:02,020
Welcome back.

2
00:00:02,020 --> 00:00:04,310
Now that we mastered the most important

3
00:00:04,310 --> 00:00:06,000
architectural building blocks

4
00:00:06,000 --> 00:00:10,000
in terms of traffic, API, and content delivery management,

5
00:00:10,000 --> 00:00:13,200
it's time for us to talk about databases.

6
00:00:13,200 --> 00:00:14,930
When it comes to databases

7
00:00:14,930 --> 00:00:17,600
there are so many options to choose from

8
00:00:17,600 --> 00:00:21,120
that it's natural for us to feel overwhelmed.

9
00:00:21,120 --> 00:00:25,160
So instead of choosing the most popular or trendy database,

10
00:00:25,160 --> 00:00:28,690
in the next few lectures we will learn a methodical way

11
00:00:28,690 --> 00:00:30,700
on how to choose the right database

12
00:00:30,700 --> 00:00:32,512
for our systems use case.

13
00:00:33,500 --> 00:00:37,240
In this lecture we'll talk about the first type of database

14
00:00:37,240 --> 00:00:39,700
called relational database.

15
00:00:39,700 --> 00:00:42,980
We will first learn what relational databases are

16
00:00:42,980 --> 00:00:46,720
and what advantages and disadvantages they provide us with,

17
00:00:46,720 --> 00:00:50,830
and later we'll talk about when to use a relational database

18
00:00:50,830 --> 00:00:53,053
and when we should go the other route.

19
00:00:54,730 --> 00:00:57,793
So let's start talking about relational databases.

20
00:00:58,700 --> 00:01:03,100
In a relational database data is stored in tables.

21
00:01:03,100 --> 00:01:06,670
Each row in the table represents a single record

22
00:01:06,670 --> 00:01:09,630
and all the records are related to each other

23
00:01:09,630 --> 00:01:12,943
through a predefined set of columns that they all have.

24
00:01:13,810 --> 00:01:17,180
Each column in a table has a name, a type,

25
00:01:17,180 --> 00:01:20,120
and optionally, a set of constraints.

26
00:01:20,120 --> 00:01:24,050
This relationship between all the records inside a table

27
00:01:24,050 --> 00:01:26,200
is what gives this type of database

28
00:01:26,200 --> 00:01:28,930
the name relational database.

29
00:01:28,930 --> 00:01:32,860
Now each record in the table is uniquely identified

30
00:01:32,860 --> 00:01:34,820
by what's called a primary key,

31
00:01:34,820 --> 00:01:38,010
which can be represented by either one column

32
00:01:38,010 --> 00:01:40,163
or a set of columns in the table.

33
00:01:41,330 --> 00:01:45,820
The structure of each table is always defined ahead of time

34
00:01:45,820 --> 00:01:49,073
and is also referred to as the schema of the table.

35
00:01:49,920 --> 00:01:52,980
Because of this predefined schema for each table,

36
00:01:52,980 --> 00:01:54,270
which gives us the knowledge

37
00:01:54,270 --> 00:01:57,290
of what each record in the table must have,

38
00:01:57,290 --> 00:02:00,140
we can use a very robust query language

39
00:02:00,140 --> 00:02:03,653
to both analyze and update the data in the table.

40
00:02:04,580 --> 00:02:07,080
The industry standard scripting language

41
00:02:07,080 --> 00:02:09,669
to perform such queries is called SQL

42
00:02:09,669 --> 00:02:13,210
which stands for structured query language.

43
00:02:13,210 --> 00:02:15,970
Different relational database implementations

44
00:02:15,970 --> 00:02:18,030
have their own additional features

45
00:02:18,030 --> 00:02:20,400
in their own version of the language.

46
00:02:20,400 --> 00:02:23,490
However, the majority of the standard operations

47
00:02:23,490 --> 00:02:26,453
are the same for all relational databases.

48
00:02:27,720 --> 00:02:32,100
Relational databases have been around since the 1970s

49
00:02:32,100 --> 00:02:35,100
so it's a very well known and proven way

50
00:02:35,100 --> 00:02:37,060
of structuring data.

51
00:02:37,060 --> 00:02:40,410
One of the biggest advantages of relational databases

52
00:02:40,410 --> 00:02:43,670
that gave rise to their popularity so early on

53
00:02:43,670 --> 00:02:47,320
was the fact that storage used to be very expensive

54
00:02:47,320 --> 00:02:49,720
and storing data in separate tables

55
00:02:49,720 --> 00:02:53,810
allows us to eliminate the need for data duplication.

56
00:02:53,810 --> 00:02:57,490
Although storage became a lot cheaper since then,

57
00:02:57,490 --> 00:03:00,720
the amount of data that collect nowadays

58
00:03:00,720 --> 00:03:02,970
is a lot larger than before.

59
00:03:02,970 --> 00:03:04,860
So for large scale systems

60
00:03:04,860 --> 00:03:07,763
storage costs are still a major factor.

61
00:03:08,880 --> 00:03:10,990
Let's see how we can save space

62
00:03:10,990 --> 00:03:14,193
and avoid data duplication in a relational database

63
00:03:14,193 --> 00:03:18,060
with a small, but very realistic example.

64
00:03:18,060 --> 00:03:20,270
Let's assume we have an online store

65
00:03:20,270 --> 00:03:23,530
where we sell a limited number of products.

66
00:03:23,530 --> 00:03:27,090
We store all the information about those products in a table

67
00:03:27,090 --> 00:03:29,540
that contains each product's information

68
00:03:29,540 --> 00:03:31,740
like its name, its model year,

69
00:03:31,740 --> 00:03:34,863
company, category, and base price.

70
00:03:36,170 --> 00:03:39,800
Now as customers place orders for those products

71
00:03:39,800 --> 00:03:43,210
we need to store those orders in a separate table.

72
00:03:43,210 --> 00:03:45,170
However, in addition,

73
00:03:45,170 --> 00:03:47,440
we also want to have the ability

74
00:03:47,440 --> 00:03:50,930
to easily analyze and report on things like

75
00:03:50,930 --> 00:03:53,970
which products sell the most or the least

76
00:03:53,970 --> 00:03:55,490
at a given timeframe,

77
00:03:55,490 --> 00:03:58,190
we also may want to rank companies

78
00:03:58,190 --> 00:04:00,850
based on their performance of their products,

79
00:04:00,850 --> 00:04:03,430
or we may want to get a breakdown

80
00:04:03,430 --> 00:04:06,270
of which category of products does better

81
00:04:06,270 --> 00:04:08,653
during certain sales or seasons.

82
00:04:09,520 --> 00:04:12,870
So an approach would be to have one table

83
00:04:12,870 --> 00:04:15,600
where each row contains a single purchase

84
00:04:15,600 --> 00:04:17,040
of a particular product

85
00:04:17,040 --> 00:04:20,610
as well as all the information about that product

86
00:04:20,610 --> 00:04:24,070
that we need to perform all those analytics.

87
00:04:24,070 --> 00:04:26,280
Of course, we can already see

88
00:04:26,280 --> 00:04:29,040
that this table will take a lot of space

89
00:04:29,040 --> 00:04:31,390
because it contains a lot of information

90
00:04:31,390 --> 00:04:33,090
on each purchase product.

91
00:04:33,090 --> 00:04:35,570
And this table will continuously grow

92
00:04:35,570 --> 00:04:38,920
as we get more orders for our products.

93
00:04:38,920 --> 00:04:41,090
However, if we look closely,

94
00:04:41,090 --> 00:04:43,400
most of the information in this table

95
00:04:43,400 --> 00:04:45,863
is duplicated from the products table.

96
00:04:46,930 --> 00:04:50,420
So because a relational database allows us to have

97
00:04:50,420 --> 00:04:54,160
not only relationships between rows inside a table

98
00:04:54,160 --> 00:04:57,480
but also relationships between multiple tables,

99
00:04:57,480 --> 00:04:59,960
we can actually avoid this duplication

100
00:04:59,960 --> 00:05:02,670
and keep the orders table a lot smaller

101
00:05:02,670 --> 00:05:05,560
by containing only the ID of the product

102
00:05:05,560 --> 00:05:08,240
that was involved in each purchase.

103
00:05:08,240 --> 00:05:12,130
Using this one column that is shared between the two tables

104
00:05:12,130 --> 00:05:14,110
we can use the join operation

105
00:05:14,110 --> 00:05:17,220
and combine the information from both tables

106
00:05:17,220 --> 00:05:21,363
without having to store that information twice anywhere.

107
00:05:22,520 --> 00:05:26,030
Now let's talk about the advantages and disadvantages

108
00:05:26,030 --> 00:05:27,653
of relational databases.

109
00:05:28,700 --> 00:05:31,350
The clear advantage of storing our data

110
00:05:31,350 --> 00:05:32,950
in a relational database

111
00:05:32,950 --> 00:05:35,690
is the ability to perform those complex

112
00:05:35,690 --> 00:05:39,960
and very flexible queries using the SQL language.

113
00:05:39,960 --> 00:05:42,490
This allows us to gain deep insight

114
00:05:42,490 --> 00:05:46,453
into the data we have about our business or our users.

115
00:05:47,430 --> 00:05:50,220
Also, as we mentioned a few moments ago,

116
00:05:50,220 --> 00:05:54,430
because of our ability to analyze and join multiple tables

117
00:05:54,430 --> 00:05:57,160
we can save a lot of space on storage

118
00:05:57,160 --> 00:06:01,383
which directly translates to cost savings for our business.

119
00:06:02,240 --> 00:06:05,120
Another advantage of relational databases

120
00:06:05,120 --> 00:06:08,310
is that they're very easy to reason about.

121
00:06:08,310 --> 00:06:11,050
The reason for that is this table structure

122
00:06:11,050 --> 00:06:13,400
is very natural for human beings,

123
00:06:13,400 --> 00:06:15,540
and it doesn't require any knowledge

124
00:06:15,540 --> 00:06:18,973
about sophisticated data structures and computer science.

125
00:06:19,970 --> 00:06:22,410
Finally, relational databases

126
00:06:22,410 --> 00:06:25,150
provide us with very powerful properties

127
00:06:25,150 --> 00:06:26,880
called ACID transactions

128
00:06:27,980 --> 00:06:31,740
where ACID stands for atomicity, consistency,

129
00:06:31,740 --> 00:06:33,753
isolation, and durability.

130
00:06:34,910 --> 00:06:36,990
In the context of a database,

131
00:06:36,990 --> 00:06:39,800
a transaction is a sequence of operations

132
00:06:39,800 --> 00:06:41,810
that for an external observer

133
00:06:41,810 --> 00:06:44,313
should appear as a single operation.

134
00:06:45,260 --> 00:06:47,340
For instance, transferring money

135
00:06:47,340 --> 00:06:49,630
from one user's account to another

136
00:06:49,630 --> 00:06:52,500
may involve multiple operations internally,

137
00:06:52,500 --> 00:06:57,090
but should be perceived as one single operation externally.

138
00:06:57,090 --> 00:07:00,050
After all, we don't wanna have a situation

139
00:07:00,050 --> 00:07:01,740
even for a brief moment

140
00:07:01,740 --> 00:07:04,410
where it appears that the money was withdrawn

141
00:07:04,410 --> 00:07:05,670
from the first account

142
00:07:05,670 --> 00:07:08,360
but it's still missing on the second account.

143
00:07:08,360 --> 00:07:11,570
Or similarly, we don't want to have a situation

144
00:07:11,570 --> 00:07:13,440
where the money appears twice

145
00:07:13,440 --> 00:07:15,793
in both accounts simultaneously.

146
00:07:16,860 --> 00:07:21,070
So to make sure we don't get into either of those situations

147
00:07:21,070 --> 00:07:23,400
relational databases guarantee

148
00:07:23,400 --> 00:07:25,750
the atomicity of transactions,

149
00:07:25,750 --> 00:07:28,330
which means each set of operations

150
00:07:28,330 --> 00:07:30,380
that are part of one transaction

151
00:07:30,380 --> 00:07:33,943
either appear all at once or don't appear at all.

152
00:07:35,040 --> 00:07:38,230
In other words, it's either everything or nothing,

153
00:07:38,230 --> 00:07:40,870
but never anything in between.

154
00:07:40,870 --> 00:07:42,863
Now let's talk about consistency.

155
00:07:43,890 --> 00:07:47,170
Consistency guarantees that a transaction

156
00:07:47,170 --> 00:07:48,750
that was already committed

157
00:07:48,750 --> 00:07:52,543
will be seen by all future queries and transactions.

158
00:07:53,690 --> 00:07:56,730
For example, if we transfer a hundred dollars

159
00:07:56,730 --> 00:08:00,260
from user A to user B as a single transaction,

160
00:08:00,260 --> 00:08:02,610
once that transaction is complete

161
00:08:02,610 --> 00:08:05,200
there is no way that a future query

162
00:08:05,200 --> 00:08:07,596
will suddenly see those a hundred dollars

163
00:08:07,596 --> 00:08:10,280
either in the first user's account

164
00:08:10,280 --> 00:08:13,943
or will not see those funds in the second user's account.

165
00:08:15,190 --> 00:08:18,470
But consistency doesn't guarantee only that,

166
00:08:18,470 --> 00:08:21,060
it also guarantees that a transaction

167
00:08:21,060 --> 00:08:25,033
doesn't violate any constraints that we set for our data.

168
00:08:25,990 --> 00:08:28,470
For example, if we set a constraint

169
00:08:28,470 --> 00:08:29,930
that a particular user

170
00:08:29,930 --> 00:08:33,150
can't have more than a thousand dollars in their account,

171
00:08:33,150 --> 00:08:37,280
the consistency guarantee will make sure that a transaction

172
00:08:37,280 --> 00:08:40,350
will not leave that user with more money

173
00:08:40,350 --> 00:08:43,710
than the allowed amount of money in their account.

174
00:08:43,710 --> 00:08:47,030
So if that limit is reached during a transaction,

175
00:08:47,030 --> 00:08:49,233
that transaction will simply fail.

176
00:08:50,200 --> 00:08:53,410
The next property the relational database guarantees

177
00:08:53,410 --> 00:08:57,550
as part ACID transaction semantics is isolation.

178
00:08:57,550 --> 00:09:01,170
Isolation is somewhat related to atomicity,

179
00:09:01,170 --> 00:09:04,070
but in the context of concurrent operations

180
00:09:04,070 --> 00:09:06,003
performed on our database.

181
00:09:06,900 --> 00:09:10,220
For example, if we continue with the same example

182
00:09:10,220 --> 00:09:13,810
of transferring money from one user's account to another,

183
00:09:13,810 --> 00:09:18,200
isolation guarantees that if there's another transaction

184
00:09:18,200 --> 00:09:20,070
happening simultaneously,

185
00:09:20,070 --> 00:09:22,330
that second concurrent transaction

186
00:09:22,330 --> 00:09:24,970
will not see an intermediate state

187
00:09:24,970 --> 00:09:28,200
of the money being either present in both accounts

188
00:09:28,200 --> 00:09:30,163
or missing in both accounts.

189
00:09:31,020 --> 00:09:32,540
So in other words,

190
00:09:32,540 --> 00:09:34,650
those two concurrent transactions

191
00:09:34,650 --> 00:09:37,320
are isolated from each other in such a way

192
00:09:37,320 --> 00:09:40,533
that they do not see each other's intermediate state.

193
00:09:41,770 --> 00:09:45,180
The last guarantee that comes from a relational database

194
00:09:45,180 --> 00:09:49,600
as part of the asset transaction properties is durability.

195
00:09:49,600 --> 00:09:53,860
Durability simply means that once a transaction is complete

196
00:09:53,860 --> 00:09:55,990
its final state will persist

197
00:09:55,990 --> 00:09:58,803
and remain permanently inside the database.

198
00:09:59,770 --> 00:10:01,260
So with this property

199
00:10:01,260 --> 00:10:04,470
we will never have a situation where for example

200
00:10:04,470 --> 00:10:08,300
a user makes a purchase of a product from our online store

201
00:10:08,300 --> 00:10:11,590
and transfers the money to us as part of the transaction

202
00:10:11,590 --> 00:10:14,820
but the record about the purchase disappears.

203
00:10:14,820 --> 00:10:18,250
So as long as the purchase and the transfer of the money

204
00:10:18,250 --> 00:10:20,150
were part of the same transaction

205
00:10:20,150 --> 00:10:23,130
and that transaction was completed successfully,

206
00:10:23,130 --> 00:10:26,030
the final state of that entire transaction

207
00:10:26,030 --> 00:10:28,640
will contain both the record of the purchase

208
00:10:28,640 --> 00:10:29,963
and the money transfer.

209
00:10:30,940 --> 00:10:32,250
Now that we talked about

210
00:10:32,250 --> 00:10:34,790
all the awesome advantages and properties

211
00:10:34,790 --> 00:10:37,180
that we get from our relational database,

212
00:10:37,180 --> 00:10:40,313
let's talk about its disadvantages and drawbacks.

213
00:10:41,330 --> 00:10:44,730
The main disadvantage of using a relational database

214
00:10:44,730 --> 00:10:49,513
is its rigid structure enforced by each data table schema.

215
00:10:50,500 --> 00:10:54,350
Since the schema applies to all the rows inside a table

216
00:10:54,350 --> 00:10:56,640
it has to be defined ahead of time

217
00:10:56,640 --> 00:10:59,160
before we can use the table.

218
00:10:59,160 --> 00:11:00,750
And if at some point

219
00:11:00,750 --> 00:11:02,920
we want to change the schema of the table,

220
00:11:02,920 --> 00:11:05,420
for example, by adding a new column

221
00:11:05,420 --> 00:11:08,290
or removing a column that we no longer need,

222
00:11:08,290 --> 00:11:10,800
we would have to have some maintenance time

223
00:11:10,800 --> 00:11:14,030
which results in our table not being available

224
00:11:14,030 --> 00:11:16,120
for any operations.

225
00:11:16,120 --> 00:11:18,620
So when we design the schema of our tables,

226
00:11:18,620 --> 00:11:20,150
we need to be very careful

227
00:11:20,150 --> 00:11:23,030
and very thorough in planning ahead

228
00:11:23,030 --> 00:11:25,920
so we don't need to change the schema very often

229
00:11:25,920 --> 00:11:28,593
or preferably not change it at all.

230
00:11:29,670 --> 00:11:33,590
Also relational databases tend to be a bit harder

231
00:11:33,590 --> 00:11:36,160
and more costly to maintain and scale

232
00:11:36,160 --> 00:11:38,380
because of their complexity.

233
00:11:38,380 --> 00:11:41,460
After all, supporting the SQL query language

234
00:11:41,460 --> 00:11:44,110
and providing ACID transaction guarantees

235
00:11:44,110 --> 00:11:45,733
is not a simple task.

236
00:11:46,900 --> 00:11:49,840
Additionally, because of all this complexity

237
00:11:49,840 --> 00:11:51,240
and all those guarantees

238
00:11:51,240 --> 00:11:53,570
that we get from our relational database,

239
00:11:53,570 --> 00:11:57,240
read operations tend to be a bit on the slower side

240
00:11:57,240 --> 00:11:59,720
compared to the other type of databases

241
00:11:59,720 --> 00:12:03,000
that we're going to talk about in another lecture.

242
00:12:03,000 --> 00:12:06,870
Of course, different implementations of relational databases

243
00:12:06,870 --> 00:12:10,220
have different performance optimizations and guarantees.

244
00:12:10,220 --> 00:12:12,030
But as a general rule,

245
00:12:12,030 --> 00:12:15,460
relational databases are notoriously slower

246
00:12:15,460 --> 00:12:17,633
than the non relational databases.

247
00:12:18,780 --> 00:12:20,450
So now that we talked about

248
00:12:20,450 --> 00:12:22,640
both the advantages and disadvantages

249
00:12:22,640 --> 00:12:24,100
of a relational database,

250
00:12:24,100 --> 00:12:26,520
it's easier to extrapolate that knowledge

251
00:12:26,520 --> 00:12:30,550
and see when it's a good idea to use a relational database

252
00:12:30,550 --> 00:12:31,893
to store our data.

253
00:12:33,120 --> 00:12:35,770
All we need to do is look at the advantages

254
00:12:35,770 --> 00:12:38,910
and see if those properties are important to us

255
00:12:38,910 --> 00:12:42,400
while the disadvantages can take a backseat.

256
00:12:42,400 --> 00:12:43,440
For example,

257
00:12:43,440 --> 00:12:46,920
if performing those complex and flexible queries

258
00:12:46,920 --> 00:12:50,440
to analyze our data is an important use case for us,

259
00:12:50,440 --> 00:12:53,380
or we need to guarantee ACID transactions

260
00:12:53,380 --> 00:12:55,990
between different entities in our database,

261
00:12:55,990 --> 00:12:59,523
then a relational database is the perfect choice for us.

262
00:13:00,500 --> 00:13:04,120
However, if there isn't any inherent relationship

263
00:13:04,120 --> 00:13:05,630
between different records

264
00:13:05,630 --> 00:13:08,710
that justifies storing our data in tables,

265
00:13:08,710 --> 00:13:10,670
or if read performance

266
00:13:10,670 --> 00:13:13,320
is the most important quality that we need

267
00:13:13,320 --> 00:13:15,560
for providing good user experience,

268
00:13:15,560 --> 00:13:18,400
then there are a other better alternatives

269
00:13:18,400 --> 00:13:20,950
that we're going to talk about in the next lecture.

270
00:13:21,840 --> 00:13:25,990
In this lecture, we started the discussion about databases.

271
00:13:25,990 --> 00:13:28,330
We learned about the first type of database

272
00:13:28,330 --> 00:13:30,410
called relational databases

273
00:13:30,410 --> 00:13:34,080
which is also commonly referred to as SQL databases

274
00:13:34,080 --> 00:13:37,920
because of their support for the SQL query language.

275
00:13:37,920 --> 00:13:40,890
Then we talked about some of the main advantages

276
00:13:40,890 --> 00:13:43,250
that we get from relational databases

277
00:13:43,250 --> 00:13:45,200
such as the ability to perform

278
00:13:45,200 --> 00:13:48,820
powerful and flexible analysis of our data,

279
00:13:48,820 --> 00:13:50,100
efficient storage,

280
00:13:50,100 --> 00:13:53,210
which allows us to eliminate data duplication,

281
00:13:53,210 --> 00:13:57,130
the natural structure of the data in human readable tables,

282
00:13:57,130 --> 00:13:59,580
and the last but definitely not least,

283
00:13:59,580 --> 00:14:01,723
the ACID transaction guarantees.

284
00:14:02,810 --> 00:14:05,780
After that, we talked about some of the drawbacks

285
00:14:05,780 --> 00:14:08,090
of typical relational databases

286
00:14:08,090 --> 00:14:11,330
which mainly revolve around the rigid schema

287
00:14:11,330 --> 00:14:14,430
that we have to define for each table.

288
00:14:14,430 --> 00:14:18,500
This rigid schema increases the complexity of our database

289
00:14:18,500 --> 00:14:20,670
and also provides challenges

290
00:14:20,670 --> 00:14:23,490
for scalability and performance.

291
00:14:23,490 --> 00:14:24,730
In the next lecture,

292
00:14:24,730 --> 00:14:27,610
we're going to talk about the other type of databases

293
00:14:27,610 --> 00:14:30,913
and what qualities those databases can provide to us

294
00:14:30,913 --> 00:14:33,433
that relational databases cannot.

295
00:14:34,750 --> 00:14:36,523
See you soon in the next lecture.

