1
00:00:00,280 --> 00:00:03,020
Welcome back to another lecture.

2
00:00:03,020 --> 00:00:05,160
In this lecture, we're going to continue

3
00:00:05,160 --> 00:00:08,270
our discussion about big data processing.

4
00:00:08,270 --> 00:00:09,850
We will build upon the knowledge

5
00:00:09,850 --> 00:00:11,290
from the previous lecture

6
00:00:11,290 --> 00:00:13,400
and learn about a very powerful,

7
00:00:13,400 --> 00:00:16,563
modern architecture for processing big data.

8
00:00:17,470 --> 00:00:20,310
So if we remember from the previous lecture,

9
00:00:20,310 --> 00:00:23,040
we had two strategies that we can use

10
00:00:23,040 --> 00:00:25,360
to process large volumes of data

11
00:00:25,360 --> 00:00:27,203
coming into our system.

12
00:00:28,300 --> 00:00:30,510
We had the batch processing strategy,

13
00:00:30,510 --> 00:00:33,370
which provided us with a lot of benefits.

14
00:00:33,370 --> 00:00:36,510
In particular, since our batch processing job

15
00:00:36,510 --> 00:00:39,250
operated on our entire data set,

16
00:00:39,250 --> 00:00:42,470
we could get deep insight into historic data

17
00:00:42,470 --> 00:00:45,050
as well as the ability to fuse data

18
00:00:45,050 --> 00:00:47,000
that came from different sources

19
00:00:47,000 --> 00:00:49,850
and build prediction models that could benefit

20
00:00:49,850 --> 00:00:52,543
our users as well as our business.

21
00:00:53,610 --> 00:00:56,500
However, when we chose batch processing,

22
00:00:56,500 --> 00:00:58,960
we had to make a trade off and settle

23
00:00:58,960 --> 00:01:01,270
for high delay between the data

24
00:01:01,270 --> 00:01:03,770
coming into our system and the time

25
00:01:03,770 --> 00:01:06,423
we can query that data from the database.

26
00:01:07,340 --> 00:01:10,960
On the other hand, we had the real-time processing strategy,

27
00:01:10,960 --> 00:01:14,700
which gave us only visibility into recent data.

28
00:01:14,700 --> 00:01:17,210
However, it allowed us to query

29
00:01:17,210 --> 00:01:19,560
and respond to data in real-time

30
00:01:19,560 --> 00:01:21,573
as it came into our system.

31
00:01:22,600 --> 00:01:25,390
So for each use case, we had to choose

32
00:01:25,390 --> 00:01:28,410
and sacrifice either the fast response time

33
00:01:28,410 --> 00:01:31,423
or deep analysis into historic data.

34
00:01:32,320 --> 00:01:34,570
And there are indeed use cases

35
00:01:34,570 --> 00:01:37,470
that are perfect for just batch processing

36
00:01:37,470 --> 00:01:40,040
where the delay is not that important.

37
00:01:40,040 --> 00:01:41,890
And there are other use cases

38
00:01:41,890 --> 00:01:44,400
that are perfect for real-time processing

39
00:01:44,400 --> 00:01:47,510
that do not really need any deep analysis

40
00:01:47,510 --> 00:01:49,740
that involves historic data.

41
00:01:49,740 --> 00:01:53,130
However, in many cases, making this decision

42
00:01:53,130 --> 00:01:54,870
between real-time processing

43
00:01:54,870 --> 00:01:57,200
and batch processing is very hard

44
00:01:57,200 --> 00:02:00,623
because we need the properties of both strategies.

45
00:02:01,750 --> 00:02:05,330
Let's look at a few examples to get some motivation.

46
00:02:05,330 --> 00:02:08,520
For example, we have a system that aggregates

47
00:02:08,520 --> 00:02:11,560
and analyzes logs and performance metrics

48
00:02:11,560 --> 00:02:14,910
from hundreds and thousands of production servers.

49
00:02:14,910 --> 00:02:17,400
As we already know from the previous lecture

50
00:02:17,400 --> 00:02:20,380
that this is a use case for real-time processing.

51
00:02:20,380 --> 00:02:23,230
We absolutely have to have visibility

52
00:02:23,230 --> 00:02:25,160
into those logs and metrics

53
00:02:25,160 --> 00:02:27,010
as they arrive at our system.

54
00:02:27,010 --> 00:02:28,520
So if something happens,

55
00:02:28,520 --> 00:02:31,390
engineers can see those logs and metrics

56
00:02:31,390 --> 00:02:34,210
in real-time and figure out what happened

57
00:02:34,210 --> 00:02:35,483
and how to respond.

58
00:02:36,610 --> 00:02:39,760
However, if we deploy a new feature to production

59
00:02:39,760 --> 00:02:42,210
that may have some performance impact,

60
00:02:42,210 --> 00:02:44,550
we want to be able to look back

61
00:02:44,550 --> 00:02:46,720
and compare the current performance

62
00:02:46,720 --> 00:02:50,030
we see in real-time to historic performance

63
00:02:50,030 --> 00:02:52,100
based on the logs and metrics

64
00:02:52,100 --> 00:02:54,973
that we collected throughout the week or month.

65
00:02:55,920 --> 00:02:58,330
Similarly, if we want to implement

66
00:02:58,330 --> 00:03:00,360
automatic anomaly detection,

67
00:03:00,360 --> 00:03:03,650
this type of system would have to have access

68
00:03:03,650 --> 00:03:07,010
to both real-time data and historic data.

69
00:03:07,010 --> 00:03:09,810
So it can compare the two and find anomalies

70
00:03:09,810 --> 00:03:12,303
in the current behavior of our system.

71
00:03:13,350 --> 00:03:15,600
Another great example of a system

72
00:03:15,600 --> 00:03:17,280
that requires the properties

73
00:03:17,280 --> 00:03:18,880
of both batch processing

74
00:03:18,880 --> 00:03:22,440
and real-time processing is ride sharing service.

75
00:03:22,440 --> 00:03:24,920
In this case, the events we are getting

76
00:03:24,920 --> 00:03:26,360
are the current location

77
00:03:26,360 --> 00:03:30,330
of both the drivers and the clients that need a ride.

78
00:03:30,330 --> 00:03:32,610
Of course, to provide the service,

79
00:03:32,610 --> 00:03:35,380
we need to know where each driver and client

80
00:03:35,380 --> 00:03:37,400
is located in real-time,

81
00:03:37,400 --> 00:03:39,090
so we can match them together

82
00:03:39,090 --> 00:03:41,843
and also show their location on the map.

83
00:03:42,760 --> 00:03:46,010
On the other hand, we want to look at historic data

84
00:03:46,010 --> 00:03:49,500
and detect patterns, such as busy times or days

85
00:03:49,500 --> 00:03:52,420
when a lot of clients need a ride.

86
00:03:52,420 --> 00:03:55,940
Alternatively, we can detect certain locations

87
00:03:55,940 --> 00:03:58,030
that typically have more drivers

88
00:03:58,030 --> 00:04:00,900
than clients or vice versa.

89
00:04:00,900 --> 00:04:04,260
If we can analyze this data and find those patterns,

90
00:04:04,260 --> 00:04:07,027
we can share this information with our drivers

91
00:04:07,027 --> 00:04:09,670
and make sure that at any given moment,

92
00:04:09,670 --> 00:04:12,170
we have enough drivers in each location

93
00:04:12,170 --> 00:04:14,690
to provide them with steady income

94
00:04:14,690 --> 00:04:17,920
and our riders with good writing experience.

95
00:04:17,920 --> 00:04:19,690
So in this case, again,

96
00:04:19,690 --> 00:04:21,680
it's not that easy to choose

97
00:04:21,680 --> 00:04:24,583
one data processing strategy over the other.

98
00:04:25,490 --> 00:04:29,060
For those use cases, we have the Lambda Architecture,

99
00:04:29,060 --> 00:04:31,500
which takes advantage of both batch

100
00:04:31,500 --> 00:04:34,390
and real-time processing models to provide us

101
00:04:34,390 --> 00:04:36,563
with the best of both worlds.

102
00:04:37,480 --> 00:04:39,430
The first person who came up

103
00:04:39,430 --> 00:04:42,570
with the Lambda Architecture term is Nathan Marz,

104
00:04:42,570 --> 00:04:45,660
based on his experience working on big data

105
00:04:45,660 --> 00:04:48,150
at BackType and Twitter.

106
00:04:48,150 --> 00:04:51,410
Lambda Architecture attempts to find a balance

107
00:04:51,410 --> 00:04:53,430
between the high fault tolerance

108
00:04:53,430 --> 00:04:55,870
and comprehensive analysis of the data

109
00:04:55,870 --> 00:04:58,000
that we get from batch processing

110
00:04:58,000 --> 00:04:59,960
and the low latency that we get

111
00:04:59,960 --> 00:05:01,663
from real-time processing.

112
00:05:02,740 --> 00:05:05,420
In Lambda Architecture, the infrastructure

113
00:05:05,420 --> 00:05:07,800
is divided into three layers.

114
00:05:07,800 --> 00:05:10,150
The batch layer, the speed layer,

115
00:05:10,150 --> 00:05:11,393
and the serving layer.

116
00:05:12,300 --> 00:05:15,460
Data that comes into our system is dispatched

117
00:05:15,460 --> 00:05:17,180
both into the batch layer

118
00:05:17,180 --> 00:05:19,263
and the speed layer simultaneously.

119
00:05:20,390 --> 00:05:24,210
The batch layer is very similar to what we had before

120
00:05:24,210 --> 00:05:26,760
when we talked about batch processing.

121
00:05:26,760 --> 00:05:28,380
In Lambda Architecture,

122
00:05:28,380 --> 00:05:31,250
the batch layer serves two purposes.

123
00:05:31,250 --> 00:05:33,540
The first purpose of the batch layer

124
00:05:33,540 --> 00:05:37,670
is to manage our data set and be the system of records.

125
00:05:37,670 --> 00:05:41,280
So just like before, the data in our master set

126
00:05:41,280 --> 00:05:44,730
is immutable and new data is only appended

127
00:05:44,730 --> 00:05:46,163
but is never modified.

128
00:05:47,460 --> 00:05:49,660
This data is usually stored

129
00:05:49,660 --> 00:05:51,490
on a distributed file system,

130
00:05:51,490 --> 00:05:54,320
specifically designed and optimized

131
00:05:54,320 --> 00:05:56,910
for storing big files containing

132
00:05:56,910 --> 00:05:58,673
massive amounts of data.

133
00:05:59,620 --> 00:06:01,720
The second purpose of the batch layer

134
00:06:01,720 --> 00:06:04,233
is to pre-compute our batch views.

135
00:06:05,260 --> 00:06:08,320
Every time we run our batch processing job,

136
00:06:08,320 --> 00:06:10,150
it processes all the data

137
00:06:10,150 --> 00:06:12,560
that we have in our master data set,

138
00:06:12,560 --> 00:06:14,800
which is also a perfect point

139
00:06:14,800 --> 00:06:17,730
to make corrections or to duplicate

140
00:06:17,730 --> 00:06:22,230
the otherwise raw data that we have in our master set.

141
00:06:22,230 --> 00:06:24,430
Once the processing job is complete,

142
00:06:24,430 --> 00:06:27,010
it indexes and stores the result

143
00:06:27,010 --> 00:06:29,280
in a read-only database.

144
00:06:29,280 --> 00:06:32,110
Typically, this overrides the existing

145
00:06:32,110 --> 00:06:34,730
pre-computed views that were created

146
00:06:34,730 --> 00:06:37,563
the previous time we ran the processing job.

147
00:06:38,480 --> 00:06:41,350
It's important to note that the batch layer

148
00:06:41,350 --> 00:06:44,660
aims at perfect accuracy and operates

149
00:06:44,660 --> 00:06:46,413
on our entire data set.

150
00:06:47,460 --> 00:06:49,800
In parallel to the batch layer,

151
00:06:49,800 --> 00:06:52,450
the incoming data goes to the speed layer,

152
00:06:52,450 --> 00:06:55,460
which uses the real-time processing strategy

153
00:06:55,460 --> 00:06:58,250
that we learned in the previous lecture.

154
00:06:58,250 --> 00:07:00,220
All the data goes in to a queue

155
00:07:00,220 --> 00:07:03,080
or a message broker and is then picked up

156
00:07:03,080 --> 00:07:05,910
as it arrives by the processing job.

157
00:07:05,910 --> 00:07:09,600
The processing job quickly analyzes the new event

158
00:07:09,600 --> 00:07:12,910
and adds the processed record to the real-time view.

159
00:07:12,910 --> 00:07:15,763
So it can be available for anyone to query.

160
00:07:16,850 --> 00:07:19,800
The speed layer essentially compensates

161
00:07:19,800 --> 00:07:23,280
for the high latency that we have in our batch layer.

162
00:07:23,280 --> 00:07:25,090
But unlike the batch layer,

163
00:07:25,090 --> 00:07:28,240
it operates only on the most recent data.

164
00:07:28,240 --> 00:07:31,310
And doesn't attempt to provide a complete view

165
00:07:31,310 --> 00:07:34,163
or make any complex data corrections.

166
00:07:35,190 --> 00:07:37,450
In other words, the speed layer

167
00:07:37,450 --> 00:07:38,910
is there to close the gap

168
00:07:38,910 --> 00:07:41,570
that we have between the present moment.

169
00:07:41,570 --> 00:07:44,270
And the last time our batch job ran

170
00:07:44,270 --> 00:07:45,803
and provided a result.

171
00:07:46,780 --> 00:07:49,940
Now, the serving layers purpose is to respond

172
00:07:49,940 --> 00:07:52,650
to ad hoc queries and merge the data

173
00:07:52,650 --> 00:07:55,760
from both the batch and their real-time views.

174
00:07:55,760 --> 00:07:59,250
This way, any query can take all the data

175
00:07:59,250 --> 00:08:01,150
that we have into account

176
00:08:01,150 --> 00:08:03,850
both historic data that was processed

177
00:08:03,850 --> 00:08:06,610
by the batch layer and the most up to date,

178
00:08:06,610 --> 00:08:08,990
real-time data we just received

179
00:08:08,990 --> 00:08:11,133
and processed in the speed layer.

180
00:08:12,100 --> 00:08:15,810
Now let's explore the Lambda Architecture and action

181
00:08:15,810 --> 00:08:18,600
using a real-life practical example

182
00:08:18,600 --> 00:08:20,973
taken from the ad-tech industry.

183
00:08:21,970 --> 00:08:23,650
In the ad-tech industry,

184
00:08:23,650 --> 00:08:26,550
on one end, we have advertisers.

185
00:08:26,550 --> 00:08:29,150
Those can be companies that sell tickets

186
00:08:29,150 --> 00:08:31,360
to concerts or different events

187
00:08:31,360 --> 00:08:33,679
and would like to advertise those events

188
00:08:33,679 --> 00:08:36,590
to potential customers or those companies

189
00:08:36,590 --> 00:08:39,179
can be physical or online stores

190
00:08:39,179 --> 00:08:41,840
that would like to advertise their existing

191
00:08:41,840 --> 00:08:43,970
or upcoming products.

192
00:08:43,970 --> 00:08:46,120
On the other end, we have the online

193
00:08:46,120 --> 00:08:50,110
content producers, such as bloggers or news agencies,

194
00:08:50,110 --> 00:08:52,450
that provide content on their websites

195
00:08:52,450 --> 00:08:54,863
to the general public free of charge.

196
00:08:55,960 --> 00:08:59,300
The way those content producers make their money

197
00:08:59,300 --> 00:09:02,240
is by leaving empty slots on their websites

198
00:09:02,240 --> 00:09:05,410
where advertisers can place their ads.

199
00:09:05,410 --> 00:09:09,590
And anytime a user either sees or clicks on an ad,

200
00:09:09,590 --> 00:09:13,253
the advertiser pays the content producer a small fee.

201
00:09:14,220 --> 00:09:17,120
Our system acts as a middleman.

202
00:09:17,120 --> 00:09:20,830
On the one hand, our system provides the advertisers

203
00:09:20,830 --> 00:09:23,920
with websites that they can advertise on.

204
00:09:23,920 --> 00:09:27,830
On the other hand, our system provides content producers

205
00:09:27,830 --> 00:09:31,040
with advertisers that are willing to provide ads

206
00:09:31,040 --> 00:09:32,193
and pay them money.

207
00:09:33,230 --> 00:09:37,680
So when a user opens their favorite blog or news website,

208
00:09:37,680 --> 00:09:40,830
this website sends a request to our system,

209
00:09:40,830 --> 00:09:42,660
requesting an ad.

210
00:09:42,660 --> 00:09:45,810
Then we find the best and most relevant ad

211
00:09:45,810 --> 00:09:47,950
for the user from all the pool

212
00:09:47,950 --> 00:09:49,920
of advertisers that we have,

213
00:09:49,920 --> 00:09:52,700
and then send that ad back to the website,

214
00:09:52,700 --> 00:09:55,070
so the user will see both the content

215
00:09:55,070 --> 00:09:57,230
and the ad all on one page

216
00:09:57,230 --> 00:10:00,200
when the page loads in the user's browser.

217
00:10:00,200 --> 00:10:03,503
So now that was the synchronous part of our system.

218
00:10:04,650 --> 00:10:08,890
The asynchronous big data processing part kicks in

219
00:10:08,890 --> 00:10:12,480
when we start getting events about each ad.

220
00:10:12,480 --> 00:10:15,210
The three types of events we're going to process

221
00:10:15,210 --> 00:10:16,610
are as follows:

222
00:10:16,610 --> 00:10:19,940
The first type of event when the user scrolls

223
00:10:19,940 --> 00:10:22,050
through the page and gets to the part

224
00:10:22,050 --> 00:10:24,720
where they see the ad on the website.

225
00:10:24,720 --> 00:10:27,000
The second type of event happens

226
00:10:27,000 --> 00:10:29,350
when a user clicks on a particular ad

227
00:10:29,350 --> 00:10:33,450
and is then redirected to the advertiser's website.

228
00:10:33,450 --> 00:10:35,480
The third type of event happens

229
00:10:35,480 --> 00:10:37,570
when the user makes a purchase

230
00:10:37,570 --> 00:10:39,560
on the advertiser website,

231
00:10:39,560 --> 00:10:42,533
which is the ultimate goal of each advertiser.

232
00:10:43,410 --> 00:10:45,650
To process this massive stream

233
00:10:45,650 --> 00:10:47,580
of those three types of events,

234
00:10:47,580 --> 00:10:50,330
we're going to use the Lambda Architecture.

235
00:10:50,330 --> 00:10:53,570
Each one of those events is going to be processed

236
00:10:53,570 --> 00:10:56,480
by both the batch layer and the speed layer.

237
00:10:56,480 --> 00:10:59,120
And eventually, it will be ready to be queried

238
00:10:59,120 --> 00:11:00,403
by the serving layer.

239
00:11:01,480 --> 00:11:04,780
The serving layer may get different types of queries

240
00:11:04,780 --> 00:11:07,080
which will require different parts

241
00:11:07,080 --> 00:11:09,720
of our big data processing system.

242
00:11:09,720 --> 00:11:12,910
Let's see a few concrete examples to understand

243
00:11:12,910 --> 00:11:15,653
how Lambda Architecture works in practice.

244
00:11:16,660 --> 00:11:21,350
For example, an advertiser can send us a query asking

245
00:11:21,350 --> 00:11:23,170
about the number of users

246
00:11:23,170 --> 00:11:25,430
that are currently viewing their ads

247
00:11:25,430 --> 00:11:28,150
across all the different websites.

248
00:11:28,150 --> 00:11:30,530
This can easily be satisfied

249
00:11:30,530 --> 00:11:34,200
by the data that we have from the speed layer.

250
00:11:34,200 --> 00:11:37,240
On the other hand, the advertiser can send us

251
00:11:37,240 --> 00:11:40,030
a query asking about how many ads

252
00:11:40,030 --> 00:11:43,540
were shown to users in the last 24 hours.

253
00:11:43,540 --> 00:11:46,410
The purpose of the query may be to make sure

254
00:11:46,410 --> 00:11:49,810
that the advertiser stays within their budget

255
00:11:49,810 --> 00:11:52,713
and doesn't spend more money than they intended to.

256
00:11:53,870 --> 00:11:56,890
Now, if we assume that our batch layer

257
00:11:56,890 --> 00:11:59,640
processes incoming events every two hours,

258
00:11:59,640 --> 00:12:03,560
for example, this query will have to merge data

259
00:12:03,560 --> 00:12:06,117
from both the batch layer and the speed layer,

260
00:12:06,117 --> 00:12:09,210
taking the last two hours from the speed layer

261
00:12:09,210 --> 00:12:12,373
and the previous 22 hours from the batch layer.

262
00:12:13,400 --> 00:12:17,320
Also at some point, the advertiser will want to know

263
00:12:17,320 --> 00:12:19,210
the return on their investment

264
00:12:19,210 --> 00:12:22,370
and may ask us how much money they need to spend

265
00:12:22,370 --> 00:12:25,763
on advertising before a user makes a purchase.

266
00:12:26,890 --> 00:12:28,950
Now, it's important to understand

267
00:12:28,950 --> 00:12:31,350
that a user may see the same ad

268
00:12:31,350 --> 00:12:34,360
on multiple websites, sometimes in a course

269
00:12:34,360 --> 00:12:37,450
of a month until they finally click on it,

270
00:12:37,450 --> 00:12:39,770
and maybe only a few weeks later,

271
00:12:39,770 --> 00:12:41,563
they decide to make a purchase.

272
00:12:42,620 --> 00:12:45,250
So in this case, we would have to rely

273
00:12:45,250 --> 00:12:47,950
on the deep analytics and data fusion

274
00:12:47,950 --> 00:12:49,940
that we have in the batch layer.

275
00:12:49,940 --> 00:12:52,763
So we can provide this data to the advertiser.

276
00:12:53,860 --> 00:12:57,790
In this lecture, we learned about a very architecture

277
00:12:57,790 --> 00:13:01,240
for big data processing, the Lambda Architecture.

278
00:13:01,240 --> 00:13:03,630
This architecture is a hybrid

279
00:13:03,630 --> 00:13:05,590
between the batch processing model

280
00:13:05,590 --> 00:13:07,480
and the real-time processing model

281
00:13:07,480 --> 00:13:09,600
which allows us to enjoy the benefits

282
00:13:09,600 --> 00:13:13,430
of both worlds for use cases when choosing only one

283
00:13:13,430 --> 00:13:16,350
of those strategies is not an option.

284
00:13:16,350 --> 00:13:18,970
And finally, we explored the real-life

285
00:13:18,970 --> 00:13:21,750
practical scenario where Lambda Architecture

286
00:13:21,750 --> 00:13:25,450
could be used to satisfy a wide range of queries

287
00:13:25,450 --> 00:13:28,773
that involved both real-time and historic data.

288
00:13:29,830 --> 00:13:31,320
I hope you had a lot of fun

289
00:13:31,320 --> 00:13:33,390
and learn a lot in this course

290
00:13:33,390 --> 00:13:35,230
and I hope to see you soon again

291
00:13:35,230 --> 00:13:37,113
for another learning experience.

