1
00:00:00,260 --> 00:00:02,330
Hey, welcome back.

2
00:00:02,330 --> 00:00:04,300
Now that we got a good introduction

3
00:00:04,300 --> 00:00:06,450
to the field of Big Data in general,

4
00:00:06,450 --> 00:00:09,130
let's learn about two architectural patterns

5
00:00:09,130 --> 00:00:13,430
for processing big data using event driven architecture.

6
00:00:13,430 --> 00:00:16,300
But before we talk about big data processing,

7
00:00:16,300 --> 00:00:19,433
let's clearly define the problem we're trying to solve.

8
00:00:20,460 --> 00:00:23,760
Let's assume we're getting a continuous stream of data

9
00:00:23,760 --> 00:00:27,100
coming from different sources into our system.

10
00:00:27,100 --> 00:00:29,410
This may be user interactions

11
00:00:29,410 --> 00:00:33,080
on our social media platform, logs or metrics

12
00:00:33,080 --> 00:00:35,900
coming from production application instances,

13
00:00:35,900 --> 00:00:39,050
or data coming from transportation devices

14
00:00:39,050 --> 00:00:43,210
such as planes, trains, or autonomous cars.

15
00:00:43,210 --> 00:00:46,030
All this large volume of raw data

16
00:00:46,030 --> 00:00:48,140
is coming to us in real time.

17
00:00:48,140 --> 00:00:51,230
And we want to process it so we can analyze it

18
00:00:51,230 --> 00:00:55,720
and provide insights, visualization, or predictions.

19
00:00:55,720 --> 00:00:58,653
Now how do we go about processing this data?

20
00:00:59,700 --> 00:01:02,330
Well, there are generally two strategies

21
00:01:02,330 --> 00:01:04,690
or patterns we can use.

22
00:01:04,690 --> 00:01:07,563
The first strategy is called Batch Processing.

23
00:01:08,580 --> 00:01:10,430
When we use batch processing,

24
00:01:10,430 --> 00:01:12,940
we normally store the incoming data

25
00:01:12,940 --> 00:01:15,000
either in a distributed database

26
00:01:15,000 --> 00:01:19,400
or more commonly directly on a distributed file system.

27
00:01:19,400 --> 00:01:21,910
That data is never modified

28
00:01:21,910 --> 00:01:25,253
and we only append to it as more data arrives.

29
00:01:26,400 --> 00:01:29,360
Now the key principle in batch processing

30
00:01:29,360 --> 00:01:32,210
is we do not process each piece of data

31
00:01:32,210 --> 00:01:34,830
that comes in our system individually.

32
00:01:34,830 --> 00:01:38,940
Instead, we process that data in batches of records

33
00:01:38,940 --> 00:01:41,290
and we typically perform this processing

34
00:01:41,290 --> 00:01:42,910
on a fixed schedule.

35
00:01:42,910 --> 00:01:47,110
Though we can also run it based on a fixed number of records

36
00:01:47,110 --> 00:01:48,503
that we want to process.

37
00:01:49,640 --> 00:01:52,850
This batch processing can run on any schedule

38
00:01:52,850 --> 00:01:54,370
that fits our use case,

39
00:01:54,370 --> 00:01:56,960
which can be once a month, once a day,

40
00:01:56,960 --> 00:01:58,933
once an hour, and so on.

41
00:02:00,020 --> 00:02:02,450
The logic that goes into that job

42
00:02:02,450 --> 00:02:05,370
that processes this data is written by us

43
00:02:05,370 --> 00:02:08,383
and we can easily update it whenever we want to.

44
00:02:09,490 --> 00:02:12,400
Every time this batch processing job runs,

45
00:02:12,400 --> 00:02:14,830
it picks up the new raw data

46
00:02:14,830 --> 00:02:17,070
that came since the last time it ran,

47
00:02:17,070 --> 00:02:21,160
then it analyzes it and produces an up-to-date view

48
00:02:21,160 --> 00:02:24,070
of all the data we currently have.

49
00:02:24,070 --> 00:02:27,150
This view can be stored in a well structured

50
00:02:27,150 --> 00:02:30,400
and indexed database that we can easily query

51
00:02:30,400 --> 00:02:32,233
to get the desired insights.

52
00:02:33,390 --> 00:02:34,690
It's important to note

53
00:02:34,690 --> 00:02:38,180
that this view we generate should reflect the knowledge

54
00:02:38,180 --> 00:02:41,410
we have about our entire data set.

55
00:02:41,410 --> 00:02:43,370
And depending on the use case,

56
00:02:43,370 --> 00:02:47,140
this batch processing job can pick up only the data

57
00:02:47,140 --> 00:02:48,880
that arrived recently

58
00:02:48,880 --> 00:02:51,720
or it can process the entire dataset

59
00:02:51,720 --> 00:02:55,080
from scratch to provide us with a new view of the data

60
00:02:55,080 --> 00:02:55,993
we collected.

61
00:02:57,010 --> 00:02:59,540
Let's look at two perfect use cases

62
00:02:59,540 --> 00:03:03,090
for batch processing to illustrate the process.

63
00:03:03,090 --> 00:03:04,360
Let's assume we have

64
00:03:04,360 --> 00:03:07,170
an online learning subscription platform

65
00:03:07,170 --> 00:03:09,710
where we offer thousands of video courses

66
00:03:09,710 --> 00:03:12,670
to millions of students around the world.

67
00:03:12,670 --> 00:03:15,430
While students watch those video courses,

68
00:03:15,430 --> 00:03:18,060
they can leave a review and rate the course

69
00:03:18,060 --> 00:03:19,770
that they are currently watching

70
00:03:19,770 --> 00:03:22,230
to reflect their satisfaction from the course

71
00:03:22,230 --> 00:03:23,863
at that point in time.

72
00:03:24,930 --> 00:03:29,930
On our end what we get is a stream of two types of events.

73
00:03:30,220 --> 00:03:33,300
The first type of event indicates the progress

74
00:03:33,300 --> 00:03:36,910
a student is making by watching a video course.

75
00:03:36,910 --> 00:03:39,700
So for example, every additional minute

76
00:03:39,700 --> 00:03:43,483
that a student watches, we get an event into our system.

77
00:03:44,620 --> 00:03:46,260
We can roughly estimate

78
00:03:46,260 --> 00:03:50,090
that if we have a hundred million students on our platform,

79
00:03:50,090 --> 00:03:53,950
even if only 10% of them are active on our platform

80
00:03:53,950 --> 00:03:55,220
at a given moment,

81
00:03:55,220 --> 00:03:58,630
we can expect about a million events per minute.

82
00:03:58,630 --> 00:04:02,273
So this definitely falls within the scope of big data.

83
00:04:03,260 --> 00:04:07,660
Another type of event we get is a review and a star rating

84
00:04:07,660 --> 00:04:09,083
for a particular course.

85
00:04:10,170 --> 00:04:14,940
Now we do not need to process any of this data in real time.

86
00:04:14,940 --> 00:04:17,950
However, if we process it in batches

87
00:04:17,950 --> 00:04:21,640
and take into account historical data for each course,

88
00:04:21,640 --> 00:04:24,950
we can provide a lot of mission critical insights

89
00:04:24,950 --> 00:04:26,253
for our business.

90
00:04:27,320 --> 00:04:29,550
For example, we can use the data

91
00:04:29,550 --> 00:04:32,200
about the content consumption for each course

92
00:04:32,200 --> 00:04:35,600
to compensate our instructors based on the percentage

93
00:04:35,600 --> 00:04:38,790
of the content that was consumed from their courses

94
00:04:38,790 --> 00:04:42,500
out of all the available content on the platform.

95
00:04:42,500 --> 00:04:45,830
Also, based the ratings we get for each course,

96
00:04:45,830 --> 00:04:50,360
we can recalculate the average rating for each course daily.

97
00:04:50,360 --> 00:04:52,960
However, we can go much farther

98
00:04:52,960 --> 00:04:56,670
and fuse the data from those two types of events.

99
00:04:56,670 --> 00:04:59,660
For example, when we calculate the average rating

100
00:04:59,660 --> 00:05:02,590
for each course, we can provide higher weight

101
00:05:02,590 --> 00:05:05,010
to ratings that came from students

102
00:05:05,010 --> 00:05:07,490
who watched a higher percentage of the course

103
00:05:07,490 --> 00:05:11,010
and give lower weight to ratings that came from students

104
00:05:11,010 --> 00:05:13,683
who barely watched just a few lectures.

105
00:05:14,760 --> 00:05:18,980
This way if we have 10 students that left a one star rating

106
00:05:18,980 --> 00:05:21,780
and watched less than 10% of the course,

107
00:05:21,780 --> 00:05:25,620
and we have another 10 students who left a five star rating

108
00:05:25,620 --> 00:05:27,360
and watch the entire course,

109
00:05:27,360 --> 00:05:30,560
we can rate the course much closer to five stars

110
00:05:30,560 --> 00:05:33,670
with more confidence that this reflects the quality

111
00:05:33,670 --> 00:05:35,493
of the course more accurately.

112
00:05:36,620 --> 00:05:38,870
Another example of data fusion

113
00:05:38,870 --> 00:05:41,230
we can have from those two types of events

114
00:05:41,230 --> 00:05:44,060
is to rank the courses in each category

115
00:05:44,060 --> 00:05:47,570
based on the rating, as well as the overall engagement

116
00:05:47,570 --> 00:05:49,543
of the students in each course.

117
00:05:50,580 --> 00:05:53,290
Finally, we can combine all that data

118
00:05:53,290 --> 00:05:55,250
to build a machine learning model

119
00:05:55,250 --> 00:05:58,880
and try to predict what type of students can benefit

120
00:05:58,880 --> 00:06:02,420
from which courses and send them push notifications

121
00:06:02,420 --> 00:06:05,523
or display those courses at the top of the page.

122
00:06:06,800 --> 00:06:10,270
Another very common use case for batch processing

123
00:06:10,270 --> 00:06:12,460
is search engine services.

124
00:06:12,460 --> 00:06:14,590
When we have large amounts of data

125
00:06:14,590 --> 00:06:17,900
such as websites, articles, or images

126
00:06:17,900 --> 00:06:20,700
that we want to provide search capabilities for,

127
00:06:20,700 --> 00:06:22,830
if we try to scan all of it

128
00:06:22,830 --> 00:06:25,170
every time a user perform the search,

129
00:06:25,170 --> 00:06:28,173
it would take us hours to provide a result.

130
00:06:29,250 --> 00:06:32,030
So what search companies typically do

131
00:06:32,030 --> 00:06:37,030
is crawl the entire data set periodically and organize index

132
00:06:37,140 --> 00:06:40,640
and store it in a way that is very easy and fast

133
00:06:40,640 --> 00:06:41,653
to search for.

134
00:06:42,700 --> 00:06:45,210
So because this crawling process

135
00:06:45,210 --> 00:06:48,910
takes a very long time anyway and there is no real need

136
00:06:48,910 --> 00:06:52,920
or expectation that every new website or article

137
00:06:52,920 --> 00:06:55,640
would appear in the search results immediately,

138
00:06:55,640 --> 00:06:59,063
this is also a perfect use case for batch processing.

139
00:07:00,100 --> 00:07:03,580
So now that we got some intuition for batch processing

140
00:07:03,580 --> 00:07:07,120
from real use cases, let's talk about the advantages

141
00:07:07,120 --> 00:07:10,350
and disadvantages of this processing model.

142
00:07:10,350 --> 00:07:12,750
The first advantage of batch processing

143
00:07:12,750 --> 00:07:15,230
is that it's very easy to implement

144
00:07:15,230 --> 00:07:18,193
because we don't have to worry about latency.

145
00:07:19,290 --> 00:07:22,880
Batch processing also provides us with high availability

146
00:07:23,950 --> 00:07:26,280
because until a currently running job

147
00:07:26,280 --> 00:07:29,200
is done analyzing the entire dataset

148
00:07:29,200 --> 00:07:30,800
and providing the new view,

149
00:07:30,800 --> 00:07:34,300
the old view is still available for us for querying.

150
00:07:34,300 --> 00:07:37,463
So there's essentially no downtime for the users.

151
00:07:38,490 --> 00:07:42,560
Another advantage of batch processing is efficiency.

152
00:07:42,560 --> 00:07:47,010
Processing data in batches is usually a lot more efficient

153
00:07:47,010 --> 00:07:50,123
than processing each incoming record individually.

154
00:07:51,350 --> 00:07:52,750
With batch processing

155
00:07:52,750 --> 00:07:55,770
we also have a much higher fault tolerance

156
00:07:55,770 --> 00:07:57,123
towards human error.

157
00:07:58,140 --> 00:08:02,600
For example, if we push some bad code to our processing job,

158
00:08:02,600 --> 00:08:04,430
there is not much harm done

159
00:08:04,430 --> 00:08:07,750
because we still have our original data.

160
00:08:07,750 --> 00:08:11,330
So we can fix our bug, redeploy the processing job,

161
00:08:11,330 --> 00:08:15,160
and run it again to analyze our entire data set

162
00:08:15,160 --> 00:08:18,803
which would produce a new and correct view of our data.

163
00:08:19,810 --> 00:08:24,020
And finally, batch processing can perform very complex

164
00:08:24,020 --> 00:08:27,060
and deep analysis of large data sets

165
00:08:27,060 --> 00:08:30,060
that can span years of data points.

166
00:08:30,060 --> 00:08:33,970
This can provide us with very advanced prediction models

167
00:08:33,970 --> 00:08:35,053
and insights.

168
00:08:36,059 --> 00:08:40,520
On the other hand, batch processing has one major drawback

169
00:08:40,520 --> 00:08:44,090
that makes it not suitable for many use cases.

170
00:08:44,090 --> 00:08:47,080
This drawback is the long delay we have

171
00:08:47,080 --> 00:08:49,697
between the data coming into our system

172
00:08:49,697 --> 00:08:53,410
and the result we get from the processing job.

173
00:08:53,410 --> 00:08:58,270
If we run our batch processing job every day or every hour,

174
00:08:58,270 --> 00:09:02,820
we don't get a real time view of the data coming in.

175
00:09:02,820 --> 00:09:05,880
And if we don't get a real view of what's happening

176
00:09:05,880 --> 00:09:09,530
on our platform, we can't respond to it fast enough,

177
00:09:09,530 --> 00:09:13,270
which is extremely important in many use cases.

178
00:09:13,270 --> 00:09:17,230
This also forces our users to wait a long time

179
00:09:17,230 --> 00:09:19,760
before they can get feedback on actions

180
00:09:19,760 --> 00:09:21,480
they take on our system.

181
00:09:21,480 --> 00:09:24,230
This in turn may cause some confusion

182
00:09:24,230 --> 00:09:26,260
if they're not aware of the fact

183
00:09:26,260 --> 00:09:28,823
that we analyze our data in batches.

184
00:09:29,830 --> 00:09:32,510
For example, if we have an online store

185
00:09:32,510 --> 00:09:35,940
that uses batch processing to index different products

186
00:09:35,940 --> 00:09:38,700
on our platform for search purposes,

187
00:09:38,700 --> 00:09:40,860
a merchant who just added

188
00:09:40,860 --> 00:09:44,430
or updated their product description may be surprised

189
00:09:44,430 --> 00:09:47,560
why their product doesn't show up in the search results

190
00:09:47,560 --> 00:09:49,610
on our online store.

191
00:09:49,610 --> 00:09:53,000
In this particular case, we can simply inform them

192
00:09:53,000 --> 00:09:55,360
that those changes may take effect

193
00:09:55,360 --> 00:09:57,440
only after one business day.

194
00:09:57,440 --> 00:09:59,120
And they may be okay with that.

195
00:09:59,120 --> 00:10:03,173
But in other cases, batch processing just won't work.

196
00:10:04,210 --> 00:10:08,170
For example, if we have a log and metrics analysis system

197
00:10:08,170 --> 00:10:11,490
that collects streams of log files and data points

198
00:10:11,490 --> 00:10:14,800
from thousands of production application instances,

199
00:10:14,800 --> 00:10:19,100
we have to be able to ingest and visualize all this data

200
00:10:19,100 --> 00:10:20,520
in real time.

201
00:10:20,520 --> 00:10:24,330
Otherwise, if there's a production issue in our data center,

202
00:10:24,330 --> 00:10:27,620
the engineers on call have to have those logs

203
00:10:27,620 --> 00:10:30,480
and metric graphs available immediately

204
00:10:30,480 --> 00:10:33,403
so they can figure out how to fix the issue.

205
00:10:34,460 --> 00:10:38,160
Similarly, if we have an online stock trading system,

206
00:10:38,160 --> 00:10:42,290
we have to be able to take in both bids and asks,

207
00:10:42,290 --> 00:10:45,830
match them together so people can trade on our platform

208
00:10:45,830 --> 00:10:49,743
and also update the price on each stock in real time.

209
00:10:50,870 --> 00:10:53,860
So the second way we can process big data

210
00:10:53,860 --> 00:10:55,513
is real time processing.

211
00:10:56,570 --> 00:10:59,860
In real time processing, we put each new event

212
00:10:59,860 --> 00:11:03,700
that comes to our system in a queue or a message broker.

213
00:11:03,700 --> 00:11:06,860
And on the other end, we have a processing job

214
00:11:06,860 --> 00:11:10,663
that processes each individual data record as it comes.

215
00:11:11,600 --> 00:11:13,920
After that record is processed,

216
00:11:13,920 --> 00:11:16,740
the processing job updates the database

217
00:11:16,740 --> 00:11:18,840
that provides querying capabilities

218
00:11:18,840 --> 00:11:21,823
for real time visualization and analysis.

219
00:11:22,890 --> 00:11:26,550
The obvious advantage of the real time processing model

220
00:11:26,550 --> 00:11:29,510
is we can analyze and respond to data

221
00:11:29,510 --> 00:11:32,200
as it comes to our system immediately

222
00:11:32,200 --> 00:11:36,073
without having to wait hours for a job to process it.

223
00:11:37,090 --> 00:11:39,630
The drawback for real time processing

224
00:11:39,630 --> 00:11:43,330
is that it's very hard to do any complex analysis

225
00:11:43,330 --> 00:11:44,450
in real time.

226
00:11:44,450 --> 00:11:48,770
So we don't get as much value or insight into our system

227
00:11:48,770 --> 00:11:52,470
as we get if we process it using batch processing.

228
00:11:52,470 --> 00:11:56,350
Additionally, doing data fusion from different events

229
00:11:56,350 --> 00:11:58,760
that happened at different time points

230
00:11:58,760 --> 00:12:03,220
or analyzing historic data is also nearly impossible.

231
00:12:03,220 --> 00:12:05,880
So we're limited to the recent data

232
00:12:05,880 --> 00:12:08,820
that we currently have to provide those insights

233
00:12:08,820 --> 00:12:10,003
or predictions.

234
00:12:11,050 --> 00:12:14,320
In this lecture, we'll learn about two different strategies

235
00:12:14,320 --> 00:12:18,660
for processing large volumes of data into our system.

236
00:12:18,660 --> 00:12:21,520
Those two strategies are batch processing

237
00:12:21,520 --> 00:12:23,103
and real time processing.

238
00:12:24,170 --> 00:12:26,320
We covered quite a few use cases

239
00:12:26,320 --> 00:12:28,300
that fit each of those models

240
00:12:28,300 --> 00:12:30,810
and also compared the two strategies

241
00:12:30,810 --> 00:12:33,733
by listing their advantages and disadvantages.

242
00:12:34,640 --> 00:12:36,873
I will see you soon in the next lecture.

