1
00:00:00,660 --> 00:00:03,660
Welcome back to another lecture.

2
00:00:03,660 --> 00:00:04,740
In this lecture,

3
00:00:04,740 --> 00:00:07,450
we're going to get an introduction and motivation

4
00:00:07,450 --> 00:00:09,820
for big data processing.

5
00:00:09,820 --> 00:00:13,193
So let's start with describing what big data means.

6
00:00:14,300 --> 00:00:18,000
Big data is a term used to describe datasets

7
00:00:18,000 --> 00:00:21,780
that are either too large in size, too complex in structure,

8
00:00:21,780 --> 00:00:24,720
or come to our system at such a high rate

9
00:00:24,720 --> 00:00:28,170
that exceeds the capacity of a traditional application

10
00:00:28,170 --> 00:00:31,493
to process it fast enough to provide any value.

11
00:00:32,530 --> 00:00:35,200
There are many characteristics of big data,

12
00:00:35,200 --> 00:00:37,683
but three of them are the most prominent.

13
00:00:38,650 --> 00:00:42,003
The first characteristic of big data is volume.

14
00:00:43,270 --> 00:00:46,230
Volume refers to the quantity of the data

15
00:00:46,230 --> 00:00:49,393
that we need to process, store, and analyze.

16
00:00:50,230 --> 00:00:52,000
When we talk about big data,

17
00:00:52,000 --> 00:00:54,730
we're talking about large quantities of data,

18
00:00:54,730 --> 00:00:59,000
typically in the order of magnitude of terabytes, petabytes,

19
00:00:59,000 --> 00:01:00,853
or even more per day.

20
00:01:02,050 --> 00:01:05,930
A few technology fields where we have a high volume of data

21
00:01:05,930 --> 00:01:08,210
include internet search companies

22
00:01:08,210 --> 00:01:10,880
that have to analyze the entire internet

23
00:01:10,880 --> 00:01:14,830
and provide instant search capabilities for their users.

24
00:01:14,830 --> 00:01:18,030
Another example is medical software systems

25
00:01:18,030 --> 00:01:21,760
that collect, analyze, and store a lot of information

26
00:01:21,760 --> 00:01:24,620
about patients in hospitals or clinics

27
00:01:24,620 --> 00:01:28,300
and can help in preventing or detecting diseases.

28
00:01:28,300 --> 00:01:31,280
Then we have real-time security systems

29
00:01:31,280 --> 00:01:34,800
that analyze multiple video streams coming from cameras

30
00:01:34,800 --> 00:01:37,660
located throughout certain neighborhoods, cities,

31
00:01:37,660 --> 00:01:39,940
or high security facilities.

32
00:01:39,940 --> 00:01:43,950
The purpose of those systems is to help combat crime.

33
00:01:43,950 --> 00:01:46,930
Finally, we have the weather prediction systems

34
00:01:46,930 --> 00:01:48,980
that analyze a lot of data

35
00:01:48,980 --> 00:01:51,630
from different sensors located on satellites,

36
00:01:51,630 --> 00:01:53,540
as well as in different locations

37
00:01:53,540 --> 00:01:56,020
throughout the large geographical region.

38
00:01:56,020 --> 00:01:58,880
Those systems can help us predict the weather,

39
00:01:58,880 --> 00:02:02,963
as well as alert us about upcoming storms or tsunamis.

40
00:02:04,120 --> 00:02:07,433
The second characteristic of big data is variety.

41
00:02:08,660 --> 00:02:11,460
In traditional non-big data systems,

42
00:02:11,460 --> 00:02:14,890
we typically work with a limited number of structured

43
00:02:14,890 --> 00:02:17,620
and well-defined types of data.

44
00:02:17,620 --> 00:02:20,680
However, when we move to the field of big data,

45
00:02:20,680 --> 00:02:25,070
we can have a large variety of potentially unstructured data

46
00:02:25,070 --> 00:02:27,730
that we collect from multiple sources.

47
00:02:27,730 --> 00:02:32,110
Our goal is to process all that data and combine it together

48
00:02:32,110 --> 00:02:34,980
through a process called data fusion.

49
00:02:34,980 --> 00:02:37,730
This can help us find hidden patterns

50
00:02:37,730 --> 00:02:41,270
or provide business insights for our organization

51
00:02:41,270 --> 00:02:45,373
that aren't obvious if we analyze only one data source.

52
00:02:46,310 --> 00:02:50,010
An example of that are social media services or apps

53
00:02:50,010 --> 00:02:52,940
that collect a lot of different types of data

54
00:02:52,940 --> 00:02:56,343
about the behavior of their user base in real time.

55
00:02:57,240 --> 00:02:59,880
For instance, they can collect information

56
00:02:59,880 --> 00:03:03,730
about the user's clicks, likes, shares, or posts,

57
00:03:03,730 --> 00:03:05,800
as well as capture the amount of time

58
00:03:05,800 --> 00:03:08,370
a user spent watching a particular video

59
00:03:08,370 --> 00:03:12,620
or even hovering over an article or a digital ad.

60
00:03:12,620 --> 00:03:15,870
All those types of seemingly unrelated activities

61
00:03:15,870 --> 00:03:18,600
can be combined together and build models

62
00:03:18,600 --> 00:03:22,480
that can predict the behavior and response of each user

63
00:03:22,480 --> 00:03:25,520
to future ads of particular products.

64
00:03:25,520 --> 00:03:27,840
But also on an aggregate level,

65
00:03:27,840 --> 00:03:31,290
if we combine all that data from multiple users,

66
00:03:31,290 --> 00:03:33,210
we can detect internet trends,

67
00:03:33,210 --> 00:03:35,223
as well as clusters of interest.

68
00:03:36,280 --> 00:03:38,480
Now finally, the third characteristic

69
00:03:38,480 --> 00:03:40,373
of big data is velocity.

70
00:03:41,520 --> 00:03:43,180
When we deal with big data,

71
00:03:43,180 --> 00:03:46,200
we normally have a continuous stream of data

72
00:03:46,200 --> 00:03:49,950
that comes to our system at a very high rate.

73
00:03:49,950 --> 00:03:52,020
The high rate of incoming data

74
00:03:52,020 --> 00:03:55,110
can be either due to the large scale of our system

75
00:03:55,110 --> 00:03:57,793
or simply the high frequency of events.

76
00:03:58,720 --> 00:04:01,410
For example, if we have an online store

77
00:04:01,410 --> 00:04:03,440
that operates on a global scale

78
00:04:03,440 --> 00:04:07,250
with millions of users visiting our website every day,

79
00:04:07,250 --> 00:04:09,840
browsing and purchasing our products,

80
00:04:09,840 --> 00:04:13,610
then the higher rate of events simply comes from the fact

81
00:04:13,610 --> 00:04:16,483
that we have a very large number of users.

82
00:04:17,500 --> 00:04:20,920
On the other hand, we have the field of internet of things

83
00:04:20,920 --> 00:04:23,630
which deals with connecting multiple devices

84
00:04:23,630 --> 00:04:26,143
and getting analytics from their sensors.

85
00:04:27,190 --> 00:04:31,290
In this case, we can have a relatively small fleet of buses,

86
00:04:31,290 --> 00:04:33,460
trains, or autonomous cars,

87
00:04:33,460 --> 00:04:35,710
but all those vehicles can generate

88
00:04:35,710 --> 00:04:38,830
lots and lots of data points from their sensors

89
00:04:38,830 --> 00:04:43,093
about their location, speed, surrounding objects and so on.

90
00:04:44,130 --> 00:04:48,180
Similarly, we can have a food or clothing production factory

91
00:04:48,180 --> 00:04:52,170
which is full of robots, assembly lines, and other machinery

92
00:04:52,170 --> 00:04:55,860
that constantly generate data about their production quality

93
00:04:55,860 --> 00:04:58,330
and speed from their sensors.

94
00:04:58,330 --> 00:05:00,160
In both those use cases,

95
00:05:00,160 --> 00:05:02,990
the number of devices may not be large.

96
00:05:02,990 --> 00:05:06,810
However, each sensor on a robot, a piece of machinery,

97
00:05:06,810 --> 00:05:08,750
or autonomous car can generate

98
00:05:08,750 --> 00:05:10,950
a continuous stream of data points

99
00:05:10,950 --> 00:05:13,313
that we have to ingest very quickly.

100
00:05:14,350 --> 00:05:17,250
Now, it's important to point out that storing

101
00:05:17,250 --> 00:05:20,320
and processing big data is pretty complex,

102
00:05:20,320 --> 00:05:22,200
as well as very expensive,

103
00:05:22,200 --> 00:05:24,000
but the value we get from it

104
00:05:24,000 --> 00:05:27,073
usually outweighs the cost associated with it.

105
00:05:28,210 --> 00:05:31,490
The insights we get from analyzing big data

106
00:05:31,490 --> 00:05:34,370
can provide a significant competitive advantage

107
00:05:34,370 --> 00:05:36,640
over our competitors.

108
00:05:36,640 --> 00:05:40,090
Those insights can come in a form of visualization,

109
00:05:40,090 --> 00:05:43,173
querying capabilities or predictive analysis.

110
00:05:44,270 --> 00:05:48,870
Visualization is a very powerful tool that can allow humans

111
00:05:48,870 --> 00:05:52,040
to make sense of otherwise meaningless data

112
00:05:52,040 --> 00:05:55,103
stored in some file system or database.

113
00:05:56,050 --> 00:05:59,220
In many cases, after we collect a lot of data,

114
00:05:59,220 --> 00:06:02,010
we don't necessarily know what to do with it

115
00:06:02,010 --> 00:06:05,320
or how we can benefit from it right away,

116
00:06:05,320 --> 00:06:08,260
so querying capabilities allow us to run

117
00:06:08,260 --> 00:06:10,690
ad hoc analysis on that data,

118
00:06:10,690 --> 00:06:14,660
which eventually helps us find those insights or patterns

119
00:06:14,660 --> 00:06:16,513
that were not obvious before.

120
00:06:17,540 --> 00:06:20,360
And finally, on the predictive analysis front,

121
00:06:20,360 --> 00:06:23,310
we can go as fancy as building algorithms

122
00:06:23,310 --> 00:06:24,920
or machine learning models

123
00:06:24,920 --> 00:06:27,250
to predict the behavior of our users

124
00:06:27,250 --> 00:06:31,470
and suggest products that they will more likely purchase.

125
00:06:31,470 --> 00:06:35,130
But we can also go as simple as detecting anomalies

126
00:06:35,130 --> 00:06:38,890
in our system by analyzing logs coming from our servers

127
00:06:38,890 --> 00:06:41,450
and automatically roll back a new release

128
00:06:41,450 --> 00:06:43,633
or alert the engineers on-call.

129
00:06:45,360 --> 00:06:48,510
So now that we have the understanding and the motivation

130
00:06:48,510 --> 00:06:50,340
behind big data processing,

131
00:06:50,340 --> 00:06:53,930
let's proceed and learn about a few architectural styles

132
00:06:53,930 --> 00:06:57,413
that help us in processing and analyzing big data.

