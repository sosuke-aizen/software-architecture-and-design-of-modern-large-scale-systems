1
00:00:00,140 --> 00:00:02,040
Welcome back.

2
00:00:02,040 --> 00:00:03,350
In the previous lecture

3
00:00:03,350 --> 00:00:05,520
we talked about availability in general

4
00:00:05,520 --> 00:00:08,820
and in particular the importance of high availability

5
00:00:08,820 --> 00:00:11,300
for both users and our business.

6
00:00:11,300 --> 00:00:14,440
In this lecture, we're going to focus on what prevents us

7
00:00:14,440 --> 00:00:17,010
from getting high availability out of the box

8
00:00:17,010 --> 00:00:19,550
and what measures and tactics we can take

9
00:00:19,550 --> 00:00:22,173
to improve the availability of our system.

10
00:00:23,550 --> 00:00:25,980
So generally there are three sources

11
00:00:25,980 --> 00:00:27,883
or categories of failures.

12
00:00:28,880 --> 00:00:30,770
The first and most common source

13
00:00:30,770 --> 00:00:32,850
of failures are human error,

14
00:00:32,850 --> 00:00:35,830
which includes pushing a faulty config to production,

15
00:00:35,830 --> 00:00:38,590
running the wrong command or the wrong script,

16
00:00:38,590 --> 00:00:41,290
or deploying a new version of our software

17
00:00:41,290 --> 00:00:43,163
that hasn't been thoroughly tested.

18
00:00:44,220 --> 00:00:47,610
The second category of failures, is software errors.

19
00:00:47,610 --> 00:00:50,810
Those include exceptionally long garbage collections

20
00:00:50,810 --> 00:00:53,550
and crashes such as out-of-memory exceptions,

21
00:00:53,550 --> 00:00:57,203
null pointer exceptions, segmentation faults, and so on.

22
00:00:58,350 --> 00:01:00,620
Lastly, we have hardware failures

23
00:01:00,620 --> 00:01:02,340
such as servers, routers

24
00:01:02,340 --> 00:01:04,610
or storage devices breaking down due

25
00:01:04,610 --> 00:01:06,230
to made shelf life,

26
00:01:06,230 --> 00:01:08,980
power outages due to natural disasters

27
00:01:08,980 --> 00:01:12,400
or network failures because of infrastructure issues

28
00:01:12,400 --> 00:01:13,943
or general congestion.

29
00:01:15,235 --> 00:01:17,430
Or, how can we deal with all those failures

30
00:01:17,430 --> 00:01:19,653
and still achieve high availability?

31
00:01:20,590 --> 00:01:22,680
Despite the obvious improvements

32
00:01:22,680 --> 00:01:24,650
that we can make to our code review,

33
00:01:24,650 --> 00:01:26,900
testing and release processes

34
00:01:26,900 --> 00:01:30,220
as well as performing ongoing maintenance to our hardware,

35
00:01:30,220 --> 00:01:32,623
inevitably failures will happen.

36
00:01:33,540 --> 00:01:37,110
So the best way to achieve high availability in our system

37
00:01:37,110 --> 00:01:38,853
is through fault tolerance.

38
00:01:39,790 --> 00:01:43,860
Fault tolerance enables our system to remain operational

39
00:01:43,860 --> 00:01:46,700
and available to the users despite failures

40
00:01:46,700 --> 00:01:49,563
within one or multiple of its components.

41
00:01:50,530 --> 00:01:52,380
When those failures happen,

42
00:01:52,380 --> 00:01:55,680
a fault tolerance system may continue operating

43
00:01:55,680 --> 00:01:57,780
at the same level of performance

44
00:01:57,780 --> 00:02:00,050
or a reduced level of performance,

45
00:02:00,050 --> 00:02:01,570
but it will prevent the system

46
00:02:01,570 --> 00:02:04,133
from becoming entirely unavailable.

47
00:02:05,080 --> 00:02:05,930
Generally,

48
00:02:05,930 --> 00:02:09,800
fault tolerance revolves around three major tactic,

49
00:02:09,800 --> 00:02:12,620
which are failure prevention, failure detection

50
00:02:12,620 --> 00:02:14,673
and isolation and recovery.

51
00:02:15,950 --> 00:02:18,690
The first thing we can do to prevent our system

52
00:02:18,690 --> 00:02:22,030
from going down is to eliminate any single point

53
00:02:22,030 --> 00:02:24,420
of failure in our system.

54
00:02:24,420 --> 00:02:27,100
A few examples of a single point of failure

55
00:02:27,100 --> 00:02:30,980
can be that one server where we are running our application

56
00:02:30,980 --> 00:02:35,010
or storing all our data on one instance of a database

57
00:02:35,010 --> 00:02:37,350
that runs on a single computer.

58
00:02:37,350 --> 00:02:40,470
The best way to eliminate a single point of failure

59
00:02:40,470 --> 00:02:43,033
is through replication and redundancy.

60
00:02:43,890 --> 00:02:46,790
For example, instead of running our service

61
00:02:46,790 --> 00:02:49,200
as a single instance on one server,

62
00:02:49,200 --> 00:02:50,790
we can run multiple copies

63
00:02:50,790 --> 00:02:54,193
or instances of our application on multiple servers.

64
00:02:55,120 --> 00:02:58,840
So if one of those servers goes down, for whatever reason,

65
00:02:58,840 --> 00:03:01,960
we can potentially direct all the network traffic

66
00:03:01,960 --> 00:03:03,613
to the other servers instead.

67
00:03:04,510 --> 00:03:07,010
Similarly, when it comes to databases,

68
00:03:07,010 --> 00:03:10,400
instead of storing all the information on one computer

69
00:03:10,400 --> 00:03:11,950
we can run multiple replicas

70
00:03:11,950 --> 00:03:14,410
of our database containing the same data

71
00:03:14,410 --> 00:03:16,140
on multiple computers.

72
00:03:16,140 --> 00:03:19,620
So losing any of those replicas will not cause us

73
00:03:19,620 --> 00:03:20,943
to lose any data.

74
00:03:21,940 --> 00:03:23,690
When it comes to computations,

75
00:03:23,690 --> 00:03:26,010
besides the spatial redundancy,

76
00:03:26,010 --> 00:03:28,270
which comes in the form of running replicas

77
00:03:28,270 --> 00:03:31,000
of our application on different computers,

78
00:03:31,000 --> 00:03:34,493
we can also have, what's referred to as time redundancy.

79
00:03:35,770 --> 00:03:38,490
Time redundancy essentially means repeating

80
00:03:38,490 --> 00:03:42,370
the same operation or the same request multiple times,

81
00:03:42,370 --> 00:03:44,783
until we either succeed or give up.

82
00:03:45,840 --> 00:03:48,910
Now, when it comes to redundancy and replication

83
00:03:48,910 --> 00:03:52,790
we have two strategies, both of which are extensively used

84
00:03:52,790 --> 00:03:55,470
in the industry in different systems.

85
00:03:55,470 --> 00:03:58,943
The first strategy is called active-active architecture.

86
00:04:00,159 --> 00:04:01,780
If we take the database example,

87
00:04:01,780 --> 00:04:04,960
it means that requests go to all the replicas,

88
00:04:04,960 --> 00:04:08,250
which forces them to all be in sync with each other.

89
00:04:08,250 --> 00:04:10,430
So if one of the replicas goes down,

90
00:04:10,430 --> 00:04:11,740
then the other replicas

91
00:04:11,740 --> 00:04:15,183
can step in and take all the requests immediately.

92
00:04:16,329 --> 00:04:20,100
The second strategy is called active-passive architecture,

93
00:04:20,100 --> 00:04:24,210
which implies that we have one primary instance or replica

94
00:04:24,210 --> 00:04:25,960
that takes all the requests,

95
00:04:25,960 --> 00:04:27,800
and the other passive replica

96
00:04:27,800 --> 00:04:30,810
or replicas follow the primary replica

97
00:04:30,810 --> 00:04:33,483
by taking periodic snapshots of its state.

98
00:04:34,690 --> 00:04:37,610
The advantage of the active-active architecture

99
00:04:37,610 --> 00:04:39,580
is that essentially we can spread

100
00:04:39,580 --> 00:04:41,910
the load among all the replicas,

101
00:04:41,910 --> 00:04:45,260
which is identical to horizontal scalability.

102
00:04:45,260 --> 00:04:47,730
This allows us to take more traffic

103
00:04:47,730 --> 00:04:49,623
and provide better performance.

104
00:04:50,860 --> 00:04:52,800
The disadvantage of this approach

105
00:04:52,800 --> 00:04:55,830
is that all the replicas are taking requests.

106
00:04:55,830 --> 00:04:59,200
So keeping all those active replicas in sync with each other

107
00:04:59,200 --> 00:05:00,400
is not trivial

108
00:05:00,400 --> 00:05:03,573
and it requires additional coordination and overhead.

109
00:05:04,610 --> 00:05:05,443
Similarly,

110
00:05:05,443 --> 00:05:08,370
if we take the active-passive architecture approach,

111
00:05:08,370 --> 00:05:11,240
then we lose the ability to scale our system,

112
00:05:11,240 --> 00:05:14,493
because all the requests still go to one machine.

113
00:05:15,360 --> 00:05:18,800
But the implementation of this approach is a lot easier

114
00:05:18,800 --> 00:05:20,530
because there is a clear leader

115
00:05:20,530 --> 00:05:22,910
that has the most up-to-date data

116
00:05:22,910 --> 00:05:25,693
and the rest of the replicas are just followers.

117
00:05:26,740 --> 00:05:27,910
The second and tactic

118
00:05:27,910 --> 00:05:30,200
for achieving fault tolerance in our system

119
00:05:30,200 --> 00:05:33,150
is failure detection and isolation.

120
00:05:33,150 --> 00:05:36,720
If we go back to this picture of our application running

121
00:05:36,720 --> 00:05:39,750
as multiple instances on different computers,

122
00:05:39,750 --> 00:05:41,960
if one of the instances crashes

123
00:05:41,960 --> 00:05:44,420
because of software or hardware issues

124
00:05:44,420 --> 00:05:46,210
then we need the ability

125
00:05:46,210 --> 00:05:48,570
to detect that this instance is faulty

126
00:05:48,570 --> 00:05:51,330
and isolated from the rest of the group.

127
00:05:51,330 --> 00:05:54,510
This way we can take actions like stopping requests

128
00:05:54,510 --> 00:05:56,493
for going to this particular server.

129
00:05:57,670 --> 00:05:59,940
Typically, to detect such failures

130
00:05:59,940 --> 00:06:04,090
we need a separate system that acts as a monitoring service.

131
00:06:04,090 --> 00:06:05,480
The monitoring service

132
00:06:05,480 --> 00:06:07,840
can monitor the health of our instances

133
00:06:07,840 --> 00:06:11,070
by simply sending it period health check messages.

134
00:06:11,070 --> 00:06:12,380
Or alternatively,

135
00:06:12,380 --> 00:06:15,460
it can listen to periodic messages called heartbeat

136
00:06:15,460 --> 00:06:17,200
that should come periodically

137
00:06:17,200 --> 00:06:19,150
from our healthy application instances.

138
00:06:20,500 --> 00:06:22,190
In either of those strategies,

139
00:06:22,190 --> 00:06:24,420
if the monitoring service does not hear

140
00:06:24,420 --> 00:06:28,100
from a particular server for a predefined duration of time

141
00:06:28,100 --> 00:06:31,803
it can assume that that server is no longer available.

142
00:06:33,000 --> 00:06:35,820
Of course, if the issue was in the network

143
00:06:35,820 --> 00:06:40,270
or that host simply had a temporarily on garbage collection

144
00:06:40,270 --> 00:06:43,620
the monitoring service is going to have a false positive

145
00:06:43,620 --> 00:06:46,813
and assume that a healthy host is faulty.

146
00:06:47,840 --> 00:06:48,970
But that's okay,

147
00:06:48,970 --> 00:06:51,510
we don't need the perfect monitoring service

148
00:06:51,510 --> 00:06:54,280
as long as it doesn't have false negatives.

149
00:06:54,280 --> 00:06:55,760
Because false negatives

150
00:06:55,760 --> 00:06:58,630
would mean that some of our servers may have crashed

151
00:06:58,630 --> 00:07:01,483
but our monitoring system did not detect that.

152
00:07:02,570 --> 00:07:05,310
Now, besides the simple exchange of messages

153
00:07:05,310 --> 00:07:07,760
in the form of pings and heartbeats,

154
00:07:07,760 --> 00:07:10,350
our monitoring system can be more complex

155
00:07:10,350 --> 00:07:12,593
and monitor for certain conditions.

156
00:07:13,550 --> 00:07:16,840
For example, our monitoring system can collect data

157
00:07:16,840 --> 00:07:18,660
about the number of exceptions

158
00:07:18,660 --> 00:07:21,710
or errors each host gets per minute.

159
00:07:21,710 --> 00:07:22,870
And if the error rate

160
00:07:22,870 --> 00:07:26,270
in one of those hosts becomes exceptionally high,

161
00:07:26,270 --> 00:07:28,400
our monitoring system can interpret

162
00:07:28,400 --> 00:07:31,590
that as an overall failure on that host.

163
00:07:31,590 --> 00:07:34,230
Similarly, it can collect information

164
00:07:34,230 --> 00:07:37,120
about how long each host takes to respond

165
00:07:37,120 --> 00:07:38,890
to incoming requests.

166
00:07:38,890 --> 00:07:42,210
And if that time becomes too long the monitoring system

167
00:07:42,210 --> 00:07:45,330
can decide that something must be wrong with that server

168
00:07:45,330 --> 00:07:47,473
because it became exceptionally slow.

169
00:07:48,370 --> 00:07:50,660
Now, let's talk about the third tactic

170
00:07:50,660 --> 00:07:52,230
in achieving fault tolerance,

171
00:07:52,230 --> 00:07:54,530
which is recovery from a failure.

172
00:07:54,530 --> 00:07:57,420
If we recall the second formula of availability

173
00:07:57,420 --> 00:07:59,290
that we learned in the previous lecture,

174
00:07:59,290 --> 00:08:03,030
we can see that regardless of our system failure rate

175
00:08:03,030 --> 00:08:04,980
if we can detect and recover

176
00:08:04,980 --> 00:08:08,280
from each failure faster than the user can notice

177
00:08:08,280 --> 00:08:11,003
then our system will have high availability.

178
00:08:12,010 --> 00:08:16,150
So once we detect and isolate the faulty instance or server

179
00:08:16,150 --> 00:08:18,150
we can take several actions.

180
00:08:18,150 --> 00:08:21,600
The obvious one is to stop sending any traffic

181
00:08:21,600 --> 00:08:24,790
or any workload to that particular host.

182
00:08:24,790 --> 00:08:26,490
The second action we can take

183
00:08:26,490 --> 00:08:28,280
is to attempt to restart it,

184
00:08:28,280 --> 00:08:31,010
with the assumption that after the restart

185
00:08:31,010 --> 00:08:33,470
the problem may go away.

186
00:08:33,470 --> 00:08:35,880
Another strategy which is commonly used

187
00:08:35,880 --> 00:08:37,770
is called a rollback.

188
00:08:37,770 --> 00:08:41,230
A rollback essentially means going back to a version

189
00:08:41,230 --> 00:08:43,943
that was still considered stable and correct.

190
00:08:44,890 --> 00:08:48,240
Rollbacks are very common in databases.

191
00:08:48,240 --> 00:08:49,790
If for some reason,

192
00:08:49,790 --> 00:08:52,720
we get to a state that violates some condition

193
00:08:52,720 --> 00:08:54,600
or the integrity of the data,

194
00:08:54,600 --> 00:08:57,710
we simply rolled back to the last state in the past

195
00:08:57,710 --> 00:08:59,543
which is considered to be correct.

196
00:09:00,490 --> 00:09:01,590
Similarly,

197
00:09:01,590 --> 00:09:04,330
if we're rolling out a new version of our software

198
00:09:04,330 --> 00:09:08,080
to all the servers, and we detect that all the servers

199
00:09:08,080 --> 00:09:11,050
with the new version are getting a lot of errors,

200
00:09:11,050 --> 00:09:14,070
we can automatically roll back to the previous version

201
00:09:14,070 --> 00:09:16,560
of our software to prevent our system

202
00:09:16,560 --> 00:09:18,793
from becoming entirely unavailable.

203
00:09:19,970 --> 00:09:22,740
In this lecture, we learned about fault tolerance

204
00:09:22,740 --> 00:09:24,930
which is how we achieve high availability

205
00:09:24,930 --> 00:09:27,000
in large-scale systems.

206
00:09:27,000 --> 00:09:29,830
We first discussed the different sources of failures

207
00:09:29,830 --> 00:09:31,280
we can have in our system,

208
00:09:31,280 --> 00:09:33,840
which are human error, software crashes,

209
00:09:33,840 --> 00:09:35,263
and hardware failures.

210
00:09:36,470 --> 00:09:39,030
Later we defined fault tolerance

211
00:09:39,030 --> 00:09:41,990
as the ability for our system to remain operational

212
00:09:41,990 --> 00:09:44,860
and available to the users despite failures

213
00:09:44,860 --> 00:09:47,670
to one or some of its components.

214
00:09:47,670 --> 00:09:51,260
And finally, we discussed in detail the three tactics

215
00:09:51,260 --> 00:09:53,190
we use to achieve fault tolerance,

216
00:09:53,190 --> 00:09:54,830
which are failure prevention,

217
00:09:54,830 --> 00:09:57,603
detection and isolation and recovery.

218
00:09:59,000 --> 00:10:01,013
I will see you soon in the next lecture.

