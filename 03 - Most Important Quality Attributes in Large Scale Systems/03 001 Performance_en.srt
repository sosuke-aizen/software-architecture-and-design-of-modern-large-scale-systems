1
00:00:00,360 --> 00:00:01,760
In the previous lectures,

2
00:00:01,760 --> 00:00:04,860
we talked extensively about system requirements

3
00:00:04,860 --> 00:00:06,500
and their importance.

4
00:00:06,500 --> 00:00:08,260
As part of those discussions,

5
00:00:08,260 --> 00:00:11,560
we also talked in general about quality attributes,

6
00:00:11,560 --> 00:00:14,570
as well as their impact on the software architecture

7
00:00:14,570 --> 00:00:15,653
of our system.

8
00:00:17,230 --> 00:00:19,310
We also mentioned that the list

9
00:00:19,310 --> 00:00:22,200
of possible quality attributes is very long,

10
00:00:22,200 --> 00:00:25,710
but not all quality attributes are equally important

11
00:00:25,710 --> 00:00:27,290
for every system.

12
00:00:27,290 --> 00:00:29,570
So in this and the following lectures,

13
00:00:29,570 --> 00:00:31,940
we're going to focus on a small subset

14
00:00:31,940 --> 00:00:35,090
of quality attributes that are the most important

15
00:00:35,090 --> 00:00:38,653
in any large scale system, regardless of the use case.

16
00:00:40,080 --> 00:00:41,310
So in this lecture,

17
00:00:41,310 --> 00:00:42,780
we're going to start talking about

18
00:00:42,780 --> 00:00:45,790
the first quality attribute, which is performance.

19
00:00:45,790 --> 00:00:48,920
We're going to define a few types of performance metrics.

20
00:00:48,920 --> 00:00:52,110
And later, we'll talk about important considerations

21
00:00:52,110 --> 00:00:54,100
when setting goals, measuring,

22
00:00:54,100 --> 00:00:56,853
and analyzing the performance of our system.

23
00:00:57,860 --> 00:01:00,810
So let's start talking about the first performance metric,

24
00:01:00,810 --> 00:01:01,853
the response time.

25
00:01:02,860 --> 00:01:06,160
Frequently, when we talk about performance of a system

26
00:01:06,160 --> 00:01:07,370
or an application,

27
00:01:07,370 --> 00:01:10,230
we immediately think about speed.

28
00:01:10,230 --> 00:01:12,930
Intuitively, when we interact with a system

29
00:01:12,930 --> 00:01:15,240
that responds fast to our actions,

30
00:01:15,240 --> 00:01:18,253
we associate that system with a high performance.

31
00:01:19,300 --> 00:01:21,510
So let's define formally one

32
00:01:21,510 --> 00:01:23,880
of the most common performance metrics,

33
00:01:23,880 --> 00:01:25,040
the response time,

34
00:01:25,040 --> 00:01:28,090
as the time between a client sending a request

35
00:01:28,090 --> 00:01:29,683
and receiving a response.

36
00:01:30,910 --> 00:01:34,700
The response time is usually broken into two parts.

37
00:01:34,700 --> 00:01:38,350
The first and most obvious part is the processing time,

38
00:01:38,350 --> 00:01:42,230
which is the time it takes our system to process the request

39
00:01:42,230 --> 00:01:43,940
and send the response.

40
00:01:43,940 --> 00:01:47,800
This is essentially the amount of time spent in our code,

41
00:01:47,800 --> 00:01:49,200
in our databases

42
00:01:49,200 --> 00:01:52,390
or other parts of our system actively working

43
00:01:52,390 --> 00:01:55,500
on processing the request, applying business logic,

44
00:01:55,500 --> 00:01:57,763
and building and sending the response.

45
00:01:58,860 --> 00:02:01,360
The second, usually less obvious

46
00:02:01,360 --> 00:02:04,060
and frequently forgotten part of the response time,

47
00:02:04,060 --> 00:02:05,780
is the waiting time.

48
00:02:05,780 --> 00:02:07,933
The waiting time is the duration of time

49
00:02:07,933 --> 00:02:12,000
that the request or the response spends inactively

50
00:02:12,000 --> 00:02:13,173
in our system.

51
00:02:14,090 --> 00:02:18,440
Usually that time is spent in transit in our network wires,

52
00:02:18,440 --> 00:02:20,250
switches, and gateways,

53
00:02:20,250 --> 00:02:22,960
or in our network or software queues,

54
00:02:22,960 --> 00:02:26,213
waiting to be handled or sent to its destination.

55
00:02:27,490 --> 00:02:31,050
As a side note, in some textbooks or resources,

56
00:02:31,050 --> 00:02:33,870
the waiting time is referred to as latency,

57
00:02:33,870 --> 00:02:35,520
while in other resources,

58
00:02:35,520 --> 00:02:39,450
the response time and latency are used interchangeably.

59
00:02:39,450 --> 00:02:41,820
The reason this term is so overloaded

60
00:02:41,820 --> 00:02:44,393
is latency simply means delay.

61
00:02:45,600 --> 00:02:48,110
To make sure those two are not confused,

62
00:02:48,110 --> 00:02:52,233
it's common to call a response time as end to end latency.

63
00:02:53,730 --> 00:02:56,990
The response time is an important performance metric

64
00:02:56,990 --> 00:03:00,000
when we're designing a system or a subsystem

65
00:03:00,000 --> 00:03:03,240
that is in the critical path of a user interaction.

66
00:03:03,240 --> 00:03:07,100
The reason for that is users have very little patience

67
00:03:07,100 --> 00:03:10,480
for long response times and get quickly frustrated

68
00:03:10,480 --> 00:03:13,363
when there is no immediate feedback to their actions.

69
00:03:14,870 --> 00:03:18,380
Many engineers falsely assume that response time

70
00:03:18,380 --> 00:03:22,460
is either the only or most important performance metric,

71
00:03:22,460 --> 00:03:25,810
and some go as far as thinking that response time

72
00:03:25,810 --> 00:03:27,800
is the definition of performance,

73
00:03:27,800 --> 00:03:29,070
but the response time

74
00:03:29,070 --> 00:03:31,500
is not the ultimate performance metric,

75
00:03:31,500 --> 00:03:32,780
and for some systems,

76
00:03:32,780 --> 00:03:34,260
it's not as important

77
00:03:34,260 --> 00:03:37,183
as the next performance metric we're going to talk about.

78
00:03:38,220 --> 00:03:39,170
For example,

79
00:03:39,170 --> 00:03:41,920
if we're designing a distributed logging system

80
00:03:41,920 --> 00:03:44,260
that aggregates and analyzes

81
00:03:44,260 --> 00:03:47,430
a constant stream of logs coming from hundreds

82
00:03:47,430 --> 00:03:49,250
and maybe thousands of servers,

83
00:03:49,250 --> 00:03:51,420
the most important performance metric

84
00:03:51,420 --> 00:03:53,430
is the ability to ingest

85
00:03:53,430 --> 00:03:57,633
and analyze large amounts of data at a given period of time.

86
00:03:58,750 --> 00:04:01,420
And the more data our system can ingest

87
00:04:01,420 --> 00:04:03,440
and analyze per unit of time,

88
00:04:03,440 --> 00:04:06,190
the higher the performance of our system is.

89
00:04:06,190 --> 00:04:09,020
The term we use for this type of performance metric

90
00:04:09,020 --> 00:04:09,983
is throughput.

91
00:04:11,320 --> 00:04:13,460
The general definition of throughput

92
00:04:13,460 --> 00:04:16,560
is either the amount of work performed by our system

93
00:04:16,560 --> 00:04:17,660
per unit of time,

94
00:04:17,660 --> 00:04:19,959
in which case the throughput is measured

95
00:04:19,959 --> 00:04:22,880
as the number of tasks accomplished per second,

96
00:04:22,880 --> 00:04:25,830
or alternatively, depending on the use case,

97
00:04:25,830 --> 00:04:27,730
throughput can also be defined

98
00:04:27,730 --> 00:04:30,520
as the amount of data processed by our system

99
00:04:30,520 --> 00:04:31,680
per unit of time,

100
00:04:31,680 --> 00:04:35,070
in which case it's going to be measured in bits, bytes,

101
00:04:35,070 --> 00:04:36,683
or megabytes per second.

102
00:04:37,650 --> 00:04:40,680
Now let's talk about a few important considerations

103
00:04:40,680 --> 00:04:44,083
when we measure and analyze the performance of our system.

104
00:04:45,130 --> 00:04:46,530
The first consideration

105
00:04:46,530 --> 00:04:49,680
is going to be in regards to measuring the response time

106
00:04:49,680 --> 00:04:52,610
as perceived by the client correctly.

107
00:04:52,610 --> 00:04:55,540
Very often engineers make the mistake

108
00:04:55,540 --> 00:04:57,950
of measuring only the processing time

109
00:04:57,950 --> 00:05:00,350
by measuring the time spent in our code

110
00:05:00,350 --> 00:05:03,840
and mistaking it for measuring the response time.

111
00:05:03,840 --> 00:05:04,960
In such cases,

112
00:05:04,960 --> 00:05:08,530
the user might experience very long response times,

113
00:05:08,530 --> 00:05:11,050
while our performance graphs are showing

114
00:05:11,050 --> 00:05:12,423
that everything is normal.

115
00:05:13,540 --> 00:05:15,400
To illustrate the situation,

116
00:05:15,400 --> 00:05:17,350
let's consider two requests

117
00:05:17,350 --> 00:05:20,400
that come to our system pretty much simultaneously,

118
00:05:20,400 --> 00:05:22,689
and let's assume that our system

119
00:05:22,689 --> 00:05:25,762
can process only one request at a time.

120
00:05:25,762 --> 00:05:29,064
While the first request is being processed by our system,

121
00:05:29,064 --> 00:05:31,785
the second request is waiting in our network

122
00:05:31,785 --> 00:05:33,701
or software queues,

123
00:05:33,701 --> 00:05:36,686
and only once the response for the first request

124
00:05:36,686 --> 00:05:39,336
is constructed and sent back to the client,

125
00:05:39,336 --> 00:05:42,564
our system proceeds to process the second request

126
00:05:42,564 --> 00:05:44,461
and send the response.

127
00:05:44,461 --> 00:05:46,737
Now, if we forget about the waiting time

128
00:05:46,737 --> 00:05:50,564
and measure only the time spent in our code or our system,

129
00:05:50,564 --> 00:05:54,387
then we get the processing time and not the response time,

130
00:05:54,387 --> 00:05:56,014
which in this case shows

131
00:05:56,014 --> 00:06:00,574
that each request took 10 milliseconds to complete.

132
00:06:00,574 --> 00:06:02,251
But from the user perspective,

133
00:06:02,251 --> 00:06:04,487
who sees the overall response time,

134
00:06:04,487 --> 00:06:07,074
the first request took 10 milliseconds

135
00:06:07,074 --> 00:06:10,086
while the second request took 20 milliseconds

136
00:06:10,086 --> 00:06:12,164
because of that additional wait time

137
00:06:12,164 --> 00:06:15,487
for the first request to be completed.

138
00:06:15,487 --> 00:06:17,450
And if we take the average of that,

139
00:06:17,450 --> 00:06:21,763
we get 15 milliseconds as the average response time.

140
00:06:21,763 --> 00:06:23,926
Now, this may happen not only

141
00:06:23,926 --> 00:06:26,610
if we can process only one request at a time,

142
00:06:26,610 --> 00:06:29,700
but anytime our system is a bit overloaded

143
00:06:29,700 --> 00:06:31,083
with other requests.

144
00:06:32,120 --> 00:06:36,160
The second consideration is the response time distribution.

145
00:06:36,160 --> 00:06:37,550
Let's say we have hundreds

146
00:06:37,550 --> 00:06:39,730
of servers running our application

147
00:06:39,730 --> 00:06:42,680
and in each minute we collect the response times

148
00:06:42,680 --> 00:06:43,880
from each server,

149
00:06:43,880 --> 00:06:47,030
where each response time corresponds to a different request

150
00:06:47,030 --> 00:06:48,283
from a different user.

151
00:06:49,640 --> 00:06:52,960
Now, ideally, we would like all those samples

152
00:06:52,960 --> 00:06:55,110
of response times to be the same,

153
00:06:55,110 --> 00:06:57,300
which means all our clients are getting

154
00:06:57,300 --> 00:06:59,313
the exact same experience.

155
00:07:00,250 --> 00:07:01,800
However, in practice,

156
00:07:01,800 --> 00:07:04,970
we will always get a distribution of response times.

157
00:07:04,970 --> 00:07:06,400
Some will be very good

158
00:07:06,400 --> 00:07:09,723
and some will be surprisingly very bad.

159
00:07:10,820 --> 00:07:14,590
So knowing that we will always get such a distribution,

160
00:07:14,590 --> 00:07:15,840
what is the metric

161
00:07:15,840 --> 00:07:19,200
that we should set our goals around and measure?

162
00:07:19,200 --> 00:07:21,430
Should this be the average response time,

163
00:07:21,430 --> 00:07:24,930
the median, or maybe the maximum response time?

164
00:07:24,930 --> 00:07:26,410
To answer this question,

165
00:07:26,410 --> 00:07:28,900
first, we need to learn how to organize

166
00:07:28,900 --> 00:07:31,123
and analyze all those samples.

167
00:07:32,380 --> 00:07:35,750
The first way to start analyzing such a distribution

168
00:07:35,750 --> 00:07:38,020
is to sort the response time samples

169
00:07:38,020 --> 00:07:40,790
and bucket them into a histogram.

170
00:07:40,790 --> 00:07:44,740
This histogram shows the frequency of each response time,

171
00:07:44,740 --> 00:07:47,690
which gives us a better picture of the distribution

172
00:07:47,690 --> 00:07:49,520
of our response times.

173
00:07:49,520 --> 00:07:52,180
For example, by looking at this histogram,

174
00:07:52,180 --> 00:07:55,180
we can see that three requests had a response time

175
00:07:55,180 --> 00:07:56,730
of 10 milliseconds,

176
00:07:56,730 --> 00:07:59,420
and only one request had a response time

177
00:07:59,420 --> 00:08:01,133
of 40 milliseconds.

178
00:08:02,150 --> 00:08:03,570
From that histogram,

179
00:08:03,570 --> 00:08:06,880
it's easy to create a percentile distribution chart

180
00:08:06,880 --> 00:08:07,970
or table.

181
00:08:07,970 --> 00:08:11,170
And in that table, the xth percentile

182
00:08:11,170 --> 00:08:14,730
is the value below which x percent of the values

183
00:08:14,730 --> 00:08:15,663
can be found.

184
00:08:17,280 --> 00:08:19,830
For example, by looking at this chart,

185
00:08:19,830 --> 00:08:23,690
we see that the 20th percentile is 10 milliseconds,

186
00:08:23,690 --> 00:08:27,870
which means that the response time of 20% of the requests

187
00:08:27,870 --> 00:08:29,863
was under 10 milliseconds.

188
00:08:30,720 --> 00:08:33,789
Equally, we can look at the 50th percentile,

189
00:08:33,789 --> 00:08:35,559
which is also called the median,

190
00:08:35,559 --> 00:08:37,870
and this tells us that the response time

191
00:08:37,870 --> 00:08:42,592
of 50% of the requests was lower than 20 milliseconds.

192
00:08:43,690 --> 00:08:47,140
And the 90th percentile tells us that the response time

193
00:08:47,140 --> 00:08:52,140
of 90% of the requests was 52 milliseconds at most.

194
00:08:53,350 --> 00:08:55,160
If we collect enough samples,

195
00:08:55,160 --> 00:08:58,910
we can see that about 20% of our users experience

196
00:08:58,910 --> 00:09:01,200
an excessively long response time,

197
00:09:01,200 --> 00:09:02,620
which we can see only,

198
00:09:02,620 --> 00:09:05,090
thanks to this percentile distribution,

199
00:09:05,090 --> 00:09:08,640
while both the median and the average completely hide

200
00:09:08,640 --> 00:09:12,080
the story of those 20% of users.

201
00:09:12,080 --> 00:09:14,180
The term we use for this section

202
00:09:14,180 --> 00:09:18,063
of the percentile distribution graph is the tail latency.

203
00:09:19,010 --> 00:09:21,910
More formally, we define the tail latency

204
00:09:21,910 --> 00:09:25,270
as the small percentage of response times from our system

205
00:09:25,270 --> 00:09:27,710
that take the longest in comparison

206
00:09:27,710 --> 00:09:29,590
to the rest of the values.

207
00:09:29,590 --> 00:09:30,690
Needless to say,

208
00:09:30,690 --> 00:09:34,443
we want this tail latency to be as short as possible.

209
00:09:35,520 --> 00:09:38,890
So when we define the response time goals for our system,

210
00:09:38,890 --> 00:09:42,830
we need to do it in terms of percentiles and tail latency,

211
00:09:42,830 --> 00:09:45,690
and not in terms of average.

212
00:09:45,690 --> 00:09:48,020
And later, when we launch our system,

213
00:09:48,020 --> 00:09:51,190
we need to measure and validate our response times

214
00:09:51,190 --> 00:09:54,800
by analyzing the percentile distribution and comparing them

215
00:09:54,800 --> 00:09:58,600
to the goals we set for ourselves ahead of time.

216
00:09:58,600 --> 00:10:01,080
So for example, we can set our goal

217
00:10:01,080 --> 00:10:04,580
to be that the 95th percentile of our response times

218
00:10:04,580 --> 00:10:07,070
would be less than 30 milliseconds,

219
00:10:07,070 --> 00:10:09,570
which means that we allow our system

220
00:10:09,570 --> 00:10:14,570
to exceed that response time only for 5% of our requests.

221
00:10:14,570 --> 00:10:17,090
Alternatively, we can be more aggressive

222
00:10:17,090 --> 00:10:19,710
and set our goal to be 30 milliseconds

223
00:10:19,710 --> 00:10:22,080
at the 99th percentile.

224
00:10:22,080 --> 00:10:24,970
This still leaves our system some room

225
00:10:24,970 --> 00:10:27,570
to exceed that number due to unexpected

226
00:10:27,570 --> 00:10:29,573
and transient system jitter.

227
00:10:30,770 --> 00:10:32,510
Now, the third consideration

228
00:10:32,510 --> 00:10:35,330
when it comes to measuring the performance of our system,

229
00:10:35,330 --> 00:10:38,180
and this time it refers to both response time

230
00:10:38,180 --> 00:10:39,240
and throughput,

231
00:10:39,240 --> 00:10:42,390
is identifying the performance degradation point

232
00:10:42,390 --> 00:10:46,560
and how fast or how steep this degradation is.

233
00:10:46,560 --> 00:10:49,110
The degradation point is that point

234
00:10:49,110 --> 00:10:51,800
in our performance graph where the performance

235
00:10:51,800 --> 00:10:54,290
is starting to get significantly worse

236
00:10:54,290 --> 00:10:56,270
as the load increases.

237
00:10:56,270 --> 00:10:58,860
If we see that our system's performance

238
00:10:58,860 --> 00:11:00,570
degrades very quickly,

239
00:11:00,570 --> 00:11:03,950
it likely means that one or some of our resources

240
00:11:03,950 --> 00:11:05,423
are fully utilized.

241
00:11:06,610 --> 00:11:08,690
That resource can be the CPU,

242
00:11:08,690 --> 00:11:10,990
in which case the CPU utilization

243
00:11:10,990 --> 00:11:14,040
of one or some of our servers in our system

244
00:11:14,040 --> 00:11:16,810
is very close to 100%.

245
00:11:16,810 --> 00:11:19,170
It can be a high memory consumption

246
00:11:19,170 --> 00:11:20,810
of one of our processes.

247
00:11:20,810 --> 00:11:23,120
It can be that we have too many connections

248
00:11:23,120 --> 00:11:24,700
and our operating system

249
00:11:24,700 --> 00:11:28,013
or our network simply can't handle so much IO.

250
00:11:28,970 --> 00:11:32,160
And it can also mean that one of our software queues

251
00:11:32,160 --> 00:11:34,880
is at capacity and just can't keep up

252
00:11:34,880 --> 00:11:38,083
with the number of messages coming into our system.

253
00:11:39,270 --> 00:11:40,370
In this lecture,

254
00:11:40,370 --> 00:11:43,380
we talked about a very important quality attribute,

255
00:11:43,380 --> 00:11:44,820
the performance.

256
00:11:44,820 --> 00:11:48,180
We learned about the two most common performance metrics,

257
00:11:48,180 --> 00:11:49,420
the response time,

258
00:11:49,420 --> 00:11:51,260
which is defined as the time

259
00:11:51,260 --> 00:11:53,280
between the client sending a request

260
00:11:53,280 --> 00:11:55,750
and receiving a response, and the throughput,

261
00:11:55,750 --> 00:11:59,820
which is the amount of work or data processed by our system

262
00:11:59,820 --> 00:12:01,700
per unit of time.

263
00:12:01,700 --> 00:12:05,050
Later, we talked about three important considerations

264
00:12:05,050 --> 00:12:08,150
when setting goals and analyzing the performance

265
00:12:08,150 --> 00:12:09,550
of our system.

266
00:12:09,550 --> 00:12:12,540
The first consideration was the correct measurement

267
00:12:12,540 --> 00:12:13,870
of the response time,

268
00:12:13,870 --> 00:12:16,400
which takes into account the waiting time

269
00:12:16,400 --> 00:12:19,120
in our network and software queues.

270
00:12:19,120 --> 00:12:21,850
The second consideration was both reasoning

271
00:12:21,850 --> 00:12:24,360
and analyzing the response time

272
00:12:24,360 --> 00:12:26,567
in terms of percentile distribution,

273
00:12:26,567 --> 00:12:29,380
and not just simple averages.

274
00:12:29,380 --> 00:12:32,470
And finally, we talked about performance degradation

275
00:12:32,470 --> 00:12:35,250
in response to an increasing load on our system,

276
00:12:35,250 --> 00:12:36,940
which would be a perfect segue

277
00:12:36,940 --> 00:12:38,720
to our next quality attribute,

278
00:12:38,720 --> 00:12:40,970
which we will talk about in the next lecture.

279
00:12:42,640 --> 00:12:43,633
See you guys soon.

