1
00:00:00,360 --> 00:00:02,820
Hey, welcome back.

2
00:00:02,820 --> 00:00:05,260
In this lecture, we're going to talk about one

3
00:00:05,260 --> 00:00:07,837
of the most important quality attributes,

4
00:00:07,837 --> 00:00:10,220
"The scalability of the system."

5
00:00:10,220 --> 00:00:12,940
As always, we'll start with some motivation

6
00:00:12,940 --> 00:00:15,390
and a precise definition for scalability

7
00:00:15,390 --> 00:00:16,700
and later we'll learn

8
00:00:16,700 --> 00:00:19,203
about the three scalability dimensions.

9
00:00:20,240 --> 00:00:22,343
So let's start with some motivation.

10
00:00:23,560 --> 00:00:26,770
Generally, the load or the traffic on our system

11
00:00:26,770 --> 00:00:30,873
never stays the same and it can follow different patterns.

12
00:00:32,020 --> 00:00:34,260
We can have a seasonal traffic pattern

13
00:00:34,260 --> 00:00:36,730
such as in the case of retail websites.

14
00:00:36,730 --> 00:00:40,460
In this case, we can have a lot of traffic during holidays

15
00:00:40,460 --> 00:00:44,103
or sales and lower traffic in between those events.

16
00:00:45,030 --> 00:00:47,620
Similarly, if we're a search engine,

17
00:00:47,620 --> 00:00:50,390
those spikes would represent a global event

18
00:00:50,390 --> 00:00:53,800
where a lot of users suddenly open their browsers

19
00:00:53,800 --> 00:00:56,623
and start looking for news regarding this event.

20
00:00:57,600 --> 00:01:00,310
We can have completely different traffic patterns

21
00:01:00,310 --> 00:01:02,160
even on a daily basis.

22
00:01:02,160 --> 00:01:04,580
For example, if we're a news website

23
00:01:04,580 --> 00:01:07,250
or some kind of tool that people use at work,

24
00:01:07,250 --> 00:01:09,580
we could have different load on our system

25
00:01:09,580 --> 00:01:12,550
during the weekdays, comparing to the weekends.

26
00:01:12,550 --> 00:01:14,630
And the traffic we have during the days

27
00:01:14,630 --> 00:01:16,070
would look very different

28
00:01:16,070 --> 00:01:18,173
than the one we have during the night.

29
00:01:19,370 --> 00:01:23,220
Generally in the long run, if our business is doing well,

30
00:01:23,220 --> 00:01:26,830
we should expect to see more users using our system

31
00:01:26,830 --> 00:01:30,473
which should result in an increase in traffic and load.

32
00:01:31,480 --> 00:01:34,710
Now, in the previous lecture we talked about performance

33
00:01:34,710 --> 00:01:37,940
and one of the considerations was finding the point

34
00:01:37,940 --> 00:01:40,140
where our system starts degrading

35
00:01:40,140 --> 00:01:42,510
and cannot handle the additional load

36
00:01:42,510 --> 00:01:45,210
with the same level of performance.

37
00:01:45,210 --> 00:01:46,880
We call that point in a graph,

38
00:01:46,880 --> 00:01:48,793
the performance degradation point.

39
00:01:49,980 --> 00:01:53,550
Now let's focus on the response time only for a moment

40
00:01:53,550 --> 00:01:56,890
and compare two different designs of the same system

41
00:01:56,890 --> 00:02:00,453
based on how they perform under different amount of load.

42
00:02:01,510 --> 00:02:04,680
Now, let's say that both team one and team two

43
00:02:04,680 --> 00:02:07,900
get the same budget, both in terms of time and money

44
00:02:07,900 --> 00:02:11,062
to upgrade their systems so they can take more load.

45
00:02:12,010 --> 00:02:15,130
So after the upgrades, the graphs look like this.

46
00:02:15,130 --> 00:02:18,140
From those graphs, we can see that design number two

47
00:02:18,140 --> 00:02:20,900
can perform much better for a higher load

48
00:02:20,900 --> 00:02:25,010
than design number one for the same budget and time.

49
00:02:25,010 --> 00:02:28,290
So informally, we can say that design number two

50
00:02:28,290 --> 00:02:31,350
is a lot more scalable than design number one.

51
00:02:31,350 --> 00:02:34,300
Because for the same amount of effort and cost,

52
00:02:34,300 --> 00:02:37,023
it was able to achieve much better results.

53
00:02:38,030 --> 00:02:40,960
So now that we got some intuition for scalability,

54
00:02:40,960 --> 00:02:43,293
let's define it in a more formal way.

55
00:02:44,520 --> 00:02:47,950
Scalability is the measure of the system's ability

56
00:02:47,950 --> 00:02:50,160
to handle a growing amount of work

57
00:02:50,160 --> 00:02:52,460
in an easy and cost effective way

58
00:02:52,460 --> 00:02:54,693
by adding resources to the system.

59
00:02:55,910 --> 00:02:57,910
So to clarify this definition,

60
00:02:57,910 --> 00:03:01,420
if we plot the amount of work our system can perform

61
00:03:01,420 --> 00:03:04,070
as the function of our system resources,

62
00:03:04,070 --> 00:03:05,970
we have a few options.

63
00:03:05,970 --> 00:03:08,440
The most optimistic scenario we can have

64
00:03:08,440 --> 00:03:11,240
is full linear scalability.

65
00:03:11,240 --> 00:03:14,750
For example, if our system is currently in this spot

66
00:03:14,750 --> 00:03:17,000
and we double the amount of resources,

67
00:03:17,000 --> 00:03:19,440
we would get double the amount of work

68
00:03:19,440 --> 00:03:21,840
or we can take double the amount of load

69
00:03:21,840 --> 00:03:23,920
before our system degrades.

70
00:03:23,920 --> 00:03:25,530
However in practice,

71
00:03:25,530 --> 00:03:28,700
linear scalability is very hard to achieve.

72
00:03:28,700 --> 00:03:31,360
So we would have either of the following options

73
00:03:31,360 --> 00:03:34,450
where the last option is the least desired

74
00:03:34,450 --> 00:03:36,890
as adding more resources to the system

75
00:03:36,890 --> 00:03:39,570
will actually give us worse performance.

76
00:03:39,570 --> 00:03:42,940
And that can happen because of the overhead involved

77
00:03:42,940 --> 00:03:46,513
in managing and coordinating between those resources.

78
00:03:47,800 --> 00:03:50,120
Generally, we can scale our system

79
00:03:50,120 --> 00:03:53,100
on three orthogonal dimensions.

80
00:03:53,100 --> 00:03:54,880
We can scale our system up

81
00:03:54,880 --> 00:03:58,240
which is also referred to as vertical scalability.

82
00:03:58,240 --> 00:04:00,170
We can scale our system out

83
00:04:00,170 --> 00:04:02,920
which is also called horizontal scalability.

84
00:04:02,920 --> 00:04:04,540
And on the third dimension,

85
00:04:04,540 --> 00:04:07,653
we have the team or organizational scalability.

86
00:04:08,620 --> 00:04:11,130
So let's start talking about the first dimension

87
00:04:11,130 --> 00:04:12,813
which is vertical scalability.

88
00:04:13,890 --> 00:04:17,079
Let's assume we have a service running on a single computer

89
00:04:17,079 --> 00:04:20,250
which is taking traffic from many users.

90
00:04:20,250 --> 00:04:22,300
As the number of users grows,

91
00:04:22,300 --> 00:04:26,090
at some point our system cannot take any more load

92
00:04:26,090 --> 00:04:28,253
and reaches the point of degradation.

93
00:04:29,230 --> 00:04:32,070
One way we can solve it is simply upgrading

94
00:04:32,070 --> 00:04:34,780
our service to a faster and newer machine

95
00:04:34,780 --> 00:04:37,370
that has a faster CPU, more memory,

96
00:04:37,370 --> 00:04:39,300
and maybe even a network card

97
00:04:39,300 --> 00:04:41,313
that can handle higher bandwidth.

98
00:04:42,450 --> 00:04:44,900
Similarly, we can upgrade the database

99
00:04:44,900 --> 00:04:47,000
by putting it on a stronger hardware

100
00:04:47,000 --> 00:04:48,823
with more storage capacity.

101
00:04:49,770 --> 00:04:53,110
This type of upgrade is called scaling up

102
00:04:53,110 --> 00:04:55,913
or is commonly referred to as vertical scaling.

103
00:04:57,120 --> 00:05:00,520
Essentially, vertical scalability means adding more

104
00:05:00,520 --> 00:05:03,830
resources or upgrading the existing resources

105
00:05:03,830 --> 00:05:05,340
on a single computer.

106
00:05:05,340 --> 00:05:07,750
All with the purpose to allow our system

107
00:05:07,750 --> 00:05:10,223
to handle higher traffic or load.

108
00:05:11,200 --> 00:05:14,080
Now, let's talk about the second type of scalability.

109
00:05:14,080 --> 00:05:19,080
Horizontal Scalability, in contrast to vertical scalability,

110
00:05:19,080 --> 00:05:21,470
instead of upgrading the existing hardware

111
00:05:21,470 --> 00:05:24,590
we can simply add more units of the resources

112
00:05:24,590 --> 00:05:27,250
that we already have, like running our service

113
00:05:27,250 --> 00:05:31,030
on multiple computers instead of on a single computer.

114
00:05:31,030 --> 00:05:35,150
This allows us to spread the load among a group of machines

115
00:05:35,150 --> 00:05:37,010
keeping each individual machine

116
00:05:37,010 --> 00:05:39,053
far from its degradation point.

117
00:05:40,220 --> 00:05:43,460
Similarly, we can scale our database by running it

118
00:05:43,460 --> 00:05:46,630
on multiple machines instead of on a single machine.

119
00:05:46,630 --> 00:05:49,120
This will release the performance pressure

120
00:05:49,120 --> 00:05:51,023
from any individual computer.

121
00:05:52,030 --> 00:05:54,920
So more formally, horizontal scalability

122
00:05:54,920 --> 00:05:56,980
means adding more resources

123
00:05:56,980 --> 00:06:00,870
in a form of new instances running on different machines

124
00:06:00,870 --> 00:06:03,870
all with the same intent to allow our system

125
00:06:03,870 --> 00:06:06,263
to handle higher traffic or load.

126
00:06:07,320 --> 00:06:10,610
Now, before we move on to the third scalability dimension,

127
00:06:10,610 --> 00:06:12,820
let's compare vertical scalability

128
00:06:12,820 --> 00:06:14,780
to horizontal scalability.

129
00:06:14,780 --> 00:06:17,330
The pros of scaling the system vertically

130
00:06:17,330 --> 00:06:20,740
is that pretty much any application can benefit from it

131
00:06:20,740 --> 00:06:23,910
without any additional code changes.

132
00:06:23,910 --> 00:06:27,450
For example, if the bottleneck that brings our application

133
00:06:27,450 --> 00:06:31,130
to its degradation point is the high CPU utilization,

134
00:06:31,130 --> 00:06:35,260
then upgrading it to a faster CPU will solve that problem

135
00:06:35,260 --> 00:06:38,020
without any special changes.

136
00:06:38,020 --> 00:06:40,010
Also with vertical scalability,

137
00:06:40,010 --> 00:06:42,370
the migration is fairly simple.

138
00:06:42,370 --> 00:06:43,720
When the traffic is high,

139
00:06:43,720 --> 00:06:46,845
we can simply migrate our application to a stronger machine.

140
00:06:46,845 --> 00:06:48,640
And when the traffic is low,

141
00:06:48,640 --> 00:06:50,530
we can move our application instance

142
00:06:50,530 --> 00:06:53,310
to a weaker machine and save money.

143
00:06:53,310 --> 00:06:56,920
This is particularly easy if we're renting our hardware

144
00:06:56,920 --> 00:06:58,623
from a Cloud service provider.

145
00:06:59,600 --> 00:07:03,170
The major downside of scaling our system vertically,

146
00:07:03,170 --> 00:07:04,500
is that there's a limit

147
00:07:04,500 --> 00:07:07,460
on how much we can upgrade our hardware.

148
00:07:07,460 --> 00:07:10,610
And when we're building a system on an internet scale,

149
00:07:10,610 --> 00:07:12,870
we reach that limit very fast

150
00:07:12,870 --> 00:07:15,920
and there's nowhere to go beyond that point.

151
00:07:15,920 --> 00:07:19,030
The second, and probably even bigger issue

152
00:07:19,030 --> 00:07:21,400
with scaling our system vertically only

153
00:07:21,400 --> 00:07:23,130
is that we're locking ourselves

154
00:07:23,130 --> 00:07:25,787
to an inherently centralized system.

155
00:07:25,787 --> 00:07:28,660
And a centralized system cannot provide us

156
00:07:28,660 --> 00:07:31,230
with high availability and full tolerance

157
00:07:31,230 --> 00:07:34,380
which are two extremely important quality attributes

158
00:07:34,380 --> 00:07:37,130
which we're going to discuss in the following lectures.

159
00:07:38,210 --> 00:07:41,690
Now in comparison, when we scale our system horizontally,

160
00:07:41,690 --> 00:07:45,400
there is virtually no limit to our ability to scale

161
00:07:45,400 --> 00:07:48,150
and that's because we can easily add more

162
00:07:48,150 --> 00:07:49,990
and more machines as needed.

163
00:07:49,990 --> 00:07:52,630
And we can also remove them just as easily

164
00:07:52,630 --> 00:07:55,043
when the load on our system goes down.

165
00:07:56,050 --> 00:07:59,350
And finally, when we scale our system horizontally

166
00:07:59,350 --> 00:08:01,570
if we design the system correctly,

167
00:08:01,570 --> 00:08:04,600
our system can provide us with high availability

168
00:08:04,600 --> 00:08:06,863
and fault tolerance out of the box.

169
00:08:07,920 --> 00:08:10,720
On the other hand, not every application

170
00:08:10,720 --> 00:08:12,740
can be easily ported to run

171
00:08:12,740 --> 00:08:15,830
as multiple instances on different computers.

172
00:08:15,830 --> 00:08:18,130
So potentially we would need

173
00:08:18,130 --> 00:08:20,810
to make significant changes to our code.

174
00:08:20,810 --> 00:08:24,530
However, once we do make those changes once,

175
00:08:24,530 --> 00:08:27,040
then adding or removing resources

176
00:08:27,040 --> 00:08:29,350
shouldn't require any changes.

177
00:08:29,350 --> 00:08:33,460
Finally, the biggest disadvantage of horizontal scalability

178
00:08:33,460 --> 00:08:36,690
in running a group of instances of our application

179
00:08:36,690 --> 00:08:40,630
is the increased complexity and coordination overhead.

180
00:08:40,630 --> 00:08:42,940
We will talk about some of those challenges

181
00:08:42,940 --> 00:08:45,113
and how to solve them during the course.

182
00:08:46,150 --> 00:08:48,390
So now that we covered both vertical

183
00:08:48,390 --> 00:08:50,070
and horizontal scalability

184
00:08:50,070 --> 00:08:51,720
and compared them to each other,

185
00:08:51,720 --> 00:08:54,510
let's talk about the last but definitely not least

186
00:08:54,510 --> 00:08:56,670
scalability dimension which is team

187
00:08:56,670 --> 00:08:58,763
or organizational scalability.

188
00:08:59,830 --> 00:09:02,930
If we look back at the definition of scalability,

189
00:09:02,930 --> 00:09:05,700
and we look at this not from a client's perspective

190
00:09:05,700 --> 00:09:07,760
but from a developer's perspective,

191
00:09:07,760 --> 00:09:10,020
the work that we need to do on the system

192
00:09:10,020 --> 00:09:13,320
is adding new features, testing, fixing bugs,

193
00:09:13,320 --> 00:09:15,940
deploying releases, and so on.

194
00:09:15,940 --> 00:09:18,920
So to increase that type of work for our system

195
00:09:18,920 --> 00:09:22,133
the resources that we can add is more engineers.

196
00:09:23,280 --> 00:09:26,230
Now, let's assume that we have a group of engineers

197
00:09:26,230 --> 00:09:29,143
working on one big monolithic code base.

198
00:09:30,170 --> 00:09:33,190
It turns out that if we plot the productivity

199
00:09:33,190 --> 00:09:36,180
as the function of the number of engineers in the team,

200
00:09:36,180 --> 00:09:38,070
before we reach a certain point,

201
00:09:38,070 --> 00:09:39,450
the more engineers we have

202
00:09:39,450 --> 00:09:42,220
in the team, the more work we get done

203
00:09:42,220 --> 00:09:45,610
which means our productivity increases.

204
00:09:45,610 --> 00:09:49,950
However, at some point the more engineers we add to the team

205
00:09:49,950 --> 00:09:52,443
the less productivity we actually get.

206
00:09:53,490 --> 00:09:56,840
Intuitively, if a team of many engineers work

207
00:09:56,840 --> 00:09:58,390
on the same big code base,

208
00:09:58,390 --> 00:10:00,350
we can think of many reasons

209
00:10:00,350 --> 00:10:03,030
why we see such a degradation and productivity

210
00:10:03,030 --> 00:10:05,630
as we hire more engineers to the team.

211
00:10:05,630 --> 00:10:09,340
For example, now meetings become a lot more frequent

212
00:10:09,340 --> 00:10:11,120
and a lot more crowded

213
00:10:11,120 --> 00:10:14,210
which makes them a lot less productive.

214
00:10:14,210 --> 00:10:15,910
Also because there are a lot

215
00:10:15,910 --> 00:10:19,230
of developers working on many features simultaneously,

216
00:10:19,230 --> 00:10:21,720
Code merge conflicts are inevitable

217
00:10:21,720 --> 00:10:24,800
which of course adds a lot of overhead.

218
00:10:24,800 --> 00:10:26,210
Essentially what happens,

219
00:10:26,210 --> 00:10:29,580
is everyone starts stepping on each other toes.

220
00:10:29,580 --> 00:10:33,160
Also as the code base grows with the number of developers,

221
00:10:33,160 --> 00:10:35,430
each new developer who joins the team

222
00:10:35,430 --> 00:10:39,430
has a lot more to learn before they become productive.

223
00:10:39,430 --> 00:10:42,950
Also testing becomes very hard and slow.

224
00:10:42,950 --> 00:10:46,330
The reason for that, is there is basically no isolation.

225
00:10:46,330 --> 00:10:49,930
So every minor change can potentially break something.

226
00:10:49,930 --> 00:10:52,780
So every time we make any change to the code base,

227
00:10:52,780 --> 00:10:54,780
we need to test everything.

228
00:10:54,780 --> 00:10:58,130
And finally releases also become very risky

229
00:10:58,130 --> 00:11:01,250
because each release includes a lot of new changes

230
00:11:01,250 --> 00:11:03,530
that came from many engineers.

231
00:11:03,530 --> 00:11:06,840
As a response to that, a lot of companies start making

232
00:11:06,840 --> 00:11:08,860
releases even less frequent

233
00:11:08,860 --> 00:11:11,070
which actually makes things even worse

234
00:11:11,070 --> 00:11:14,543
because now every release contains even more changes.

235
00:11:15,690 --> 00:11:19,090
So believe it or not, our software architecture

236
00:11:19,090 --> 00:11:21,700
impacts not only the system performance

237
00:11:21,700 --> 00:11:24,380
but also the ability to scale our team

238
00:11:24,380 --> 00:11:26,503
or increase the engineering velocity.

239
00:11:27,410 --> 00:11:31,300
For example, if we split our code base into separate modules

240
00:11:31,300 --> 00:11:34,560
or abstract, the way pieces into separate libraries

241
00:11:34,560 --> 00:11:37,210
then separate groups of developers can work

242
00:11:37,210 --> 00:11:39,860
on each individual module independently

243
00:11:39,860 --> 00:11:42,763
and not interfere with each other as much.

244
00:11:43,700 --> 00:11:45,870
However, even with the separation

245
00:11:45,870 --> 00:11:47,960
into modules and libraries,

246
00:11:47,960 --> 00:11:51,350
all those pieces are still part of the same service

247
00:11:51,350 --> 00:11:54,360
which means that there's still very tightly coupled

248
00:11:54,360 --> 00:11:55,920
especially when it comes

249
00:11:55,920 --> 00:11:58,313
to releasing new versions to production.

250
00:11:59,300 --> 00:12:02,350
So the next step, is to separate our code base

251
00:12:02,350 --> 00:12:04,120
into separate services.

252
00:12:04,120 --> 00:12:06,480
Each service has its own code base,

253
00:12:06,480 --> 00:12:09,130
its own stack and its own release schedule.

254
00:12:09,130 --> 00:12:11,910
And the services are communicating with each other

255
00:12:11,910 --> 00:12:15,200
through loosely coupled protocols over the network.

256
00:12:15,200 --> 00:12:18,140
This breakdown into multiple services allows

257
00:12:18,140 --> 00:12:20,610
for much better engineering productivity

258
00:12:20,610 --> 00:12:23,770
and helps us scale our organization.

259
00:12:23,770 --> 00:12:26,660
Of course breaking our monolithic code base

260
00:12:26,660 --> 00:12:29,440
into multiple services, doesn't come for free.

261
00:12:29,440 --> 00:12:32,240
But we'll talk about it in much greater detail

262
00:12:32,240 --> 00:12:35,053
when we get to the topic or of architectural patterns.

263
00:12:36,140 --> 00:12:39,120
As a final note, as this chart suggests

264
00:12:39,120 --> 00:12:42,157
vertical scalability, horizontal scalability,

265
00:12:42,157 --> 00:12:45,340
and team scalability are orthogonal to each other

266
00:12:45,340 --> 00:12:49,010
which means we can scale our system on either one dimension,

267
00:12:49,010 --> 00:12:51,803
two dimensions or all three dimensions.

268
00:12:52,840 --> 00:12:54,620
In this lecture, we talked about

269
00:12:54,620 --> 00:12:57,370
a very important quality attribute of the system

270
00:12:57,370 --> 00:12:59,330
which is scalability.

271
00:12:59,330 --> 00:13:01,960
We first got some motivation based on the fact

272
00:13:01,960 --> 00:13:04,450
that the traffic and the load on our system

273
00:13:04,450 --> 00:13:07,670
is never constant and it can increase over time

274
00:13:07,670 --> 00:13:11,070
as well as on a seasonal or sporadic pattern.

275
00:13:11,070 --> 00:13:14,460
After that, we define scalability more formally

276
00:13:14,460 --> 00:13:16,940
as the measure of system's ability

277
00:13:16,940 --> 00:13:19,070
to handle a growing amount of work

278
00:13:19,070 --> 00:13:21,550
in an easy and cost effective way

279
00:13:21,550 --> 00:13:24,113
by adding more resources to the system.

280
00:13:25,410 --> 00:13:27,770
Later, we went ahead and learned about

281
00:13:27,770 --> 00:13:31,010
the three orthogonal ways we can scale our system

282
00:13:31,010 --> 00:13:33,840
in terms of performance and productivity.

283
00:13:33,840 --> 00:13:36,470
The first way was the vertical scalability

284
00:13:36,470 --> 00:13:38,700
which we achieved by adding resources

285
00:13:38,700 --> 00:13:43,050
or upgrading the existing resources on a single computer.

286
00:13:43,050 --> 00:13:46,200
After that, we talked about the horizontal scalability

287
00:13:46,200 --> 00:13:48,780
which can be achieved by adding more resources

288
00:13:48,780 --> 00:13:53,670
in a form of new instances running on different computers.

289
00:13:53,670 --> 00:13:55,170
And finally, we talked about

290
00:13:55,170 --> 00:13:56,990
the third scalability dimension

291
00:13:56,990 --> 00:14:00,370
which is theme or organizational scalability.

292
00:14:00,370 --> 00:14:03,890
Unlike the first two that are focused mainly on performance,

293
00:14:03,890 --> 00:14:06,300
this scalability allows our company

294
00:14:06,300 --> 00:14:08,420
to keep increasing its productivity

295
00:14:08,420 --> 00:14:11,023
while hiring more engineers into the team.

296
00:14:12,100 --> 00:14:14,173
I will see you soon in the next lecture.

