1
00:00:00,500 --> 00:00:02,860
Hey, welcome back.

2
00:00:02,860 --> 00:00:06,600
Availability is one of the most important quality attributes

3
00:00:06,600 --> 00:00:08,109
of a large scale system.

4
00:00:08,109 --> 00:00:10,950
So in this lecture, we're going to talk about the importance

5
00:00:10,950 --> 00:00:12,210
of high availability,

6
00:00:12,210 --> 00:00:15,270
how to define and measure availability in general

7
00:00:15,270 --> 00:00:17,670
and later we will talk specifically,

8
00:00:17,670 --> 00:00:20,863
about what constitutes high availability of a system.

9
00:00:21,820 --> 00:00:23,970
So, let's start talking about the importance

10
00:00:23,970 --> 00:00:25,223
of high availability.

11
00:00:26,370 --> 00:00:30,150
Availability is one of the most important quality attributes

12
00:00:30,150 --> 00:00:31,560
when designing a system.

13
00:00:31,560 --> 00:00:34,580
Because arguably it has the greatest impact

14
00:00:34,580 --> 00:00:37,223
on both our users and our business.

15
00:00:38,230 --> 00:00:42,120
When it comes to users, we can only imagine the frustration

16
00:00:42,120 --> 00:00:45,530
of a user trying to purchase something from our online store

17
00:00:45,530 --> 00:00:49,070
but when they type in the URL of our service in the browser,

18
00:00:49,070 --> 00:00:51,140
the page just doesn't load.

19
00:00:51,140 --> 00:00:54,550
Or maybe it does load but when they go to checkout

20
00:00:54,550 --> 00:00:57,810
instead of getting a confirmation, they get an error.

21
00:00:57,810 --> 00:01:00,870
Similarly, if we're an email service provider,

22
00:01:00,870 --> 00:01:03,100
we can only imagine the impact

23
00:01:03,100 --> 00:01:05,970
of everyone losing in their access to their email

24
00:01:05,970 --> 00:01:08,563
for a few hours or even a whole day.

25
00:01:09,690 --> 00:01:13,040
On that note, if we are a company that provides services

26
00:01:13,040 --> 00:01:16,060
to other companies instead of to end users,

27
00:01:16,060 --> 00:01:19,440
the impact of a prolonged outage in our services

28
00:01:19,440 --> 00:01:21,120
is compounded.

29
00:01:21,120 --> 00:01:23,010
A very famous example of that,

30
00:01:23,010 --> 00:01:26,330
is the AWS simple storage, four hour outage

31
00:01:26,330 --> 00:01:28,910
in February, 2017.

32
00:01:28,910 --> 00:01:30,870
Because hundreds of companies

33
00:01:30,870 --> 00:01:33,290
and more than a hundred thousand websites

34
00:01:33,290 --> 00:01:34,740
were using that service,

35
00:01:34,740 --> 00:01:37,700
it's safe to say that that kind of outage

36
00:01:37,700 --> 00:01:39,893
almost took down the entire internet.

37
00:01:41,110 --> 00:01:45,640
But system downtime is not always just an inconvenience.

38
00:01:45,640 --> 00:01:49,350
In case our software is used for mission critical services

39
00:01:49,350 --> 00:01:52,600
like air traffic control in an airport for example,

40
00:01:52,600 --> 00:01:56,040
or to help manage the healthcare of patients in a hospital,

41
00:01:56,040 --> 00:01:59,210
if our system crashes or becomes unavailable

42
00:01:59,210 --> 00:02:01,010
for an extended period of time,

43
00:02:01,010 --> 00:02:03,773
we potentially have people lives on the line.

44
00:02:04,780 --> 00:02:06,890
Now, from the business perspective,

45
00:02:06,890 --> 00:02:10,960
the consequences of our system going down are twofold.

46
00:02:10,960 --> 00:02:14,170
First of all, if the users cannot use our system

47
00:02:14,170 --> 00:02:18,170
then our ability to make money, simply goes to zero.

48
00:02:18,170 --> 00:02:21,600
Now, the second consequence is when the users lose access

49
00:02:21,600 --> 00:02:24,570
to our system for too long or too frequently,

50
00:02:24,570 --> 00:02:27,830
they will inevitably go to our competitors.

51
00:02:27,830 --> 00:02:30,530
So, essentially when our system goes down

52
00:02:30,530 --> 00:02:34,453
our business may lose both money and also its customers.

53
00:02:35,530 --> 00:02:38,370
Now, that we got a feel of how important it is

54
00:02:38,370 --> 00:02:40,300
for our system to be available,

55
00:02:40,300 --> 00:02:42,860
let's proceed with defining availability

56
00:02:42,860 --> 00:02:44,163
a bit more formally.

57
00:02:45,130 --> 00:02:48,580
We can define availability as either the fraction of time

58
00:02:48,580 --> 00:02:50,950
or the probability that our service

59
00:02:50,950 --> 00:02:54,223
is operationally functional and accessible to the user.

60
00:02:55,850 --> 00:02:59,230
That time that our system is operationally functional

61
00:02:59,230 --> 00:03:00,860
and accessible to the user,

62
00:03:00,860 --> 00:03:04,260
is often referred to as the 'uptime' of the system.

63
00:03:04,260 --> 00:03:07,450
While the time that our system is unavailable to the user,

64
00:03:07,450 --> 00:03:10,210
is commonly referred to as 'downtime'.

65
00:03:10,210 --> 00:03:13,390
Availability is usually measured as a percentage,

66
00:03:13,390 --> 00:03:16,310
representing the ratio between the uptime

67
00:03:16,310 --> 00:03:18,900
and the entire time our system is running,

68
00:03:18,900 --> 00:03:23,003
which is the total sum of uptime and downtime of the system.

69
00:03:23,950 --> 00:03:28,200
Now, as a side note, sometimes uptime and availability

70
00:03:28,200 --> 00:03:30,080
are used interchangeably.

71
00:03:30,080 --> 00:03:31,710
So, if you read in an agreement

72
00:03:31,710 --> 00:03:34,910
and you see that uptime is described in percentages

73
00:03:34,910 --> 00:03:36,680
and not as an absolute time,

74
00:03:36,680 --> 00:03:39,173
they simply talk about availability instead.

75
00:03:40,190 --> 00:03:42,350
Another alternative way to define

76
00:03:42,350 --> 00:03:44,370
as well as estimate availability

77
00:03:44,370 --> 00:03:49,343
is using two other statistical metrics, the MTBF and MTTR.

78
00:03:51,530 --> 00:03:54,990
MTBF, which is the meantime between failures,

79
00:03:54,990 --> 00:03:59,260
represents the average time our system is operational.

80
00:03:59,260 --> 00:04:01,260
This metric is very useful,

81
00:04:01,260 --> 00:04:03,810
if we're dealing with multiple pieces of hardware

82
00:04:03,810 --> 00:04:07,750
such as computers, routers, hard drives and so on,

83
00:04:07,750 --> 00:04:11,760
where each component has its own operational shelf life.

84
00:04:11,760 --> 00:04:15,160
Typically though, if we're dealing with an existing service

85
00:04:15,160 --> 00:04:19,079
like a cloud service provider or a third party API,

86
00:04:19,079 --> 00:04:22,400
those services usually give us their general uptime

87
00:04:22,400 --> 00:04:24,260
and availability upfront.

88
00:04:24,260 --> 00:04:27,113
So, we don't really need to make any calculations.

89
00:04:28,170 --> 00:04:31,630
MTTR, which stands for meantime to recovery,

90
00:04:31,630 --> 00:04:34,400
is the average time it takes us to detect

91
00:04:34,400 --> 00:04:36,320
and recover from a failure,

92
00:04:36,320 --> 00:04:40,260
which intuitively is the average downtime of our system.

93
00:04:40,260 --> 00:04:43,190
Because until we fully recover from a failure,

94
00:04:43,190 --> 00:04:46,520
our system is essentially nonoperational.

95
00:04:46,520 --> 00:04:50,120
So the alternative formula for the availability of a system,

96
00:04:50,120 --> 00:04:55,120
is MTBF divided by the sum of MTBF and MTTR.

97
00:04:55,820 --> 00:04:57,820
Feel free to post the video

98
00:04:57,820 --> 00:05:01,570
and convince yourself that this formula makes sense.

99
00:05:01,570 --> 00:05:05,123
And conceptually, is very similar to the previous formula.

100
00:05:06,130 --> 00:05:08,800
Now, because this formula is more statistical

101
00:05:08,800 --> 00:05:10,640
and talks about probabilities,

102
00:05:10,640 --> 00:05:13,930
it's much more useful for availability estimation

103
00:05:13,930 --> 00:05:17,300
rather than actual measurement of availability.

104
00:05:17,300 --> 00:05:19,660
Now, one very important aspect

105
00:05:19,660 --> 00:05:21,440
that we can see in this formula,

106
00:05:21,440 --> 00:05:24,240
which we cannot easily see in the previous formula

107
00:05:24,240 --> 00:05:27,000
is if we minimize the average time to detect

108
00:05:27,000 --> 00:05:28,740
and recover from a failure,

109
00:05:28,740 --> 00:05:30,960
theoretically all the way to zero

110
00:05:30,960 --> 00:05:34,460
then we essentially achieve a 100% availability

111
00:05:34,460 --> 00:05:37,533
regardless of the average time between failures.

112
00:05:38,740 --> 00:05:40,400
Of course, in practice

113
00:05:40,400 --> 00:05:43,600
we cannot get the recovery time completely to zero.

114
00:05:43,600 --> 00:05:47,210
But make a mental note of the effect detectability

115
00:05:47,210 --> 00:05:50,020
and fast recovery has on availability,

116
00:05:50,020 --> 00:05:51,830
as this serves as a clue

117
00:05:51,830 --> 00:05:54,793
on one way we can achieve high availability.

118
00:05:55,850 --> 00:05:58,793
Now, how much availability should we aim for?

119
00:05:59,720 --> 00:06:04,120
Obviously, 100% availability is what our users would want.

120
00:06:04,120 --> 00:06:07,940
But on the engineering side, it is extremely hard to achieve

121
00:06:07,940 --> 00:06:10,760
and essentially it leaves us with no time

122
00:06:10,760 --> 00:06:15,110
for either planned or emergency maintenance or upgrades.

123
00:06:15,110 --> 00:06:17,560
So, what would be an acceptable number

124
00:06:17,560 --> 00:06:20,733
that still provides our users with high availability?

125
00:06:21,600 --> 00:06:23,610
Before we try to answer this question,

126
00:06:23,610 --> 00:06:26,500
let's look at a few percentages of availability

127
00:06:26,500 --> 00:06:29,380
and try to get a feel of what that measure means

128
00:06:29,380 --> 00:06:31,020
for our users.

129
00:06:31,020 --> 00:06:33,710
Let's start with a 90% availability

130
00:06:33,710 --> 00:06:36,140
which may seem like a pretty high number

131
00:06:36,140 --> 00:06:37,710
but if we look at the table

132
00:06:37,710 --> 00:06:39,770
we see that with that availability,

133
00:06:39,770 --> 00:06:42,830
our users may not be able to use our system

134
00:06:42,830 --> 00:06:47,550
for more than two hours every day or for more than 36 days

135
00:06:47,550 --> 00:06:50,730
which is more than a month out of an entire year.

136
00:06:50,730 --> 00:06:53,330
So, intuitively we already see,

137
00:06:53,330 --> 00:06:56,950
that this cannot be considered high availability.

138
00:06:56,950 --> 00:06:59,830
Now, what about 95% availability?

139
00:06:59,830 --> 00:07:01,740
Well, that's definitely better

140
00:07:01,740 --> 00:07:04,910
but still one day of downtime every month

141
00:07:04,910 --> 00:07:08,760
or one hour of every day is not considered high enough

142
00:07:08,760 --> 00:07:10,363
for most use cases.

143
00:07:11,310 --> 00:07:13,610
So, there's no strict definition

144
00:07:13,610 --> 00:07:15,830
of what constitutes high availability

145
00:07:15,830 --> 00:07:18,940
but the industry standards set by cloud vendors

146
00:07:18,940 --> 00:07:23,180
is generally anywhere between 99% to 100%.

147
00:07:23,180 --> 00:07:26,803
Typically at 99.9% or higher.

148
00:07:27,690 --> 00:07:30,470
If we look back at our availability table

149
00:07:30,470 --> 00:07:33,550
that means that their service may be unavailable

150
00:07:33,550 --> 00:07:36,200
for less than one and a half minutes per day,

151
00:07:36,200 --> 00:07:38,400
which would not be very noticeable

152
00:07:38,400 --> 00:07:41,070
but it still leaves out eight hours

153
00:07:41,070 --> 00:07:44,413
of emergency maintenance work, every year if needed.

154
00:07:45,340 --> 00:07:48,510
To make discussions about availability a bit easier,

155
00:07:48,510 --> 00:07:51,370
percentages are also sometimes referred to

156
00:07:51,370 --> 00:07:54,160
by the number of nines in their digits.

157
00:07:54,160 --> 00:07:59,160
So for example, 99.9% would be referred to as three nines.

158
00:07:59,400 --> 00:08:04,400
99.99% would be referred to as four nines and so on.

159
00:08:05,490 --> 00:08:09,160
Now, before we move on to learning about concrete strategies

160
00:08:09,160 --> 00:08:11,150
on how to achieve high availability,

161
00:08:11,150 --> 00:08:14,160
let's quickly summarize what we learned in this lecture.

162
00:08:14,160 --> 00:08:15,220
In this lecture,

163
00:08:15,220 --> 00:08:17,670
we learned the importance of high availability

164
00:08:17,670 --> 00:08:20,770
both from a user and business perspective.

165
00:08:20,770 --> 00:08:23,250
Specifically, we mentioned the two risks

166
00:08:23,250 --> 00:08:24,800
that our business faces,

167
00:08:24,800 --> 00:08:27,870
if our system goes down for prolonged periods of time

168
00:08:27,870 --> 00:08:29,140
or too frequently.

169
00:08:29,140 --> 00:08:30,730
Which are the loss of revenue,

170
00:08:30,730 --> 00:08:33,140
as well as the potential loss of customers

171
00:08:33,140 --> 00:08:34,950
to our competitors.

172
00:08:34,950 --> 00:08:38,830
After that, we defined availability as the fraction of time

173
00:08:38,830 --> 00:08:42,320
or probability that our service is operationally functional

174
00:08:42,320 --> 00:08:43,883
and accessible to the user.

175
00:08:44,790 --> 00:08:48,020
And later, we learned about the two formulas to measure

176
00:08:48,020 --> 00:08:50,920
and estimate the system's availability.

177
00:08:50,920 --> 00:08:54,070
One using the uptime and downtime of a system

178
00:08:54,070 --> 00:08:56,890
and another using the meantime between failures

179
00:08:56,890 --> 00:08:59,190
and meantime to recovery.

180
00:08:59,190 --> 00:09:01,770
Finally, we defined high availability

181
00:09:01,770 --> 00:09:03,700
as three nines or above,

182
00:09:03,700 --> 00:09:06,380
based on the standard that the cloud vendor set

183
00:09:06,380 --> 00:09:07,403
in the industry.

