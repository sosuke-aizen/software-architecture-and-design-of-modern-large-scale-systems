

####  **Cloud Vendor SLA Examples**

  * [AWS Service Level Agreements (SLAs)](https://aws.amazon.com/legal/service-level-agreements/?aws-sla-cards.sort-by=item.additionalFields.serviceNameLower&aws-sla-cards.sort-order=asc&awsf.tech-category-filter=*all)

  

  * [Google Cloud Platform Service Level Agreements](https://cloud.google.com/terms/sla)

  

  * [Microsoft Azure Service Level Agreement](https://azure.microsoft.com/en-us/support/legal/sla/)

####  **Other Examples**

  * [GitHub Enterprise Service Level Agreement](https://github.com/customer-terms/github-online-services-sla)

