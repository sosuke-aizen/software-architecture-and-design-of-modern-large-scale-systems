1
00:00:00,000 --> 00:00:02,240
[Michael Pogrebinsky] Welcome back!

2
00:00:02,240 --> 00:00:05,480
Now that we covered the most important quality attributes

3
00:00:05,480 --> 00:00:07,720
and everything that we need to know about them,

4
00:00:07,720 --> 00:00:11,410
let's conclude the section with three very important terms

5
00:00:11,410 --> 00:00:15,180
that essentially aggregate the promises we make to our users

6
00:00:15,180 --> 00:00:17,740
in regards to those quality attributes.

7
00:00:17,740 --> 00:00:21,463
Those are SLA, SLOs and SLIs.

8
00:00:22,380 --> 00:00:24,490
So let's start with the first term

9
00:00:24,490 --> 00:00:28,023
which is service level agreement or SLA in short.

10
00:00:28,900 --> 00:00:33,110
The SLA is an agreement between us, the service provider

11
00:00:33,110 --> 00:00:35,223
and our clients or users.

12
00:00:36,360 --> 00:00:38,490
It's essentially a legal contract

13
00:00:38,490 --> 00:00:41,300
that represents the promise we make to our users

14
00:00:41,300 --> 00:00:43,140
in terms of quality service

15
00:00:43,140 --> 00:00:46,680
such as availability, performance, data durability,

16
00:00:46,680 --> 00:00:48,210
the time it takes us to respond

17
00:00:48,210 --> 00:00:50,283
to system failures and so on.

18
00:00:51,400 --> 00:00:54,470
It also explicitly states the penalties

19
00:00:54,470 --> 00:00:56,700
and financial consequences to us

20
00:00:56,700 --> 00:00:59,060
if we fail to deliver those promises

21
00:00:59,060 --> 00:01:00,990
and bridge the contract.

22
00:01:00,990 --> 00:01:04,840
Those penalties can include full or partial refunds,

23
00:01:04,840 --> 00:01:07,440
subscription or license extensions,

24
00:01:07,440 --> 00:01:09,363
service credits and so on.

25
00:01:10,270 --> 00:01:14,480
SLAs almost always exist for external paying users

26
00:01:14,480 --> 00:01:18,150
but sometimes also exist for free external users

27
00:01:18,150 --> 00:01:22,303
and occasionally even for internal users within our company.

28
00:01:23,280 --> 00:01:28,280
Although internal SLAs would unlikely include any penalties.

29
00:01:28,320 --> 00:01:32,410
The reason SLA still makes sense for free external users

30
00:01:32,410 --> 00:01:36,200
is for example if we're providing a three day free trial

31
00:01:36,200 --> 00:01:38,210
of our service to our users,

32
00:01:38,210 --> 00:01:41,930
enduring that free trial our system had major issues,

33
00:01:41,930 --> 00:01:45,690
then it would make sense for us to compensate those users

34
00:01:45,690 --> 00:01:47,850
by giving giving them an extension of the trial

35
00:01:47,850 --> 00:01:51,280
or maybe some credits that they can use for the future.

36
00:01:51,280 --> 00:01:55,350
However, most companies that provide entirely free services

37
00:01:55,350 --> 00:01:58,260
avoid making any strict promises to the public

38
00:01:58,260 --> 00:02:00,453
and don't publish any SLA.

39
00:02:01,509 --> 00:02:04,590
When it comes to internal customers within our company,

40
00:02:04,590 --> 00:02:07,010
those customers maybe other teams

41
00:02:07,010 --> 00:02:10,470
that are providing services to external customers.

42
00:02:10,470 --> 00:02:13,220
And in order for them to meet their SLA,

43
00:02:13,220 --> 00:02:14,720
they need to know ours

44
00:02:14,720 --> 00:02:17,043
and rely on us to meet that agreement.

45
00:02:18,290 --> 00:02:21,200
Now let's talk about the next term, the SLO

46
00:02:21,200 --> 00:02:23,653
which stands for service level objective.

47
00:02:24,750 --> 00:02:29,370
SLOs are the individual goals that we set for our system.

48
00:02:29,370 --> 00:02:32,020
Each SLO represents a target value

49
00:02:32,020 --> 00:02:36,083
or a target range of values that our service needs to meet.

50
00:02:36,980 --> 00:02:38,430
For example, we can have

51
00:02:38,430 --> 00:02:42,070
an availability service level objective of three nines.

52
00:02:42,070 --> 00:02:45,550
A response time SLO of less than a hundred milliseconds

53
00:02:45,550 --> 00:02:50,060
at the 90th percentile or an issue resolution time objective

54
00:02:50,060 --> 00:02:52,823
of between 24 or 48 hours.

55
00:02:53,760 --> 00:02:56,200
All the quality attribute requirements

56
00:02:56,200 --> 00:02:58,970
that we gathered at the beginning of the design process

57
00:02:58,970 --> 00:03:02,530
will make their way into one or multiple SLOs

58
00:03:02,530 --> 00:03:04,230
as well as other objectives

59
00:03:04,230 --> 00:03:06,090
that we want to hit with our system

60
00:03:06,090 --> 00:03:08,313
and hold ourselves accountable for.

61
00:03:09,310 --> 00:03:11,220
If our system has an SLA,

62
00:03:11,220 --> 00:03:14,540
then each SLO represent an individual agreement

63
00:03:14,540 --> 00:03:18,490
within that SLA about a specific metric in the system.

64
00:03:18,490 --> 00:03:21,390
So essentially an SLA aggregates

65
00:03:21,390 --> 00:03:25,483
all the service level objectives in a single legal document.

66
00:03:26,890 --> 00:03:31,740
But even systems that don't have an SLA still must have SLOs

67
00:03:31,740 --> 00:03:34,070
because if we don't have those objectives,

68
00:03:34,070 --> 00:03:38,060
our internal or external users don't know what to expect

69
00:03:38,060 --> 00:03:39,933
when they rely on our system.

70
00:03:41,240 --> 00:03:43,650
Now the third term we're going to cover

71
00:03:43,650 --> 00:03:46,733
is the service level indicator or SLI.

72
00:03:47,830 --> 00:03:51,150
A service level indicator is a quantitative measure

73
00:03:51,150 --> 00:03:54,720
of our compliance with a service level objective.

74
00:03:54,720 --> 00:03:58,100
In other words, it's the actual numbers we measure

75
00:03:58,100 --> 00:03:59,740
using a monitoring system

76
00:03:59,740 --> 00:04:02,430
or that we calculate from our logs.

77
00:04:02,430 --> 00:04:04,270
Once we calculate the numbers

78
00:04:04,270 --> 00:04:06,720
based on those service level indicators,

79
00:04:06,720 --> 00:04:10,000
we can later compare them to the goals we set for ourselves

80
00:04:10,000 --> 00:04:12,623
when we define the service level objectives.

81
00:04:13,670 --> 00:04:17,339
For example, we can measure the percentage of user requests

82
00:04:17,339 --> 00:04:19,630
that receive the successful response

83
00:04:19,630 --> 00:04:21,630
and use that as an indicator

84
00:04:21,630 --> 00:04:24,840
to measure the availability of our system and compare it

85
00:04:24,840 --> 00:04:28,650
to the availability service level objective of three nines

86
00:04:28,650 --> 00:04:30,513
that we set for our service.

87
00:04:31,440 --> 00:04:33,930
Similarly, we can collect the response times

88
00:04:33,930 --> 00:04:37,590
of each request, then bucket them into time windows

89
00:04:37,590 --> 00:04:41,010
and calculate the average or the percentile distribution

90
00:04:41,010 --> 00:04:44,560
of the response time that our users experienced.

91
00:04:44,560 --> 00:04:46,910
Later, we can compare those numbers

92
00:04:46,910 --> 00:04:50,927
to the end-to-end latency SLO of a hundred milliseconds

93
00:04:50,927 --> 00:04:54,663
at the 90th percentile that we originally set for ourselves.

94
00:04:55,770 --> 00:04:59,670
Since the SLOs essentially represent the target values

95
00:04:59,670 --> 00:05:02,430
for our most important quality attributes,

96
00:05:02,430 --> 00:05:05,460
now we understand why it was so important

97
00:05:05,460 --> 00:05:09,290
for our quality attributes to be testable and measurable.

98
00:05:09,290 --> 00:05:11,160
Because if they weren't measurable,

99
00:05:11,160 --> 00:05:14,880
we wouldn't be able to find any service level indicators

100
00:05:14,880 --> 00:05:18,700
to validate that we are indeed meeting our SLOs.

101
00:05:18,700 --> 00:05:20,420
And if we can't measure

102
00:05:20,420 --> 00:05:22,950
and prove that we are meeting our SLOs,

103
00:05:22,950 --> 00:05:24,870
we can't definitively say

104
00:05:24,870 --> 00:05:27,570
that we are meeting our service level agreement

105
00:05:27,570 --> 00:05:29,293
which is a legal contract.

106
00:05:30,230 --> 00:05:32,700
Now, while the service level agreements

107
00:05:32,700 --> 00:05:35,520
are crafted by the business and legal teams,

108
00:05:35,520 --> 00:05:37,680
the software engineers and architects

109
00:05:37,680 --> 00:05:40,210
have a lot more control in defining

110
00:05:40,210 --> 00:05:42,540
and setting the service level objectives

111
00:05:42,540 --> 00:05:46,593
and the service level indicators associated with those SLOs.

112
00:05:47,620 --> 00:05:50,240
So when it comes to the service level objectives,

113
00:05:50,240 --> 00:05:51,970
there are a few considerations

114
00:05:51,970 --> 00:05:53,793
that we need to take into account.

115
00:05:54,810 --> 00:05:56,480
The first consideration

116
00:05:56,480 --> 00:05:59,780
is we shouldn't take every service level indicator

117
00:05:59,780 --> 00:06:01,790
that we can measure in our system

118
00:06:01,790 --> 00:06:05,353
and define an objective associated with this indicator.

119
00:06:06,330 --> 00:06:09,540
Instead, we should first think about the metrics

120
00:06:09,540 --> 00:06:11,950
that users care about the most

121
00:06:11,950 --> 00:06:14,060
and define the service level objectives

122
00:06:14,060 --> 00:06:16,160
around those metrics.

123
00:06:16,160 --> 00:06:19,480
From that, we find the right service level indicators

124
00:06:19,480 --> 00:06:21,813
to track those service level objectives.

125
00:06:23,090 --> 00:06:24,550
The second consideration

126
00:06:24,550 --> 00:06:27,580
is the fewer service level objective we promise,

127
00:06:27,580 --> 00:06:29,253
the better it is for us.

128
00:06:30,190 --> 00:06:32,810
With too many SLOs, it's hard for us

129
00:06:32,810 --> 00:06:35,690
to prioritize all of them in an equal manner.

130
00:06:35,690 --> 00:06:39,400
So when we have just a few SLOs, it is much easier

131
00:06:39,400 --> 00:06:42,460
for us to focus our entire software architecture

132
00:06:42,460 --> 00:06:45,453
around them to make sure we meet our goals.

133
00:06:46,490 --> 00:06:49,660
The third consideration is setting realistic goals

134
00:06:49,660 --> 00:06:51,433
with a budget for error.

135
00:06:52,530 --> 00:06:55,950
Just because we can provide five-nines of availability

136
00:06:55,950 --> 00:06:58,560
doesn't mean that we should commit to that.

137
00:06:58,560 --> 00:07:02,130
Instead, we should commit to a much lower availability

138
00:07:02,130 --> 00:07:03,920
than we can provide.

139
00:07:03,920 --> 00:07:06,060
This allows us to save costs

140
00:07:06,060 --> 00:07:10,143
and also leaves us enough room for unexpected issues.

141
00:07:11,140 --> 00:07:12,860
And it's especially true

142
00:07:12,860 --> 00:07:14,830
when our service level objectives

143
00:07:14,830 --> 00:07:18,960
are represented in an external service level agreement.

144
00:07:18,960 --> 00:07:21,700
In those cases, sometimes companies

145
00:07:21,700 --> 00:07:25,080
define separate external service level objectives

146
00:07:25,080 --> 00:07:26,490
which are a lot looser

147
00:07:26,490 --> 00:07:28,830
than internal service level objectives

148
00:07:28,830 --> 00:07:30,940
that tend to be more aggressive.

149
00:07:30,940 --> 00:07:32,730
For example, externally,

150
00:07:32,730 --> 00:07:36,470
we can commit to 99.9% of availability

151
00:07:36,470 --> 00:07:41,393
but internally, we can commit to 99.99% availability.

152
00:07:42,290 --> 00:07:44,870
This way, we can strive for better quality

153
00:07:44,870 --> 00:07:46,350
of service internally

154
00:07:46,350 --> 00:07:49,790
while committing to a lot less to our clients.

155
00:07:49,790 --> 00:07:53,570
This would also avoid any financial penalties to us

156
00:07:53,570 --> 00:07:55,630
if we don't reach this high bar

157
00:07:55,630 --> 00:07:57,733
that we set for ourselves internally.

158
00:07:58,750 --> 00:08:02,560
The final consideration is we need to create a recovery plan

159
00:08:02,560 --> 00:08:06,630
to deal with any situation when the service level indicators

160
00:08:06,630 --> 00:08:09,160
start showing us that we are not meeting

161
00:08:09,160 --> 00:08:11,043
our service level objectives.

162
00:08:12,000 --> 00:08:15,150
In other words, we need to decide ahead of time

163
00:08:15,150 --> 00:08:18,410
what to do if all of a sudden our system goes down

164
00:08:18,410 --> 00:08:22,060
for long periods of time, if our performance degrades

165
00:08:22,060 --> 00:08:24,810
or we suddenly get too many reports

166
00:08:24,810 --> 00:08:26,970
about issues or bugs in our system

167
00:08:26,970 --> 00:08:28,543
in a short period of time.

168
00:08:29,460 --> 00:08:32,070
This plan should include automatic alerts

169
00:08:32,070 --> 00:08:35,400
to engineers or DevOps, automatic failovers,

170
00:08:35,400 --> 00:08:39,010
restarts, rollbacks or auto scaling policies

171
00:08:39,010 --> 00:08:40,860
and predefined handbooks

172
00:08:40,860 --> 00:08:43,460
on what to do in certain situations

173
00:08:43,460 --> 00:08:46,610
so that the person on call doesn't have to improvise

174
00:08:46,610 --> 00:08:49,763
when they're alerted about an emergency in our system.

175
00:08:51,010 --> 00:08:52,700
In this lecture, we'll learn about

176
00:08:52,700 --> 00:08:57,030
three very important terms in designing a real life system.

177
00:08:57,030 --> 00:08:59,630
The service level agreement or SLA,

178
00:08:59,630 --> 00:09:02,480
the service level objectives or SLOs

179
00:09:02,480 --> 00:09:05,593
and the service level indicators or SLIs.

180
00:09:06,650 --> 00:09:09,560
We'll learn that the SLA is a legal contract

181
00:09:09,560 --> 00:09:12,250
between a service provider and its customers

182
00:09:12,250 --> 00:09:14,220
which essentially aggregates

183
00:09:14,220 --> 00:09:17,000
the most important service level objectives

184
00:09:17,000 --> 00:09:18,980
that we promise to our users,

185
00:09:18,980 --> 00:09:21,130
while the service level indicators

186
00:09:21,130 --> 00:09:24,913
are used to measure our compliance with those objectives.

187
00:09:25,840 --> 00:09:29,200
Finally, we'll learn about four important considerations

188
00:09:29,200 --> 00:09:31,990
when defining service level objectives.

189
00:09:31,990 --> 00:09:32,990
The first one

190
00:09:32,990 --> 00:09:35,973
was defining the most important service level objectives

191
00:09:35,973 --> 00:09:37,890
that our users care about

192
00:09:37,890 --> 00:09:40,660
and then find the service level indicators

193
00:09:40,660 --> 00:09:43,030
based on those objectives.

194
00:09:43,030 --> 00:09:45,310
The second and third considerations

195
00:09:45,310 --> 00:09:47,550
told us to commit to the bare minimum

196
00:09:47,550 --> 00:09:49,540
in terms of the number of objectives

197
00:09:49,540 --> 00:09:51,820
and how aggressive they are.

198
00:09:51,820 --> 00:09:54,080
And finally, the last consideration

199
00:09:54,080 --> 00:09:56,580
was having a recovery plan ahead of time

200
00:09:56,580 --> 00:09:58,100
to deal with situations

201
00:09:58,100 --> 00:10:00,933
wherein potential breach of our SLOs.

202
00:10:02,010 --> 00:10:03,860
I'll see you all in the next lecture.

